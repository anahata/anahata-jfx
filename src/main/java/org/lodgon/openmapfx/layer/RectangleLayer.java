/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lodgon.openmapfx.layer;

import java.util.Optional;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Callback;
import lombok.Getter;
import lombok.Setter;
import org.lodgon.openmapfx.core.LayeredMap;
import org.lodgon.openmapfx.core.MapLayer;

/**
 * A PositionLayer is used to show an Image or a Node at the position that is
 * maintained by this layer. Once a PositionLayer is created, it should be added
 * to a LayeredMap. This LayeredMap is further responsible for doing the
 * required calculations.
 *
 * @see LayeredMap#getLayers()
 * @author johan
 */
public class RectangleLayer extends Region implements MapLayer, InvalidationListener {

    private double lat;

    private double lon;

    private double lat2;

    private double lon2;

    private final Rectangle rectangle = new Rectangle();

    private final Label label = new Label();

    private LayeredMap layeredMap;

    private SimpleBooleanProperty selected = new SimpleBooleanProperty();

    private StringProperty overlayColor = new SimpleStringProperty();

    @Getter
    @Setter
    private Callback<String, Void> onOverlaySelect;

    @Getter
    @Setter
    private Callback<String, Void> onOverlayDeselect;

    private boolean flag = false;

    @Setter
    private boolean nonUBD = false;

    /**
     * Create a PositionLayer with a specific Node that serves as the icon for
     * the location. The node can be styled using css. When the position
     * changes, the provided translateX and translateY will be added to the new
     * x and y translations.
     *
     * @param icon       the icon Node.
     * @param translateX extra x translation to add to the icon
     * @param translateY extra y translation to add to the icon
     */
    public RectangleLayer(String text) {
        getStylesheets().add("/org/lodgon/openmapfx/layer/" + getClass().getSimpleName() + ".css");
        getStyleClass().add("rectangle-layer");
        rectangle.getStyleClass().add("rectangle-overlay");

        getChildren().add(rectangle);
        label.setText(text);
        label.setStyle("-fx-fill:#000000;-fx-text-fill:#000000;");
        getChildren().add(label);

        label.translateXProperty().bind(rectangle.widthProperty().subtract(
                label.widthProperty()).divide(2));
        label.translateYProperty().bind(rectangle.heightProperty().subtract(
                label.heightProperty()).divide(2));
        label.visibleProperty().bind(label.widthProperty().lessThan(rectangle.widthProperty()).and(
                label.heightProperty().lessThan(rectangle.heightProperty())));
        rectangle.setStyle("-fx-fill:" + getOverlayColor() + ";");
        configureEvents();
    }

    private void configureEvents() {
        setOnMouseClicked((e) -> {
            selected.set(!selected.get());
        });

        overlayColor.addListener((e) -> {
            rectangle.setStyle("-fx-fill:" + getOverlayColor() + ";");
        });
        selected.addListener((o, v, sel) -> {
            if (!flag) {
                if (sel) {
                    selectOverlay();
                    if (onOverlaySelect != null) {
                        onOverlaySelect.call(label.getText());
                    }
                } else {
                    deselectOverlay();
                    if (onOverlayDeselect != null) {
                        onOverlayDeselect.call(label.getText());
                    }
                }
            }
        });
    }

    @Override
    public Node getView() {
        return this;
    }

    public void setFill(Color color) {
        rectangle.setFill(color);
    }

    public void updatePosition(double lat, double lon, double lat2, double lon2) {
        this.lat = lat;
        this.lon = lon;
        this.lat2 = lat2;
        this.lon2 = lon2;
        refreshLayer();
    }

    protected void refreshLayer() {
        Point2D cartPoint = this.layeredMap.getMapPoint(lat, lon);
        Point2D cartPoint2 = this.layeredMap.getMapPoint(lat2, lon2);
        if (cartPoint == null || cartPoint2 == null) {
            System.out.println("[JVDBG] Null cartpoint, probably no scene, dont show.");
            return;
        }

        setVisible(true);
        setTranslateX(cartPoint.getX());
        setTranslateY(cartPoint.getY());
        double width = cartPoint2.getX() - cartPoint.getX();
        double height = cartPoint2.getY() - cartPoint.getY();
        rectangle.setHeight(height);
        rectangle.setWidth(width);

    }

    @Override
    public void gotLayeredMap(LayeredMap map) {
        this.layeredMap = map;
        this.layeredMap.zoomProperty().addListener(this);
        this.layeredMap.centerLatitudeProperty().addListener(this);
        this.layeredMap.centerLongitudeProperty().addListener(this);
        this.layeredMap.xShiftProperty().addListener(this);
        this.layeredMap.yShiftProperty().addListener(this);
        refreshLayer();
    }

    @Override
    public void invalidated(Observable observable) {
        refreshLayer();
    }

    public void selectOverlay() {
        getStyleClass().add("rectangle-layer-select");
    }

    public void deselectOverlay() {
        getStyleClass().removeAll("rectangle-layer-select");
    }

    public String getOverlayColor() {
        return Optional.ofNullable(overlayColor.get()).orElse(nonUBD ? "#888888" : "#F95F5D");
    }

    public void setOverlayColor(String overlayColor) {
        flag = true;
        selected.set(overlayColor != null && !overlayColor.equals("#888888"));
        this.overlayColor.set(overlayColor);
        flag = false;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lodgon.openmapfx.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Rectangle;
import javafx.util.Callback;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.lodgon.openmapfx.desktop.SimpleProviderPicker;
import org.lodgon.openmapfx.layer.LicenceLayer;
import org.lodgon.openmapfx.layer.RectangleLayer;

/**
 * Custom map view control.
 */
@Slf4j
public class MapView extends BorderPane {

    @Getter
    private LayeredMap map;

    @Getter
    private SimpleProviderPicker providerPicker;

    @Getter
    private DefaultBaseMapProvider defaultBaseMapProvider;

    @Getter
    private LicenceLayer licenceLayer;

    @Getter
    @Setter
    private Callback<String, Void> onOverlaySelect;

    @Getter
    @Setter
    private Callback<String, Void> onOverlayDeselect;

    /**
     * Instantiates a new {@code MapView}.
     */
    public MapView() {
        try {
            initialize();
        } catch (Exception ex) {
            log.error("Exception in initializing MapView ", ex);
        }
    }

    /**
     * Initializes the map on the map view.
     *
     * @throws Exception
     */
    protected void initialize() throws Exception {

        defaultBaseMapProvider = new DefaultBaseMapProvider();

        providerPicker = new SimpleProviderPicker(defaultBaseMapProvider);
        map = new LayeredMap(defaultBaseMapProvider);
        BorderPane cbp = new BorderPane();
        cbp.setCenter(map);
        map.install(cbp);

        Rectangle clip = new Rectangle(700, 600);
        cbp.setClip(clip);
        //cbp.setC
        clip.heightProperty().bind(cbp.heightProperty());
        clip.widthProperty().bind(cbp.widthProperty());

        setTop(providerPicker);
        setCenter(cbp);

        map.setZoom(8);
        setPerthAsCenter();

        licenceLayer = new LicenceLayer(defaultBaseMapProvider);
        map.getLayers().add(licenceLayer);
    }

    /**
     * Sets Perth to the center of the map.
     */
    public void setPerthAsCenter() {
        map.setCenter(-31.957023, 115.861189);
    }

    /**
     * Adds an overlay rectangle with the provided number on it at the provided coordinates.
     *
     * @param text String to display on the overlay.
     * @param nw   NorthWest coordinates.
     * @param se   SouthEast coordinates.
     */
    public void addRectangleOverlay(String text, double[] nw, double[] se) {
        addRectangleOverlay(text, nw, se, false);
    }

    /**
     * Adds an overlay rectangle with the provided number on it at the provided coordinates.
     *
     * @param text String to display on the overlay.
     * @param nw   NorthWest coordinates.
     * @param se   SouthEast coordinates.
     */
    public void addRectangleOverlay(String text, double[] nw, double[] se, boolean nonUBD) {
        RectangleLayer positionLayer = new RectangleLayer(text);
        positionLayer.setId("overlay-" + text);
        positionLayer.setNonUBD(nonUBD);
        positionLayer.setOnOverlaySelect(onOverlaySelect);
        positionLayer.setOnOverlayDeselect(onOverlayDeselect);
        map.getLayers().add(positionLayer);
        positionLayer.updatePosition(nw[0], nw[1], se[0], se[1]);
        if (nonUBD) {
            positionLayer.setOverlayColor("#888888");
        }
    }

    /**
     * Removes all the overlays on the map.
     */
    public void removeAllOverlays() {
        final List<MapLayer> overlays = new ArrayList<>();
        for (MapLayer layer : map.getLayers()) {
            if (layer instanceof RectangleLayer) {
                overlays.add(layer);
            }
        }
        map.getLayers().removeAll(overlays);
    }

    /**
     * Removes the overlay which has the provided number as the display value.
     *
     * @param number Display string of the overlay.
     */
    public void removeOverlay(String number) {
        String id = "overlay-" + number;
        for (Iterator<MapLayer> iterator = map.getLayers().iterator(); iterator.hasNext();) {
            MapLayer next = iterator.next();
            if (next instanceof RectangleLayer && id.equals(((RectangleLayer)next).getId())) {
                iterator.remove();
                break;
            }
        }
    }

    /**
     * Adds an overlay rectangle with the provided number on it at the provided coordinates.
     *
     * @param number String to display on the overlay.
     * @param color  Color code for the selection.
     */
    public void selectOverlay(String number, String color) {
        String id = "overlay-" + number;
        for (Iterator<MapLayer> iterator = map.getLayers().iterator(); iterator.hasNext();) {
            MapLayer next = iterator.next();
            if (next instanceof RectangleLayer && id.equals(((RectangleLayer)next).getId())) {
                ((RectangleLayer)next).setOverlayColor(color);
                break;
            }
        }
    }

    /**
     * Removes all the overlays on the map.
     */
    public void deselectAllOverlays() {
        for (MapLayer layer : map.getLayers()) {
            if (layer instanceof RectangleLayer) {
                ((RectangleLayer)layer).setOverlayColor(null);
            }
        }
    }

    /**
     * Removes the overlay which has the provided number as the display value.
     *
     * @param number Display string of the overlay.
     */
    public void deselectOverlay(String number) {
        String id = "overlay-" + number;
        for (Iterator<MapLayer> iterator = map.getLayers().iterator(); iterator.hasNext();) {
            MapLayer layer = iterator.next();
            if (layer instanceof RectangleLayer && id.equals(((RectangleLayer)layer).getId())) {
                ((RectangleLayer)layer).setOverlayColor(null);
                break;
            }
        }
    }
}

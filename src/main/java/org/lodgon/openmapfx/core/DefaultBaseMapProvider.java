/**
 * Copyright (c) 2014, OpenMapFX
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of LodgON, the website lodgon.com, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL LODGON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.lodgon.openmapfx.core;

import java.util.LinkedList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.lodgon.openmapfx.providers.FileProvider;
import org.lodgon.openmapfx.providers.OSMTileProvider;
import org.lodgon.openmapfx.providers.StamenTileProvider;

/**
 *
 * @author johan
 */
public class DefaultBaseMapProvider implements BaseMapProvider {
    
    private static final String mapName = "OpenMapFX Tiled Map";
    
    private MapArea baseMap;
    
    private static final List<TileProvider> tileProviders = new LinkedList<>();
    static {
        tileProviders.add(new OSMTileProvider());
        //tileProviders.add(new MapQuestTileProvider()); // TODO: Temporarily disabling as generating heaps of logs.
	tileProviders.add(new StamenTileProvider());
        if (System.getProperty("fileProvider")!= null) {
            FileProvider fp = new FileProvider("OSM local", System.getProperty("fileProvider"));
            tileProviders.add(fp);           
        }
    }
    
    private final ObjectProperty<TileProvider> selectedProvider = new SimpleObjectProperty<>();
    private final ObjectProperty<MapTileType> tilesOfSelectedProvider = new SimpleObjectProperty<>();
    
    private final ObservableList<MapTileType> selectedTileTypes = FXCollections.observableArrayList();
    
    public DefaultBaseMapProvider() {
        selectedProvider.set(tileProviders.get(0));
        tilesOfSelectedProvider.set(selectedProvider.get().getDefaultType());
    }
    
    @Override
    public String getMapName() {
        return mapName;
    }
    
    
    @Override
    public BaseMap getBaseMap() {
        if (baseMap == null) {
            baseMap = new MapArea(selectedTileTypes);
        }
        
        return baseMap;
    }

//    @Override
//    public List<TileType> getSupportedMapStyles() {
//        return tileProvider.get().getTileTypes();
//    }
    
	@Override
    public List<TileProvider> getTileProviders() {
        return tileProviders;
    }
    
    @Override
    public ObservableList<MapTileType> tileTypesProperty() {
        return selectedTileTypes;
    }

    
    @Override
    public String toString() {
        return getMapName();
    }

    
    
}
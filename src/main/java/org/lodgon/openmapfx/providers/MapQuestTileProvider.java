/**
 * Copyright (c) 2014, OpenMapFX
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of LodgON, the website lodgon.com, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL LODGON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.lodgon.openmapfx.providers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.lodgon.openmapfx.core.SimpleTileType;
import org.lodgon.openmapfx.core.TileProvider;

/**
 *
 * @author Geoff Capper
 */
public class MapQuestTileProvider implements TileProvider {
    
    public static final String providerName = "MapQuest";
    
    private static ObservableList<SimpleTileType> tileTypes = FXCollections.observableArrayList();
    static {
        tileTypes.add(new SimpleTileType("Map", "http://otile1.mqcdn.com/tiles/1.0.0/map/", "© OpenStreetMap contributors"));
        tileTypes.add(new SimpleTileType("Satellite", "http://otile1.mqcdn.com/tiles/1.0.0/sat/", "Portions Courtesy NASA/JPL-Caltech and U.S. Depart. of Agriculture, Farm Service Agency"));
    }
    
    public MapQuestTileProvider() {
    }
    
    @Override
    public String getProviderName() {
        return providerName;
    }

    @Override
    public ObservableList<SimpleTileType> getTileTypes() {
        return tileTypes;
    }
    
    @Override
    public SimpleTileType getDefaultType() {
        return tileTypes.get(0);
    }
    
    @Override
    public String getAttributionNotice() {
        //return "Tiles Courtesy of <a href=\"http://www.mapquest.com/\" target=\"_blank\">MapQuest</a> <img src=\"http://developer.mapquest.com/content/osm/mq_logo.png\">";
        return "Tiles Courtesy of MapQuest (http://www.mapquest.com/)";
    }
    
    @Override
    public String toString() {
        return getProviderName();
    }
    
}

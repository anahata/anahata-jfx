/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package org.lodgon.openmapfx.desktop;

import javafx.scene.control.CheckBox;
import javafx.scene.layout.FlowPane;
import org.lodgon.openmapfx.core.DefaultBaseMapProvider;
import org.lodgon.openmapfx.core.MapTileType;
/**
 *
 * @author pablo
 */
public class TileTypePicker extends FlowPane {
    private DefaultBaseMapProvider provider;

    public TileTypePicker(DefaultBaseMapProvider provider) {
        this.provider = provider;
        for (MapTileType wtt: provider.tileTypesProperty()) {
            CheckBox cb = new CheckBox(wtt.getTypeName());
            cb.setSelected(true);
            cb.setOnAction((a) -> {
                if (cb.isSelected()) {
                    provider.tileTypesProperty().add(wtt);
                } else {
                    provider.tileTypesProperty().remove(wtt);
                }
            });
            getChildren().add(cb);
        }
    }
    
}

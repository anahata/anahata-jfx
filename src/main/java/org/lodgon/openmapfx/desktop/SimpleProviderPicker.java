/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lodgon.openmapfx.desktop;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import lombok.extern.slf4j.Slf4j;
import org.lodgon.openmapfx.core.*;

/**
 *
 * @author Geoff Capper
 */
@Slf4j
public class SimpleProviderPicker extends HBox implements ListChangeListener {

    private final ComboBox<TileProvider> cmbProviders;

    private final ComboBox<MapTileType> tileType;
    
    public SimpleProviderPicker(DefaultBaseMapProvider provider) {
        super(4);
        this.setStyle("-fx-padding:4px");

        if (provider.getTileProviders().isEmpty()) {
            throw new IllegalArgumentException("Providers array passed to SimpleProviderPicker cannot be null or empty.");
        }

        cmbProviders = new ComboBox<>(FXCollections.observableArrayList(provider.getTileProviders()));
        cmbProviders.valueProperty().addListener(
                (ObservableValue<? extends TileProvider> obs, TileProvider o, TileProvider n) -> {
                    setCurrentTileProvider(n);
                });
        tileType = new ComboBox<>();
        getChildren().addAll(cmbProviders, tileType);        
        tileType.valueProperty().addListener(new ChangeListener<MapTileType>() {
            @Override
            public void changed(
                    ObservableValue<? extends MapTileType> observable, MapTileType oldValue, MapTileType newValue) {
                provider.tileTypesProperty().clear();
                if (tileType.getValue() != null) {
                    provider.tileTypesProperty().add(tileType.getValue());
                }
            }
            
        });
        cmbProviders.getSelectionModel().select(provider.getTileProviders().get(0));
        //provider.tileProviderProperty().bind(cmbProviders.getSelectionModel().selectedItemProperty());

    }
    
    public void selectProvider(String providerName) {
        for (TileProvider tp: cmbProviders.getItems()) {
            if (tp.getProviderName().equals(providerName)) {
                setCurrentTileProvider(tp);
            }
        }
    }

    
    public void addProvider(TileProvider provider) {
        cmbProviders.getItems().add(provider);
    }

    public void setCurrentTileProvider(TileProvider tp) {
        cmbProviders.setValue(tp);
        tileType.getItems().removeListener(this);
        log.debug("provider changed " + tp + " tileTypes=" + tp.getTileTypes());
        tileType.getItems().addListener(this);
        tileType.setItems((ObservableList)tp.getTileTypes());
        onChanged(null);
    }

    public ReadOnlyObjectProperty<MapTileType> selectedTileTypeProperty() {
        return tileType.getSelectionModel().selectedItemProperty();
    }

    @Override
    public void onChanged(Change c) {
        System.out.println("onChanged------------- " + c);
        
        if (!tileType.getItems().isEmpty()) {
            tileType.setValue(tileType.getItems().get(0));
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.jfx.binding;

import java.util.Arrays;
import java.util.List;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ObservableBooleanValue;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * TabPane Utilities.
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
@Slf4j
public class TabPaneUtils {

    private static final String TAB_INDEX = "tab-index";

    /**
     * Binds tab/s visible depending on ObservableValue (true makes tab visible)
     *
     * @param value   observable value on which tab visibility depends.
     * @param tabPane TabPane that contains tab/s
     * @param tabs    tabs that we want to show/hide depending on observable value.
     */
    public static void bindVisibleTabs(final ObservableBooleanValue value, final TabPane tabPane,
            final Tab... tabs) {
        // TODO: change this.
        value.addListener(new TabVisibleListener(value, tabPane, tabs));
    }

    /**
     * Helper Listener
     */
    static class TabVisibleListener implements InvalidationListener {

        private final TabPane tabPane;

        private final List<Tab> tabs;

        private final List<Tab> allTabs;

        // TODO: bind passed value to this then maybe expressions will work
        private final ObservableBooleanValue value;

        public TabVisibleListener(ObservableBooleanValue value, TabPane tabPane, Tab... tabs) {
            Validate.notNull(tabPane);
            Validate.noNullElements(tabs);
            //
            this.tabPane = tabPane;
            this.tabs = Arrays.asList(tabs);
            this.value = value;
            this.allTabs = tabPane.getTabs();
            //
            init();
        }

        private void init() {
            buildMapper();
            showHideTabs();
        }

        private void showHideTabs() {
            for (Tab tab : tabs) {
                if (value.get()) {
                    if (!tabPane.getTabs().contains(tab)) {
                        int index = (Integer)tab.getProperties().get(TAB_INDEX);
                        log.debug("index= {} tab= \"{}\" tabPane= {}", index, tab.getText(), tabPane);
                        if (index != -1) {
                            log.debug("adding tab \"{}\" at index {} to tabPane {}", tab.getText(), index, tabPane);
                            tabPane.getTabs().add(index, tab);
                        }
                    }
                }
            }
            for (Tab tab : tabs) {
                if (!value.get()) {
                    log.debug("--removing tab \"{}\" from tabPane {}", tab.getText(), tabPane);
//                tab.getContent().setVisible(false); // THIS IS NOT WORKING TITLE IS STILL THERE
                    tabPane.getTabs().remove(tab);
                }
            }
        }

        private void buildMapper() {
            log.debug("building mapper.");
            for (Tab tab : allTabs) {
                tab.getProperties().put(TAB_INDEX, tabPane.getTabs().indexOf(tab));
            }
        }

        @Override
        public void invalidated(Observable o) {
            log.debug("value= {}", value);
            showHideTabs();
        }

    }
}

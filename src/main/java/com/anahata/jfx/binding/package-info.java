/**
 * This package contains JavaFX binding extensions. It's not to be confused with the 'bind' package, which is Anahata
 * binding from JavaFX components to a JavaBean model.
 */
package com.anahata.jfx.binding;

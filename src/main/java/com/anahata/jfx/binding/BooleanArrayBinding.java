package com.anahata.jfx.binding;

import java.util.*;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.BooleanExpression;
import org.apache.commons.lang3.Validate;

/**
 * A binding to an array of observable boolean values, that only returns true if all observed booleans are true.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class BooleanArrayBinding extends BooleanBinding {
    private BindingMode bindingMode;
    
    private List<? extends BooleanExpression> dependencies = new ArrayList<>();
    
    /**
     * Construct with booleans to bind to.
     * 
     * @param bindingMode The binding mode. Required.
     * @param booleans The booleans.
     * @throws NullPointerException If bindingMode is null.
     */
    public <T extends BooleanExpression> BooleanArrayBinding(BindingMode bindingMode, T... booleans) {
        Validate.notNull(bindingMode);
        this.bindingMode = bindingMode;
        if (booleans != null) {
            setDependencies(Arrays.asList(booleans));
        }
    }

    /**
     * Construct with booleans to bind to.
     * 
     * @param bindingMode The binding mode. Required.
     * @param booleans The booleans.
     * @throws NullPointerException If bindingMode is null.
     */
    public BooleanArrayBinding(BindingMode bindingMode, Collection<? extends BooleanExpression> booleans) {        
        Validate.notNull(bindingMode);
        this.bindingMode = bindingMode;
        
        if (booleans != null) {
            setDependencies(booleans);
        }
    }
    
    public void unbind() {        
        setDependencies(Collections.EMPTY_LIST);
    }
    
    public void addDependency(BooleanExpression newDependency) {        
        ((List)dependencies).add(newDependency);//casting due to generics rubbish
        super.bind(newDependency);
        //Pablo adding this as it otehrwise doesn't update the value
        super.invalidate();
    }
    
    /**
     * Sets the new dependencies, deregistering any previous dependencies.
     * 
     * @param newDependencies 
     */
    public final void setDependencies(Collection<? extends BooleanExpression> newDependencies) {        
        super.unbind(dependencies.toArray(new BooleanExpression[0]));
        this.dependencies = new ArrayList(newDependencies);
        super.bind(newDependencies.toArray(new BooleanExpression[0]));
    }
    
    @Override
    protected boolean computeValue() {
        if (bindingMode == BindingMode.AND) {
            return computeAnd();
        }
        
        return computeOr();
    }
    
    private boolean computeAnd() {        
        for (BooleanExpression bool : dependencies) {            
            if (!bool.get()) {
                return false;
            }
        }

        return true;
    }
    
    private boolean computeOr() {
        for (BooleanExpression bool : dependencies) {
            if (bool.get()) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * How to combine boolean values, using AND or OR.
     */
    public static enum BindingMode {
        AND,
        OR;
    }
    
    public String toString() {
        String s = "BooleanArrayBinding(mode=" + this.bindingMode +", booleans=";
        for (BooleanExpression aBoolean : dependencies) {
            s+= aBoolean + ",";
        }
        return s;
    }
}

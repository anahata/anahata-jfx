/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.jfx;

import com.anahata.util.formatting.Displayable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Labeled;
import javafx.scene.control.TableCell;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class RefreshableTableCell<S, T> extends TableCell<S, T> {
    public RefreshableTableCell(BooleanProperty tableRefresh) {        
        tableRefresh.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(
                    ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {                
                updateIndex(getIndex());
            }
        });
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
        String text = "";
        if (item != null && !empty) {
            if (item instanceof Displayable) {
                text = ((Displayable)item).getDisplayValue();
            } else {
                text = item.toString();
            }
            getLabeled().setText(item.toString());
        } 
        getLabeled().setText(text);
    }
    
    protected Labeled getLabeled() {
        return this;
    }
    
}
    

package com.anahata.jfx.collections;

import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
/**
 * JavaFX collection utilities.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AnahataFXCollections {
    /**
     * Create a new SimpleListProperty with an empty ObservableList instantiated.
     * 
     * @param <T> The type.
     * @return The property.
     */    
    public static <T> SimpleListProperty<T> newListPropertySimple() {
        return new SimpleListProperty<>(FXCollections.<T>observableArrayList());
    }
}

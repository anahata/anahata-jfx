package com.anahata.jfx.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import javafx.collections.ListChangeListener;

/**
 * A ListChangeListener that provides a total list of additions or removals, but will not provide anything if the
 * addition / removal is the same.
 *
 * @param <T> The type of list elements.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public abstract class DeltaListChangeListener<T> implements ListChangeListener<T> {
    @Override
    public final void onChanged(Change<? extends T> change) {
        List<T> added = new ArrayList<>();
        List<T> removed = new ArrayList<>();
        List<T> updated = new ArrayList<>();
        
        while (change.next()) {
            added.addAll(change.getAddedSubList());
            removed.addAll(change.getRemoved());
        }
        
        for (ListIterator<T> aiter = added.listIterator(); aiter.hasNext(); ) {
            T aitem = aiter.next();
            
            for (ListIterator<T> riter = removed.listIterator(); riter.hasNext(); ) {
                T ritem = riter.next();
                
                if (aitem.equals(ritem)) {
                    aiter.remove();
                    riter.remove();
                    updated.add(aitem);
                }
            }
        }
        
        onChanged(added, removed, updated);
    }

    /**
     * Invoked when changes to a list occur, and the added and removed collections are not equal.
     *
     * @param added   The added collection.
     * @param removed The removed collection.
     * @param updated The updated entries.
     */
    protected abstract void onChanged(List<T> added, List<T> removed, List<T> updated);
}

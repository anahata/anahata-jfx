package com.anahata.jfx.dialog;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxy;
import com.anahata.util.error.ErrorDetail;
import java.io.ByteArrayInputStream;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.commons.lang3.exception.ExceptionUtils;
import static com.anahata.jfx.dialog.DialogResources.*;

class AdvancedErrorDialog extends FXDialog {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");

    public AdvancedErrorDialog(Stage parent, ErrorDetail errorDetail) {
        super("Error Details");

        initModality(Modality.APPLICATION_MODAL);
        final InetAddress localIpAddress = errorDetail.getLocalIpAddress();

        TabPane tabPane = TabPaneBuilder.create()
                .tabs(TabBuilder.create()
                        .text("Primary Details")
                        .closable(false)
                        .content(
                                ErrorGridPaneBuilder.create()
                                .row("Date / Time", DATE_FORMAT.format(errorDetail.getCreated()))
                                .row("Application", errorDetail.getApplicationName())
                                .row("Version", errorDetail.getApplicationVersion())
                                .row("Environment", errorDetail.getApplicationEnv().toString())
                                .row("Device", errorDetail.getDevice())
                                .row("Application User", errorDetail.getAppUserName())
                                .row("Local IP Address", localIpAddress == null ? "" : localIpAddress.getHostAddress())
                                .row("Local Host Name", localIpAddress == null ? "" : localIpAddress.getHostName())
                                .row("External IP Address", errorDetail.getExternalIpAddress())
                                .row("Java Vendor", errorDetail.getJavaVendor())
                                .row("Java Version", errorDetail.getJavaVersion())
                                .row("JavaFX Version", errorDetail.getJavaFxVersion())
                                .row("OS Name", errorDetail.getOsName())
                                .row("OS Version", errorDetail.getOsVersion())
                                .row("OS Architecture", errorDetail.getOsArchitecture())
                                .row("OS User", errorDetail.getUserName())
                                .row("OS User Home Dir", errorDetail.getUserHome())
                                .row("OS User Working Dir", errorDetail.getUserWorking())
                                .row("User Comments", errorDetail.getUserComments())
                                .build())
                        .build())
                .build();

        // Screenshot.
        if (!errorDetail.getScreenshots().isEmpty()) {            
            Tab tab = new Tab();
            tabPane.getTabs().add(tab);
            tab.setText("Screenshots");
            tab.setClosable(false);            
            ScrollPane sp = new ScrollPane();
            tab.setContent(sp);            
            VBox vb = new VBox();
            sp.setContent(vb);
            for (byte[] barr : errorDetail.getScreenshots()) {
                vb.getChildren().add(new ImageView(new Image(new ByteArrayInputStream(barr))));
            } 

        }

        // Log trace.
        if (errorDetail.getEvents() != null) {
            ObservableList<ILoggingEvent> events = FXCollections.observableArrayList(errorDetail.getEvents());

            tabPane.getTabs().add(TabBuilder.create()
                    .text("Log Trace")
                    .closable(false)
                    .content(TableViewBuilder.<ILoggingEvent>create()
                            .maxHeight(Double.MAX_VALUE)
                            .columns(
                                    TableColumnBuilder.<ILoggingEvent, String>create()
                                    .text("Date / Time")
                                    .prefWidth(140)
                                    .cellValueFactory(
                                            new Callback<CellDataFeatures<ILoggingEvent, String>, ObservableValue<String>>() {
                                                @Override
                                                public ObservableValue<String> call(
                                                        CellDataFeatures<ILoggingEvent, String> p) {
                                                            return new ReadOnlyObjectWrapper<>(DATE_FORMAT.format(
                                                                            new Date(p.getValue().getTimeStamp())));
                                                        }
                                            })
                                    .build(),
                                    TableColumnBuilder.<ILoggingEvent, String>create()
                                    .text("Thread")
                                    .prefWidth(200)
                                    .cellValueFactory(
                                            new Callback<CellDataFeatures<ILoggingEvent, String>, ObservableValue<String>>() {
                                                @Override
                                                public ObservableValue<String> call(
                                                        CellDataFeatures<ILoggingEvent, String> p) {
                                                            return new ReadOnlyObjectWrapper<>(
                                                                    p.getValue().getThreadName());
                                                        }
                                            })
                                    .build(),
                                    TableColumnBuilder.<ILoggingEvent, String>create()
                                    .text("Level")
                                    .prefWidth(50)
                                    .cellValueFactory(
                                            new Callback<CellDataFeatures<ILoggingEvent, String>, ObservableValue<String>>() {
                                                @Override
                                                public ObservableValue<String> call(
                                                        CellDataFeatures<ILoggingEvent, String> p) {
                                                            return new ReadOnlyObjectWrapper<>(
                                                                    p.getValue().getLevel().toString());
                                                        }
                                            })
                                    .build(),
                                    TableColumnBuilder.<ILoggingEvent, String>create()
                                    .text("Logger")
                                    .prefWidth(300)
                                    .cellValueFactory(
                                            new Callback<CellDataFeatures<ILoggingEvent, String>, ObservableValue<String>>() {
                                                @Override
                                                public ObservableValue<String> call(
                                                        CellDataFeatures<ILoggingEvent, String> p) {
                                                            return new ReadOnlyObjectWrapper<>(
                                                                    p.getValue().getLoggerName());
                                                        }
                                            })
                                    .build(),
                                    TableColumnBuilder.<ILoggingEvent, String>create()
                                    .text("Message")
                                    .prefWidth(1000)
                                    .cellValueFactory(
                                            new Callback<CellDataFeatures<ILoggingEvent, String>, ObservableValue<String>>() {
                                                @Override
                                                public ObservableValue<String> call(
                                                        CellDataFeatures<ILoggingEvent, String> p) {
                                                            return new ReadOnlyObjectWrapper<>(
                                                                    p.getValue().getFormattedMessage());
                                                        }
                                            })
                                    .build()
                            )
                            .items(events)
                            .build()
                    )
                    .build());

            boolean tabCreated = false;
            TextArea textArea = TextAreaBuilder.create()
                    .editable(false)
                    .build();

            for (ILoggingEvent event : errorDetail.getEvents()) {
                final IThrowableProxy throwableProxy = event.getThrowableProxy();

                if (throwableProxy != null) {
                    Throwable throwable = ((ThrowableProxy)throwableProxy).getThrowable();
                    final String stackTrace = ExceptionUtils.getStackTrace(throwable);

                    if (!tabCreated) {
                        tabCreated = true;
                        tabPane.getTabs().add(TabBuilder.create()
                                .text("Exceptions")
                                .closable(false)
                                .content(textArea)
                                .build());
                        textArea.textProperty().set(stackTrace);
                    } else {
                        textArea.textProperty().set(textArea.textProperty().get() + "\n\n" + stackTrace);
                    }
                }
            }
        }

        VBox contentPanel = VBoxBuilder.create()
                .styleClass("more-info-dialog")
                .prefWidth(800)
                .prefHeight(600)
                .minHeight(Control.USE_PREF_SIZE)
                .spacing(8)
                .children(
                        tabPane,
                        HBoxBuilder.create()
                        .styleClass("button-panel")
                        .children(
                                ButtonBuilder.create()
                                .text(getMessage("common.close.btn"))
                                .prefWidth(80)
                                .onAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent t) {
                                        hide();
                                    }
                                })
                                .defaultButton(true)
                                .build()
                        )
                        .build())
                .build();

        setContentPane(contentPanel);
    }

    private static class ErrorGridPaneBuilder {
        private GridPane gp;

        private int row;

        private ErrorGridPaneBuilder() {
            gp = GridPaneBuilder.create()
                    .hgap(8)
                    .vgap(4)
                    .build();
            row = 1;
        }

        public static ErrorGridPaneBuilder create() {
            return new ErrorGridPaneBuilder();
        }

        public ErrorGridPaneBuilder row(String labelText, String value) {
            Label label = LabelBuilder.create()
                    .text(labelText)
                    .styleClass("error-label")
                    .build();
            gp.add(label, 1, row);
            label = LabelBuilder.create()
                    .text(value)
                    .styleClass("error-value")
                    .minWidth(Control.USE_PREF_SIZE)
                    .minHeight(Control.USE_PREF_SIZE)
                    .wrapText(true)
                    .build();
            gp.add(label, 2, row);
            row++;
            return this;
        }

        public GridPane build() {
            return gp;
        }
    }
}

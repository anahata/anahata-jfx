package com.anahata.jfx.dialog;

import static com.anahata.jfx.dialog.DialogResources.getIcon;
import javafx.scene.image.ImageView;

enum DialogType {
    ADVANCED_ERROR(DialogOptions.OK, "error48.image") {
        @Override
        public String getDefaultMasthead() {
            return "Error";
        }
    },
    REPORT_ISSUE(DialogOptions.OK, "error48.image") {
        @Override
        public String getDefaultMasthead() {
            return "Error";
        }
    },
    ERROR(DialogOptions.OK, "error48.image") {
        @Override
        public String getDefaultMasthead() {
            return "Error";
        }
    },
    INFORMATION(DialogOptions.OK, "info48.image") {
        @Override
        public String getDefaultMasthead() {
            return "Message";
        }
    },
    WARNING(DialogOptions.OK, "warning48.image") {
        @Override
        public String getDefaultMasthead() {
            return "Warning";
        }
    },
    CONFIRMATION(DialogOptions.YES_NO_CANCEL, "confirm48.image") {
        @Override
        public String getDefaultMasthead() {
            return "Select an Option";
        }
    },
    INPUT(DialogOptions.OK_CANCEL, "confirm48.image") {
        @Override
        public String getDefaultMasthead() {
            return "Select an Option";
        }
    };

    private final DialogOptions defaultOptions;

    private final String imageResource;

    DialogType(DialogOptions defaultOptions, String imageResource) {
        this.defaultOptions = defaultOptions;
        this.imageResource = imageResource;
    }

    public ImageView getImage() {
        return getIcon(imageResource);
    }

    public String getDefaultTitle() {
        return getDefaultMasthead();
    }

    public abstract String getDefaultMasthead();

    public DialogOptions getDefaultOptions() {
        return defaultOptions;
    }
}

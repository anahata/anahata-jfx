/*
 * Copyright (c) 2012, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.anahata.jfx.dialog;

import com.anahata.util.error.ErrorDetail;
import com.anahata.jfx.validator.Validator;
import java.util.Arrays;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.scene.Node;
import javafx.stage.Stage;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import static com.anahata.jfx.dialog.DialogResponse.*;

/**
 * A class containing a number of pre-built JavaFX modal dialogs. <p> Note: This is a copy of the official OpenJFX UI
 * Sandbox Control revision rt-9e5ef340d95f. Changes are marked and described in the readme file.
 *
 * @author OpenJFX Authors
 * @author Marco Jakob (http://edu.makery.ch)
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Dialogs {
    //== confirmation dialogs =========================================================================================
    
    /**
     * Brings up a dialog with the options Yes, No and Cancel; with the title, <b>Select an Option</b>.
     *
     * @param owner The parent stage of the dialog.
     * @param message The message to display.
     * @return The response from user input.
     */
    public static DialogResponse showConfirmDialog(final Stage owner, final String message) {
        return showConfirmDialog(owner, message, DialogType.CONFIRMATION.getDefaultMasthead());
    }

    public static DialogResponse showConfirmDialog(final Stage owner, final String message, final String masthead) {
        return showConfirmDialog(owner, message, masthead, DialogType.CONFIRMATION.getDefaultTitle());
    }

    public static DialogResponse showConfirmDialog(final Stage owner, final String message,
            final String masthead, final String title) {
        return showConfirmDialog(owner, message, masthead, title, DialogType.CONFIRMATION.getDefaultOptions());
    }

    public static DialogResponse showConfirmDialog(final Stage owner, final String message,
            final String masthead, final String title, final DialogOptions options) {
        return showSimpleContentDialog(owner, title, masthead, message, DialogType.CONFIRMATION, options);
    }

    //== information dialogs ==========================================================================================
    
    public static void showInformationDialog(final Stage owner, final String message) {
        showInformationDialog(owner, message, DialogType.INFORMATION.getDefaultMasthead());
    }

    public static void showInformationDialog(final Stage owner, final String message, final String masthead) {
        showInformationDialog(owner, message, masthead, DialogType.INFORMATION.getDefaultTitle());
    }

    /*
     * Info message string displayed in the masthead
     * Info icon 48x48 displayed in the masthead
     * "OK" button at the bottom.
     *
     * text and title strings are already translated strings.
     */
    public static void showInformationDialog(final Stage owner, final String message, final String masthead,
            final String title) {
        showSimpleContentDialog(owner, title, masthead, message, DialogType.INFORMATION,
                DialogType.INFORMATION.getDefaultOptions());
    }

    //== warning dialogs ==============================================================================================
    
    /**
     * Displays a warning icon instead of "Java" logo icon in the upper right corner of masthead. Has masthead and
     * message that is displayed in the middle part of the dialog. No bullet is displayed.
     * 
     * @param owner   Parent of the dialog.
     * @param message Question to display in the middle part.
     * @return The user's response.
     */
    public static DialogResponse showWarningDialog(final Stage owner, final String message) {
        return showWarningDialog(owner, message, DialogType.WARNING.getDefaultMasthead());
    }

    /**
     * Displays a warning icon instead of "Java" logo icon in the upper right corner of masthead. Has masthead and
     * message that is displayed in the middle part of the dialog. No bullet is displayed.
     * 
     * @param owner    Parent of the dialog.
     * @param message  Question to display in the middle part.
     * @param masthead Masthead in the top part of the dialog.
     * @return The user's response.
     */
    public static DialogResponse showWarningDialog(final Stage owner, final String message, final String masthead) {
        return showWarningDialog(owner, message, masthead, DialogType.WARNING.getDefaultTitle());
    }

    /**
     * Displays a warning icon instead of "Java" logo icon in the upper right corner of masthead. Has masthead and
     * message that is displayed in the middle part of the dialog. No bullet is displayed.
     * 
     * @param owner    Parent of the dialog.
     * @param message  Question to display in the middle part.
     * @param masthead Masthead in the top part of the dialog.
     * @param title    The title of the dialog.
     * @return The user's response.
     */
    public static DialogResponse showWarningDialog(final Stage owner, final String message, String masthead,
            final String title) {
        return showWarningDialog(owner, message, masthead, title, DialogType.WARNING.getDefaultOptions());
    }

    /**
     * Displays a warning icon instead of "Java" logo icon in the upper right corner of masthead. Has masthead and
     * message that is displayed in the middle part of the dialog. No bullet is displayed.
     * 
     * @param owner    Parent of the dialog.
     * @param message  Question to display in the middle part.
     * @param masthead Masthead in the top part of the dialog.
     * @param title    The title of the dialog.
     * @param options  Dialog options - which buttons to show.
     * @return The user's response.
     */
    public static DialogResponse showWarningDialog(final Stage owner, final String message, final String masthead,
            final String title, DialogOptions options) {
        return showSimpleContentDialog(owner, title, masthead, message, DialogType.WARNING, options);
    }

    //== exception / error dialogs ====================================================================================
    
    /**
     * Displays an error dialog.
     *
     * @param owner   Parent of the dialog.
     * @param message Question to display in the middle part.
     * @return The user's response.
     */
    public static DialogResponse showErrorDialog(final Stage owner, final String message) {
        return showErrorDialog(owner, message, DialogType.ERROR.getDefaultMasthead());
    }

    /**
     * Displays an error dialog.
     * 
     * @param owner    Parent of the dialog.
     * @param message  Question to display in the middle part.
     * @param masthead Masthead in the top part of the dialog.
     * @return The user's response.
     */
    public static DialogResponse showErrorDialog(final Stage owner, final String message, final String masthead) {
        return showErrorDialog(owner, message, masthead, masthead);
    }

    /**
     * Displays an error dialog.
     * 
     * @param owner    Parent of the dialog.
     * @param message  Question to display in the middle part.
     * @param masthead Masthead in the top part of the dialog.
     * @param title    The title of the dialog.
     * @return The user's response.
     */
    public static DialogResponse showErrorDialog(final Stage owner, final String message, final String masthead,
            final String title) {
        return showErrorDialog(owner, message, masthead, title, DialogType.ERROR.getDefaultOptions());
    }

    /**
     * Displays an error dialog.
     * 
     * @param owner    Parent of the dialog.
     * @param message  Question to display in the middle part.
     * @param masthead Masthead in the top part of the dialog.
     * @param title    The title of the dialog.
     * @param options  Dialog options - which buttons to show.
     * @return The user's response.
     */
    public static DialogResponse showErrorDialog(final Stage owner, final String message, final String masthead,
            final String title, DialogOptions options) {
        return showSimpleContentDialog(owner, title, masthead, message, DialogType.ERROR, options);
    }

    /**
     * Displays an error dialog, displaying an exception.
     * 
     * @param owner     Parent of the dialog.
     * @param message   Question to display in the middle part.
     * @param masthead  Masthead in the top part of the dialog.
     * @param title     The title of the dialog.
     * @param throwable The exception to show.
     * @return The user's response.
     */
    public static DialogResponse showErrorDialog(final Stage owner, final String message, final String masthead,
            final String title, final Throwable throwable) {
        DialogTemplate template = new DialogTemplate(owner, title, masthead, null);
        template.setErrorContent(message, throwable);
        return showDialog(template);
    }

    /**
     * Displays an advanced error dialog that displays full contextual error details when 'Details...' is pressed. It
     * also prompts the user for details of the error and if a screenshot should be sent in error reporting.
     * 
     * @param owner       Parent of the dialog.
     * @param message     Question to display in the middle part.
     * @param masthead    Masthead in the top part of the dialog.
     * @param title       The title of the dialog.
     * @param errorDetail Error details.
     * @return The user's response.
     */
    public static DialogResponse showAdvancedErrorDialog(final Stage owner, final String message,
            final String masthead, final String title, ErrorDetail errorDetail) {
        DialogTemplate template = new DialogTemplate(owner, title, masthead, null);
        template.setAdvancedErrorContent(message, errorDetail);
        return showDialog(template);
    }

    /**
     * Displays a report issue dialog, which is similar to the advanced error dialog except that the error text message
     * is required, and the user can cancel the dialog.
     * 
     * @param owner       Parent of the dialog.
     * @param message     Question to display in the middle part.
     * @param masthead    Masthead in the top part of the dialog.
     * @param title       The title of the dialog.
     * @param errorDetail Error details.
     * @return The user's response.
     */
    public static DialogResponse showReportIssueDialog(final Stage owner, final String message, final String masthead,
            final String title, ErrorDetail errorDetail) {
        DialogTemplate template = new DialogTemplate(owner, title, masthead, null);
        template.setReportIssueContent(message, errorDetail);
        return showDialog(template);
    }

    //== user input dialogs ===========================================================================================
    
    /**
     * Displays a user input dialog prompting for text.
     * 
     * @param owner    Parent of the dialog.
     * @param message  Question to display in the middle part.
     * @return The text entered by the user.
     */
    public static String showInputDialog(final Stage owner, final String message) {
        return showInputDialog(owner, message, "Masthead");
    }

    /**
     * Displays a user input dialog prompting for text.
     * 
     * @param owner    Parent of the dialog.
     * @param message  Question to display in the middle part.
     * @param masthead Masthead in the top part of the dialog.
     * @return The text entered by the user.
     */
    public static String showInputDialog(final Stage owner, final String message, final String masthead) {
        return showInputDialog(owner, message, masthead, "Title");
    }

    /**
     * Displays a user input dialog prompting for text.
     * 
     * @param owner     Parent of the dialog.
     * @param message   Question to display in the middle part.
     * @param masthead  Masthead in the top part of the dialog.
     * @param title     The title of the dialog.
     * @return The text entered by the user.
     */
    public static String showInputDialog(final Stage owner, final String message, final String masthead,
            final String title) {
        return showInputDialog(owner, message, masthead, title, null);
    }

    /**
     * Displays a user input dialog prompting for text.
     * 
     * @param owner        Parent of the dialog.
     * @param message      Question to display in the middle part.
     * @param masthead     Masthead in the top part of the dialog.
     * @param title        The title of the dialog.
     * @param initialValue The initial value of the entered text.
     * @return The text entered by the user.
     */
    public static String showInputDialog(final Stage owner, final String message, final String masthead,
            final String title, final String initialValue) {
        return showInputDialog(owner, message, masthead, title, initialValue, (Validator)null);
    }

    /**
     * Displays a user input dialog prompting for a select list of choices as radio buttons.
     * 
     * @param <T>          The type of the choices.
     * @param owner        Parent of the dialog.
     * @param message      Question to display in the middle part.
     * @param masthead     Masthead in the top part of the dialog.
     * @param title        The title of the dialog.
     * @param initialValue The initial value of the entered text.
     * @param choices      The available choices to be selected.
     * @return The chosen value.
     */
    public static <T> T showInputDialog(final Stage owner, final String message, final String masthead,
            final String title, final T initialValue, final T... choices) {
        return showInputDialog(owner, message, masthead, title, initialValue, Arrays.asList(choices));
    }

    /**
     * Displays a user input dialog prompting for a select list of choices as radio buttons.
     * 
     * @param <T>          The type of the choices.
     * @param owner        Parent of the dialog.
     * @param message      Question to display in the middle part.
     * @param masthead     Masthead in the top part of the dialog.
     * @param title        The title of the dialog.
     * @param initialValue The initial value of the entered text.
     * @param choices      The available choices to be selected.
     * @return The chosen value.
     */
    public static <T> T showInputDialog(final Stage owner, final String message, final String masthead,
            final String title, final T initialValue, final List<T> choices) {
        DialogTemplate<T> template = new DialogTemplate<>(owner, title, masthead, null);
        template.setInputContent(message, initialValue, choices, null);
        return showUserInputDialog(template);
    }

    /**
     * Displays a user input dialog prompting for a select list of choices as radio buttons.
     * 
     * @param <T>            The type of the choices.
     * @param owner          Parent of the dialog.
     * @param message        Question to display in the middle part.
     * @param masthead       Masthead in the top part of the dialog.
     * @param title          The title of the dialog.
     * @param initialValue   The initial value of the entered text.
     * @param inputValidator Validate input text.
     * @return The chosen value.
     */
    public static <T> T showInputDialog(final Stage owner, final String message, final String masthead,
            final String title, final T initialValue, final Validator inputValidator) {
        DialogTemplate<T> template = new DialogTemplate<>(owner, title, masthead, null);
        template.setInputContent(message, initialValue, null, inputValidator);
        return showUserInputDialog(template);
    }

    /**
     * Show a custom input dialog. The caller provides their own node and stylesheets.
     *
     * @param owner         Owning stage.
     * @param masthead      Heading.
     * @param title         Title.
     * @param node          Node of custom dialog.
     * @param validProperty Property indicating if fields are valid.
     * @param stylesheets   A list of stylesheets to add to the dialog.
     * @return The user response.
     */
    public static DialogResponse showCustomInputDialog(final Stage owner, final String masthead, final String title,
            final Node node, final BooleanProperty validProperty, String... stylesheets) {
        DialogTemplate template = new DialogTemplate(owner, title, masthead, null);
        template.setCustomInputContent(node, validProperty);
        template.getDialog().getScene().getStylesheets().addAll(stylesheets);
        return showCustomInputDialog(template);
    }

    //== private api ==================================================================================================
    
    private static DialogResponse showSimpleContentDialog(final Stage owner, final String title, final String masthead,
            final String message, DialogType dialogType, final DialogOptions options) {
        DialogTemplate template = new DialogTemplate(owner, title, masthead, options);
        template.setSimpleContent(message, dialogType);
        return showDialog(template);
    }

    private static DialogResponse showDialog(DialogTemplate template) {
        try {
//            template.getDialog().centerOnScreen();
            template.show();
            return template.getResponse();
        } catch (Throwable e) {
            return CLOSED;
        }
    }

    private static <T> T showUserInputDialog(DialogTemplate<T> template) {
        // !CHANGE START! return null if user did not click ok
//        template.getDialog().centerOnScreen();
        template.show();
        
        if (template.getResponse() == OK) {
            return template.getInputResponse();
        } else {
            return null;
        }
        // !CHANGE END!
    }

    private static DialogResponse showCustomInputDialog(DialogTemplate template) {
//        template.getDialog().centerOnScreen();
        template.show();
        return template.getResponse();
    }
}

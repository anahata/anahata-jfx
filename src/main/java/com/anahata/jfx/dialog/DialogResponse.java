package com.anahata.jfx.dialog;

/**
 * An enumeration used to specify the response provided by the user when interacting with a dialog.
 */
public enum DialogResponse {
    /**
     * Used to represent that the user has selected the option corresponding with YES.
     */
    YES,
    /**
     * Used to represent that the user has selected the option corresponding with NO.
     */
    NO,
    /**
     * Used to represent that the user has selected the option corresponding with CANCEL.
     */
    CANCEL,
    /**
     * Used to represent that the user has selected the option corresponding with OK.
     */
    OK,
    /**
     * Used to represent that the user has selected the option corresponding with CLOSED.
     */
    CLOSED
}

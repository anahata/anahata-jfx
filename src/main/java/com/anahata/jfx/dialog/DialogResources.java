package com.anahata.jfx.dialog;

import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class DialogResources {
    // Localization strings.
    private final static ResourceBundle dialogsResourceBundle =
            ResourceBundle.getBundle("com.anahata.jfx.dialog.dialog-resources");

    /**
     * Method to get an internationalized string from the deployment resource.
     */
    static String getMessage(String key) {
        try {
            return dialogsResourceBundle.getString(key);
        } catch (MissingResourceException ex) {
            // Do not trace this exception, because the key could be
            // an already translated string.
            log.error("Failed to get string for key '{}'", key);
            return key;
        }
    }

    /**
     * Returns a string from the resources
     */
    static String getString(String key) {
        try {
            return dialogsResourceBundle.getString(key);
        } catch (MissingResourceException mre) {
            // Do not trace this exception, because the key could be
            // an already translated string.
            log.error("Failed to get string for key '{}'", key);
            return key;
        }
    }

    /**
     * Returns a string from a resource, substituting argument 1
     */
    static String getString(String key, Object... args) {
        return MessageFormat.format(getString(key), args);
    }

    /**
     * Returns an
     * <code>ImageView</code> given an image file name or resource name
     */
    static public ImageView getIcon(final String key) {
        try {
            return AccessController.doPrivileged(
                    new PrivilegedExceptionAction<ImageView>() {
                        @Override
                        public ImageView run() {
                            String resourceName = getString(key);
                            URL url = DialogResources.class.getResource(resourceName);
                            
                            if (url == null) {
                                log.debug("Can't create ImageView for key '{}', which has resource name '{}' and URL 'null'",
                                        key, resourceName);
                                return null;
                            }
                            
                            return getIcon(url);
                        }
                    });
        } catch (Exception ex) {
            log.error("Error getting icon", ex);
            return null;
        }
    }

    static public ImageView getIcon(URL url) {
        return new ImageView(new Image(url.toString()));
    }
}

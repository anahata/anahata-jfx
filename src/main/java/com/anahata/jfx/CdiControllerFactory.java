package com.anahata.jfx;

import com.anahata.util.cdi.Cdi;
import javafx.util.Callback;
import lombok.extern.slf4j.Slf4j;

/**
 * JavaFX calls made through CDI.
 *
 * @author Robert Nagajek
 */
@Slf4j
public class CdiControllerFactory implements Callback<Class<?>, Object> {
    public static final CdiControllerFactory INSTANCE = new CdiControllerFactory();

    @Override
    public Object call(Class<?> clazz) {
        Object ret = Cdi.get(clazz);
        log.debug("clazz= {} ret = {} identityHashCode={}", clazz, ret, System.identityHashCode(ret));
        return ret;
    }
}

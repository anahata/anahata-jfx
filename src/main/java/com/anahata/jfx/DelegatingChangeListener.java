/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * Listener for changes to either ObservableValues or ObservableLists
 * @author pablo
 */
@AllArgsConstructor
@Slf4j
public class DelegatingChangeListener<T> implements ChangeListener<T>, ListChangeListener<T>{
    
    private Runnable delegate;
    
    @Override
    public void changed(
            ObservableValue<? extends T> observable, T oldValue, T newValue) {
        //log.debug("Delegating change listener got change on {}, ov={}, nw={}", observable, newValue, oldValue);
        delegate.run();
    }

    @Override
    public void onChanged(
            Change<? extends T> c) {
        //log.debug("Delegating change listener got list change on: {}", c.getList());
        delegate.run();
    }
    
}

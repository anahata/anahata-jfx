/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.concurrent;

import com.anahata.util.formatting.Displayable;
import com.anahata.util.progress.ProgressMessage;
import com.sun.javafx.tk.Toolkit;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Node;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import static org.apache.commons.lang3.ClassUtils.*;

/**
 *
 * @author Vijay *
 */
@Slf4j
public class BackgroundInvocationHandler implements InvocationHandler {

    @Setter
    protected Node nodeToDisable;

    @Setter
    protected BooleanProperty runningProperty = new SimpleBooleanProperty();

    @Setter
    protected Object delegate;

    public static void disableWhileRunning(Node node, Object... proxies) {
        for (Object remoteService : proxies) {
            ((BackgroundInvocationHandler)Proxy.getInvocationHandler(remoteService)).setNodeToDisable(node);
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        log.debug("method {} ", method);

        if (method.getDeclaringClass().equals(Object.class)) {
            return method.invoke(this, args);
        }

        if (Platform.isFxApplicationThread()) {

            final Object key = new Object();
            ApplicationTask applicationTask = new ApplicationTask() {

                @Override
                protected Object call() throws Exception {
                    log.trace("ApplicationTask cal() starts {} ", method);
                    String title = "Processing";

                    if (method.isAnnotationPresent(ProgressMessage.class)) {
                        title = method.getAnnotation(ProgressMessage.class).value();
                    }

                    String message = method.getDeclaringClass().getSimpleName() + " " + method.getName();
                    if (args != null && args.length > 0) {
                        Object arg = args[0];
                        if (arg instanceof Displayable) {
                            try {
                                message += " " + ((Displayable)arg).getDisplayValue();
                            } catch (Exception e) {
                                log.warn("Could not call getDisplayValue on parameter 0 of " + title + " " + message, e);
                            }
                        }
                    }

                    updateTitle(title);
                    updateMessage(message);
                    
                    Object ret = method.invoke(delegate, args);
                    log.trace("ApplicationTask cal() finished {} ", method);
                    return ret;
                }

                @Override
                protected void succeeded() {
                    log.trace("ApplicationTask suceeded(). exitingNestedEventLoop {} ", method);
                    Toolkit.getToolkit().exitNestedEventLoop(key, super.getValue());
                }

                @Override
                protected void cancelled() {
                    super.cancelled();
                    //cales Thread.currentThread.interrup();
                    log.debug("ApplicationTask cancelled() exitingNestedEventLoop {} ", method);
                    Toolkit.getToolkit().exitNestedEventLoop(key, new BackgroundTaskCancelledException());
                }

                @Override
                protected void failed() {
                    super.failed();
                    log.debug("ApplicationTask failed() exitingNestedEventLoop method = {}, exception = {}", method,super.getException());
                    Toolkit.getToolkit().exitNestedEventLoop(key, super.getException());
                }

            };

            if (runningProperty != null) {
                runningProperty.unbind();
                runningProperty.bind(applicationTask.runningProperty());
            }

            if (nodeToDisable != null) {
                nodeToDisable.disableProperty().unbind();
                nodeToDisable.disableProperty().bind(applicationTask.runningProperty());
            }

            long ts = System.currentTimeMillis();
            log.trace("Calling ApplicationTask.launch() {} ", method);
            applicationTask.launch();

            Object returnObj;
            try {
                log.trace("entering nested event loop() {} ", method);
                returnObj = Toolkit.getToolkit().enterNestedEventLoop(key);
            } catch (IllegalStateException e) {
                String msg = "Exceptiong attempting to enter the nested event loop to process the call in @Background. Talk to Pablo or Goran to figure out why this is happening. "
                        + "Known reasons are that the background annotation is called from within a TimeLine or triggered by a value change in CalendarTextField. The call will be "
                        + "ran in the JavaFX thread but please look into the problem. A known workaround is to do Platform.runLater.";
                log.error(msg, e);

                return invokeSynchronously(method, delegate, args);

            } finally {
                ts = System.currentTimeMillis() - ts;
                log.trace("ApplicationTask exited nestedEventLoop in {} {} ", ts, method);
            }

            if (returnObj instanceof Throwable) {
                Throwable t = ((Throwable)returnObj);
                //method.invoke always wraps into InvocationTargetException
                if (t instanceof InvocationTargetException) {
                    t = t.getCause();
                }
                throw t;
            } else {
                return returnObj;
            }

        } else {
            return invokeSynchronously(method, delegate, args);
        }

    }
    
    /**
     * Invokes synchronously, unwrapping InvocationTargetException
     * 
     * @param method
     * @param delegate
     * @param args
     * @return
     * @throws Throwable 
     */
    private static Object invokeSynchronously(Method method, Object delegate, Object[] args) throws Throwable {
        try {
            return method.invoke(delegate, args);
        } catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }

    /**
     * Checks if a given object is already an instance of a dynamic proxy using this invocation handler.
     *
     * @param delegate an instance of an object such as a remote service proxy
     * @return true if it is a proxy, false otherwise
     */
    public static boolean isAsynchProxy(Object delegate) {
        return Proxy.isProxyClass(delegate.getClass()) && Proxy.getInvocationHandler(delegate) instanceof BackgroundInvocationHandler;
    }

    /**
     * Creates the proxy for the given delegate disabling the node specified while running.
     *
     * @param <T>
     * @param service
     * @param node
     * @return
     */
    public static <T> T newAsynchProxy(T delegate, Node node, BooleanProperty running) {
        BackgroundInvocationHandler proxy = new BackgroundInvocationHandler();
        proxy.setDelegate(delegate);
        proxy.setNodeToDisable(node);
        if (running != null) {
            proxy.setRunningProperty(running);
        }
        return (T)Proxy.newProxyInstance(delegate.getClass().getClassLoader(),
                getAllInterfaces(delegate.getClass()).toArray(new Class[0]), proxy);
    }

}

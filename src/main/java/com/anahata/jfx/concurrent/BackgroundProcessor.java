/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.concurrent;

import com.anahata.util.reflect.ReflectionUtils;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Node;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * Replaces fields annotated as @Backgroun with a JavaFXRemoteService proxy, using the field annotated as @BackgroundDisable
 * as the node to disable.
 * 
 * @author pablo
 */
@Slf4j
public class BackgroundProcessor {
    public static void process(@NonNull Object controller, Node node) {
        process(controller, node, new HashSet());
    }
    
    private static void process(@NonNull Object controller, Node node, Set<Object> processed) {
        log.info("Processing {} default node to disable = {}", controller, node);
        try {
            
            List<Field> fields = ReflectionUtils.getAllDeclaredFields(controller.getClass());
            
            //preemptive check for asynch proxies to make sure we don't update the @BackgroundRunning property
            for (Field f : fields) {                
                if (f.isAnnotationPresent(Background.class)) {
                    f.setAccessible(true);
                    Object o = f.get(controller);
                    if (BackgroundInvocationHandler.isAsynchProxy(o)) {
                        log.warn("Field {} is already an asynch proxy, assuming controller has already been processed", f, controller);
                        return;
                    } 
                }
            }
            
            Node n = node;
            BooleanProperty runningProperty = null;
            
            for (Field f : fields) {
                f.setAccessible(true);
                if (f.isAnnotationPresent(BackgroundRunning.class)) {
                    Object o = f.get(controller);
                    if (o == null) {
                      runningProperty = new SimpleBooleanProperty();
                      f.set(controller, runningProperty);
                    } if ((o instanceof BooleanProperty)) {
                        runningProperty = (BooleanProperty)o;
                    } else {
                        throw new RuntimeException(
                                "Field " + controller.getClass().getSimpleName() + "." + f.getName() + " annotated as @BackgroundRunning but neither null or a BooleanProperty");
                    }
                } else if (f.isAnnotationPresent(BackgroundDisable.class)) {
                    Object o = f.get(controller);
                    if ((o instanceof Node)) {
                        n = (Node)o;
                    } else {
                        throw new RuntimeException(
                                "Field " + controller.getClass().getSimpleName() + "." + f.getName() + " annotated as @BackgroundDisable but not a node");
                    }
                }
            }

            for (Field f : fields) {
                if (f.isAnnotationPresent(Background.class)) {
                    Object o = f.get(controller);
                    log.info("Creating asynx proxy for {}.{} ", controller,f.getName());
                    o = BackgroundInvocationHandler.newAsynchProxy(o, n, runningProperty);
                    f.set(controller, o);
                }
            }
            
            processed.add(controller);
            
            //now look for nested controllers
            for (Field f : fields) {
                if(f.getName().endsWith("Controller")) {
                    f.setAccessible(true);
                    Object nestedController = f.get(controller);
                    if (nestedController != null) {
                        if (processed.contains(nestedController)) {
                            log.info("Nested controller already processed {}.{} = {}", controller,f.getName(),nestedController);
                        } else {
                            log.info("Processing child controller {}.{} = {}", controller,f.getName(),nestedController);
                            //TODO check whether we would be better of passing the same node to disable or to let the child controller decide
                            BackgroundProcessor.process(nestedController, node, processed);
                        }
                    }
                }
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }
}

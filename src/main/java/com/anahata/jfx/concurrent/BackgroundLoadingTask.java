/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.jfx.concurrent;

import javafx.concurrent.Worker.State;
import javafx.scene.Node;

/**
 * Manages an ApplicationTask to load data on the background disabling a given screen component while the data is
 * running.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Deprecated
public class BackgroundLoadingTask<T> {
    private BackgroundLoading<T> backgroundLoading;

    /**
     * The node whic will be dissabled.
     */
    private Node node;

    /**
     * The message for the ApplicationTask. (optional)
     */
    private String message;

    /**
     * The title for the ApplicationTask. (optional)
     */
    private String title;

    /**
     * The internally used application task.
     */
    private ApplicationTask task;

    public BackgroundLoadingTask(BackgroundLoading<T> backgroundLoading, Node node) {
        this(backgroundLoading, node, "Loading...");
    }

    public BackgroundLoadingTask(BackgroundLoading<T> backgroundLoading, Node node, String title) {
        this(backgroundLoading, node, title, null);
    }

    public BackgroundLoadingTask(BackgroundLoading<T> backgroundLoading, Node node, String title, String message) {
        this.backgroundLoading = backgroundLoading;
        this.node = node;
        this.message = message;
        this.title = title;
    }

    public void loadInBackground() {
        loadInBackground(true);
    }

    public void loadInBackground(boolean disableNodeWhileRunning) {

        if (task != null) {
            task.cancel();
        }

        task = new ApplicationTask<T>() {
            @Override
            protected T call() throws Exception {
                updateTitle(title);
                updateMessage(message);
                if (task == this) {
                    return backgroundLoading.load();
                } else {
                    cancel();
                    return null;
                }
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                if (task == this) {
                    backgroundLoading.loadSucceeded(super.getValue());
                }
            }
            @Override
            protected void failed() {
                backgroundLoading.loadFailed(super.getException());
            }
        };
        if (disableNodeWhileRunning) {
            node.disableProperty().bind(task.stateProperty().isNotEqualTo(State.SUCCEEDED).and(task.stateProperty().isNotEqualTo(State.FAILED)));
        }
        task.launch();
    }
}

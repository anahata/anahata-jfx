package com.anahata.jfx.concurrent;

import com.anahata.util.cdi.Cdi;
import com.anahata.util.progress.ProgressListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * JavaFX task that notifies the application on state change events and provides a JavaFX component composed of
 * application title, progress bar, message and a stop button. Implements JavaFX EventHandler class so it can be used as
 * an event listener.
 *
 * Sample usage as EventHandler:
 * <tt>
 * <pre>
 * <code>
 * something.setOnEvent(new ApplicationTask(){
 *  public Object call() throws Exception {
 *      updateTitle("Doing this or that on the background");
 *      //.... background code execution
 *      return null;
 *  }});
 * <tt>
 * </pre>
 * </code>
 *
 * @param <T> The type of task.
 * @author pablo
 */
@Slf4j
public abstract class ApplicationTask<T> extends Task<T> implements EventHandler, ProgressListener {
    /**
     * Thread pool for launching Application Tasks.
     */
    private static ExecutorService threadPool = Executors.newCachedThreadPool(new ThreadFactory() {
        volatile int threadCount = 1;

        @Override
        public Thread newThread(Runnable r) {
            threadCount++;
            log.info("Creating thread " + threadCount);
            Thread t = new Thread(r);
            //For debugging purposes, it would probably be good to override update title
            //so when the application updates the task title, we update the
            //thread name too and when the task finishes and the thread goes back to the pool
            //we revert it to the old name
            t.setName("ApplicationTask Thread " + threadCount);
            t.setDaemon(true);
            return t;
        }
    });

    /**
     * A JavaFX Node to display the state of the task.
     */
    private VBox mainVbox;

    /**
     * Default Constructor.
     */
    public ApplicationTask() {
        super.setOnScheduled(new EventHandler() {
            @Override
            public void handle(Event t) {
                log.trace("{} scheduled", getTitle());

                //notify application when there is a task status change
                ChangeListener<Object> updateMainAppListener = new ChangeListener<Object>() {
                    @Override
                    public void changed(ObservableValue<? extends Object> ov, Object t, Object t1) {
                        Cdi.fireEvent(ApplicationTask.this);
                    }
                };

                titleProperty().addListener(updateMainAppListener);
                messageProperty().addListener(updateMainAppListener);
                runningProperty().addListener(updateMainAppListener);
                exceptionProperty().addListener(updateMainAppListener);

                updateProgress(-1, -1);

                Cdi.fireEvent(ApplicationTask.this);
            }
        });

        super.setOnFailed(new EventHandler() {
            @Override
            public void handle(Event t) {
                Throwable throwable = ApplicationTask.this.getException();
                log.error("ApplicationTask title=" + getTitle() + " message= " + getMessage() + " failed", throwable);
            }
        });
        
    }

    @Override
    protected void cancelled() {
        log.debug("Application Task {} cancelled {}");
    }
    

    /**
     * Event Handler implementation that simply invokes {@link launch{}}.
     *
     * @param t
     */
    @Override
    public void handle(Event t) {
        launch();
    }

    /**
     * Launches the task in a new thread, if the current thread is the JavaFX app thread and
     */
    public void launch() {
        //TODO use thread pool
        threadPool.submit(this);
    }
    
    /**
     * Launches the task in a new thread, if the current thread is the JavaFX app thread and
     */
    public void delayedLaunch(final long delay) {
        //TODO use thread pool
        threadPool.submit(new Runnable() {
            @Override
            public void run() {
                log.debug("launching with delay will sleep for {} ms", delay);
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    
                }
                if (!isCancelled()) {
                    log.debug("launching with delay woke up after {} ms, launching {}", delay);
                    threadPool.submit(ApplicationTask.this);
                } else {
                    log.debug("launching with delay woke up after {} ms will not launch as this application task has already being cancelled");
                }
            }
        });
    }

    /**
     * A JavaFX component associated to this task object that renders taks name, description, progress bar and a stop
     * icon.
     *
     * @return the JavaFX component that renders taks name, description, progress bar and a stop icon.
     */
    public VBox getComponent() {

        Validate.isTrue(Platform.isFxApplicationThread(), "getComponent() can only run inside FX thread");

        if (mainVbox == null) {

            mainVbox = new VBox();

            Label titleLabel = new Label();
            ProgressBar pb = new ProgressBar();
            ImageView cancelIcon = new ImageView(new Image("/img/yam/action/delete_16.png"));
            Button cancelButton = new Button(null, cancelIcon);
            cancelButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    ApplicationTask.this.cancel(true);
                }
            });
            Label messageLabel = new Label();

            cancelButton.setTooltip(new Tooltip("Cancel"));

            HBox titleAndCancel = new HBox();

            HBox titleWrapper = new HBox();
            titleWrapper.getChildren().add(titleLabel);

            titleAndCancel.getChildren().addAll(titleWrapper, cancelButton);
            HBox.setHgrow(titleWrapper, Priority.ALWAYS);

            mainVbox.getChildren().add(titleAndCancel);
            mainVbox.getChildren().add(pb);
            mainVbox.getChildren().add(messageLabel);

            messageLabel.getStyleClass().add("applicationTaskMessage");
            titleLabel.getStyleClass().add("applicationTaskTitle");

            titleLabel.textProperty().bind(titleProperty());
            messageLabel.textProperty().bind(messageProperty());
            pb.progressProperty().bind(progressProperty());
            pb.prefWidthProperty().bind(mainVbox.widthProperty().subtract(5));
        }

        return mainVbox;
    }

    @Override
    public void progress(double progress) {
        if (progress <= 1) {
            updateProgress(progress, 1);
        } else {
            updateProgress(-1, 1);
        }
    }

}

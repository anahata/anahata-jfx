package com.anahata.jfx.concurrent;

/**
 * Interface for BackgroundLoading that provides loading and screen updating logic.
 * 
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Deprecated
public interface BackgroundLoading<T> {
    /**
     * Performs the data load (runs on non jfx thread).
     * @return the data.
     */
    T load() throws Exception;
    
    /**
     * Callback method invoked when the data has been loaded that runs on fx thread.
     * @param data the loaded data.
     */
    void loadSucceeded(T data);
    
    default void loadFailed(Throwable e) {
        
    }
}

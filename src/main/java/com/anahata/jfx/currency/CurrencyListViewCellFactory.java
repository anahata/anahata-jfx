package com.anahata.jfx.currency;

import java.util.Currency;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import lombok.AllArgsConstructor;
/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@AllArgsConstructor
public class CurrencyListViewCellFactory implements Callback<ListView<Currency>, ListCell<Currency>> {

    public static final CurrencyListViewCellFactory MEDIUM_INSTANCE = new CurrencyListViewCellFactory(false);
    public static final CurrencyListViewCellFactory LONG_INSTANCE = new CurrencyListViewCellFactory(true);
    final boolean longStyle;
    
    @Override
    public ListCell<Currency> call(
            ListView<Currency> param) {
        return new CurrencyCell();
    }
    
    /**
     * Renders The items in the GST drop down.
     */
    @AllArgsConstructor
    private class CurrencyCell extends ListCell<Currency> {
        
        @Override
        protected void updateItem(Currency item, boolean empty) {
            super.updateItem(item, empty);
            if (empty || item == null) {
                setText(null);
            } else {
                if (longStyle) {
                    setText(item.getCurrencyCode() + " - " + item.getDisplayName());
                } else {
                    setText(item.getCurrencyCode());
                }
            }
        }
    }
}

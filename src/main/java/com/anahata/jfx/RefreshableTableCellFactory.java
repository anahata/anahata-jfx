/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.jfx;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@AllArgsConstructor
public class  RefreshableTableCellFactory<S,T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {
    final protected BooleanProperty refresh;

    public RefreshableTableCellFactory() {
        refresh = new SimpleBooleanProperty();
    }
    
    @Override
    public TableCell<S, T> call(
            TableColumn<S, T> param) {
        return new RefreshableTableCell(refresh);
    }
    public void refresh() {
        refresh.setValue(!refresh.get());
    }
}

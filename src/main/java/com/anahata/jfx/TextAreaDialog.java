/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx;

import javafx.application.Platform;
import javafx.beans.NamedArg;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

/**
 * A dialog that shows a text area control to the user.
 *
 * @see Dialog
 */
public class TextAreaDialog extends Dialog<String> {

    /** ************************************************************************
     *
     * Fields
     *
     ************************************************************************* */
    private final GridPane grid;

    private final Label label;

    private final TextArea textArea;

    private final String defaultValue;

    /** ************************************************************************
     *
     * Constructors
     *
     ************************************************************************* */
    /**
     * Creates a new TextAreaDialog without a default value entered into the
     * dialog {@link TextArea}.
     */
    public TextAreaDialog() {
        this("");
    }

    /**
     * Creates a new TextAreaDialog with the default value entered into the
     * dialog {@link TextArea}.
     *
     * @param defaultValue
     */
    public TextAreaDialog(@NamedArg("defaultValue") String defaultValue) {
        final DialogPane dialogPane = getDialogPane();

        // -- textfield
        this.textArea = new TextArea(defaultValue);
        this.textArea.setWrapText(true);
        this.textArea.setMaxWidth(Double.MAX_VALUE);
        GridPane.setHgrow(textArea, Priority.ALWAYS);
        GridPane.setFillWidth(textArea, true);

        // -- label
        label = createContentLabel(dialogPane.getContentText());
        label.setPrefWidth(Region.USE_COMPUTED_SIZE);
        label.textProperty().bind(dialogPane.contentTextProperty());

        this.defaultValue = defaultValue;

        this.grid = new GridPane();
        this.grid.setHgap(10);
        this.grid.setMaxWidth(Double.MAX_VALUE);
        this.grid.setAlignment(Pos.CENTER_LEFT);

        dialogPane.contentTextProperty().addListener(o -> updateGrid());

        setTitle("Title");
        dialogPane.setHeaderText("Header");
        dialogPane.getStyleClass().add("text-input-dialog");
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        updateGrid();

        setResultConverter((dialogButton) -> {
            ButtonData data = dialogButton == null ? null : dialogButton.getButtonData();
            return data == ButtonData.OK_DONE ? textArea.getText() : null;
        });
    }

    static Label createContentLabel(String text) {
        Label label = new Label(text);
        label.setMaxWidth(Double.MAX_VALUE);
        label.setMaxHeight(Double.MAX_VALUE);
        label.getStyleClass().add("content");
        label.setWrapText(true);
        label.setPrefWidth(360);
        return label;
    }

    /** ************************************************************************
     *
     * Public API
     *
     ************************************************************************* */
    /**
     * Returns the {@link TextArea} used within this dialog.
     */
    public final TextArea getEditor() {
        return textArea;
    }

    /**
     * Returns the default value that was specified in the constructor.
     */
    public final String getDefaultValue() {
        return defaultValue;
    }

    /** ************************************************************************
     *
     * Private Implementation
     *
     ************************************************************************* */
    private void updateGrid() {
        grid.getChildren().clear();
        grid.add(label, 0, 0);
        grid.add(textArea, 1, 0);
        getDialogPane().setContent(grid);
        Platform.runLater(() -> textArea.requestFocus());
    }
}

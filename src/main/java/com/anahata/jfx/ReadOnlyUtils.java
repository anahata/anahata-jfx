/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx;

import com.anahata.jfx.bind.table.BindingTableViewUtils;
import com.anahata.jfx.scene.control.CalendarTextField;
import com.anahata.util.lang.builder.HashMapBuilder;
import java.util.List;
import java.util.Map;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Provide support for read only styles.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ReadOnlyUtils {
    private static final Map<Class, String> STYLECLASSES = new HashMapBuilder<Class, String>()
            .add(TextField.class, "read-only-text-input")
            .add(Label.class, "read-only-label")
            .add(ComboBox.class, "read-only-combo-box")
            .add(CheckBox.class, "read-only-check-box")
            .add(TableView.class, "read-only-table-view")
            .add(CalendarTextField.class, "read-only-calendar")
            .build();
    
    /**
     * Get the read only styleclass to use for a node of a given class.
     * 
     * @param clazz The node class. Required.
     * @return The styleclass, or null if not found.
     * @throws NullPointerException If clazz is null.
     */
    public static String getStyleClass(@NonNull Class clazz) {
        return STYLECLASSES.get(clazz);
    }
    
    /**
     * Get the read only styleclass to use for a node.
     * 
     * @param node The node. Required.
     * @return The styleclass, or null if not found.
     * @throws NullPointerException If node is null.
     */
    public static String getStyleClass(@NonNull Object node) {
        return getStyleClass(node.getClass());
    }
    
    public static void bindReadOnlyStyleClass(@NonNull ObservableValue<? extends Boolean> readOnly,
            @NonNull Object... nodes) {
        for (Object node : nodes) {
            if (node instanceof Node) {
                JfxUtils.bindStyleClass((Node)node, readOnly, getStyleClass(node.getClass()), null);
            }
        }
    }
    
    /**
     * Bind a set of nodes to be read only to a given list of nodes.
     * 
     * @param readOnly The observable value containing the read only status.
     * @param nodes    The nodes.
     */
    public static void bindReadOnly(@NonNull ObservableValue<? extends Boolean> readOnly, @NonNull Object... nodes) {
        for (Object nodeObj : nodes) {
            if (nodeObj instanceof TableView) {
                TableView tv = (TableView)nodeObj;
                JfxUtils.bindStyleClass(tv, readOnly, getStyleClass(tv), null);
                BindingTableViewUtils.bindReadOnly(readOnly, tv);
            } else if (nodeObj instanceof Node) {
                Node node = (Node)nodeObj;
                JfxUtils.bindStyleClass(node, readOnly, getStyleClass(node), null);
                node.disableProperty().bind(readOnly);
            } else if (nodeObj instanceof Tab) {
                Tab tab = (Tab)nodeObj;
                JfxUtils.bindStyleClass(tab, readOnly, getStyleClass(tab), null);
                tab.disableProperty().bind(readOnly);
            } else {
                throw new IllegalArgumentException("Unknown node type: " + nodeObj.getClass().getName());
            }
        }
    }
    
    public static void bindReadOnlyList(@NonNull ObservableValue<? extends Boolean> readOnly,
            @NonNull List<? extends Object> nodes) {
        bindReadOnly(readOnly, nodes.toArray());
    }
}

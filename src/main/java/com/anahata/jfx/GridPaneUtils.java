package com.anahata.jfx;

import javafx.application.Platform;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * Helpers for manage GridPane JavaFX nodes.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GridPaneUtils {
    /**
     * Make cells visible or invisible for a GridPane given the field node. Handles shrinking a row to hide the row as
     * well.
     *
     * @param visible the visibility status
     * @param nodes   the right node (field) of the form in a grid pane
     */
    public static void manageGridFormCells(final BooleanExpression visible, final Node... nodes) {
        Validate.notNull(visible, "visible is required");
        Validate.notEmpty(nodes, "at least one node must be specfied");
        for (Node node : nodes) {
            Node leftNode = getLeftNode(node);
            manageGridCells((GridPane)node.getParent(), visible, leftNode, node);
        }
    }

    /**
     * Make cells visible or invisible for a GridPane. Handles shrinking a row to hide the row as well.
     *
     * @param gridPane The GridPane. Required.
     * @param nodes    The nodes in a cell to make visible or invisible. Requires at least one.
     * @param visible  The ObservableValue<Boolean> containing the visibility status.
     * @throws NullPointerException If any arg is null.
     */
    public static void manageGridCells(final GridPane gridPane, final BooleanExpression visible,
            final Node... nodes) {
        Validate.notNull(gridPane, "gridPane is required");
        Validate.notNull(visible, "visible is required");
        Validate.notNull(nodes, "nodes are required");
        Validate.noNullElements(nodes, "Node contains a null element");

        for (final Node node : nodes) {
            final Integer columnIndex = GridPane.getColumnIndex(node);
            final Integer rowIndex = GridPane.getRowIndex(node);
            if (rowIndex >= gridPane.getRowConstraints().size()) {
                throw new IllegalArgumentException(
                        "Node " + node + " id=" + node.getId() + " doesn't have an associated row constraint at index " + rowIndex + " on the grid pane.");
            }
            RowConstraints rc = gridPane.getRowConstraints().get(rowIndex);
            final double minHeight = rc.getMinHeight(); // note these here are final and they don't change value in listener never
            final double maxHeight = rc.getMaxHeight(); // same as above
            final double prefHeight = rc.getPrefHeight(); // same as above            
            node.visibleProperty().bind(visible);
            node.managedProperty().bind(node.visibleProperty());            
            node.visibleProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                    manageGridCell(gridPane, node, columnIndex, rowIndex, minHeight, maxHeight, prefHeight,
                            newValue);
                }
            });

            // Seem to have to run it later.
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    manageGridCell(gridPane, node, columnIndex, rowIndex, minHeight, maxHeight, prefHeight,
                            visible.getValue());
                }
            });
        }
    }
    
    private static void manageGridCell(GridPane gridPane, Node node, Integer columnIndex, Integer rowIndex,
            double minHeight,
            double maxHeight, double prefHeight, boolean value) {
        RowConstraints rc = gridPane.getRowConstraints().get(rowIndex);

        if (value) {
            GridPane.setConstraints(node, columnIndex, rowIndex);
            rc.setMinHeight(minHeight);
            rc.setMaxHeight(maxHeight);
            rc.setPrefHeight(prefHeight);          
        } else {
            GridPane.setColumnIndex(node, null);
            GridPane.setRowIndex(node, null);
            rc.setMinHeight(0);
            rc.setMaxHeight(0);
            rc.setPrefHeight(0);
        }
    }

    /**
     * Gets the node for a given row / column.
     *
     * @param gridPane the GridPane
     * @param row      the row
     * @param column   the column
     * @return the node or null
     */
    public static Node getNode(final GridPane gridPane, int row, int column) {
        for (Node node : gridPane.getChildren()) {
            int r = GridPane.getRowIndex(node);
            int c = GridPane.getColumnIndex(node);
            if (r == row && c == column) {
                return node;
            }
        }
        return null;
    }

    /**
     * Returns the first node cell to the left of the specified node (col = col -1, row = row)
     *
     * @param node the node on the right
     * @return the node on the left
     */
    public static Node getLeftNode(Node node) {
        GridPane gridPane = (GridPane)node.getParent();
        int row = GridPane.getRowIndex(node);
        int col = GridPane.getColumnIndex(node);
        return getNode(gridPane, row, col - 1);
    }

    /**
     * Binds the visible property of a list of nodes and the nodes to the left of those nodes.
     *
     * @param visible the visible binding
     * @param nodes   the nodes whose visible status we are intending to bind.
     */
    public static void bindFieldAndLabelVisible(ObservableValue<Boolean> visible, Node... nodes) {
        for (Node node : nodes) {
            node.visibleProperty().bind(visible);
            getLeftNode(node).visibleProperty().bind(visible);
        }
    }

    /**
     * Dynamically changes a gridpanes row height based on the row content height. (what one would think would happen
     * automatically)
     *
     * @param region - the region that will determine the row height
     */
    public static void bindRowHeightToComponentHeight(final Region region) {
        final GridPane gridPane = (GridPane)region.getParent();
        final RowConstraints rc = gridPane.getRowConstraints().get(GridPane.getRowIndex(region));
        region.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(
                    ObservableValue<? extends Number> ov, Number t, Number t1) {
                rc.setMinHeight(t1.doubleValue());
                rc.setMaxHeight(t1.doubleValue());
                gridPane.requestLayout();
                gridPane.layout();
                region.layout();
                region.requestLayout();
            }
        });
    }
}

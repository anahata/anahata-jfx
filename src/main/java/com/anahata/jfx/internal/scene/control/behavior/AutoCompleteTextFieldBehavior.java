/*
 * Copyright 2012 myTDev & Narayan G. Maharjan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.jfx.internal.scene.control.behavior;

import com.anahata.jfx.concurrent.ApplicationTask;
import com.anahata.jfx.internal.scene.control.skin.AutoCompleteTextFieldSkin;
import com.anahata.jfx.scene.control.AutoCompleteTextField;
import com.anahata.jfx.scene.control.AutoCompleteTextField.DataFormatter;
import com.anahata.jfx.scene.control.AutoCompleteTextField.DataProvider;
import com.anahata.jfx.scene.control.AutoCompleteTextField.Mode;
import com.anahata.util.lang.Nvl;
import com.anahata.yam.model.search.ExpandSearch;
import com.anahata.yam.model.search.FullTextCriteria;
import com.anahata.yam.service.search.PojoSearch;
import com.sun.javafx.scene.control.behavior.BehaviorBase;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import static com.anahata.util.lang.Nvl.*;

/**
 * AutoCompleteTextField control behavior.
 *
 * @param <T> the type of the objects in the drop down
 * @author Narayan G. Maharjan
 * @author Yann D'Isanto
 */
@Slf4j
public final class AutoCompleteTextFieldBehavior<T> extends BehaviorBase<AutoCompleteTextField<T>> {
    //== attributes ===================================================================================================

    /**
     * Displayed auto-complete items.
     */
    private final ObservableList<T> autoCompleteList = FXCollections.observableArrayList();

    /**
     * Comparator used to sort the displayed items.
     */
    private Comparator<T> listComparator;

    /**
     * Updating field flag to avoid loop on text change events.
     */
    private boolean updatingField = false;

    /**
     * Control skin.
     */
    private AutoCompleteTextFieldSkin<T> skin;

    /**
     * Current text input value.
     */
    private StringProperty text = new SimpleStringProperty("");

    /**
     * A background task to use when fetching data from the data provider.
     */
    private DataProviderTask dataProviderTask = null;

    private Timeline dataProviderTaskLauncher = new Timeline();

    //== listeners ====================================================================================================
    /**
     * Control data list change listener.
     */
    private ListChangeListener<T> dataChangeListener = new ListChangeListener<T>() {
        @Override
        public void onChanged(Change<? extends T> c) {
            log.debug("data changed {}", c);

            while (c.next()) {
                autoCompleteList.removeAll(c.getRemoved());
                extractMatchingItems(c.getAddedSubList(), Nvl.nvl(skin.getTextField().getText()).trim().toLowerCase());
            }
        }
    };

    /**
     * Listens for changes on the value property of the control.
     */
    private InvalidationListener valueInvalidationListener = new InvalidationListener() {

        @Override
        public void invalidated(Observable observable) {
            Object newValue = getControl().getValue();
            log.debug("valueInvalidationListener {} updating={}", newValue, updatingField);

            if (!updatingField) {
                updatingField = true;

                if (newValue == null) {
                    skin.getTextField().setText(null);
                } else if (newValue instanceof String) {
                    if (getControl().getMode() == Mode.TYPED_ONLY) {
                        throw new IllegalStateException(
                                "String '" + newValue + "' can not be used as value when working in checked mode");
                    }
                    skin.getTextField().setText((String)newValue);
                } else {
                    if (getControl().getMode() == Mode.STRING_ONLY) {
                        throw new IllegalStateException("Only string values are supported");
                    }

                    @SuppressWarnings("unchecked")
                    String text = getControl().getDataFormatter().toTextFieldString((T)newValue);
                    skin.getTextField().setText(text);
                }

                updatingField = false;
            }
        }
    };

    private ChangeListener valueChangeListener = new ChangeListener() {
        @Override
        public void changed(ObservableValue observable, Object oldValue, Object newValue) {
            log.debug("valueChangeListener {} updating={}", newValue, updatingField);

            if (!updatingField) {
                updatingField = true;

                if (newValue == null) {
                    skin.getTextField().setText(null);
                } else if (newValue instanceof String) {
                    if (getControl().getMode() == Mode.TYPED_ONLY) {
                        throw new IllegalStateException(
                                "String '" + newValue + "' can not be used as value when working in checked mode");
                    }
                    skin.getTextField().setText((String)newValue);
                } else {
                    if (getControl().getMode() == Mode.STRING_ONLY) {
                        throw new IllegalStateException("Only string values are supported");
                    }

                    @SuppressWarnings("unchecked")
                    String text = getControl().getDataFormatter().toTextFieldString((T)newValue);
                    skin.getTextField().setText(text);
                }

                updatingField = false;
            }
        }
    };

    /**
     * Control text change listener.
     */
    private ChangeListener<String> textChangeListener = new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
            log.trace("textChangeListener updating to {}", newValue);

            if (updatingField) {
                log.trace("textChangeListener updating field so return");
                return;
            }

            text.set(skin.getTextField().getText());

            if (getControl().getDataProvider() != null) {
                dataProviderTaskLauncher.stop();
                dataProviderTaskLauncher.play();
            }

            //If there was a selected item, remove it as the user is now typing
            oldValue = oldValue != null ? oldValue : "";
            String refText = newValue != null ? newValue.trim().toLowerCase() : "";

            if (getControl().getDataProvider() != null && newValue != null && oldValue.length() > 0 && newValue.startsWith(
                    oldValue) && getControl().isFilterItems()) {
                // New value is more accurate than the previous one => narrow down the current auto-complete list.
                List<T> toRemove = new ArrayList<>();
                DataFormatter<T> dataToString = getControl().getDataFormatter();

                for (T item : autoCompleteList) {
                    String str = dataToString.toListViewString(item).toLowerCase();
                    if (!StringUtils.startsWith(str, refText)) {
                        toRemove.add(item);
                    }
                }

                autoCompleteList.removeAll(toRemove);
            } else {
                autoCompleteList.clear();

                if (refText.length() > 0) {
                    extractMatchingItems(getControl().getItems(), refText);
                }
            }

            if (autoCompleteList.size() > 0) {
                skin.showPopup();
            } else {
                skin.hidePopup();
            }

            updatingField = true;

            if (getControl().getMode() != Mode.TYPED_ONLY) {
                getControl().setValue(newValue);
            } else {
                getControl().setValue(null);
            }

            updatingField = false;
        }
    };

    //== events =======================================================================================================
    /**
     * Mouse events handler.
     */
    private final EventHandler<? super MouseEvent> mouseEventHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            if (event.getEventType() == MouseEvent.MOUSE_RELEASED && event.getSource() == skin.getListView()) {
                selectAutoCompleteItem();
            }
        }
    };

    /**
     * Key events handler.
     */
    private final EventHandler<? super KeyEvent> keyEventHandler = new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent event) {
            if (event.getEventType() == KeyEvent.KEY_PRESSED && event.getSource() == skin.getTextField()) {
                KeyEvent t = event;

                if (t.getCode() == KeyCode.DOWN) {
                    if (!skin.isPopupShowing()) {
                        String refText = nvl(skin.getTextField().getText()).trim().toLowerCase();

                        if (refText.isEmpty() && autoCompleteList.isEmpty()) {
                            List<T> toAdd = getControl().getItems();
                            int limit = getControl().getLimit();

                            if (limit > 0) {
                                toAdd = toAdd.subList(0, limit);
                            }

                            autoCompleteList.addAll(toAdd);
                            FXCollections.sort(autoCompleteList, listComparator);
                        }
                        skin.showPopup();
                    } else {
                        skin.getListView().requestFocus();
                        skin.getListView().getSelectionModel().select(0);
                    }
                }
            } else if (event.getEventType() == KeyEvent.KEY_RELEASED && event.getSource() == skin.getListView()) {
                KeyEvent t = event;

                switch (t.getCode()) {
                    case ENTER:
                        selectAutoCompleteItem();
                        break;

                    case UP:
                        if (skin.getListView().getSelectionModel().getSelectedIndex() == 0) {
                            skin.getTextField().requestFocus();
                        }
                        break;

                    case ESCAPE:
                        skin.getTextField().replaceText(skin.getTextField().getSelection(), "");
                        skin.getTextField().requestFocus();
                        skin.hidePopup();
                        break;
                        
                    case TAB:
                        skin.hidePopup();
                        break;
                }
            }
        }
    };

    //== initialization ===============================================================================================
    /**
     * Constructor.
     *
     * @param control
     */
    public AutoCompleteTextFieldBehavior(AutoCompleteTextField<T> control) {
        super(control, Collections.EMPTY_LIST);
    }

    /**
     * Initializes using the specified skin.
     *
     * @param skin the control skin.
     */
    @SuppressWarnings("unchecked")
    public void initialize(final AutoCompleteTextFieldSkin<T> skin) {
        this.skin = skin;
        final AutoCompleteTextField control = getControl();
        // Gross, but how to bind otherwise while exposing a read only property?
        ((StringProperty)control.textProperty()).bind(text);
        text.addListener(new ChangeListener<String>() {
            @Override
            public void changed(
                    ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!StringUtils.isBlank(newValue)) {
                    ((StringProperty)control.lastTextProperty()).set(newValue);
                }
            }
        });

        listComparator = new DataToStringComparator<>(control.getDataFormatter(), true);
        control.getItems().addListener(dataChangeListener);

        control.itemsProperty().addListener(new ChangeListener<ObservableList<T>>() {
            @Override
            public void changed(ObservableValue<? extends ObservableList<T>> observable, ObservableList<T> oldValue,
                    ObservableList<T> newValue) {
                if (oldValue != null) {
                    oldValue.removeListener(dataChangeListener);
                }
                if (newValue != null) {
                    newValue.addListener(dataChangeListener);
                }
            }
        });

        control.modeProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if (newValue == Mode.STRING_ONLY) {
                    autoCompleteList.clear();
                }
            }
        });

        control.dataFormatterProperty().addListener(new ChangeListener<DataFormatter<T>>() {
            @Override
            public void changed(ObservableValue<? extends DataFormatter<T>> observable, DataFormatter<T> oldValue,
                    DataFormatter<T> newValue) {
                listComparator = new DataToStringComparator<>(newValue, true);
                FXCollections.sort(autoCompleteList, listComparator);
            }
        });

        skin.getListView().setOnMouseReleased(mouseEventHandler);
        skin.getListView().setOnKeyReleased(keyEventHandler);
        skin.getListView().setItems(autoCompleteList);
        skin.getListView().getItems().addListener(new ListChangeListener<T>() {

            @Override
            public void onChanged(
                    Change<? extends T> c) {
                if (!c.getList().isEmpty() && getControl().isShowListOnData()) {
                    skin.showPopup();
                }
            }
        });
        skin.getTextField().setOnKeyPressed(keyEventHandler);
        skin.getTextField().textProperty().addListener(textChangeListener);

        skin.getTextField().focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(
                    ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue && control.getMode() == Mode.TYPED_ONLY && control.getValue() == null) {
                    skin.getTextField().setText("");
                }
            }
        });

        getControl().valueProperty().addListener(valueInvalidationListener);

        if (getControl().getValue() != null) {
            //As this object gets lazily instantiated after the control, the value property could have already been set
            //but the new value has not been processed, hence we explicitely call the value change listener.
//            valueChangeListener.changed(null, null, getControl().getValue());
            valueInvalidationListener.invalidated(getControl().valueProperty());
        }

        dataProviderTaskLauncher.getKeyFrames().add(new KeyFrame(new Duration(500), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (dataProviderTask != null) {
                    dataProviderTask.cancel();
                }
                dataProviderTask = new DataProviderTask(skin.getTextField().getText(), getControl().getDataProvider());
                dataProviderTask.launch();
                getControl().dataProcessingProperty().bind(dataProviderTask.runningProperty());
            }
        }));
        autoCompleteList.addListener(new ListChangeListener<T>() {

            @Override
            public void onChanged(Change<? extends T> c) {
                if (autoCompleteList.size() == 1 && control.isSingleMatchSelection() && dataProviderTask != null && dataProviderTask.isDone()) {
                    selectSingleMatchingItem();
                }
            }
        });
    }

    //== public methods ===============================================================================================
    /**
     * Adds focus behavior on the specified cell.
     *
     * //TODO Pablo: Review this, not currently used
     *
     * @param cell auto-complete list view cell.
     */
    public void registerCellFocusInvalidationListener(final ListCell<T> cell) {
        InvalidationListener listener = new InvalidationListener() {
            @Override
            public void invalidated(Observable ove) {
                if (cell.getItem() != null && cell.isFocused()) {
                    //here we are using 'temporaryTxt' as temporary saving text
                    //If temporaryTxt length is 0 then assign with current rawText()

                    //first check ...(either texmporaryTxt is empty char or not)
                    if (text.get().length() <= 0) {
                        //second check...
                        if (autoCompleteList.size() != getControl().getItems().size()) {
                            text.set(skin.getTextField().getText());
                        }
                    }

                    String prev = text.get();
                    updatingField = true;
                    String string = getControl().getDataFormatter().toTextFieldString(cell.getItem());
                    skin.getTextField().textProperty().setValue(string);
                    updatingField = false;
                    skin.getTextField().selectRange(prev.length(), string.length());
                    log.debug("{}={}::{}", text.get().length(), skin.getTextField().getText().length(), string.length());
                }
            }
        };

        cell.focusedProperty().addListener(listener);
    }

    //== private methods ==============================================================================================
    /**
     * Extracts the items matching the specifed text from the specified items list. Matching items are then added to the
     * auto-complete list.
     *
     * @param items the list to extract items from.
     * @param text  the text items string representation must start with.
     */
    private void extractMatchingItems(List<? extends T> items, String text) {
        if (getControl().isFilterItems()) {
            int itemsCount = autoCompleteList.size();
            int limit = getControl().getLimit();
            if (text.isEmpty()) {
                List<T> toAdd = getControl().getItems();
                if (limit > 0) {
                    toAdd = toAdd.subList(0, limit - itemsCount);
                }
                autoCompleteList.addAll(toAdd);
            } else {

                DataFormatter<T> dataToString = getControl().getDataFormatter();
                FullTextCriteria ftc = new FullTextCriteria(text, false, false);
                List<PojoSearchWrapper<T>> itemsCopy = items.stream().map((e) -> new PojoSearchWrapper<T>(e,
                        dataToString.toFilterString(e))).collect(Collectors.toList());

                PojoSearch.processList(itemsCopy, ftc, null, true, null);

                if (getControl().getLimit() > 0 && itemsCopy.size() > getControl().getLimit()) {
                    itemsCopy = itemsCopy.subList(0, getControl().getLimit());
                }
                List<T> resulting = itemsCopy.stream().map((e) -> e.item).collect(Collectors.toList());

                autoCompleteList.setAll(resulting);

            }

            //FXCollections.sort(autoCompleteList, listComparator);
        } else {
            autoCompleteList.setAll(items);
        }
    }

    @AllArgsConstructor
    public static class PojoSearchWrapper<T> {
        @ExpandSearch(false)
        private T item;

        @Getter
        private String value;
    }

    /**
     * Populates the textfield according to the current selected item.
     */
    private void selectAutoCompleteItem() {
        dataProviderTaskLauncher.stop();
        T item = skin.getListView().getSelectionModel().getSelectedItem();
        skin.hidePopup();

        if (item == null) {
            return;
        }

        //updatingField = true;
        if (getControl().getMode() != Mode.STRING_ONLY) {
            getControl().setValue(item);
        } else {
            getControl().setValue(getControl().getDataFormatter().toTextFieldString(item));
        }

        skin.getTextField().requestFocus();
        skin.getTextField().requestLayout();
        skin.getTextField().end();
        skin.hidePopup();

        updatingField = false;

        final EventHandler<ActionEvent> onAction = getControl().getOnAction();

        if (onAction != null) {
            onAction.handle(new ActionEvent(item, getControl()));
        }
    }

    /**
     * Populates the textfield with matching result value if only one result found based on textinput.
     */
    private void selectSingleMatchingItem() {
        dataProviderTaskLauncher.stop();
        T item = autoCompleteList.get(0);

        if (item == null || getControl().getValue() == item) {
            return;
        }
        log.debug("getControl().getValue() == item {} ", (getControl().getValue() == item));

        //updatingField = true;
        if (getControl().getMode() != Mode.STRING_ONLY) {
            getControl().setValue(item);
        } else {
            getControl().setValue(getControl().getDataFormatter().toTextFieldString(item));
        }

        updatingField = false;

        final EventHandler<ActionEvent> onAction = getControl().getOnAction();

        if (onAction != null) {
            onAction.handle(new ActionEvent(item, getControl()));
        }

        autoCompleteList.clear();
        getControl().getItems().clear();

    }

    //== classes ======================================================================================================
    /**
     * A comparator based on object string representation. The string representation is retreived using a DataFormatter
     * instance.
     *
     * @param <T> the data type.
     */
    private final class DataToStringComparator<T> implements Comparator<T> {

        private final DataFormatter<T> dataToString;

        private final boolean ignoreCase;

        /**
         * Constructor.
         *
         * @param dataToString a DataFormatter instance.
         */
        public DataToStringComparator(DataFormatter<T> dataToString) {
            this(dataToString, true);
        }

        /**
         * Constructor.
         *
         * @param dataToString a DataFormatter instance.
         * @param ignoreCase   ignore case flag.
         */
        public DataToStringComparator(DataFormatter<T> dataToString, boolean ignoreCase) {
            this.dataToString = dataToString;
            this.ignoreCase = ignoreCase;
        }

        @Override
        public int compare(T o1, T o2) {
            return ignoreCase
                    ? dataToString.toListViewString(o1).compareToIgnoreCase(dataToString.toListViewString(o2))
                    : dataToString.toListViewString(o1).compareTo(dataToString.toListViewString(o2));
        }
    }

    /**
     * Creates a background task to look up the data
     */
    private final class DataProviderTask extends ApplicationTask<List<T>> {

        private String text;

        private DataProvider<T> dataProvider;

        public DataProviderTask(String text, DataProvider<T> dataProvider) {
            this.text = text;
            this.dataProvider = dataProvider;
        }

        @Override
        protected List<T> call() throws Exception {
            //Thread.sleep(300);
            if (dataProviderTask != this) {
                //already expired
                super.cancel();
                return null;
            }
            updateTitle(dataProvider.getTaskMessage(text));
            try {
                List<T> ret = dataProvider.getData(text);
                log.debug("Got data {}", ret);
                return ret;
            } catch (Exception e) {
                log.warn("Exception looking up data in data provider", e);
            }

            return null;
        }

        @Override
        protected void succeeded() {
            List<T> ret = super.getValue();
            //if there was another task fired after this one, check that the last fired task is still running
            //to not override any updates done by the last fired task
            if (this == dataProviderTask || dataProviderTask.isRunning()) {
                if (ret != null) {
                    getControl().getItems().setAll(ret);
                } else {
                    getControl().getItems().clear();
                }
                if (!autoCompleteList.isEmpty() && getControl().textFieldFocusedProperty().get()) {
                    skin.showPopup();
                } else {
                    skin.hidePopup();
                }
            }
        }
    }
}

/*
 * Copyright 2012 myTDev & Narayan G. Maharjan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.jfx.internal.scene.control.skin;

import com.anahata.jfx.internal.scene.control.behavior.AutoCompleteTextFieldBehavior;
import com.anahata.jfx.scene.control.AutoCompleteTextField;
import com.anahata.jfx.scene.control.AutoCompleteTextField.DataFormatter;
import com.anahata.jfx.scene.control.AutoCompleteTextField.GraphicDataFormatter;
import com.sun.javafx.scene.control.skin.BehaviorSkinBase;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.IntegerBinding;
import javafx.beans.binding.NumberBinding;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Popup;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.Duration;
import lombok.extern.slf4j.Slf4j;

/**
 * AutoCompleteTextField control skin.
 *
 * @param <T> Inner type.
 * @author Narayan G. Maharjan
 * @author Yann D'Isanto
 */
@Slf4j
public class AutoCompleteTextFieldSkin<T> extends BehaviorSkinBase<AutoCompleteTextField<T>, AutoCompleteTextFieldBehavior<T>> {
    //== attributes ===================================================================================================
    /**
     * ListView for showing the matched words.
     */
    private final ListView<T> listView = new ListView<>();

    private final AnchorPane image = new AnchorPane();

    /**
     * Entry TextField.
     */
    private final TextField textField = new TextField();

    /**
     * Popup to display matched word ListView in.
     */
    private Popup popup = new Popup();

    private final Timeline showImageTimeline = new Timeline();

    private T itemToShowInImage;

    //== initialization ===============================================================================================
    /**
     * Constructor.
     *
     * @param control the skinned control.
     */
    public AutoCompleteTextFieldSkin(final AutoCompleteTextField<T> control) {
        super(control, new AutoCompleteTextFieldBehavior<>(control));
        initialize();
    }

    /**
     * Initializes this skin.
     */
    @SuppressWarnings("unchecked")
    private void initialize() {
        final AutoCompleteTextField control = getSkinnable();

        control.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    if (newValue) {
                        Platform.runLater(() -> {
                            textField.requestFocus();
                        });
                    }
                });

        control.setAlignment(textField.getAlignment());
        control.textFieldFocusedProperty().bind((textField.focusedProperty()));
        control.editorTextProperty().bindBidirectional(textField.textProperty());
        textField.promptTextProperty().bind(control.promptTextProperty());
        textField.alignmentProperty().bind(control.alignmentProperty());
        //textField.onActionProperty().bind(control.onActionProperty());
        textField.prefColumnCountProperty().bind(control.prefColumnCountProperty());
        textField.focusTraversableProperty().bind(control.focusTraversableProperty());
        
        getBehavior().initialize(this);

        //This cell factory helps to know which cell has been selected so that
        //when ever any cell is selected the textbox rawText must be changed
        listView.setCellFactory(new Callback<ListView<T>, ListCell<T>>() {
            @Override
            public ListCell<T> call(ListView<T> p) {
                //A simple ListCell containing only Label

                final ListCell<T> cell = new ListCell<T>() {
                    {
                        super.setOnMouseEntered((Event event) -> {
                            showImageTimeline.stop();
                            showImageTimeline.play();
                            T item1 = getItem();
                            itemToShowInImage = item1;
                        });

                        super.setOnMouseExited((Event event) -> {
                            showImageTimeline.stop();
                            showImage(getListView().getSelectionModel().getSelectedItem());
                        });
                    }

                    @Override
                    public void updateItem(T item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            DataFormatter df = control.getDataFormatter();
                            setText(control.getDataFormatter().toListViewString(item));
                            if (df instanceof GraphicDataFormatter) {
                                GraphicDataFormatter gdf = (GraphicDataFormatter)df;
                                setGraphic(gdf.getListViewGraphic(item));
                                setPrefHeight(gdf.getCellHeight());
                            }
                        } else {
                            setGraphic(null);
                            setText(null);
                        }
                    }
                };
                //getBehavior().registerCellFocusInvalidationListener(cell);
                return cell;
            }
        });
        textField.focusedProperty().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    textField.end();
                });

        //popup
        popup = new Popup();
        popup.setAutoHide(true);
        HBox popupContent = new HBox();
        popupContent.setFillHeight(false);
        popupContent.setSpacing(5);
        popupContent.setAlignment(Pos.TOP_LEFT);
        popupContent.getChildren().add(listView);
        popupContent.getChildren().add(image);
        popup.getContent().add(popupContent);
        popupContent.prefHeightProperty().bind(listView.prefHeightProperty().add(15));

        //Adding textbox in this control Children
        getChildren().addAll(textField);

        //listView.prefWidthProperty().bind(Bindings.min(textField.widthProperty(), 250));
        listView.getFocusModel().focusedItemProperty().addListener(
                (ObservableValue<? extends T> observable, T oldValue, T newValue) -> {
                    showImage(newValue);
                });

        showImageTimeline.getKeyFrames().add(new KeyFrame(new Duration(500), (ActionEvent event) -> {
            if (itemToShowInImage != null) {
                showImage(itemToShowInImage);
            }
        }));
    }

    //== public methods ===============================================================================================
    /**
     * A Popup containing Listview is trigged from this function This function automatically resize it's height and
     * width according to the width of textbox and item's cell height
     */
    public void showPopup() {
        int itemsCount = listView.getItems().size();

        if (itemsCount == 0) {
            return;
        }
        
        IntegerBinding itemsCountProperty = Bindings.size(listView.getItems());
        GraphicDataFormatter gdf = getSkinnable().getGraphicDataFormatter();
        final int cellHeight = gdf != null ? gdf.getCellHeight() : 24; // TODO: retreive cell height
        
        NumberBinding itemsToShow = Bindings.min(itemsCountProperty, getSkinnable().popupSize());
        
        listView.prefHeightProperty().bind(itemsToShow.multiply(cellHeight).add(15));
        listView.scrollTo(0);

        // Calculating the x and y popup position so it is displayed just below the textfield.
        Scene scene = getSkinnable().getScene();

        //NPE seen here
        if (scene != null) {
            Window window = scene.getWindow();
            int prefListWidth = getSkinnable().getPrefListWidth();
            if (prefListWidth == 0) {
                listView.setPrefWidth(textField.getWidth());
            } else {
                listView.setPrefWidth(prefListWidth);
            }
            listView.getSelectionModel().clearSelection();
            listView.getFocusModel().focus(-1);
            //Sometimes when changing the focus of the window to which the autocomplete it supposed to belong,
            //the autocomplete remains visible
            //TODO: try something else, this doesn't work
//            window.focusedProperty().addListener(new ChangeListener<Boolean>() {
//                @Override
//                public void changed(
//                        ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                    if (!newValue) {
//                        log.debug("Popup window lost focus, hiding popup");
//                        popup.hide();
//                    }
//                }
//            });

            Point2D fieldPosition = textField.localToScene(0, 0);

//            Screen sc = Screen.getPrimary();
//            Rectangle2D rec = sc.getVisualBounds();
            double anchorY = window.getY() + fieldPosition.getY() + scene.getY() + textField.getHeight();
//            log.debug("Pos Y:"+ anchorY+" ListView Height:"+listView.getPrefHeight() +" Rect H:"+ window.getHeight()+" Cond:"+(anchorY+listView.getPrefHeight() > rec.getHeight()));  

            if (anchorY + listView.getPrefHeight() > window.getHeight()) {
                anchorY = anchorY - listView.getPrefHeight() - (textField.getHeight());

            }
//            log.debug("Pos Y Change:"+anchorY);
            popup.show(
                    window,
                    window.getX() + fieldPosition.getX() + scene.getX(),
                    anchorY);
        }
    }

    /**
     * This function hides the popup containing listview
     */
    public void hidePopup() {
        popup.hide();
    }

    /**
     * Returns true if the auto-complete popup is displayed.
     *
     * @return true if the auto-complete popup is displayed.
     */
    public boolean isPopupShowing() {
        return popup.isShowing();
    }

    /**
     * Returns the TextField used by this skin.
     *
     * @return a TextField instance.
     */
    public TextField getTextField() {
        return textField;
    }

    /**
     * Returns the ListView used by this skin.
     *
     * @return a ListView instance.
     */
    public ListView<T> getListView() {
        return listView;
    }

    //== private methods ==============================================================================================
    private void showImage(T newValue) {
        DataFormatter<T> df = getSkinnable().getDataFormatter();

        if (df instanceof GraphicDataFormatter) {
            GraphicDataFormatter<T> gdf = (GraphicDataFormatter<T>)df;
            Node graphic = gdf.toImageGraphic(newValue);

            if (graphic != null) {
                image.getChildren().setAll(graphic);
            } else {
                image.getChildren().clear();
            }
        }
    }
}

package com.anahata.jfx;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * Extension to JavaFX Bindings class
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class AnahataBindings {

    /**
     * Creates an in expression a-la SQL IN(val1, val2, val3)
     *
     * @param oe     the object being evaluated
     * @param values the possible values
     * @return the boolean expression
     */
    public static BooleanExpression in(ObjectExpression oe, Object... values) {
        Validate.notNull(oe, "oe is required");
        Validate.notNull(values, "values is required");
        Validate.notEmpty(values, "at least one value is required");
        BooleanBinding ret = null;
        for (Object value : values) {
            if (ret == null) {
                ret = oe.isEqualTo(value);
            } else {
                ret = ret.or(oe.isEqualTo(value));
            }
        }

        return ret;
    }

    /**
     * Creates an or expression from a list of boolean expressions.
     *
     * @param booleanExpressions the expressions
     * @return all expressions in disjunction: e1.or(o2).or(o3) ... .or(on)
     */
    public static BooleanExpression or(BooleanExpression... booleanExpressions) {
        Validate.notNull(booleanExpressions, "oe is required");
        BooleanExpression ret = null;
        for (BooleanExpression be : booleanExpressions) {
            if (ret == null) {
                ret = be;
            } else {
                ret = ret.or(be);
            }
        }

        return ret;
    }

    /**
     * Creates <BooleanBinding> that checks if <ObservableObjectValue> is instance of some class
     *
     * @param oov   <ObservableObjectValue> to check instance
     * @param clazz class that we are checking.
     * @return <BooleanBinding> instance
     */
    public static BooleanBinding isInstanceOf(final ObservableObjectValue<?> oov, final Class<?> clazz) {
        Validate.notNull(oov);
        Validate.notNull(clazz);
        return new BooleanBinding() {
            {
                super.bind(oov);
            }

            @Override
            public void dispose() {
                super.unbind(oov);
            }

            @Override
            protected boolean computeValue() {
                log.debug("oov= {}", oov);
                return (oov == null ? false : (clazz.isInstance(oov.get())));
            }

            @Override
            public ObservableList<?> getDependencies() {
                return FXCollections.singletonObservableList(oov);
            }

        };
    }

    /**
     * Creates BooleanBinding that checks if ObservableObjectValue is instance of some class
     *
     * @param oov     ObservableObjectValue to check instance
     * @param clazzes classes that we are checking.
     * @return BooleanBinding instance
     */
    public static BooleanBinding isInstanceOf(final ObservableObjectValue<?> oov, final Class<?>... clazzes) {
        Validate.notNull(oov);
        Validate.noNullElements(clazzes);
        return new BooleanBinding() {
            {
                super.bind(oov);
            }

            @Override
            public void dispose() {
                super.unbind(oov);
            }

            @Override
            protected boolean computeValue() {
                log.debug("oov= {}", oov);
                if (oov == null) {
                    return false;
                }
                Object obj = oov.get();
                for (Class c : clazzes) {
                    if (c.isInstance(obj)) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public ObservableList<?> getDependencies() {
                return FXCollections.singletonObservableList(oov);
            }

        };
    }
}

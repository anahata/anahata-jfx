package com.anahata.jfx;

import com.anahata.jfx.concurrent.BackgroundProcessor;
import com.anahata.util.awt.AWTUtils;
import com.anahata.util.cdi.Cdi;
import com.sun.javafx.robot.impl.FXRobotHelper;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.Window;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * JavaFX helpers.
 *
 * @author Robert Nagajek
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class JfxUtils {
    private static final String PATH_PREFIX = "/fxml";

    /**
     * Creates a CDI enabled FXMLLoader.
     *
     * @param path The FXML file path, must be in the format /a/b/c.fxml. The paths start below /fxml (i.e. that is not
     *             required).
     * @return The loader.
     * @throws NullPointerException     If path is null.
     * @throws IllegalArgumentException If path does not start with a forward slash.
     * @throws RuntimeException         If the loading fails.
     */
    public static FXMLLoader loader(String path) {
        return (loader(path, null));
    }

    /**
     * Creates a CDI enabled FXMLLoader.
     *
     * @param path      The FXML file path, must be in the format /a/b/c.fxml. The paths start below /fxml (i.e. that is
     *                  not required).
     * @param resources The resource bundle.
     * @return The loader.
     * @throws NullPointerException     If path is null.
     * @throws IllegalArgumentException If path does not start with a forward slash.
     * @throws RuntimeException         If the loading fails.
     */
    public static FXMLLoader loader(String path, ResourceBundle resources) {
        Validate.notNull(path, "The path is required");
        Validate.isTrue(path.startsWith("/"), "The path must begin with a slash");
        final String fullPath = PATH_PREFIX + path;
        URL url = JfxUtils.class.getResource(fullPath);
        Validate.notNull(url, "Could not locate fxml with path %s", fullPath);
        log.debug("loading from URL {} ", url);
        FXMLLoader loader = new FXMLLoader(url, resources);
        loader.setControllerFactory(CdiControllerFactory.INSTANCE);
        return loader;
    }

    /**
     * Load an FXML scene through CDI.
     *
     * @param path The FXML file path, must be in the format /a/b/c.fxml. The paths start below /fxml (i.e. that is not
     *             required).
     * @return The loaded scene.
     * @throws NullPointerException     If path is null.
     * @throws IllegalArgumentException If path does not start with a forward slash.
     * @throws RuntimeException         If the loading fails.
     */
    public static FXMLLoader load(String path) {
        try {
            FXMLLoader loader = loader(path);
            loader.load();  
            BackgroundProcessor.process(loader.getController(), loader.getRoot());
            return loader;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Loads a custom JavaFX control
     *
     * @param <T>             the type of the controller.
     * @param path            the path to the xml file.
     *                        required.
     * @param root            the root node of the control. required.
     * @param controllerClass the controller class required.
     * @return the controller instance
     */
    public static <T> T loadControl(String path, Node root, Class<T> controllerClass) {
        Validate.notNull(path);
        Validate.notNull(root);
        Validate.notNull(controllerClass);

        FXMLLoader loader = loader(path);
        loader.setRoot(root);
        T controller = Cdi.get(controllerClass);
        BackgroundProcessor.process(controller, root);
        loader.setController(controller);
        try {
            loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return controller;
    }

    /**
     * Construct an ImageView from an application resource path.
     *
     * @param path The resource path. Required.
     * @return The ImageView.
     * @throws NullPointerException If path is null.
     */
    public static ImageView makeImageView(String path) {
        Validate.notNull(path);
        ImageView iv = new ImageView();
        Image i = new Image(JfxUtils.class.getResource(path).toString());
        iv.setImage(i);
        return iv;
    }

    /**
     * Take a snapshot of a node in PNG format and return a byte array of the data.
     *
     * @return The PNG format image byte array.
     * @throws NullPointerException If node is null.
     */
    public static List<byte[]> snapshotAllStages() {
        List<byte[]> ret = new ArrayList<>();
        for (Stage s : FXRobotHelper.getStages()) {
            ret.add(JfxUtils.snapshot(s.getScene().getRoot()));
        }
        return ret;
    }

    /**
     * Take a snapshot of a node in PNG format and return a byte array of the data.
     *
     * @param node The node. Required.
     * @return The PNG format image byte array.
     * @throws NullPointerException If node is null.
     */
    public static byte[] snapshot(Node node) {
        SnapshotParameters sp = new SnapshotParameters();
        sp.setFill(Color.TRANSPARENT);
        WritableImage snapshot = node.snapshot(sp, null);
        BufferedImage fromFXImage = SwingFXUtils.fromFXImage(snapshot, null);
        byte[] result = AWTUtils.toPNG(fromFXImage);
        log.debug("snapshot: Returning image size={}", result.length);
        return result;
    }

    /**
     * Displays a tooltip next to a control.
     *
     * @param control the control
     * @param tooltip the tooltip
     */
    public static void showTooltipRightOfControl(Control control, Tooltip tooltip) {
        Point2D p = control.localToScene(0.0, 0.0);
        Tooltip.install(control, tooltip);
        tooltip.setAutoFix(true);
        tooltip.show(control,
                p.getX() + control.getScene().getX() + control.getScene().getWindow().getX() + control.getWidth(),
                p.getY() + control.getScene().getY() + control.getScene().getWindow().getY());
    }

    /**
     * Ensures the runnable is executed in the FX thread.
     *
     * @param runnable the code to be executed in FX Thread
     */
    public static void ensureFXThread(Runnable runnable) {
        if (Platform.isFxApplicationThread()) {
            runnable.run();
        } else {
            Platform.runLater(runnable);
        }
    }

    /**
     * Invokes a Runnable in JFX Thread and waits while it's finished. Like SwingUtilities.invokeAndWait does for EDT.
     *
     * @param run The Runnable that has to be called on JFX thread.
     * @throws InterruptedException f the execution is interrupted.
     * @throws ExecutionException   If a exception is occurred in the run method of the Runnable
     */
    public static void runAndWait(final Runnable run)
            throws InterruptedException, ExecutionException {
        if (Platform.isFxApplicationThread()) {
            try {
                run.run();
            } catch (Exception e) {
                throw new ExecutionException(e);
            }
        } else {
            final Lock lock = new ReentrantLock();
            final Condition condition = lock.newCondition();
            final ThrowableWrapper throwableWrapper = new ThrowableWrapper();
            lock.lock();
            try {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        lock.lock();
                        try {
                            run.run();
                        } catch (Throwable e) {
                            throwableWrapper.t = e;
                        } finally {
                            try {
                                condition.signal();
                            } finally {
                                lock.unlock();
                            }
                        }
                    }
                });
                condition.await();
                if (throwableWrapper.t != null) {
                    throw new ExecutionException(throwableWrapper.t);
                }
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Make an icon from an image, resized to a given maximum size.
     *
     * @param image   The image. Required.
     * @param maxSize The maximum size.
     * @return The image view.
     * @throws NullPointerException If image is null.
     */
    public static ImageView makeIcon(Image image, int maxSize) {
        Validate.notNull(image);
        ImageView iv = new ImageView(image);
        iv.setFitHeight(maxSize);
        iv.setFitWidth(maxSize);
        iv.setCache(true);
        iv.setPreserveRatio(true);
        iv.setSmooth(true);
        return iv;
    }

    /**
     * Make an Image from a a byte array returning null if the byte array is null.
     *
     * @param image The image data.
     * @return The image .
     */
    public static Image makeImage(byte[] image) {
        if (image == null) {
            return null;
        }
        Image i = new Image(new ByteArrayInputStream(image));
        return i;
    }

    /**
     * Finds the first parent node of a given type.
     *
     * @param <T>        the type
     * @param n          the node
     * @param parentType the type's class
     * @return the first parent of type T or null if there isn't such parent in the hierarchy,
     */
    @SuppressWarnings("unchecked")
    public static <T> T findParent(Node n, Class<T> parentType) {
        Parent p = n.getParent();

        while (p != null && !parentType.isAssignableFrom(p.getClass())) {
            p = p.getParent();
        }

        return (T)p;
    }

    /**
     * Determine if a descendant node is contained within an ancestor node.
     *
     * @param child    The descendant node. Required.
     * @param ancestor The ancestor node. Required.
     * @return True if within the ancestor, false if not.
     * @throws NullPointerException If any arg is null.
     */
    public static boolean isInAncestor(Node child, Node ancestor) {
        Validate.notNull(child);
        Validate.notNull(ancestor);
        Parent parent = child.getParent();

        while (parent != null) {
            if (ancestor.equals(parent)) {
                return true;
            }

            parent = parent.getParent();
        }

        return false;
    }

    /**
     * Bind a node's display status - which is defined as it's visibility that will make other nodes collapse around it
     * if it's hidden - to the given boolean observable.
     *
     * @param node The node. Required.
     * @param ov   The ObservableValue. Required.
     * @throws NullPointerException If any arg is null.
     * @deprecated use {@link #bindDisplayed(javafx.beans.value.ObservableValue, javafx.scene.Node[]) } instead
     */
    @Deprecated
    public static void bindDisplayed(Node node, ObservableValue<? extends Boolean> ov) {
        bindDisplayed(ov, node);
    }

    /**
     * Directly set a list of nodes display status.
     *
     * @param displayed Set to true to display, false to hide (collapses other nodes).
     * @param nodes     The Nodes.
     */
    public static void setDisplayed(boolean displayed, @NonNull Node... nodes) {
        for (Node node : nodes) {
            node.setVisible(displayed);
            node.managedProperty().bind(node.visibleProperty());
        }
    }

    /**
     * Bind a a list of node's display status - which is defined as it's visibility that will make other nodes collapse
     * around it if it's hidden - to the given boolean observable.
     *
     * @param ov    The ObservableValue. Required.
     * @param nodes The nodes. Required.
     * @throws NullPointerException If any arg is null.
     */
    public static void bindDisplayed(ObservableValue<? extends Boolean> ov, Node... nodes) {
        Validate.notNull(ov);
        for (Node node : nodes) {
            Validate.notNull(node);
            node.visibleProperty().bind(ov);
            node.managedProperty().bind(node.visibleProperty());
        }
    }

    /**
     * Binds the disableProperty() of a list of nodes.
     *
     * @param ov    The ObservableValue. Required.
     * @param nodes The nodes. Required.
     * @throws NullPointerException If any arg is null.
     */
    public static void bindDisabled(ObservableValue<? extends Boolean> ov, Node... nodes) {
        Validate.notNull(ov);
        Validate.notNull(nodes);
        Validate.notEmpty(nodes);

        for (int i=0; i < nodes.length; i++) {
            Node n = nodes[i];
            if (nodes[i] == null) {
                throw new NullPointerException("Null value at position " + i);
            }
            n.disableProperty().bind(ov);
            
        }
    }

    /**
     * Binds the disableProperty() of a list of nodes.
     *
     * @param ov    The ObservableValue. Required.
     * @param nodes The nodes. Required.
     * @throws NullPointerException If any arg is null.
     */
    public static void bindDisabled(ObservableValue<? extends Boolean> ov, List<Node> nodes) {
        Validate.notNull(ov);
        Validate.notNull(nodes);
        Validate.notEmpty(nodes);

        for (Node node : nodes) {
            node.disableProperty().bind(ov);
        }
    }

    /**
     * Bind a style to a node dependant on a boolean property. A true style is used for a true value, a false style is
     * used for a false value.
     *
     * @param node       The node. Required.
     * @param ov         The observable value. Required.
     * @param trueStyle  The true style, set when the boolean value is true. Can be null.
     * @param falseStyle The false style, set when the boolean value is false. Can be null.
     * @throws NullPointerException If node or ov are null.
     */
    public static void bindStyleClass(final Node node, final ObservableValue<? extends Boolean> ov,
            final String trueStyle, final String falseStyle) {
        Validate.notNull(node);
        Validate.notNull(ov);

        ov.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, final Boolean oldValue, final Boolean newValue) {
                log.debug("bindStyleClass: node={} newValue={}", node, newValue);

                if (newValue) {
                    if (falseStyle != null) {
                        node.getStyleClass().remove(falseStyle);
                    }

                    if (trueStyle != null && !node.getStyleClass().contains(trueStyle)) {
                        node.getStyleClass().add(trueStyle);
                    }
                } else {
                    if (trueStyle != null) {
                        node.getStyleClass().remove(trueStyle);
                    }

                    if (falseStyle != null && !node.getStyleClass().contains(falseStyle)) {
                        node.getStyleClass().add(falseStyle);
                    }
                }
            }
        });
    }

    /**
     * Bind a style to a Tab dependant on a boolean property. A true style is used for a true value, a false style is
     * used for a false value.
     *
     * @param tab        The tab. Required.
     * @param ov         The observable value. Required.
     * @param trueStyle  The true style, set when the boolean value is true. Can be null.
     * @param falseStyle The false style, set when the boolean value is false. Can be null.
     * @throws NullPointerException If node or ov are null.
     */
    public static void bindStyleClass(final Tab tab, final ObservableValue<? extends Boolean> ov,
            final String trueStyle, final String falseStyle) {
        Validate.notNull(tab);
        Validate.notNull(ov);

        ov.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, final Boolean oldValue, final Boolean newValue) {
                log.debug("bindStyleClass: node={} newValue={}", tab, newValue);

                if (newValue) {
                    if (falseStyle != null) {
                        tab.getStyleClass().remove(falseStyle);
                    }

                    if (trueStyle != null && !tab.getStyleClass().contains(trueStyle)) {
                        tab.getStyleClass().add(trueStyle);
                    }
                } else {
                    if (trueStyle != null) {
                        tab.getStyleClass().remove(trueStyle);
                    }

                    if (falseStyle != null && !tab.getStyleClass().contains(falseStyle)) {
                        tab.getStyleClass().add(falseStyle);
                    }
                }
            }
        });
    }

    /**
     * Set a stylesheet on a node based on an observable boolean value.
     *
     * @param parent     The parent node. Required.
     * @param ov         The observable value. Required.
     * @param stylesheet The stylesheet. Required.
     * @throws NullPointerException If any arg is null.
     */
    public static void bindStylesheet(final Parent parent, final ObservableValue<? extends Boolean> ov,
            final String stylesheet) {
        Validate.notNull(parent);
        Validate.notNull(ov);
        Validate.notNull(stylesheet);
        final String stylesheetResource = getReference(stylesheet);

        ov.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, final Boolean oldValue, final Boolean newValue) {
                if (newValue) {
                    if (!parent.getStylesheets().contains(stylesheetResource)) {
                        parent.getStylesheets().add(stylesheetResource);
                    }
                } else {
                    parent.getStylesheets().remove(stylesheetResource);
                }
            }
        });
    }

    /**
     * This will set focus on a node, if it exists. For text fields it will move the cursor to the end, so that the
     * entire field is not highlighted.
     *
     * @param node     The node to focus to. Can be null.
     * @param runLater Set to true to run in Platform.runLater().
     */
    public static void focusNicely(final Node node, boolean runLater) {
        if (node == null) {
            return;
        }

        if (runLater) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    focusNicelyImpl(node);
                }
            });
        } else {
            focusNicelyImpl(node);
        }
    }

    /**
     * Set focus on a node in the next refresh cycle.
     *
     * @param node The node. Required.
     * @throws NullPointerException If node is null.
     */
    public static void focus(final Node node) {
        Validate.notNull(node);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                node.requestFocus();
            }
        });
    }

    /**
     * Set focus to a TextArea and position the caret to the end in the next refresh cycle.
     *
     * @param textField The text field. Required.
     * @throws NullPointerException If textField is null.
     */
    public static void focusAndPositionToEnd(final TextField textField) {
        Validate.notNull(textField);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                textField.requestFocus();
                final String text = textField.getText();
                textField.positionCaret(text == null ? 0 : text.length());
            }
        });
    }

    /**
     * Add a styleclass to a list of nodes.
     *
     * @param styleClass The styleclass. Required.
     * @param nodes      The nodes.
     * @throws NullPointerException If styleClass is null.
     */
    public static void addStyleClass(String styleClass, Node... nodes) {
        Validate.notNull(styleClass);

        for (Node node : nodes) {
            node.getStyleClass().add(styleClass);
        }
    }

    /**
     * Add a list of stylesheets to a parent. It will only add them if they aren't already there.
     *
     * @param parent      The parent node to add to. Required.
     * @param stylesheets The stylesheets. Required.
     * @throws NullPointerException If any arg is null.
     */
    public static void addStyleSheets(Parent parent, String... stylesheets) {
        Validate.notNull(parent);
        Validate.notNull(stylesheets);

        for (String stylesheet : stylesheets) {
            URL url = JfxUtils.class.getResource(stylesheet);
            String ret = url.toExternalForm();
            log.debug("resourcePath={} url={} externalForm={}", stylesheets, url, ret);

            if (!parent.getStylesheets().contains(stylesheet)) {
                try {
                    parent.getStylesheets().add(ret);
                } catch (Exception e) {
                    log.warn("Could not add styleSheet: {} at url={}", stylesheets, url);
                }
            }
        }
    }

    /**
     * Get an absolute reference to a resource.
     *
     * @param resource The resource.
     * @return The reference.
     */
    public static String getReference(String resource) {
        URL url = JfxUtils.class.getResource(resource);
        return url.toExternalForm();
    }

    /**
     * Gets the stage of a given node if the node belongs to a stage.
     *
     * @param node the node
     * @return the stage or null if there the node is not part of a stage.
     */
    public static Stage getStage(Node node) {
        Validate.notNull(node, "node is required");
        Window window = node.getScene().getWindow();
        Stage stage = window instanceof Stage ? (Stage)window : null;
        return stage;
    }

    /**
     * Recursively find all nodes under a parent.
     *
     * @param parent The parent. Required.
     * @return The list of nodes. Never null.
     * @throws NullPointerException If any arg is null.
     */
    public static List<Node> getAllNodes(Parent parent) {
        Validate.notNull(parent);
        List<Node> nodes = new ArrayList<>();

        for (Node node : parent.getChildrenUnmodifiable()) {
            nodes.add(node);

            if (node instanceof Parent) {
                nodes.addAll(getAllNodes((Parent)node));
            }
        }

        return nodes;
    }
    
    public static <T extends Node> T findFirstParent(Node node, Class<T> type) {
        while (node != null && !type.isAssignableFrom(node.getClass())) {
            node = node.getParent();
        }
        return (T) node;
        
    }
    
    /**
     * Checks if a node is the same as the given parent or it is a descendant of it.
     * 
     * @param parent the parent
     * @param node the node
     * @return 
     */
    public static boolean isEqualsOrcontainedIn(Parent parent, Node node) {
        return node == parent || isContainedIn(parent, node);
    }
    
    /**
     * Checks if a node is contained within another node.
     * 
     * @param parent the parent
     * @param node the node
     * @return 
     */
    public static boolean isContainedIn(Parent parent, Node node) {
        if (node == null) {
            return false;
        }
        Parent curr = node.getParent();
        while (curr != null) {
            if (curr == parent) {
                return true;
            }
            curr = curr.getParent();
        }
        return false;
    }

    /**
     * Binds the managed property of one or more javafx nodes to it's own visible property.
     *
     * @param nodes
     */
    public static void bindManagedToVisible(Node... nodes) {
        for (Node node : nodes) {
            node.managedProperty().bind(node.visibleProperty());
        }
    }

    /**
     * Bind the visible property of a set of nodes to an observable boolean value.
     *
     * @param ov    The observable boolean. Required.
     * @param nodes The nodes. Required.
     */
    public static void bindVisible(@NonNull ObservableValue<? extends Boolean> ov, @NonNull Node... nodes) {
        for (Node node : nodes) {
            node.visibleProperty().bind(ov);
        }
    }
    
    /**
     * Adds a css class if the node doesn't already have it.
     * 
     * @param node the node 
     * @param cssClass the cssClass
     * @return true if the class was added, false if the node already contained cssClass
     */
    public static boolean addStyleClass(Node node, String cssClass) {
        if (!node.getStyleClass().contains(cssClass)) {
            node.getStyleClass().add(cssClass);
            return true;
        }
        return false;
    }
    
    public static double yDistanceInAncestor(Node node, Parent ancestor) {
        double y = node.getBoundsInParent().getMinY();
        //log.debug("y=" + y);
        while (node.getParent() != ancestor) {
            node = node.getParent();
            y += node.getBoundsInParent().getMinY();
            //log.debug("y=" + y + " node.getBoundsInParent().getMinY()=" + node.getBoundsInParent().getMinY() + " node= " + node);
        }
        return y;
    }

    //== private methods ==============================================================================================
    private static void focusNicelyImpl(Node node) {
        node.requestFocus();

        if (node instanceof TextInputControl) {
            TextInputControl textField = (TextInputControl)node;
            String text = textField.getText();
            textField.positionCaret(text == null ? 0 : text.length());
        }
    }
    
    //== private classes ==============================================================================================
    /**
     * Simple helper class.
     *
     * @author hendrikebbers
     *
     */
    private static class ThrowableWrapper {
        Throwable t;
    }
}

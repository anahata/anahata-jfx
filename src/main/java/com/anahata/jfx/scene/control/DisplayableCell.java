/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.jfx.scene.control;

import com.anahata.util.formatting.Displayable;
import javafx.scene.control.IndexedCell;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class DisplayableCell {
    public static void updateItem(IndexedCell cell, Displayable displayable, boolean empty) {
        if (displayable == null || empty) {
            cell.setText(null);
        } else {
            cell.setText(displayable.getDisplayValue());
        }
    }
}

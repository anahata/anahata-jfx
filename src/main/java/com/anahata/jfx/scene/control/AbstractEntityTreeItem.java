package com.anahata.jfx.scene.control;

import com.anahata.jfx.ImageUtils;
import com.anahata.jfx.JfxUtils;
import com.anahata.jfx.config.JavaFXConfig;
import com.anahata.util.cdi.Cdi;
import com.anahata.util.model.Activatable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

/**
 * Base TreeItem class for wrapping Entities inside a TreeView.
 *
 * @param <T> The implementation wrapped entity type.
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
public abstract class AbstractEntityTreeItem<T> extends TreeItem<String> {
    private JavaFXConfig config;

    private Image deleteIcon;

    //<editor-fold defaultstate="collapsed" desc="@FXProperty entity">
    private ObjectProperty<T> entity = new SimpleObjectProperty<>();

    public T getEntity() {
        return entity.getValue();
    }

    public final void setEntity(T entity) {
        //log.trace("{} updating entity to: {}", getDisplayName(), entity);
        this.entity.setValue(entity);
    }

    public ObjectProperty<T> entityProperty() {
        return entity;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="@FXProperty showDeletedChildren">
    /**
     * Property to indicate if deleted children are being displayed or not, updating this property updates the displayed
     * children list.
     */
    private BooleanProperty showDeletedChildren = new SimpleBooleanProperty(false);

    /**
     * JavaFX {@link #showDeletedChildren} property.
     *
     * @return {@link #showDeletedChildren} property.
     */
    public final BooleanProperty showDeletedChildrenProperty() {
        return showDeletedChildren;
    }

    /**
     * Checks if this node is displaying deleted children.
     *
     * @return true if deleted children are displayed, false otherwise.
     */
    public final Boolean isShowDeletedChildren() {
        return showDeletedChildrenProperty().get();
    }

    /**
     * Sets the show deleted children property value.
     *
     * @param showDeletedChildren if true, deleted children will also be displayed, if false only active ones.
     */
    public final void setShowDeletedChildren(boolean showDeletedChildren) {
        showDeletedChildrenProperty().set(showDeletedChildren);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="@FXProperty filter">
    /**
     * Filter tree nodes using a predicate.
     */
    private ObjectProperty<Predicate> filter = new SimpleObjectProperty<>();

    public Predicate getFilter() {
        return filter.getValue();
    }

    public void setFilter(Predicate filter) {
        this.filter.setValue(filter);
    }

    public ObjectProperty<Predicate> filterProperty() {
        return filter;
    }
    //</editor-fold>

    /**
     * The nodes children. This is different to super.getChildren() as this contains the entire list, not just those
     * deleted ones.
     */
    private List<AbstractEntityTreeItem> allChildren = new ArrayList<>();

    /**
     * Sole constructor.
     *
     * @param entity The entity to set.
     */
    protected AbstractEntityTreeItem() {
        config = Cdi.get(JavaFXConfig.class);
        deleteIcon = new Image(config.getDeleteIcon());

        this.entity.addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable o) {
                updateDisplay(false);
            }
        });

        showDeletedChildrenProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(
                    ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                updateDisplay(false);
            }
        });
    }

    /**
     * Sole constructor.
     *
     * @param entity The entity to set.
     */
    protected AbstractEntityTreeItem(T entity) {
        this();
        setEntity(entity);
    }

    //== public methods ===============================================================================================
    /**
     * Recursively refresh the state of the tree. This must be called if the contents of a filter changes.
     */
    @SuppressWarnings("unchecked")
    public final void refresh() {
        getChildren().setAll((List)computeDisplayedChildren());
        final List<AbstractEntityTreeItem<T>> children = (List)getChildren();

        for (AbstractEntityTreeItem<T> child : children) {
            child.refresh();
        }
    }

    /**
     * Checks if the underlying entity is active.
     *
     * @return true if the underlying entity is active or if it is a grouping item, true if any of the grouped entities
     *         is active.
     */
    public final boolean isActive() {
        return isActiveImpl();
    }

    /**
     * Computes the list of nodes between this tree item and the root including this and the root.
     *
     * @return the list of nodes between this tree item and the root including this and the root.
     */
    @SuppressWarnings("unchecked")
    public final List<AbstractEntityTreeItem> getPathToRoot() {
        List<TreeItem> ret = new ArrayList<>();
        ret.add(this);
        TreeItem parent = super.getParent();

        while (parent != null) {
            ret.add(parent);
            parent = parent.getParent();
        }

        return (List)ret;
    }

    /**
     * Expands the path to the tree item wrapping the specified entity.
     *
     * @param entity the entity whose path is to be expanded.
     * @return the tree item wrapping the specified entity or null if not found
     */
    public final AbstractEntityTreeItem expandPathToEntity(Object entity) {
        AbstractEntityTreeItem ti = findTreeItemForEntity(entity);

        if (ti != null) {
            ti.setExpanded(true);
            TreeItem parent = ti.getParent();

            while (parent != null) {
                parent.setExpanded(true);
                parent = parent.getParent();
            }
        }

        return ti;
    }

    /**
     * Finds a displayed descendant for a given entity.
     *
     * @param entity the entity
     * @return the treeitem o null if no descendant wraps the specified entity
     */
    public final AbstractEntityTreeItem findTreeItemForEntity(Object entity) {
        //Slect Site in tree
        if (Objects.equals(entity, getEntity())) {
            return this;
        }

        if (!isLeaf()) {
            for (Object o : getChildren()) {
                if (o instanceof AbstractEntityTreeItem) {
                    AbstractEntityTreeItem casted = (AbstractEntityTreeItem)o;
                    AbstractEntityTreeItem ret = casted.findTreeItemForEntity(entity);

                    if (ret != null) {
                        return ret;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Creates the image graphic based on {@link #getBaseImage() } overlying a deleted icon if it has been deleted.
     *
     * @return the graphic node for the treeitem.
     */
    public final Node makeGraphic() {
        Node base = getBaseGraphic();

        if (isActive()) {
            return base;
        }

        if (deleteIcon != null) {
            if (base != null) {
                return ImageUtils.getOverLayedImage(getBaseGraphic(), deleteIcon, 16, 0, true,
                        Pos.BOTTOM_RIGHT);
            } else {
                return JfxUtils.makeIcon(deleteIcon, 16);
            }
        }
        
        return null;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()
                + " value=" + getValue()
                + " active=" + isActive()
                + " identityHashCode=" + System.identityHashCode(this);
    }
    //== private methods ==============================================================================================

    /**
     * Finds the item for a given entity in a list of items.
     *
     * @param items  the list of items
     * @param entity the entity whose item we are looking for
     * @return the item wrapping <code>entity<code> or null if none are matching.
     */
    private static AbstractEntityTreeItem findItemForEntity(List<AbstractEntityTreeItem> items, Object entity) {
        for (AbstractEntityTreeItem item : items) {
            if (Objects.equals(item.getEntity(), entity)) {
                return item;
            }
        }

        return null;
    }

    /**
     * Gets children by active flag.
     *
     * @param active the active state of the requested children.
     * @return the children.
     */
    private List<AbstractEntityTreeItem> computeDisplayedChildren() {
        List<AbstractEntityTreeItem> items = isShowDeletedChildren() ? allChildren : computeActiveChildren();
        Predicate predicate = filter.getValue();

        if (predicate == null) {
            return items;
        }

        // Filter the list.
        List<AbstractEntityTreeItem> filtered = new ArrayList<>();

        for (AbstractEntityTreeItem item : items) {
            if (predicate.evaluate(item.getEntity())) {
                filtered.add(item);
            }
        }

        return filtered;
    }

    /**
     * Gets children by active flag.
     *
     * @param active the active state of the requested children.
     * @return the children.
     */
    private List<AbstractEntityTreeItem> computeActiveChildren() {
        List<AbstractEntityTreeItem> ret = new ArrayList<>();

        for (AbstractEntityTreeItem child : allChildren) {
            if (child.isActive()) {
                ret.add(child);
            }
        }

        return ret;
    }

    //== protected methods ============================================================================================
    /**
     * Initializes the text, graphic and children.
     *
     * @param forceRecursive if true, it will call updateDisplay on any child nodes regardless of whether the entity for
     *                       that child node has changed.
     */
    @SuppressWarnings("unchecked")
    public final void updateDisplay(boolean forceRecursive) {
        super.setValue(getDisplayName());

        //This will always cause a property change event to occur and a node re-rendering event too
        //would scale better with some logic to determine if the graphic has changed or not
        //or to be invoked explicitely by the subclass on setEntity if the attributes that determine
        //the graphic have changed
        super.setGraphic(makeGraphic());

        List latestChildEntities = getChildrenEntities();

        if (log.isTraceEnabled()) {
            log.trace("Updating display for {}, showDeleted={} displayedChildrenEntities={} latestChildEntities={}",
                    getDisplayName(),
                    showDeletedChildren.get(), getDisplayedChildrenEntities(), latestChildEntities);
        }

        AbstractEntityTreeItem[] newAllChildrenArr = new AbstractEntityTreeItem[latestChildEntities.size()];

        for (int idx = 0; idx < latestChildEntities.size(); idx++) {
            Object child = latestChildEntities.get(idx);
            AbstractEntityTreeItem currentEquivalent = findItemForEntity(allChildren, child);

            if (currentEquivalent != null) {
                //use existing wrapper and update underlying data
                newAllChildrenArr[idx] = currentEquivalent;
                //execute update
                Object currentEquivalentsEntity = currentEquivalent.getEntity();
                currentEquivalent.setEntity(child);
                if (Objects.equals(currentEquivalentsEntity, child) && forceRecursive) {
                    currentEquivalent.updateDisplay(forceRecursive);
                } else if (forceRecursive && log.isTraceEnabled()) {
                    log.trace(
                            "{} Not calling updateDisplay on child {} with forceRecursive = true as new and old wrapped entities are not equals. old={}, new={}",
                            getDisplayName(), currentEquivalent.getDisplayName(), currentEquivalentsEntity, child);
                }
            } else {
                //not in current list, create new wrapper
                AbstractEntityTreeItem childTreeItem = wrapChildEntity(child);
                newAllChildrenArr[idx] = childTreeItem;
                newAllChildrenArr[idx].showDeletedChildren.bind(showDeletedChildren);
                newAllChildrenArr[idx].filter.bind(filter);
            }
        }

        List<AbstractEntityTreeItem> newAllChildrenList = Arrays.asList(newAllChildrenArr);

        //unbind properties from those children who are no longer in the list
        for (AbstractEntityTreeItem oldChild : allChildren) {
            if (!newAllChildrenList.contains(oldChild)) {
                oldChild.showDeletedChildren.unbind();
                oldChild.filter.unbind();
            }
        }

        //update master list
        allChildren = newAllChildrenList;

        //update tree list with as little events being fired as possible
        ObservableList currentDisplayedChildren = getChildren(); // this is an observable list
        List newDisplayedChildren = computeDisplayedChildren();

        //there is a bug in diff based children update, replacing with setAll. Diff based update can be relooked if performance issues arise
        if (!CollectionUtils.isEqualCollection(currentDisplayedChildren, newDisplayedChildren)) {
            currentDisplayedChildren.setAll(newDisplayedChildren);
        }

//        if (currentDisplayedChildren.isEmpty() && !newDisplayedChildren.isEmpty()) {
//            currentDisplayedChildren.setAll(newDisplayedChildren);
//        } else if (!CollectionUtils.isEqualCollection(currentDisplayedChildren, newDisplayedChildren)) {
//
//            currentDisplayedChildren.setAll(newDisplayedChildren);
//
//            //iterate over the new list replacing existing items with new items and appending
//            //new items at the end of the list if new is bigger than current
//            for (int i = 0; i < newDisplayedChildren.size(); i++) {
//                Object newObject = newDisplayedChildren.get(i);
//                if (currentDisplayedChildren.size() > i) {
//                    Object currentObject = currentDisplayedChildren.get(i);
//                    if (currentObject != newObject) {
//                        currentDisplayedChildren.set(i, newObject);
//                    }
//                } else {
//                    currentDisplayedChildren.add(i, newObject);
//                }
//            }
//            if (newDisplayedChildren.size() < currentDisplayedChildren.size()) {
//                int removeStart = newDisplayedChildren.size();
//                int removeEnd = currentDisplayedChildren.size();
//                currentDisplayedChildren.remove(removeStart, removeEnd);
//            }
//    }
    }
//== methods to override / implement by subclasses ================================================================

    /**
     * Default isLeaf() implementation that returns true if the children list is empty.
     *
     * @return true if the children list is empty.
     */
    @Override
    public boolean isLeaf() {
        return getChildren().isEmpty();
    }

    /**
     * Default isActive implementation that checks if the wrapped entity is an instance of Activatable and returns
     * {@link Activatable#isActive()} if so and true otherwise.
     *
     * @return {@link Activatable#isActive() } if entity is Activatable, true otherwise.
     */
    protected boolean isActiveImpl() {
        if (entity.getValue() instanceof Activatable) {
            return ((Activatable)entity.getValue()).isActive();
        }

        return true;
    }

    /**
     * Returns the entity wrapped by the parent item.
     *
     * @return the entity wrapped by the parent item.
     */
    public Object getParentEntity() {
        return getParentEntityTreeItem().getEntity();
    }

    /**
     * Returns the parent AbstractEntityTreeItem.
     *
     * @return the parent AbstractEntityTreeItem
     */
    public AbstractEntityTreeItem getParentEntityTreeItem() {
        return ((AbstractEntityTreeItem)getParent());
    }

    /**
     * Returns a list of the current displayed entities.
     *
     * @return
     */
    public List getDisplayedChildrenEntities() {
        List<AbstractEntityTreeItem> current = (List)super.getChildren();
        List ret = new ArrayList<>(current.size());
        for (AbstractEntityTreeItem abstractEntityTreeItem : current) {
            ret.add(abstractEntityTreeItem.getEntity());
        }
        return ret;
    }

    /**
     * The name to show on the tree.
     *
     * @return The name to show on the tree.
     */
    protected abstract String getDisplayName();

    /**
     * An graphic representing the entity to create the icon.
     *
     * @return A graphic representing the entity to create the icon.
     */
    protected abstract Node getBaseGraphic();

    /**
     * Makes the children list.
     *
     * @return the children list.
     */
    protected abstract List getChildrenEntities();

    /**
     *
     * @param entity
     * @return
     */
    protected abstract AbstractEntityTreeItem wrapChildEntity(Object entity);

    public void setExpandedAll(boolean expanded) {
        setExpanded(expanded);
        for (TreeItem ti : getChildren()) {
            if (ti instanceof AbstractEntityTreeItem) {
                ((AbstractEntityTreeItem)ti).setExpandedAll(expanded);
            }
        }
    }

    /**
     * Gets all the child tree items recursively.
     *
     * @return
     */
    public List<AbstractEntityTreeItem> getAllChildAbstractEntityTreeItems() {
        List<AbstractEntityTreeItem> all = new ArrayList();
        for (TreeItem ti : getChildren()) {
            if (ti instanceof AbstractEntityTreeItem) {
                AbstractEntityTreeItem aeti = (AbstractEntityTreeItem)ti;
                all.add(aeti);
                all.addAll(aeti.getAllChildAbstractEntityTreeItems());
            }
        }
        return all;
    }

    public List<T> getAllChildEntities() {
        return (List)getAllChildAbstractEntityTreeItems().stream().map(ti -> ti.getEntity()).collect(Collectors.toList());
    }

}

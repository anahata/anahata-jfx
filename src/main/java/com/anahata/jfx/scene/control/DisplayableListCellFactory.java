package com.anahata.jfx.scene.control;

import com.anahata.util.formatting.Displayable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import org.apache.commons.lang3.Validate;

/**
 * A generic cell factory for classes that implement Displayable. Does not show any icon.
 *
 * @param <T> The Displayable implementing type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class DisplayableListCellFactory<T extends Displayable> implements Callback<ListView<T>, ListCell<T>> {    
    @Override
    public ListCell<T> call(ListView<T> p) {
        return new ListCell<T>() {
            @Override
            protected void updateItem(T displayable, boolean empty) {
                super.updateItem(displayable, empty);
                DisplayableCell.updateItem(this, displayable, empty);
            }
        };
    }
    
    /**
     * Configure a ComboBox with elements implementing Displayable with a cell factory to render the display value.
     * 
     * @param comboBox The combo box. Required.
     * @throws NullPointerException If comboBox is null.
     */
    public static void setComboBoxCellFactory(ComboBox<? extends Displayable> comboBox) {
        Validate.notNull(comboBox);
        DisplayableListCellFactory cellFactory = new DisplayableListCellFactory();
        comboBox.setCellFactory(cellFactory);
        comboBox.setButtonCell(cellFactory.call(null));
    }
    
//    /**
//     * Configure a ComboBox with elements implementing Displayable with a cell factory to render the display value.
//     * 
//     * @param comboBox The combo box. Required.
//     * @throws NullPointerException If comboBox is null.
//     */
//    public static void setComboBoxCellFactory(AutoCompleteComboBox<? extends Displayable> comboBox) {
//        Validate.notNull(comboBox);
//        DisplayableListCellFactory cellFactory = new DisplayableListCellFactory();
//        comboBox.setCellFactory(cellFactory);
//    }
}

package com.anahata.jfx.scene.control;

import com.anahata.jfx.bind.nodemodel.NodeModel;
import com.anahata.util.formatting.Displayable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javafx.beans.property.SetProperty;
import javafx.beans.property.SimpleSetProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;
import org.apache.commons.lang3.Validate;
import static javafx.scene.layout.Region.*;

/**
 * A JavaFX control that presents a list of checkbox (boolean) choices for a set
 * of possible.
 *
 * @param <T> Must instantiate with a concrete type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class CheckboxList<T> extends VBox implements NodeModel<CheckboxList, SetProperty> {

    //<editor-fold defaultstate="collapsed" desc="@FXProperty">

    public ObservableSet<T> getValue() {
        return value.get();
    }

    public void setValue(ObservableSet<T> value) {
        this.value.set(value);
    }

    public SetProperty<T> valueProperty() {
        return value;
    }
    //</editor-fold>

    private final SetProperty<T> value = new SimpleSetProperty<>(FXCollections.observableSet(new HashSet<T>()));

    public CheckboxList() {
        setPrefHeight(USE_COMPUTED_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setSpacing(8);
        setStyle("-fx-border: solid 2px red;");

        value.addListener(new ChangeListener<Set<T>>() {
            @Override
            public void changed(ObservableValue<? extends Set<T>> ov, Set<T> oldValue, Set<T> newValue) {
                for (Node node : CheckboxList.this.getChildren()) {
                    CheckBox cb = (CheckBox) node;
                    @SuppressWarnings("unchecked")
                    T currValue = (T) cb.getUserData();
                    cb.setSelected(newValue != null && newValue.contains(currValue));
                }
            }
        });
    }

    public void init(T[] values) {
        init(Arrays.asList(values));
    }

    public void setRawValue(Set s) {
        value.setValue(FXCollections.observableSet(s));
    }

    public void init(Collection<T> values) {
        Validate.notNull(values);

        
        getChildren().clear();
        value.setValue(FXCollections.observableSet());

        for (T currValue : values) {
            String description;

            if (currValue instanceof Displayable) {
                description = ((Displayable) currValue).getDisplayValue();
            } else {
                description = currValue.toString();
            }

            final CheckBox cb = new CheckBox(description);
            cb.setWrapText(true);
            cb.setUserData(currValue);
            getChildren().add(cb);

            cb.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                    @SuppressWarnings("unchecked")
                    final T eValue = (T) cb.getUserData();

                    if (newValue) {
                        value.get().add(eValue);
                    } else {
                        value.get().remove(eValue);
                    }
                }
            });
        }        
    }

    @Override
    public SetProperty getNodeModelValueProperty(CheckboxList node) {
        return node.valueProperty();
    }
}

package com.anahata.jfx.scene.control;

import com.sun.javafx.css.converters.EnumConverter;
import com.sun.javafx.css.converters.SizeConverter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ListChangeListener;
import javafx.css.*;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import lombok.Getter;

/**
 * A fieldset layout pane.
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class FieldSetLayout extends StackPane {
    //== constants ====================================================================================================

    private static final String DEFAULT_STYLE_CLASS = "field-set";

    private static final String CSS_PROPERTY_TITLE_POSITION = "-fx-title-position";

    private static final String CSS_PROPERTY_EDGE_DISTANCE = "-fx-edge-distance";

    private static final String PADDING_8 = "padding-8";

    //== attributes ===================================================================================================
    @Getter
    private HBox titleBox = new HBox(10);
    
    @Getter
    private Label titleLabel = new Label();
    
    @Getter        
    private Node titleGraphic;

    private StackPane leftCorner;

    private StackPane rightCorner;

    private StackPane border;

    //== init =========================================================================================================
    public FieldSetLayout() {
        this("");
    }

    public FieldSetLayout(String title) {
        setTitle(title);
        init();
    }
    
    public FieldSetLayout(String title, Node titleGraphic) {
        setTitle(title);
        this.titleGraphic = titleGraphic;
        init();
    }

    //== properties ===================================================================================================
    private final StringProperty title = new SimpleStringProperty(this, "title", "") {
        @Override
        protected void invalidated() {
            titleLabel.setText(get());
        }
    };

    /**
     * Title property
     *
     * @return title property
     */
    public StringProperty titleProperty() {
        return title;
    }

    public final void setTitle(String value) {
        title.set(value);
    }

    public final String getTitle() {
        return title.get();
    }

    // ----------------------------
    // TITLE EDGE DISTANCE PROPERTY
    // ----------------------------
    private DoubleProperty edgeDistance;

    /**
     * Edge distance property, settable via css -fx-edge-distance. Title
     * distance from left/right edge, ignored if titlePosition set to CENTER.
     * Default value 10.0d
     *
     * @return edge distance property.
     */
    public DoubleProperty edgeDistanceProperty() {
        if (edgeDistance == null) {
            edgeDistance = new javafx.css.StyleableDoubleProperty(20.0d) {

                @Override
                public Object getBean() {
                    return FieldSetLayout.this;
                }

                @Override
                public String getName() {
                    return "edgeDistance";
                }

                @Override
                public CssMetaData<? extends Styleable, Number> getCssMetaData() {
                    return StyleableProperties.EDGE_DISTANCE;
                }
            };
        }
        return edgeDistance;
    }

    public final void setEdgeDistance(double value) {
        edgeDistanceProperty().set(value);
    }

    public final double getEdgeDistance() {
        return edgeDistance == null ? 10.0d : edgeDistance.get();
    }

    private ObjectProperty<HPos> titlePosition;

    /**
     * Title position property settable via css -fx-title-position property.
     * Default value is LEFT, can be CENTER and RIGHT
     *
     * @return title position JFX property.
     */
    public ObjectProperty<HPos> titlePositionProperty() {
        if (titlePosition == null) {
            titlePosition = new StyleableObjectProperty<HPos>(HPos.LEFT) {

                @Override
                protected void invalidated() {
                    requestLayout();
                }

                @Override
                public Object getBean() {
                    return FieldSetLayout.this;
                }

                @Override
                public String getName() {
                    return "titlePosition";
                }

                @Override
                public CssMetaData<? extends Styleable, HPos> getCssMetaData() {
                    return StyleableProperties.TITLE_POSITION;
                }
            };
        }
        return titlePosition;
    }

    public final void setTitlePosition(HPos value) {
        titlePositionProperty().set(value);
    }

    public final HPos getTitlePosition() {
        return titlePosition == null ? HPos.LEFT : titlePosition.get();
    }

    //== private methods ==============================================================================================
    private void init() {
        getStyleClass().add(DEFAULT_STYLE_CLASS);
//        getStylesheets().add(
//                "/com/anahata/jfx/internal/scene/control/" + getClass().getSimpleName() + ".css");

        leftCorner = new StackPane();
        leftCorner.getStyleClass().add("left-corner");

        rightCorner = new StackPane();
        rightCorner.getStyleClass().add("right-corner");

        border = new StackPane();
        border.getStyleClass().add("content-area");
        
        titleBox.getStyleClass().add("title-area");

        getChildren().addAll(leftCorner, rightCorner, border, titleBox);      
        titleBox.getChildren().add(titleLabel);
        titleBox.setAlignment(Pos.CENTER);
        if (titleGraphic != null) {
            titleBox.getChildren().add(titleGraphic);
        }

        // == NOTE: this is just a hack so we dont have to put padding on every container inside field set
        getChildren().addListener(new ListChangeListener<Node>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends Node> c) {
                while (c.next()) {
                    List<? extends Node> added = c.getAddedSubList();
                    for (Node n : added) {
                        if (n instanceof Pane && !n.getStyleClass().contains("grid")) {
                            n.getStyleClass().add(PADDING_8);
                        }
                    }
                }
            }
        });
    }

    private double computeLeftWidth(double width, double titleWidth) {
        HPos titlePos = getTitlePosition();
        double edgeDist = getEdgeDistance();
        // assume it is center
        double delta = width - titleWidth;
        double leftWidth = delta / 2.0d; // 50% width        
        if (titlePos == HPos.LEFT) {
            leftWidth = edgeDist;
        } else if (titlePos == HPos.RIGHT) {
            leftWidth = delta - edgeDist;
        }
        return leftWidth;
    }

    private double computeRightWidth(double width, double titleWidth) {
        HPos titlePos = getTitlePosition();
        double edgeDist = getEdgeDistance();
        // assume it is center
        double delta = width - titleWidth;
        double rightWidth = delta / 2.0d; // 50% width
        if (titlePos == HPos.LEFT) {
            rightWidth = delta - edgeDist;
        } else if (titlePos == HPos.RIGHT) {
            rightWidth = edgeDist;
        }
        return rightWidth;
    }

    @Override
    protected void layoutChildren() {
        List<Node> managed = getManagedChildren();
        // remove children that will be layout manually
        managed.removeAll(Arrays.asList(titleBox, rightCorner, leftCorner, border));
        
        double top = getInsets().getTop();
        double right = getInsets().getRight();
        double bottom = getInsets().getBottom();
        double left = getInsets().getLeft();
        double width = getWidth() - left - right;
        double height = getHeight() - top - bottom;

        double titlePW = titleBox.prefWidth(-1);
        double titlePH = titleBox.prefHeight(-1);
        
        double titleHalfHeight = titlePH / 2.0d;
        double leftHeight = titleHalfHeight;
        double rightHeight = titleHalfHeight;

        double leftWidth = computeLeftWidth(width, titlePW);
        double rightWidth = computeRightWidth(width, titlePW);

        leftCorner.resize(leftWidth, leftHeight);
        positionInArea(leftCorner,
                0.0,
                top + titleHalfHeight + leftCorner.getInsets().getTop(), // add inset cuz border
                leftWidth,
                leftHeight,
                0.0d,
                HPos.CENTER, VPos.CENTER);

        // layout title
        titleBox.resize(titlePW, titlePH);
        positionInArea(titleBox,
                left + leftWidth,
                top,
                titlePW,
                titlePH,
                0.0d,
                HPos.CENTER, VPos.CENTER);        
        // layout right corener
        rightCorner.resize(rightWidth, rightHeight);
        positionInArea(rightCorner,
                left + leftWidth + titlePW + right,
                //                top,
                top + titleHalfHeight + rightCorner.getInsets().getTop(), // add insets also cuz border
                rightWidth,
                rightHeight,
                0.0d,
                HPos.CENTER, VPos.CENTER);
        // layout border - NOTE: ignores left/right padding.
        border.resize(getWidth(), height - titleHalfHeight);
        positionInArea(border, 0.0d, top + titlePH, getWidth(), height - titleHalfHeight, 0.0, HPos.CENTER, VPos.CENTER);
        // layout rest of children
        for (Node child : managed) {
            layoutInArea(child,
                    left,
                    top + titlePH + titleHalfHeight,
                    width,
                    height - titlePH - titleHalfHeight,
                    0.0d,
                    HPos.CENTER, VPos.CENTER);
        }

//        super.layoutChildren(); //To change body of generated methods, choose Tools | Templates.
    }

    //== stylesheet handling ==========================================================================================
    private static class StyleableProperties {

        // text position
        private static final CssMetaData<FieldSetLayout, HPos> TITLE_POSITION = new CssMetaData<FieldSetLayout, HPos>(
                CSS_PROPERTY_TITLE_POSITION,
                new EnumConverter<>(HPos.class),
                HPos.LEFT) {
                    @Override
                    public boolean isSettable(FieldSetLayout fieldSet) {
                        return fieldSet.titlePosition == null || !fieldSet.titlePosition.isBound();
                    }

                    @Override
                    public StyleableProperty<HPos> getStyleableProperty(FieldSetLayout fsl) {
                        return (StyleableObjectProperty<HPos>) fsl.titlePositionProperty();
                    }

                };

        // edge distance
        private static final CssMetaData<FieldSetLayout, Number> EDGE_DISTANCE = new CssMetaData<FieldSetLayout, Number>(
                CSS_PROPERTY_EDGE_DISTANCE,
                SizeConverter.getInstance(),
                20.0d) {
                    @Override
                    public boolean isSettable(FieldSetLayout fieldSet) {
                        return fieldSet.edgeDistance == null
                        || !fieldSet.edgeDistance.isBound();
                    }

                    @Override
                    public StyleableProperty<Number> getStyleableProperty(FieldSetLayout styleable) {
                        return (StyleableDoubleProperty) styleable.edgeDistanceProperty();
                    }

                };

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLE_PROPERTY_LIST;

        static {
            final List<CssMetaData< ? extends Styleable, ?>> list = new ArrayList<>(StackPane.getClassCssMetaData());
            Collections.addAll(list,
                    TITLE_POSITION,
                    EDGE_DISTANCE
            );
            STYLEABLE_PROPERTY_LIST = Collections.unmodifiableList(list);
        }

    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() {
        return StyleableProperties.STYLEABLE_PROPERTY_LIST;
    }

    @Override
    public List<CssMetaData<? extends Styleable, ?>> getCssMetaData() {
        return getClassCssMetaData();
    }
}

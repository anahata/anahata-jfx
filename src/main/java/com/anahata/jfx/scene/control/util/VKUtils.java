/*
 *  Copyright © - 2015 Urgent Learning. All rights reserved.
 */
package com.anahata.jfx.scene.control.util;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.Skin;
import javafx.scene.control.TextField;
import jfxtras.labs.internal.scene.control.skin.BigDecimalFieldSkin;
import jfxtras.labs.scene.control.BigDecimalField;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.reflect.FieldUtils;

/**
 *
 * @author goran
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class VKUtils {

    // == constants ==
    public static final String VK_TYPE_PROP_KEY = "vkType";
    public static final String VK_TYPE_NUMERIC_NAME = "numeric";

    // == public methods ==
    
    public static void configureNumericKeyboard(TextField... fields) {
        Validate.noNullElements(fields);

        for (TextField tf : fields) {
            tf.getProperties().put(VK_TYPE_PROP_KEY, VK_TYPE_NUMERIC_NAME);
        }
    }

    public static void configureNumericKeyboard(BigDecimalField... fields) {
        for (BigDecimalField bdf : fields) {
            bdf.skinProperty().addListener((ObservableValue<? extends Skin<?>> observable, Skin<?> oldValue, Skin<?> newValue) -> {
                try {
                    BigDecimalFieldSkin skin = (BigDecimalFieldSkin) bdf.getSkin();
                    TextField tf = (TextField) FieldUtils.readDeclaredField(skin, "textField", true);
                    configureNumericKeyboard(tf);
                } catch (Exception e) {
                    log.warn("Couldn't set numeric keyboard on BigDecimalField= " + bdf, e);
                }
            });
        }

    }
}

package com.anahata.jfx.scene.control;

import com.anahata.jfx.DelegatingControl;
import com.anahata.jfx.JfxUtils;
import com.anahata.jfx.bind.*;
import com.anahata.util.cdi.Cdi;
import java.io.IOException;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import lombok.Getter;

/**
 * A base HBox-based FXML control, integrated with CDI.
 *
 * @param <T> The controller type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public abstract class FxmlHBoxControl<T extends BindingController> extends HBox implements BindForm, DelegatingControl {
    @Getter
    private final T controller;
    
    /**
     * Constructor.
     * 
     * @param fxml            The FXML to load.
     * @param controllerClass The controller class.
     */
    protected FxmlHBoxControl(String fxml, Class<T> controllerClass) {
        this(fxml, controllerClass, null);
    }
    
    /**
     * Constructor.
     * 
     * @param fxml            The FXML to load.
     * @param controllerClass The controller class.
     * @param resources       The resource bundle.
     */
    protected FxmlHBoxControl(String fxml, Class<T> controllerClass, ResourceBundle resources) {
        super();
        FXMLLoader loader = JfxUtils.loader(fxml);
        loader.setRoot(this);
        controller = Cdi.get(controllerClass);
        loader.setController(controller);
        
        try {
            loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //== BindForm =====================================================================================================
    
    @Override
    public Binder getRootBinder() {
        return controller.getBinder().getRootBinder();
    }

    @Override
    public void setRootBinder(Binder binder) {
        controller.getBinder().setRootBinder(binder);
    }
    
    @Override
    public BindForm getParentBindForm() {
        return controller.getBinder().getParentBindForm();
    }
    
    @Override
    public void setParentBindForm(BindForm parentBindForm) {
        controller.getBinder().setParentBindForm(parentBindForm);
    }

    @Override
    public Binding getParentBinding() {
        return controller.getBinder().getParentBinding();
    }

    @Override
    public void setParentBinding(Binding parentBinding) {
        controller.getBinder().setParentBinding(parentBinding);
    }

    @Override
    public View getView(Binding binding) {
        return controller.getBinder().getView(binding);
    }

    @Override
    public void setView(View view, Binding binding) {
        controller.getBinder().setView(view, binding);
    }

    @Override
    public void resetFormModified() {
        controller.getBinder().resetFormModified();
    }
    
    @Override
    public Map<Node, Binding> getAllNodeBindings() {
        return controller.getBinder().getAllNodeBindings();
    }
    
    @Override
    public void bindFromModel() {
        controller.getBinder().bindFromModel();
    }
    
    @Override
    public void bindFromModelExcludingNode(Object node) {
        controller.getBinder().bindFromModelExcludingNode(node);
    }

    @Override
    public void validate(boolean publishError, Object... excludeNodes) {
        controller.getBinder().validate(publishError, excludeNodes);
    }
    
    @Override
    public BooleanProperty formValidProperty() {
        return controller.getBinder().formValidProperty();
    }

    @Override
    public BooleanProperty formModifiedProperty() {
        return controller.getBinder().formModifiedProperty();
    }

    @Override
    public BooleanProperty showContainerErrors() {
        return controller.getBinder().showContainerErrors();
    }
    
    @Override
    public void setExcludeNodes(Object... nodes) {
    }
    
    //== Validations ==================================================================================================
    
    @Override
    public void addValidationGroup(Class<?> validationGroup) {
        controller.getBinder().addValidationGroup(validationGroup);
    }

    @Override
    public void removeValidationGroup(Class<?> validationGroup) {
        controller.getBinder().removeValidationGroup(validationGroup);
    }

    @Override
    public BooleanProperty getValidationActive(Class<?> validationGroup) {
        return controller.getBinder().getValidationActive(validationGroup);
    }
    
    @Override
    public ReadOnlyBooleanProperty getFormValidProperty(Class<?> validationGroup) {
        return controller.getBinder().getFormValidProperty(validationGroup);
    }

    @Override
    public void setValid(String propertyName, Class<?> validationGroup) {
        controller.getBinder().setValid(propertyName, validationGroup);
    }

    @Override
    public void setInvalid(String propertyName, Class<?> validationGroup) {
        controller.getBinder().setInvalid(propertyName, validationGroup);
    }

    @Override
    public Set<Class<?>> getValidations() {
        return controller.getBinder().getValidations();
    }
    
    @Override
    public void setValidationRequired() {
        controller.getBinder().setValidationRequired();
    }
}

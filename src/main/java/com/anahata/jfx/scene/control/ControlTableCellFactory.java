package com.anahata.jfx.scene.control;

import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.util.Callback;

/**
 * A table cell factory that allows obtaining a node by passing in the table row.
 *
 * @param <R> The TableRow type.
 * @param <M> The row type.
 * @param <T> The cell type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public abstract class ControlTableCellFactory<R extends TableRow<M>, M, T> implements Callback<TableColumn<M, T>, TableCell<M, T>> {
    @Override
    public TableCell<M, T> call(TableColumn<M, T> column) {
        return new ControlTableCell<R, M, T>() {
            @Override
            public Node getGraphic(R row) {
                return ControlTableCellFactory.this.getGraphic(row);
            }
        };
    }
    
    public abstract Node getGraphic(R row);
}

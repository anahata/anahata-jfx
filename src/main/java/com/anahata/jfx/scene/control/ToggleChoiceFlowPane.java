package com.anahata.jfx.scene.control;

import com.anahata.jfx.bind.nodemodel.NodeModel;
import com.anahata.util.formatting.Displayable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.FlowPane;
import org.apache.commons.lang3.Validate;
import static javafx.scene.layout.Region.*;

/**
 * A JavaFX control that presents a list of toggle buttons to select one item inside the values of a enum.
 *
 * @param <T> Must instantiate with a concrete type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class ToggleChoiceFlowPane<T> extends FlowPane implements NodeModel<ToggleChoiceFlowPane, ObjectProperty> {

    //<editor-fold defaultstate="collapsed" desc="@FXProperty">

    public T getValue() {
        return value.get();
    }

    public void setValue(T value) {
        this.value.set(value);
    }

    public ObjectProperty<T> valueProperty() {
        return value;
    }
    //</editor-fold>

    private final ObjectProperty<T> value = new SimpleObjectProperty<>();
    
    private boolean block = false;

    public ToggleChoiceFlowPane() {
        setPrefHeight(USE_COMPUTED_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setHgap(8);
        setVgap(8);
        
        value.addListener(new ChangeListener<T>() {
            @Override
            public void changed(ObservableValue<? extends T> ov, T oldValue, T newValue) {
                for (Node node : ToggleChoiceFlowPane.this.getChildren()) {
                    ToggleButton tb = (ToggleButton) node;
                    @SuppressWarnings("unchecked")
                    T currValue = (T) tb.getUserData();
                    tb.setSelected(Objects.equals(currValue, newValue));
                }
            }
        });
    }

    public void init(T[] values) {
        init(Arrays.asList(values));
    }

    public void init(Collection<T> values) {
        Validate.notNull(values);
        getChildren().clear();
        value.setValue(null);

        for (T currValue : values) {
            String description;

            if (currValue instanceof Displayable) {
                description = ((Displayable) currValue).getDisplayValue();
            } else {
                description = currValue.toString();
            }

            final ToggleButton cb = new ToggleButton(description);
            cb.setWrapText(true);
            cb.setUserData(currValue);
            getChildren().add(cb);
            
            cb.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                    if (block) {
                        return;
                    }
                    
                    block = true;
                    
                    @SuppressWarnings("unchecked")
                    final T eValue = (T) cb.getUserData();

                    if (newValue) {
                        System.out.println("Setting value to " + value);
                        value.set(eValue);
                    } else {
                        value.set(null);
                    }
                    block = false;
                }
            });
        }        
    }

    @Override
    public ObjectProperty getNodeModelValueProperty(ToggleChoiceFlowPane node) {
        return node.valueProperty();
    }
}

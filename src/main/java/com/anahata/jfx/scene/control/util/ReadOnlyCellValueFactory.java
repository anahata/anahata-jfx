package com.anahata.jfx.scene.control.util;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;

/**
 * Shortcut class for creating read only cells in a table from a standard bean. Implements just have to override
 * getValue().
 *
 * @param <T> Model type.
 * @param <U> Attribute type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public abstract class ReadOnlyCellValueFactory<T, U> implements Callback<CellDataFeatures<T, U>, ObservableValue<U>> {
    @Override
    public ObservableValue<U> call(CellDataFeatures<T, U> p) {
        return new ReadOnlyObjectWrapper<>(getValue(p.getValue()));
    }

    protected abstract U getValue(T model);
}

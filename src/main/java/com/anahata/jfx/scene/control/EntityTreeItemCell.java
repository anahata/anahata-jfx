package com.anahata.jfx.scene.control;

import javafx.scene.control.TreeCell;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor
@Slf4j
public class EntityTreeItemCell extends TreeCell<String> {
    
    private static final String INACTIVE_STYLE_CLASS = "inactive";
    
    /**
     * Callback method to updates the item's display.
     *
     * @param item  the text of the item
     * @param empty true if rendering an empty cell
     */
    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (!empty && item != null) {
            AbstractEntityTreeItem treeItem = (AbstractEntityTreeItem)super.getTreeItem();
            super.setGraphic(treeItem.makeGraphic());
            super.setText(treeItem.getDisplayName());

            if (treeItem.isActive()) {
                //getStyleClass().remove(INACTIVE_STYLE_CLASS);
            } else {
                //getStyleClass().add(INACTIVE_STYLE_CLASS);
            }
        }else{
            setGraphic(null);
            setText(null);
        }
    }
}

package com.anahata.jfx.scene.control.util;

import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.Validate;
import static javafx.scene.control.Control.*;

/**
 * Utilities for JavaFX controls.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 * @version $Id: $Id
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LayoutUtils {
    /**
     * For a given node, find the coordinates of it relative to a given parent pane.
     *
     * @param node The node. Required.
     * @param pane The pane. Required.
     * @return The bounds. Never null.
     * @throws java.lang.NullPointerException If node or pane are null.
     */
    public static Bounds getNodeBoundsInPane(@NonNull Node node, @NonNull Pane pane) {
        Pane currPane = (node instanceof Pane ? (Pane)node : null);
        Bounds nodeBounds = node.getBoundsInParent();
        double x = nodeBounds.getMinX();
        double y = nodeBounds.getMinY();
        
        while (node.getParent() != null) {
            node = node.getParent();
            
            if (node instanceof Pane) {
                currPane = (Pane)node;
            }
            
            if (currPane != null && currPane.equals(pane)) {
                break;
            }
            
            Bounds bounds = node.getBoundsInParent();
            x += bounds.getMinX();
            y += bounds.getMinY();
        }
        
        Validate.validState(currPane != null, "Pane not found for node %s", node);
        return new BoundingBox(x, y, nodeBounds.getWidth(), nodeBounds.getHeight());
    }
    
    /**
     * Set the preferred, minimum and maximum width of a control.
     *
     * @param control The control.
     * @param widths  The preferred width, with optional min / max afterwards. If min or max are omitted, they default
     *                to USE_PREF_SIZE.
     */
    public static void setWidth(Control control, double... widths) {
        Validate.notNull(widths);
        Validate.isTrue(widths.length >= 1, "Require at least one width");
        Validate.isTrue(widths.length <= 3, "Require no more than three widths");
        control.setPrefWidth(widths[0]);
        control.setMinWidth(widths.length >= 2 ? widths[1] : USE_PREF_SIZE);
        control.setMaxWidth(widths.length >= 3 ? widths[2] : USE_PREF_SIZE);
    }
    
    /**
     * Set the preferred, minimum and maximum height of a control.
     *
     * @param control The control.
     * @param heights The preferred height, with optional min / max afterwards. If min or max are omitted, they default
     *                to USE_PREF_SIZE.
     */
    public static void setHeight(Control control, double... heights) {
        Validate.notNull(heights);
        Validate.isTrue(heights.length >= 1, "Require at least one height");
        Validate.isTrue(heights.length <= 3, "Require no more than three heights");
        control.setPrefHeight(heights[0]);
        control.setMinHeight(heights.length >= 2 ? heights[1] : USE_PREF_SIZE);
        control.setMaxHeight(heights.length >= 3 ? heights[2] : USE_PREF_SIZE);
    }
    
    /**
     * Set the preferred, minimum and maximum width of a region.
     *
     * @param region  The region.
     * @param widths  The preferred width, with optional min / max afterwards. If min or max are omitted, they default
     *                to USE_PREF_SIZE.
     */
    public static void setWidth(Region region, double... widths) {
        Validate.notNull(widths);
        Validate.isTrue(widths.length >= 1, "Require at least one width");
        Validate.isTrue(widths.length <= 3, "Require no more than three widths");
        region.setPrefWidth(widths[0]);
        region.setMinWidth(widths.length >= 2 ? widths[1] : USE_PREF_SIZE);
        region.setMaxWidth(widths.length >= 3 ? widths[2] : USE_PREF_SIZE);
    }
}

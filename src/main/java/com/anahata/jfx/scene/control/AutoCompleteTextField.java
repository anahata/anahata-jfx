/*
 * Copyright 2012 myTDev & Narayan G. Maharjan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.jfx.scene.control;

import java.util.List;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import lombok.extern.slf4j.Slf4j;

/**
 * Auto-complete text field.
 *
 * @param <T> The structured type presented in the auto-complete list.
 * @author Narayan G. Maharjan
 * @author Yann D'Isanto
 */
@Slf4j
public class AutoCompleteTextField<T> extends Control {
    //== constants ====================================================================================================

    private static final int DEFAULT_LIMIT = 0;

    private static final int DEFAULT_POPUP_SIZE = 6;

    //== attributes ===================================================================================================
    private StringProperty text = new SimpleStringProperty();

    /**
     * The last value of text (usefull when working on TYPED mode that it deletes the field on focus lost)
     */
    private StringProperty lastText = new SimpleStringProperty();

    private StringProperty editorText = new SimpleStringProperty();

    /**
     * The available items for auto-complete.
     */
    private final ObjectProperty<ObservableList<T>> items = new SimpleObjectProperty<>(
            FXCollections.<T>observableArrayList());

    /**
     * The maximum number of items the popup can contain. 0 meaning no limit.
     */
    private final IntegerProperty limit = new SimpleIntegerProperty(DEFAULT_LIMIT);

    /**
     * The number of items visible in the popup without scrolling.
     */
    private final IntegerProperty popupSize = new SimpleIntegerProperty(DEFAULT_POPUP_SIZE);

    /**
     * The preferable popupwidth, 0 means as wide as the text field
     */
    private final IntegerProperty prefListWidth = new SimpleIntegerProperty();

    /**
     * Specifies how the text should be aligned when there is empty space within the TextField.
     */
    private final ObjectProperty<Pos> alignment = new SimpleObjectProperty<>();

    /**
     * Specifies how the text should be aligned when there is empty space within the TextField.
     */
    private final ObjectProperty<T> selectedItem = new SimpleObjectProperty<>();

    /**
     * The action handler associated with this text field, or null if no action handler is assigned. The action handler
     * is normally called when the user types the ENTER key.
     */
    private final ObjectProperty<EventHandler<ActionEvent>> onAction = new SimpleObjectProperty<>();

    /**
     * The preferred number of text columns. This is used for calculating the TextField's preferred width.
     */
    private final IntegerProperty prefColumnCount = new SimpleIntegerProperty(TextField.DEFAULT_PREF_COLUMN_COUNT);

    /**
     * The TextField's prompt text to display, or null if no prompt text is displayed.
     */
    private final StringProperty promptText = new SimpleStringProperty();

    /**
     * The DataFormatter used to generate items string representation. By default it uses the toListViewString() method.
     */
    private final ObjectProperty<DataFormatter<T>> dateFormatter
            = new SimpleObjectProperty<DataFormatter<T>>(new DefaultDataFormatter<T>());

    /**
     * The DataFormatter used to generate items string representation. By default it uses the toListViewString() method.
     */
    private final ObjectProperty<DataProvider<T>> dataProvider
            = new SimpleObjectProperty<>(null);

    private final ObjectProperty value = new SimpleObjectProperty();

    /**
     * Flag indicating whether {@link #valueProperty()} should return the selected item or the text in the text field.
     */
    private ObjectProperty<Mode> mode = new SimpleObjectProperty<>(Mode.STRING_ONLY);

    /**
     * Indicates whether the list of {@link #items} should be filtered to only display the items matching the text in
     * the text field. When using a data provider, this flag would typically be set to false as the dataprovider will
     * most likely do the filtering.
     */
    private BooleanProperty filterItems = new SimpleBooleanProperty(true);

    /**
     * Indicates if the text field has focus.
     */
    private BooleanProperty textFieldFocused = new SimpleBooleanProperty(false);

    /**
     * Indicates if the data fetch task running
     */
    private BooleanProperty dataProcessing = new SimpleBooleanProperty(false);

    /**
     * Indicates if the single match found needs to be selected to textfield
     */
    private BooleanProperty singleMatchSelection = new SimpleBooleanProperty(false);

    /**
     * Indicates if list needs to be shown on filling data
     */
    private BooleanProperty showListOndata = new SimpleBooleanProperty(false);

    /**
     * Constructor.
     */
    public AutoCompleteTextField() {
        this(Mode.STRING_ONLY);
    }

    /**
     * Constructor.
     *
     * @param mode
     */
    public AutoCompleteTextField(Mode mode) {
        // setup the CSS
        // the -fx-skin attribute in the CSS sets which Skin class is used
        this.getStyleClass().add(this.getClass().getSimpleName().toLowerCase());
        this.mode.set(mode);
    }

    //== public methods ===============================================================================================
    public boolean isFilterItems() {
        return filterItems.get();
    }

    public void setFilterItems(boolean b) {
        filterItems.setValue(b);
    }

    public boolean isSingleMatchSelection() {
        return singleMatchSelection.get();
    }

    public void setShowListOnData(boolean b) {
        showListOndata.setValue(b);
    }

    public boolean isShowListOnData() {
        return showListOndata.get();
    }

    public void setSingleMatchSelection(boolean b) {
        singleMatchSelection.setValue(b);
    }

    public ObjectProperty valueProperty() {
        return value;
    }

    public Object getValue() {
        return value.get();
    }

    @SuppressWarnings("unchecked")
    public void setValue(Object o) {
        value.setValue(o);
    }

    /**
     * @return The checked property.
     */
    public ObjectProperty modeProperty() {
        return mode;
    }

    public Mode getMode() {
        return mode.get();
    }

    public void setMode(Mode mode) {
        this.mode.set(mode);
    }

    /**
     * The auto-complete items.
     *
     * @return The auto-complete items property.
     * @see #getItems()
     * @see #setItems(javafx.collections.ObservableList)
     */
    public ObjectProperty<ObservableList<T>> itemsProperty() {
        return items;
    }

    /**
     * Gets the value of the property items.
     *
     * @return A list of auto-complete items.
     */
    public ObservableList<T> getItems() {
        return items.get();
    }

    /**
     * Sets the value of the property items.
     *
     * @param items The auto-complete items.
     */
    public void setItems(ObservableList<T> items) {
        this.items.set(items);
    }

    public BooleanProperty filterItemsProperty() {
        return filterItems;
    }

    /**
     * Returns the limit property.
     *
     * @return the limit property.
     */
    public IntegerProperty limitProperty() {
        return limit;
    }

    /**
     * Returns the maximum number of items the popup can contain. 0 meaning no limit.
     *
     * @return the auto-complete items count limit.
     */
    public int getLimit() {
        return limit.get();
    }

    /**
     * Sets the maximum number of items the popup can contain.
     *
     * @param limit the maximum number of items the popup can contain. 0 meaning no limit.
     */
    public void setLimit(int limit) {
        this.limit.set(limit);
    }

    /**
     * Returns the popupSize property.
     *
     * @return the popupSize property.
     */
    public IntegerProperty popupSize() {
        return popupSize;
    }

    /**
     * The number of number of items visible in the popup without scrolling.
     *
     * @return the popup size as item count.
     */
    public int getPopupSize() {
        return popupSize.get();
    }

    /**
     * The number of number of items visible in the popup without scrolling.
     *
     * @param popupSize the popup size to set.
     */
    public void setPopupSize(int popupSize) {
        this.popupSize.set(popupSize);
    }

    public ReadOnlyObjectProperty<T> selectedItemProperty() {
        return selectedItem;
    }

    /**
     * Returns the dataToString property.
     *
     * @return the dataToString property.
     */
    public ObjectProperty<DataFormatter<T>> dataFormatterProperty() {
        return dateFormatter;
    }

    /**
     * Returns the DataFormatter used to retreive items string representation.
     *
     * @return a DataFormatter instance.
     */
    public DataFormatter<T> getDataFormatter() {
        return dateFormatter.get();
    }

    /**
     * If {@link #getDataFormatter()} is an instance of a {@link GraphicDataFormatter}, this method casts the results.
     *
     * @return the {@link GraphicDataFormatter} or null if one hasn't been set.
     */
    public GraphicDataFormatter<T> getGraphicDataFormatter() {
        return getDataFormatter() instanceof GraphicDataFormatter ? (GraphicDataFormatter<T>)getDataFormatter() : null;
    }

    /**
     * Sets the DataFormatter used to retreive items string representation.
     *
     * @param dataToString the DataFormatter instance to use for retreivi ng items string representation.
     */
    public void setDataFormatter(DataFormatter<T> dataToString) {
        this.dateFormatter.set(dataToString);
    }

    public StringProperty editorTextProperty() {
        return editorText;
    }

    /**
     * The TextField's prompt text to display, or null if no prompt text is displayed.
     *
     * @return The prompt text property.
     * @see #getPromptText()
     * @see #setPromptText(java.lang.String)
     */
    public StringProperty promptTextProperty() {
        return promptText;
    }

    /**
     * Gets the value of the property promptText.
     *
     * @return The prompt text.
     */
    public String getPromptText() {
        return promptText.get();
    }

    /**
     * Sets the value of the property promptText.
     *
     * @param promptText The prompt text.
     */
    public void setPromptText(String promptText) {
        this.promptText.set(promptText);
    }

    /**
     * Specifies how the text should be aligned when there is empty space within the TextField.
     *
     * @return The alignment property.
     * @see #getAlignment()
     * @see #setAlignment(javafx.geometry.Pos)
     */
    public ObjectProperty<Pos> alignmentProperty() {
        return alignment;
    }

    /**
     * Gets the value of the property alignment.
     *
     * @return The alignment value.
     */
    public Pos getAlignment() {
        return alignment.get();
    }

    /**
     * Sets the value of the property alignment.
     *
     * @param alignment The alignment value.
     */
    public void setAlignment(Pos alignment) {
        this.alignment.set(alignment);
    }

    /**
     * The action handler associated with this text field, or null if no action handler is assigned. The action handler
     * is normally called when the user types the ENTER key.
     *
     * @return The onAction property.
     * @see #getOnAction()
     * @see #setOnAction(javafx.event.EventHandler)
     */
    public ObjectProperty<EventHandler<ActionEvent>> onActionProperty() {
        return onAction;
    }

    /**
     * Gets the value of the property onAction.
     *
     * @return The onAction event handler.
     */
    public EventHandler<ActionEvent> getOnAction() {
        return onAction.get();
    }

    /**
     * Sets the value of the property onAction.
     *
     * @param onAction The onAction event handler.
     */
    public void setOnAction(EventHandler<ActionEvent> onAction) {
        this.onAction.set(onAction);
    }

    /**
     * The preferred number of text columns. This is used for calculating the TextField's preferred width.
     *
     * @return The prefColumnCount property.
     * @see #getPrefColumnCount()
     * @see #setPrefColumnCount(int)
     */
    public IntegerProperty prefColumnCountProperty() {
        return prefColumnCount;
    }

    /**
     * Gets the value of the property prefColumnCount.
     *
     * @return The prefColumnCount value.
     */
    public int getPrefColumnCount() {
        return prefColumnCount.get();
    }

    /**
     * Sets the value of the property prefColumnCount.
     *
     * @param prefColumnCount The prefColumnCount value.
     */
    public void setPrefColumnCount(int prefColumnCount) {
        this.prefColumnCount.set(prefColumnCount);
    }

    public DataProvider<T> getDataProvider() {
        return dataProvider.get();
    }

    public void setDataProvider(DataProvider<T> dp) {
        dataProvider.set(dp);
    }

    public ObjectProperty<DataProvider<T>> dataProviderProperty() {
        return dataProvider;
    }

    public BooleanProperty textFieldFocusedProperty() {
        return textFieldFocused;
    }

    public BooleanProperty dataProcessingProperty() {
        return dataProcessing;
    }

    public IntegerProperty prefListWidthProperty() {
        return prefListWidth;
    }

    public int getPrefListWidth() {
        return prefListWidth.getValue();
    }

    public void setPrefListWidth(int prefWidth) {
        prefListWidth.setValue(prefWidth);
    }

    public ReadOnlyStringProperty textProperty() {
        return text;
    }

    public ReadOnlyStringProperty lastTextProperty() {
        return lastText;
    }

    public String getText() {
        return text.get();
    }

    public String getLastText() {
        return text.get();
    }

    //== protected methods ============================================================================================
    /**
     * Return the path to the CSS file so things are setup right
     *
     * @return The stylesheet.
     */
    @Override
    public String getUserAgentStylesheet() {
        return "/com/anahata/jfx/internal/scene/control/" + getClass().getSimpleName() + ".css";
    }

    //== types ========================================================================================================
    public static enum Mode {
        TYPED_ONLY,
        STRING_ONLY,
        MIXED
    }

    /**
     * A simple interface to get a string representation of a given data.
     *
     * @param <T> the data type
     */
    public static interface DataProvider<T> {
        public List<T> getData(String prefix);

        public String getTaskMessage(String prefix);
    }

    /**
     * A simple interface to get a string representation of a given data.
     *
     * @param <T> the data type
     */
    public static interface DataFormatter<T> {
        /**
         * Returns a string representation of the specified object.
         *
         * @param item the object.
         * @return the specified object string representation.
         */
        String toListViewString(T item);

        /**
         * Returns a string representation for filtering of the specified object.
         *
         * @param item the object.
         * @return the specified object string representation.
         */
        default String toFilterString(T item) {
            return toListViewString(item);
        }

        /**
         * Returns a string representation of the specified object.
         *
         * @param item the object.
         * @return the specified object string representation.
         */
        String toTextFieldString(T item);
    }

    /**
     *
     * @param <T>
     */
    public static interface GraphicDataFormatter<T> extends DataFormatter<T> {
        public Node getListViewGraphic(T item);

        public Node toImageGraphic(T item);

        public int getCellHeight();
    }

    /**
     * A default implementation of
     * <code>DataFormatter</code> interface relying on objects
     * <code>toListViewString()</code> method.
     *
     * @param <T> the data type
     */
    public static final class DefaultDataFormatter<T> implements DataFormatter<T> {
        @Override
        public String toListViewString(T item) {
            return item == null ? null : item.toString();
        }

        @Override
        public String toTextFieldString(T item) {
            return item == null ? null : item.toString();
        }
    }
}

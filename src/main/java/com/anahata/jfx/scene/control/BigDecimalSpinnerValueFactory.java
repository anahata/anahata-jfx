/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.scene.control;

import com.anahata.util.math.BigDecimalUtils;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import javafx.beans.NamedArg;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.SpinnerValueFactory;
import javafx.util.StringConverter;
import static com.anahata.util.math.BigDecimalUtils.*;

/**
 *
 * @author ambasoft
 */
public class BigDecimalSpinnerValueFactory extends SpinnerValueFactory<BigDecimal> {

    /**
     * Constructs a new BigDecimalSpinnerValueFactory that sets the initial value
     * to be equal to the min value, and a default {@code amountToStepBy} of
     * one.
     *
     * @param min The minimum allowed BigDecimal value for the Spinner.
     * @param max The maximum allowed BigDecimal value for the Spinner.
     */
    public BigDecimalSpinnerValueFactory(@NamedArg("min") BigDecimal min,
            @NamedArg("max") BigDecimal max) {
        this(min, max, min);
    }

    /**
     * Constructs a new BigDecimalSpinnerValueFactory with a default
     * {@code amountToStepBy} of one.
     *
     * @param min          The minimum allowed BigDecimal value for the Spinner.
     * @param max          The maximum allowed BigDecimal value for the Spinner.
     * @param initialValue The value of the Spinner when first instantiated, must
     *                     be within the bounds of the min and max arguments, or
     *                     else the min value will be used.
     */
    public BigDecimalSpinnerValueFactory(@NamedArg("min") BigDecimal min,
            @NamedArg("max") BigDecimal max,
            @NamedArg("initialValue") BigDecimal initialValue) {
        this(min, max, initialValue, BigDecimal.ONE);
    }

    /**
     * Constructs a new BigDecimalSpinnerValueFactory.
     *
     * @param min            The minimum allowed BigDecimal value for the Spinner.
     * @param max            The maximum allowed BigDecimal value for the Spinner.
     * @param initialValue   The value of the Spinner when first instantiated, must
     *                       be within the bounds of the min and max arguments, or
     *                       else the min value will be used.
     * @param amountToStepBy The amount to increment or decrement by, per step.
     */
    public BigDecimalSpinnerValueFactory(@NamedArg("min") BigDecimal min,
            @NamedArg("max") BigDecimal max,
            @NamedArg("initialValue") BigDecimal initialValue,
            @NamedArg("amountToStepBy") BigDecimal amountToStepBy) {
        setMin(min);
        setMax(max);
        setAmountToStepBy(amountToStepBy);
        setConverter(new StringConverter<BigDecimal>() {
            private final DecimalFormat df = new DecimalFormat("#.##");

            @Override public String toString(BigDecimal value) {
                // If the specified value is null, return a zero-length String
                if (value == null) {
                    return "";
                }

                return df.format(value);
            }

            @Override public BigDecimal fromString(String value) {
                // If the specified value is null or zero-length, return null
                if (value == null) {
                    return null;
                }

                value = value.trim();

                if (value.length() < 1) {
                    return null;
                }

                // Perform the requested parsing
                return new BigDecimal(value);
            }
        });

        valueProperty().addListener((o, oldValue, newValue) -> {
            // when the value is set, we need to react to ensure it is a
            // valid value (and if not, blow up appropriately)
            if (BigDecimalUtils.lt(newValue, getMin())) {
                setValue(getMin());
            } else if (BigDecimalUtils.gt(newValue, getMax())) {
                setValue(getMax());
            }
        });
        setValue(ge(initialValue, min) && le(initialValue, max) ? initialValue : min);
    }

    /** *********************************************************************
     *                                                                     *
     * Properties *
     *                                                                     *
     ********************************************************************* */
    // --- min
    private ObjectProperty<BigDecimal> min = new SimpleObjectProperty<BigDecimal>((Object)this, "min") {
        @Override
        protected void invalidated() {
            BigDecimal currentValue = BigDecimalSpinnerValueFactory.this.getValue();
            if (currentValue == null) {
                return;
            }

            final BigDecimal newMin = (BigDecimal)get();
            if (gt(newMin, getMax())) {
                setMin(getMax());
                return;
            }

            if (lt(currentValue, newMin)) {
                BigDecimalSpinnerValueFactory.this.setValue(newMin);
            }
        }
    };

    public final void setMin(BigDecimal value) {
        min.set(value);
    }

    public final BigDecimal getMin() {
        return min.get();
    }

    /**
     * Sets the minimum allowable value for this value factory
     */
    public final ObjectProperty<BigDecimal> minProperty() {
        return min;
    }

    // --- max
    private ObjectProperty<BigDecimal> max = new SimpleObjectProperty<BigDecimal>(this, "max") {
        @Override
        protected void invalidated() {
            BigDecimal currentValue = BigDecimalSpinnerValueFactory.this.getValue();
            if (currentValue == null) {
                return;
            }

            final BigDecimal newMax = get();
            if (lt(newMax, getMin())) {
                setMax(getMin());
                return;
            }

            if (gt(currentValue, newMax)) {
                BigDecimalSpinnerValueFactory.this.setValue(newMax);
            }
        }
    };

    public final void setMax(BigDecimal value) {
        max.set(value);
    }

    public final BigDecimal getMax() {
        return max.get();
    }

    /**
     * Sets the maximum allowable value for this value factory
     */
    public final ObjectProperty<BigDecimal> maxProperty() {
        return max;
    }

    // --- amountToStepBy
    private ObjectProperty<BigDecimal> amountToStepBy = new SimpleObjectProperty<>(this, "amountToStepBy");

    public final void setAmountToStepBy(BigDecimal value) {
        amountToStepBy.set(value);
    }

    public final BigDecimal getAmountToStepBy() {
        return amountToStepBy.get();
    }

    /**
     * Sets the amount to increment or decrement by, per step.
     */
    public final ObjectProperty<BigDecimal> amountToStepByProperty() {
        return amountToStepBy;
    }

    /** {@inheritDoc} */
    @Override public void decrement(int steps) {
        final BigDecimal currentValue = getValue();
        final BigDecimal minBigDecimal = getMin();
        final BigDecimal maxBigDecimal = getMax();
        final BigDecimal amountToStepByBigDecimal = (getAmountToStepBy());
        BigDecimal newValue = currentValue.subtract(amountToStepByBigDecimal.multiply(new BigDecimal(steps)));
        setValue(newValue.compareTo(minBigDecimal) >= 0 ? newValue
                : (isWrapAround() ? wrapValue(newValue, minBigDecimal, maxBigDecimal) : getMin()));
    }

    /** {@inheritDoc} */
    @Override public void increment(int steps) {
        final BigDecimal currentValue = (getValue());
        final BigDecimal minBigDecimal = (getMin());
        final BigDecimal maxBigDecimal = (getMax());
        final BigDecimal amountToStepByBigDecimal = (getAmountToStepBy());
        BigDecimal newValue = currentValue.add(amountToStepByBigDecimal.multiply(new BigDecimal(steps)));
        setValue(newValue.compareTo(maxBigDecimal) <= 0 ? newValue
                : (isWrapAround() ? wrapValue(newValue, minBigDecimal, maxBigDecimal) : getMax()));
    }

    private static BigDecimal wrapValue(BigDecimal value, BigDecimal min, BigDecimal max) {
        if (max.doubleValue() == 0) {
            throw new RuntimeException();
        }

        // note that this wrap method differs from the others where we take the
        // difference - in this approach we wrap to the min or max - it feels better
        // to go from 1 to 0, rather than 1 to 0.05 (where max is 1 and step is 0.05).
        if (value.compareTo(min) < 0) {
            return max;
        } else if (value.compareTo(max) > 0) {
            return min;
        }
        return value;
    }

}

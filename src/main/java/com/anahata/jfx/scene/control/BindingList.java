package com.anahata.jfx.scene.control;

import com.anahata.jfx.JfxUtils;
import com.anahata.jfx.bind.*;
import com.anahata.jfx.bind.nodemodel.ManualNodeModel;
import com.anahata.jfx.binding.BooleanArrayBinding;
import com.anahata.jfx.binding.BooleanArrayBinding.BindingMode;
import com.anahata.jfx.collections.DeltaListChangeListener;
import com.anahata.util.collections.ListUtils;
import com.anahata.util.model.ActivePredicate;
import java.util.*;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;

/**
 * Displays a list of items providing binding.
 *
 * @param <C> The type of the control for each row of the list.
 * @param <M> The type of the model class in the list.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public abstract class BindingList<C extends Node & BindForm, M> extends VBox
        implements BindForm, ManualNodeModel<BindingList, SimpleListProperty<M>> {
    //== attributes ===================================================================================================

    //<editor-fold defaultstate="collapsed" desc="attributes">
    @Getter
    @Setter
    private BindForm parentBindForm;

    private final BooleanProperty formValid = new SimpleBooleanProperty(true);

    private final BooleanProperty formModified = new SimpleBooleanProperty(false);

    private BooleanArrayBinding childValid = new BooleanArrayBinding(BindingMode.AND);

    private BooleanArrayBinding childModified = new BooleanArrayBinding(BindingMode.OR);

    @Getter
    private final SimpleListProperty<M> items;

    private final BooleanProperty showDeleted = new SimpleBooleanProperty(false);

    @Getter
    @Setter
    private boolean autoFocus = true;

    @Getter
    @Setter
    private boolean addNewChildToFirst = false;

    private final ValidationsImpl validations = new ValidationsImpl(null, Collections.<BindForm>emptyList());

    private final BooleanProperty containerErrors = new SimpleBooleanProperty(false);

    private Set<Object> excludes = Collections.emptySet();

    @Getter
    @Setter
    private Binding parentBinding;

    /** The view which defines the base pane to overlay error messages onto. */
    private View view;

    private final Map<Binding, View> bindingViews = new HashMap<>();

    @Getter
    private Binder rootBinder;

    @Getter
    @Setter
    private boolean block = false;
    //</editor-fold>

    //== constructor ==================================================================================================
    protected BindingList() {
        ObservableList<M> observableList = FXCollections.observableArrayList();
        items = new SimpleListProperty<>(observableList);

        items.addListener(new DeltaListChangeListener<M>() {
            @Override
            public void onChanged(List<M> added, List<M> removed, List<M> updated) {
                if (block) {
                    return;
                }

                // Work out updated ourselves as the binder may not report updated entries.
                updated = new ArrayList<>(items.getValue());
                updated.removeAll(removed);

                // Remove first.
                for (M item : removed) {
                    removeChild(item);
                }

                // Add.
                for (M item : added) {
                    createControl(item);
                }

                // Rebind modified entries.
                for (M item : updated) {
                    bindFromModelChild(item);
                }

                rebind();
            }
        });
    }

    //== public methods ===============================================================================================
    /**
     * Add a new item.
     *
     * @return The created model.
     */
    public M addItem() {
        final M model = createModel();
        if (addNewChildToFirst) {
            items.add(0, model);
        } else {
            items.add(model);
        }
        return model;
    }

    /**
     * Remove all values from the UI side.
     */
    public void clear() {
        block = true;
        boolean parentBlock = getParentBinding().getBinder().isBlock();
        getParentBinding().getBinder().setBlock(true);

        for (C control : getChildItems()) {
            unbind(control);
        }

        getChildren().clear();
        rebind();
        block = false;
        getParentBinding().getBinder().setBlock(parentBlock);
    }

    //== BindForm =====================================================================================================
    @Override
    public void bindFromModel() {
        for (Node node : getChildren()) {
            @SuppressWarnings("unchecked")
            final C control = (C)node;
            log.debug("calling bindFromModel on control= {}", control);
            control.bindFromModel();
        }
    }

    @Override
    public void bindFromModelExcludingNode(Object excludeNode) {
        log.debug("public void bindFromModelExcludingNode(Object {}) {", excludeNode);
        if (parentBindForm != null && parentBindForm != excludeNode) {
            log.debug("BindingList {} bindFromModelExcludingNode {} on bindForm {}", this, excludeNode, parentBindForm);
            parentBindForm.bindFromModelExcludingNode(this);
        }
        //ignoring the exclude Node
        //Pablo, this was my idea to detect changes to activatable objects that implied removing the control
        //parentBindForm.getAllNodeBindings().get(this).bindFromModel();
        //but it blew up validation status for a long story reason,
        //so reverting back to the previous way (calling control.bindFromModelExcluding node
        //but adding somethign here itself to detect that a node has been removed

        List<M> newItems = new ArrayList<>();
        if (parentBinding != null) {
            //parentBinding.bindFromModel(excludeNodes);
            newItems = parentBinding.getModelValue();
            newItems = showDeleted.get() ? newItems : ActivePredicate.list((List)newItems);
        }

        Set<Object> excluded = BindUtils.getAllControllers(excludeNode);

        log.debug(
                "BindingList done with parent parentBindForm {} excludeNode = {} parentBindForm {} all excluded nodes {}",
                this, excludeNode, parentBindForm, excluded);

        for (C control : new ArrayList<>(getChildItems())) {

            M value = getModelValue(control);
            if (!newItems.contains(value)) {
                removeChild(value);
                continue;
            }
            Set<Object> allControllers = BindUtils.getAllControllers(control);
            log.debug("Processing control {}, all controllers={} ", control, allControllers);

            if (!CollectionUtils.containsAny(excluded, allControllers)) {
                log.debug("not excluding, so calling control.bindFromModelExcludingNode(excludeNode); on {}",
                        allControllers);
                control.bindFromModelExcludingNode(this);
            }

        }

    }

    @Override
    public View getView(Binding binding) {
        if (binding != null) {
            View bindingView = bindingViews.get(binding);

            if (bindingView != null) {
                return bindingView;
            }
        }

        if (view != null) {
            return view;
        }

        if (parentBindForm != null) {
            return parentBindForm.getView(binding);
        }

        throw new IllegalStateException("No View has been set on this BindingList: " + this);
    }

    @Override
    public void setView(View view, Binding binding) {
        if (binding == null) {
            this.view = view;
        } else {
            bindingViews.put(binding, view);
        }
    }

    @Override
    public void resetFormModified() {
        for (C control : getChildItems()) {
            control.resetFormModified();
        }
    }

    @Override
    public void validate(boolean publishError, Object... excludeNodes) {
        Set<Object> excluded = BindUtils.getAllControllers(excludeNodes);

        for (C control : getChildItems()) {
            if (!excluded.contains(control)) {
                control.validate(publishError, excludeNodes);
            }
        }
    }

    @Override
    public Map<Node, Binding> getAllNodeBindings() {
        Map<Node, Binding> nodes = new HashMap<>();

        for (C child : getChildItems()) {
            nodes.putAll(child.getAllNodeBindings());
        }

        return nodes;
    }

    @Override
    public BooleanProperty formValidProperty() {
        return formValid;
    }

    @Override
    public BooleanProperty formModifiedProperty() {
        return formModified;
    }

    @Override
    public BooleanProperty showContainerErrors() {
        return containerErrors;
    }

    @Override
    public void setExcludeNodes(Object... nodes) {
        log.debug("Excluding nodes: {}", nodes);
        excludes = BindUtils.getAllControllers(nodes);
    }

    @Override
    public void setRootBinder(Binder binder) {
        rootBinder = binder;
        validations.setRootBinder(rootBinder);

        for (C child : getChildItems()) {
            child.setRootBinder(binder);
        }
    }

    //== Validations ==================================================================================================
    @Override
    public void addValidationGroup(Class<?> validationGroup) {
        validations.addValidationGroup(validationGroup);
    }

    @Override
    public void removeValidationGroup(Class<?> validationGroup) {
        validations.removeValidationGroup(validationGroup);
    }

    @Override
    public BooleanProperty getValidationActive(Class<?> validationGroup) {
        return validations.getValidationActive(validationGroup);
    }

    @Override
    public ReadOnlyBooleanProperty getFormValidProperty(Class<?> validationGroup) {
        return validations.getFormValidProperty(validationGroup);
    }

    @Override
    public void setValid(String propertyName, Class<?> validationGroup) {
        validations.setValid(propertyName, validationGroup);
    }

    @Override
    public void setInvalid(String propertyName, Class<?> validationGroup) {
        validations.setInvalid(propertyName, validationGroup);
    }

    @Override
    public Set<Class<?>> getValidations() {
        return validations.getValidations();
    }

    @Override
    public void setValidationRequired() {
        validations.setValidationRequired();
    }

    //== NodeModel ====================================================================================================
    @Override
    @SuppressWarnings("unchecked")
    public SimpleListProperty<M> getNodeModelValueProperty(BindingList node) {
        return node.valueProperty();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setValue(BindingList node, Object value) {
        log.debug("Got value {}", value);
        List list = node.getItems();
        ListUtils.setAll(list, (List)value);
        node.bindFromModel();
    }

    public SimpleListProperty<M> valueProperty() {
        return items;
    }

    public BooleanProperty showDeletedProperty() {
        return showDeleted;
    }

    //== abstract methods =============================================================================================
    /**
     * Create a new model instance.
     *
     * @return The model instance.
     */
    protected abstract M createModel();

    /**
     * Create a new control instance.
     *
     * @param modelValue  The model value for the control.
     * @param showDeleted The property controlling whether to show a deleted item.
     * @return The control instance.
     */
    protected abstract C createControl(M modelValue, BooleanProperty showDeleted);

    /**
     * Get the model value from a control.
     *
     * @param control The control.
     * @return The model value.
     */
    protected abstract M getModelValue(C control);

    /**
     * Set a model value in the controller. This method is responsible for ensuring that bindFromModel() gets called.
     *
     * @param control    The control.
     * @param modelValue The model value.
     */
    protected abstract void setModelValue(C control, M modelValue);

    //== protected methods ============================================================================================
    /**
     * Unbind a control. By default this does nothing, and can be overridden to do the unbinding.
     *
     * @param control The control.
     */
    protected void unbind(C control) {
    }

    //== private methods ==============================================================================================
    @SuppressWarnings("unchecked")
    public List<C> getChildItems() {
        List<?> childItems = getChildren();
        return (List<C>)childItems;
    }

    private void rebind() {

        final ObservableList<Node> children = getChildren();
        final int arraySize = children.size();
        final BooleanExpression[] validArray = new BooleanExpression[arraySize];
        final BooleanExpression[] modifiedArray = new BooleanExpression[arraySize];

        validations.rebind(getChildItems());

        for (int i = 0; i < arraySize; i++) {
            @SuppressWarnings("unchecked")
            C control = (C)children.get(i);
            validArray[i] = control.formValidProperty();
            modifiedArray[i] = control.formModifiedProperty();
        }

        // TODO Need to clean up memory? It appears we don't have to.
        childValid = new BooleanArrayBinding(BindingMode.AND, validArray);
        childModified = new BooleanArrayBinding(BindingMode.OR, modifiedArray);
        formValid.bind(childValid);
        formModified.bind(childModified);
    }

    private C createControl(M modelValue) {
        C control = createControl(modelValue, showDeleted);
        control.setParentBindForm(this);
        control.setRootBinder(rootBinder);
        if (addNewChildToFirst) {
            getChildren().add(0, control);
        } else {
            getChildren().add(control);
        }

        if (autoFocus) {
            JfxUtils.focus(control);
        }

        return control;
    }

    private void removeChild(M modelValue) {
        for (C control : getChildItems()) {
            M controlValue = getModelValue(control);

            if (ObjectUtils.equals(modelValue, controlValue)) {
                unbind(control);
                getChildren().remove(control);
                break;
            }
        }
    }

    private void bindFromModelChild(M modelValue) {
        for (C control : getChildItems()) {
            Set<Object> allControllers = BindUtils.getAllControllers(control);

            if (!CollectionUtils.containsAny(excludes, allControllers)) {
                M controlValue = getModelValue(control);

                if (ObjectUtils.equals(modelValue, controlValue)) {
                    setModelValue(control, modelValue);
                    break;
                }
            } else {
                log.debug("Excluded child from binding: {}", control);
            }
        }
    }
}

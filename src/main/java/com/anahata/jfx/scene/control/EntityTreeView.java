/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.jfx.scene.control;

import javafx.scene.control.TreeView;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Utility methods to work with TreeViews containing {@link AbstractEntityTreeItem}(s)
 * 
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class EntityTreeView {
    /**
     * Selects the tree item for a given entity in the tree, expanding parent nodes to ensure it is visible,
     *
     * @param tree the tree containing AbstractEntityTreeItems
     * @param entity the entity we want to select
     */
    public static void selectInTree(TreeView tree, Object entity) {
        AbstractEntityTreeItem wrapper = ((AbstractEntityTreeItem)tree.getRoot()).expandPathToEntity(entity);
        if (wrapper != null) {
            log.debug("selectInTree found entity {}", entity);
            tree.getSelectionModel().select(wrapper);
            tree.requestFocus();
        } else {
            log.debug("selectInTree did not find entity {}", entity);
        }
    }

    /**
     * Returns the entity selected in the tree or null if the tree hasn't been initialised or there is no selection.
     *
     * @param tree the tree containing AbstractEntityTreeItems
     * @return the entity selected in the tree.
     */
    public static Object getTreeSelectedEntity(TreeView tree) {
        if (tree != null) {
            AbstractEntityTreeItem selectedItem = ((AbstractEntityTreeItem)tree.getSelectionModel().getSelectedItem());
            if (selectedItem != null) {
                return selectedItem.getEntity();
            }
        }
        return null;
    }
}

package com.anahata.jfx.scene.control;

import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableRow;

/**
 * A node cell for a table.
 * 
 * @param <R> The TableRow.
 * @param <M> The row type.
 * @param <T> The cell type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public abstract class ControlTableCell<R extends TableRow<M>, M, T> extends TableCell<M, T> {
    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        @SuppressWarnings("unchecked")
        final R row = (R)super.getTableRow();
        Node graphic = null;
        
        if (row != null && row.getItem() != null) {
            graphic = getGraphic(row);
        }
        
        setGraphic(graphic);
    }
    
    public abstract Node getGraphic(R row);
}

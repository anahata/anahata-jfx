package com.anahata.jfx.scene.control;

import com.anahata.util.formatting.Displayable;
import javafx.scene.control.*;
import javafx.util.Callback;
import org.apache.commons.lang3.Validate;

/**
 * A generic cell factory for classes that implement Displayable. Does not show any icon.
 *
 * @param <S> The table item type.
 * @param <T> The Displayable implementing type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class DisplayableTableCellFactory<S, T extends Displayable> implements Callback<TableColumn<S, T>, TableCell<S, T>> {     
    public static DisplayableTableCellFactory INSTANCE = new DisplayableTableCellFactory();
    @Override
    public TableCell<S, T> call(TableColumn<S,T> p) {
        return new TableCell<S, T>() {
            @Override
            protected void updateItem(T displayable, boolean empty) {
                super.updateItem(displayable, empty);
                DisplayableCell.updateItem(this, displayable, empty);
            }
        };
    }
}
package com.anahata.jfx.scene.control.util;

import com.anahata.jfx.scene.control.DisplayableListCellFactory;
import com.anahata.util.formatting.Displayable;
import java.util.Collection;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.Validate;

/**
 * General combo box utilities.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ComboBoxUtils {
    /**
     * Configure a ComboBox with elements with a cell factory to render the display value.
     *
     * @param <T>         The data type for the combo box.
     * @param comboBox    The combo box. Required.
     * @param cellFactory The cell factory. Required.
     * @throws NullPointerException If any arg is null.
     */
    public static <T> void setComboBoxCellFactory(ComboBox<T> comboBox, Callback<ListView<T>, ListCell<T>> cellFactory) {
        Validate.notNull(comboBox);
        Validate.notNull(cellFactory);
        comboBox.setCellFactory(cellFactory);
        comboBox.setButtonCell(cellFactory.call(null));
    }

//      /**
//     * Configure a ComboBox with elements with a cell factory to render the display value.
//     *
//     * @param <T>         The data type for the combo box.
//     * @param comboBox    The combo box. Required.
//     * @param cellFactory The cell factory. Required.
//     * @throws NullPointerException If any arg is null.
//     */
//    public static <T> void setAutoCompleteComboBoxCellFactory(AutoCompleteComboBox<T> comboBox, Callback<ListView<T>, ListCell<T>> cellFactory) {
//        Validate.notNull(comboBox);
//        Validate.notNull(cellFactory);
//        comboBox.setCellFactory(cellFactory);
//    }
    public static <T extends Displayable> void populateDisplayableCollectionComboBox(@NonNull ComboBox<T> comboBox,
            @NonNull Collection<T> values, boolean addNull) {
        DisplayableListCellFactory.setComboBoxCellFactory(comboBox);

        if (addNull) {
            comboBox.getItems().add(null);
        }

        comboBox.getItems().addAll(values);
    }

    /**
     * Populate a ComboBox with values from an enum that implements Displayable, which provides a display value. Set a
     * cell renderer on the ComboBox to display the enums correctly (without an icon).
     *
     * @param <T>       The enum type that implements Displayable.
     * @param comboBox  The combo box. Required.
     * @param enumClass The enum class to get values from. Required.
     * @param addNull   Set to true to add a null value as the first value.
     * @throws NullPointerException If comboBox or enumClass are null.
     */
    public static <T extends Enum & Displayable> void populateDisplayableEnumComboBox(ComboBox<T> comboBox,
            Class<T> enumClass, boolean addNull) {
        Validate.notNull(comboBox);
        Validate.notNull(enumClass);
        DisplayableListCellFactory.setComboBoxCellFactory(comboBox);

        if (addNull) {
            comboBox.getItems().add(null);
        }

        comboBox.getItems().addAll(enumClass.getEnumConstants());
    }

    /*
     * Shows the combobox popup on focus of combobox.
     */
    public static void showListOnFocus(ComboBox... nodes) {
        for (ComboBox node : nodes) {
            node.focusedProperty().addListener((obs, old, focused) -> {
                if (focused) {
                    node.show();
                }
            });
        }
    }

//      /**
//     * Populate a ComboBox with values from an enum that implements Displayable, which provides a display value. Set a
//     * cell renderer on the ComboBox to display the enums correctly (without an icon).
//     *
//     * @param <T>       The enum type that implements Displayable.
//     * @param comboBox  The combo box. Required.
//     * @param enumClass The enum class to get values from. Required.
//     * @param addNull   Set to true to add a null value as the first value.
//     * @throws NullPointerException If comboBox or enumClass are null.
//     */
//    public static <T extends Enum & Displayable> void populateDisplayableEnumAutoCompleteComboBox(AutoCompleteComboBox<T> comboBox,
//            Class<T> enumClass, boolean addNull) {
//        Validate.notNull(comboBox);
//        Validate.notNull(enumClass);
//        DisplayableListCellFactory.setComboBoxCellFactory(comboBox);
//
//        if (addNull) {
//            comboBox.getItems().add(null);
//        }
//
//        comboBox.getItems().addAll(enumClass.getEnumConstants());
//    }
}

/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;
import java.util.function.Consumer;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Window;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * A collection of utilities specifically for JavaFX 8 dialog API.
 *
 * @author sai.dandem
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class DialogUtils {

    public static void showError(Window owner, String title, String headerText, String contentText) {
        showAlert(owner, AlertType.ERROR, title, headerText, contentText);
    }

    public static void showInformation(Window owner, String title, String headerText, String contentText) {
        showAlert(owner, AlertType.INFORMATION, title, headerText, contentText);
    }

    public static void showWarning(Window owner, String title, String headerText, String contentText) {
        showAlert(owner, AlertType.WARNING, title, headerText, contentText);
    }

    public static void showAlert(Window owner, AlertType alertType, String title, String headerText, String contentText) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        //http://stackoverflow.com/questions/28937392/javafx-alerts-and-their-size
        alert.getDialogPane().getChildren().stream().filter(node -> node instanceof Label).forEach(
                node -> ((Label)node).setMinHeight(Region.USE_PREF_SIZE));
        alert.setResizable(true);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.initOwner(owner);
        alert.showAndWait();
    }

    public static Optional<ButtonType> showConfirmation(Window owner, String title, String headerText,
            String contentText) {
        return showConfirmation(owner, title, headerText, contentText, new ButtonType[0]);
    }

    public static Optional<ButtonType> showConfirmation(Window owner, String title, String headerText,
            String contentText,
            ButtonType... buttonTypes) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        if (buttonTypes != null && buttonTypes.length > 0) {
            alert.getButtonTypes().setAll(buttonTypes);
        }
        //http://stackoverflow.com/questions/28937392/javafx-alerts-and-their-size
        alert.getDialogPane().getChildren().stream().filter(node -> node instanceof Label).forEach(
                node -> ((Label)node).setMinHeight(Region.USE_PREF_SIZE));
        alert.initModality(Modality.WINDOW_MODAL);
        alert.initOwner(owner);
        alert.setResizable(true);
        Optional<ButtonType> ret = alert.showAndWait();
        return ret;
    }

    public static void showExceptionDialog(Window owner, Throwable ex, String title) {
        showExceptionDialog(owner, ex, title, title);
    }

    public static void showExceptionDialog(Window owner, Throwable ex, String title, String headerText) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(ex.getMessage());

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setPrefWidth(700);
        alert.getDialogPane().setExpandableContent(expContent);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.initOwner(owner);
        alert.showAndWait();
    }

    public static String showTextAreaDialog(Window owner, String title, String headerText, String contentLabel,
            String content) {
        TextAreaDialog dialog = new TextAreaDialog(content);
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentLabel);
        Optional<String> result = dialog.showAndWait();
        return result.isPresent() ? result.get() : null;
    }

    public static String showTextInputDialog(Window owner, String title, String headerText, String contentLabel,
            String content) {
        TextInputDialog dialog = new TextInputDialog(content);
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentLabel);
        Optional<String> result = dialog.showAndWait();
        return result.isPresent() ? result.get() : null;
    }

    public static Alert createAlertWithCheckBox(AlertType type, String title, String headerText,
            String message, String checkBoxText, boolean checkBoxSelected, Consumer<Boolean> checkBoxAction,
            ButtonType... buttonTypes) {
        Alert alert = new Alert(type);
        alert.getDialogPane().applyCss();
        Node graphic = alert.getDialogPane().getGraphic();
        // Create a new dialog pane that has a checkbox instead of the hide/show details button
        // Use the supplied callback for the action of the checkbox
        alert.setDialogPane(new DialogPane() {
            @Override
            protected Node createDetailsButton() {
                CheckBox checkBox = new CheckBox();
                checkBox.setSelected(checkBoxSelected);
                checkBox.setText(checkBoxText);
                checkBox.setOnAction(e -> checkBoxAction.accept(checkBox.isSelected()));
                return checkBox;
            }
        });
        alert.getDialogPane().getButtonTypes().addAll(buttonTypes);
        alert.getDialogPane().setContentText(message);
        alert.getDialogPane().setExpandableContent(new Group());
        alert.getDialogPane().setExpanded(true);
        alert.getDialogPane().setGraphic(graphic);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        return alert;
    }
}

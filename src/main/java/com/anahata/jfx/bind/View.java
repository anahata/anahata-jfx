package com.anahata.jfx.bind;

import com.anahata.jfx.message.Growl;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.layout.Pane;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * A view defines a base screen of information.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor
public class View {
    /** The base pane for the view. */
    @Getter
    private Pane pane;
    
    private BooleanProperty valid = new SimpleBooleanProperty();
    
    private Growl growl;
    
    public View(@NonNull Pane pane) {
        this.pane = pane;
    }
    
    public Growl getGrowl() {
        if (growl == null) {
            growl = new Growl(pane);
        }
        return growl;
    }
    
    public BooleanProperty validProperty() {
        return valid;
    }
    
    public boolean isValid() {
        return valid.get();
    }
    
    public void setValid(boolean valid) {
        this.valid.set(valid);
    }
}

package com.anahata.jfx.bind;

import com.anahata.jfx.binding.BooleanArrayBinding;
import com.anahata.jfx.binding.BooleanArrayBinding.BindingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * Validation status of properties for validation groups.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class ValidationStatus {
    @Setter
    private Binder rootBinder;
    
    @Getter
    private final Class<?> validationGroup;

    @Getter
    private final SetProperty<String> invalidProperties = new SimpleSetProperty<>(
            FXCollections.observableSet(new HashSet<String>()));
    
    private BooleanArrayBinding formsValid;

    private final BooleanProperty valid = new SimpleBooleanProperty(true);
    
    private final BooleanProperty active = new SimpleBooleanProperty(true);
    
    private Collection<? extends BindForm> bindForms;
    
    public ValidationStatus(@NonNull Binder rootBinder, @NonNull Class<?> validationGroup,
            @NonNull Collection<? extends BindForm> newBindForms) {
        this.rootBinder = rootBinder;
        this.validationGroup = validationGroup;
        bindForms = newBindForms;
        List<ReadOnlyBooleanProperty> binds = new ArrayList<>();

        for (BindForm bindForm : newBindForms) {
            bindForm.addValidationGroup(validationGroup);
            bindForm.getValidationActive(validationGroup).bind(active);
            binds.add(bindForm.getFormValidProperty(validationGroup));            
        }

        formsValid = new BooleanArrayBinding(BindingMode.AND, binds);
        valid.bind(active.not().or(invalidProperties.emptyProperty().and(formsValid)));
        
        active.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                ValidationStatus.this.rootBinder.setValidationRequired();
            }
        });
    }
    
    public void rebind(Collection<? extends BindForm> newBindForms) {
        Validate.notNull(newBindForms);
        valid.unbind();
        
        for (BindForm bindForm : bindForms) {
            if (bindForm.getValidations().contains(validationGroup)) {
                bindForm.getValidationActive(validationGroup).unbind();
            } 
        }
        
        List<ReadOnlyBooleanProperty> binds = new ArrayList<>();
        bindForms = newBindForms;

        for (BindForm bindForm : newBindForms) {
            bindForm.addValidationGroup(validationGroup);
            bindForm.getValidationActive(validationGroup).bind(active);
            binds.add(bindForm.getFormValidProperty(validationGroup));            
        }

        formsValid = new BooleanArrayBinding(BindingMode.AND, binds);
        valid.bind(active.not().or(invalidProperties.emptyProperty().and(formsValid)));
    }
    
    public ReadOnlyBooleanProperty validProperty() {
        return valid;
    }
    
    public boolean isValid() {
        return valid.get();
    }
    
    public boolean isFormsValid() {
        return formsValid.get();
    }
    
    public boolean isPropertiesValid() {
        return invalidProperties.isEmpty();
    }
    
    public void unbind() {
        valid.unbind();
    }
    
    public void setValid(String propertyName) {
        Validate.notNull(propertyName);
        invalidProperties.remove(propertyName);
    }
    
    public void setInvalid(String propertyName) {
        Validate.notNull(propertyName);
        invalidProperties.add(propertyName);
    }
    
    public BooleanProperty activeProperty() {
        return active;
    }
    
    public void clearInvalidProperties() {
        invalidProperties.clear();
    }
    
    public List<BindForm> getInvalidForms() {
        Stream<BindForm> s = (Stream) bindForms.stream();
        s = s.filter((bf) -> !bf.getFormValidProperty(validationGroup).get());
        return s.collect(Collectors.toList());        
    }
    
    public List<BindForm> getValidForms() {
        Stream<BindForm> s = (Stream) bindForms.stream();
        s = s.filter((bf) -> bf.getFormValidProperty(validationGroup).get());
        return s.collect(Collectors.toList());        
    }
}

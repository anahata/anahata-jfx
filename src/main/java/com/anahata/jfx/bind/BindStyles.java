package com.anahata.jfx.bind;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * JavaFX CSS styles used in binding.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BindStyles {
    /**
     * Save is invalid due to validation errors.
     */
    public static final String SAVE_INVALID = "jfxBindSaveInvalid";

    /**
     * A given action is invalid due to validation errors.
     */
    public static final String ACTION_INVALID = "jfxBindActionInvalid";

    /**
     * Tab is invalid due to validation errors.
     */
    public static final String TAB_INVALID = "jfxBindTabInvalid";
}

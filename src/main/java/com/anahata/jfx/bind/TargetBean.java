/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javafx.beans.property.ObjectProperty;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Stores bound bean information.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@EqualsAndHashCode(of = "id")
@Slf4j
public class TargetBean {
    private final Validator validator;

    @Getter
    private final String id;

    @Getter
    private final ObjectProperty<?> bean;

    @Getter
    private final boolean autoBind;

    @Getter
    @Setter
    private boolean requiresValidation = true;

    private Set<ConstraintViolation<Object>> validations = Collections.emptySet();
    
    @Getter
    private Set<ConstraintViolation<Object>> unpublishedValidations = Collections.emptySet();

    public TargetBean(Validator validator, String id, ObjectProperty<?> bean, boolean autoBind) {
        this.validator = validator;
        this.id = id;
        this.bean = bean;
        this.autoBind = autoBind;
    }
    
    public void validationConstraintIgnored(ConstraintViolation<Object> sv) {
        unpublishedValidations.remove(sv);
        log.debug("validationIgnored {}, unpublished {}", sv, unpublishedValidations);
    }
    
    public void validationContraintPublished(ConstraintViolation<Object> sv) {
        unpublishedValidations.remove(sv);
        log.debug("validationPublished {}, unpublished {}", sv, unpublishedValidations);
    }
    
    public Set<ConstraintViolation<Object>> validate(Class<?>... groups) {
        long ts = System.currentTimeMillis();

        if (requiresValidation) {
            if (bean.getValue() == null) {
                validations = Collections.emptySet();                
            } else {
                log.trace("Validating {} ", bean.getValue());
                validations = validator.validate(bean.getValue(), groups);
            }
            
            unpublishedValidations = new HashSet(validations);
            requiresValidation = false;
            log.trace("validation = {} ms. contsraints {}", System.currentTimeMillis() - ts, validations);
        }

        
        return validations;
    }
}

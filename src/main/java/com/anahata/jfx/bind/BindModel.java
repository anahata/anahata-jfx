package com.anahata.jfx.bind;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Identifies a model field for binding. The model field must be an ObjectProperty.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface BindModel {
    public static final String DEFAULT_ID = "default";
    
    /** @return The id to be linked to. */
    String id() default DEFAULT_ID;
    
    /**
     * If set to true, the binder will auto-bind to the UI whenever the model is set. Defaults to true.
     * 
     * @return The autoBind status.
     */
    boolean autoBind() default true;
}

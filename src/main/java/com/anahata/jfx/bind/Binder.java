package com.anahata.jfx.bind;

import com.anahata.jfx.JfxUtils;
import com.anahata.jfx.ReadOnlyUtils;
import com.anahata.jfx.bind.converter.Converter;
import com.anahata.jfx.bind.filter.KeystrokeFilter;
import com.anahata.jfx.bind.table.BindTable;
import com.anahata.jfx.bind.table.BindingTableView;
import com.anahata.jfx.binding.BooleanArrayBinding;
import com.anahata.jfx.binding.BooleanArrayBinding.BindingMode;
import com.anahata.jfx.config.JavaFXConfig;
import com.anahata.jfx.message.JfxMessages;
import com.anahata.util.metamodel.MetaModelProperty;
import com.anahata.util.reflect.ReflectionUtils;
import java.lang.reflect.Field;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import static com.anahata.jfx.bind.Bind.*;
import java.util.*;
import javafx.beans.property.*;
import lombok.*;

/**
 * Manages binding a set of properties from a bean to multiple destination model beans.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Dependent
@Slf4j
public class Binder implements BindForm {
    //== attributes ===================================================================================================

    @Inject
    private JfxMessages jfxMessages;

    @Inject
    private Validator validator;

    @Inject
    private JavaFXConfig config;

    @Getter
    private Object controller;

    private List<Field> controllerFields;

    /** Beans to bind to, keyed by an id. */
    @Getter(AccessLevel.PACKAGE)
    private final Map<String, TargetBean> targetBeans = new HashMap<>();

    @Getter(AccessLevel.PACKAGE)
    private final List<Binding> bindings = new ArrayList<>(40);

    private Map<Object, Binding> bindingsByNode;

    private final Map<String, List<Binding>> bindingsById = new HashMap<>();

    private final BooleanProperty formValid = new SimpleBooleanProperty(true);

    private final BooleanProperty thisFormValid = new SimpleBooleanProperty(true);

    private final BooleanProperty globalValid = new SimpleBooleanProperty(true);

    private final BooleanProperty formModified = new SimpleBooleanProperty(false);

    private final BooleanProperty thisFormModified = new SimpleBooleanProperty(false);

    private final SetProperty<String> invalidProperties = new SimpleSetProperty<>(FXCollections.observableSet(
            new HashSet<String>()));

    private final ObjectProperty<BindingPhase> bindingPhase = new SimpleObjectProperty<>(BindingPhase.INIT);

    private final List<BindForm> bindForms = new ArrayList<>();

    @Getter
    @Setter
    private boolean block;

    @Getter
    @Setter
    private BindForm parentBindForm;

    @Getter
    @Setter
    private Binding parentBinding;

    private ValidationsImpl validations;

    private final BooleanProperty containerErrors = new SimpleBooleanProperty(false);

    /**
     * The view which defines the base pane to overlay error messages onto.
     */
    private View view;

    private final Map<Binding, View> bindingViews = new HashMap<>();

    private final Map<Pane, View> subViews = new HashMap<>();

    @Getter
    @Setter
    private Binder rootBinder;

    @Getter
    @Setter
    private Node focusedField;

    private List<BindListener> listeners = new ArrayList<>();

    //== initialization ===============================================================================================
    public Binder() {
        thisFormValid.bind(invalidProperties.emptyProperty());
    }

    //== public methods ===============================================================================================
    /**
     * Initialize a controller with target beans.
     *
     * @param controller The controller. Required.
     */
    @SuppressWarnings("unchecked")
    public void init(Object controller) {
        Validate.notNull(controller);
        log.debug("init: ENTRY controller={}", controller.getClass().getSimpleName());
        rootBinder = this;
        this.controller = controller;
        controllerFields = ReflectionUtils.getAllDeclaredFields(controller.getClass());
        ObservableValue<? extends Boolean> readOnly = null;
        final List<Node> readOnlyNodes = new ArrayList<>();
        final List<TableView> readOnlyTables = new ArrayList<>();

        for (final Field field : controllerFields) {
            BindModel bindModel = field.getAnnotation(BindModel.class);

            if (bindModel != null) {
                Validate.validState(ObjectProperty.class.isAssignableFrom(field.getType()),
                        "@BindModel field %s is not an ObjectProperty", field.getName());
                final ObjectProperty targetBean = ReflectionUtils.readField(controller, ObjectProperty.class, field);
                Validate.validState(targetBean != null, "@BindModel field %s is null", field.getName());
                Validate.validState(!targetBeans.containsKey(bindModel.id()),
                        "A @BindModel with id %s has already been defined, field %s.%s is invalid",
                        bindModel.id(), controller.getClass().getSimpleName(), field.getName());
                targetBeans.put(bindModel.id(), new TargetBean(validator, bindModel.id(), targetBean,
                        bindModel.autoBind()));
            }

            BindView bindView = field.getAnnotation(BindView.class);

            if (bindView != null) {
                Validate.validState(Pane.class.isAssignableFrom(field.getType()),
                        "@BindView field %s is not a Pane", field.getName());
                Pane pane = ReflectionUtils.readField(controller, Pane.class, field);
                Validate.validState(pane != null, "@BindView field %s is null", field.getName());
                Validate.validState(view == null,
                        "A @BindView has already been defined, use @BindSubView for child views; node={}",
                        pane);
                log.debug("Registering view with pane: {}", pane);
                view = new View(pane);
            }

            BindReadOnly bindReadOnly = field.getAnnotation(BindReadOnly.class);

            if (bindReadOnly != null) {
                Validate.validState(Node.class.isAssignableFrom(field.getType()),
                        "@BindReadOnly field %s is not a Node", field.getName());
                Node node = ReflectionUtils.readField(controller, Node.class, field);
                Validate.validState(node != null, "@BindReadOnly field %s is null", field.getName());

                if (TableView.class.isAssignableFrom(field.getType())) {
                    readOnlyTables.add((TableView)node);
                } else {
                    readOnlyNodes.add(node);
                }
            }

            BindReadOnlyModel bindReadOnlyModel = field.getAnnotation(BindReadOnlyModel.class);

            if (bindReadOnlyModel != null) {
                Validate.validState(ObservableValue.class.isAssignableFrom(field.getType()),
                        "@BindReadOnlyModel field %s is not an ObservableValue<Boolean>", field.getName());
                Validate.validState(readOnly == null,
                        "A @BindReadOnlyModel has been defined twice, second one is for field %s",
                        field.getName());
                readOnly = ReflectionUtils.readField(controller, ObservableValue.class, field);
            }
        }

        Validate.validState(!targetBeans.isEmpty(), "At least one target bean is required");

        for (final Field field : controllerFields) {
            Bind bind = field.getAnnotation(Bind.class);
            BindTable bindTable = field.getAnnotation(BindTable.class);

            Validate.validState(bind == null || bindTable == null,
                    "Both @Bind and @BindTable cannot be declared on the same property: %s", field.getName());

            if (bind != null) {
                Validate.validState(!TableView.class.isAssignableFrom(field.getType()),
                        "Property for @Bind cannot be a TableView: %s", field.getName());
                Validate.validState(bind.property() != null && bind.property().length >= 1,
                        "At least one property must be defined for @Bind");

                final MetaModelProperty[] mmFields = new MetaModelProperty[bind.property().length];

                for (int j = 0; j < bind.property().length; j++) {
                    mmFields[j] = ReflectionUtils.getInstance(bind.property()[j]);
                }

                final Class declaringClass = mmFields[0].getDeclaringClass();
                final TargetBean targetBean = targetBeans.get(bind.id());

                Validate.notNull(targetBean, "Could not find bean for class %s while initialsing controller %s",
                        declaringClass.getName(), controller);

                if (targetBean != null) {
                    final Class<? extends Converter> converterClass = bind.converter();
                    Converter converter;

                    try {
                        converter = ReflectionUtils.getInstanceIfNotBase(Converter.class, converterClass);
                    } catch (Exception e) {
                        // Try an inner class.
                        converter = ReflectionUtils.getInstanceIfNotBase(Converter.class, converterClass, controller);
                    }

                    final Class<? extends KeystrokeFilter> filterClass = bind.keystrokeFilter();
                    final KeystrokeFilter filter = ReflectionUtils.getInstanceIfNotBase(KeystrokeFilter.class,
                            filterClass);
                    final Object node = ReflectionUtils.readField(controller, Object.class, field);
                    final Class[] args = ReflectionUtils.getGenericArgs(field);
                    final Binding binding = new Binding(this, validator, node, args == null ? null : args[0],
                            targetBean, mmFields, converter, filter, jfxMessages,
                            bind.readOnly(),
                            bind.id(),
                            bind.bindFromModelOnChange());
                    bindings.add(binding);
                    List<Binding> idBindings = bindingsById.get(bind.id());

                    if (idBindings == null) {
                        idBindings = new ArrayList<>();
                        bindingsById.put(bind.id(), idBindings);
                    }

                    idBindings.add(binding);

                    if (BindForm.class.isAssignableFrom(field.getType())) {
                        final BindForm bindForm = (BindForm)node;
                        log.debug("Found BindForm {}, setting parent bind form as {}", field, getBestParentBindForm());
                        bindForm.setParentBindForm(this);
                        bindForm.setParentBinding(binding);
                        bindForm.setRootBinder(rootBinder);
                        bindForms.add(bindForm);
                    }
                }
            } else if (bindTable != null) {
                Validate.validState(TableView.class.isAssignableFrom(field.getType()),
                        "Property for @BindTable must be a TableView: %s", field.getName());
                Validate.validState(bindTable.property() != null && bindTable.property().length >= 1,
                        "At least one property must be defined for @BindTable");

                final MetaModelProperty[] mmFields = new MetaModelProperty[bindTable.property().length];

                for (int j = 0; j < bindTable.property().length; j++) {
                    mmFields[j] = ReflectionUtils.getInstance(bindTable.property()[j]);
                }

                final Class declaringClass = mmFields[0].getDeclaringClass();
                final TargetBean targetBean = targetBeans.get(bindTable.id());
                final TableView tableView = ReflectionUtils.readField(controller, TableView.class, field);

                Validate.notNull(targetBean, "Could not find bean for class %s while initialsing controller %s",
                        declaringClass.getName(), controller);

                BindingTableView bindingTableView = new BindingTableView(tableView, bindTable.tableRow());
                bindingTableView.setParentBindForm(this);
                bindForms.add(bindingTableView);
                final Binding binding = new Binding(this, validator, tableView, null, targetBean,
                        mmFields, null, null, jfxMessages, true, bindTable.id(), true);
                bindingTableView.setParentBinding(binding);
                bindingTableView.setRootBinder(rootBinder);
                bindings.add(binding);
                List<Binding> idBindings = bindingsById.get(bindTable.id());

                if (idBindings == null) {
                    idBindings = new ArrayList<>();
                    bindingsById.put(bindTable.id(), idBindings);
                }

                idBindings.add(binding);
            }

            // Bind the valid and modified properties to any child BindForm implementations.
            refreshBindFormBindings();

            ObjectProperty defaultModelBeanProperty = getModelBeanProperty(DEFAULT_ID);
            BindSave bindSave = field.getAnnotation(BindSave.class);

            if (bindSave != null) {
                Button button = ReflectionUtils.readField(controller, Button.class, field);
                JfxUtils.bindStyleClass(button, formValid, null, BindStyles.SAVE_INVALID);

                button.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<Event>() {
                    @Override
                    public void handle(Event event) {
                        if (!formValid.get()) {
                            validate(true);
                            event.consume();
                        }
                    }
                });

                if (defaultModelBeanProperty != null) {
                    button.disableProperty().bind(defaultModelBeanProperty.isNull());
                }
            }

            BindRevert bindDiscard = field.getAnnotation(BindRevert.class);

            //disabling refresh if the record is not null, shouldn't we be doing this for save as well?
            if (bindDiscard != null) {
                Button button = ReflectionUtils.readField(controller, Button.class, field);
                if (defaultModelBeanProperty != null) {
                    button.disableProperty().bind(defaultModelBeanProperty.isNull());
                }
            }
        }

        for (final TargetBean targetBean : targetBeans.values()) {
            if (targetBean.isAutoBind()) {
                targetBean.getBean().addListener(new InvalidationListener() {
                    @Override
                    public void invalidated(Observable o) {
                        //Pablo:looks like when setting the model to a different subclass, 
                        //dirty validation constraints from a previous class are not cleaned up
                        //for Example RoomPrivateSession was in the model with a validation constraint of
                        //RoomPrivateSession.room. Changed the model LapDancePrivateSession and 
                        //invalidProperties= SetProperty [value: [room]] was still displayed, 
                        //Goran please review

                        //Pablo 16/3 changing to bindFromModel as it is calling invalidProperties.clear();
                        //and the list of invalidProperties is shared across all targetBeans.                        
                        validations.clear();
                        bindFromModel();

//                        targetBean.setRequiresValidation(true);
//                        final List<Binding> bindingsList = bindingsById.get(targetBean.getId());
//
//                        if (bindingsList != null) {
//                            log.debug("calling bindFromModelList bean= {}", targetBean.getBean().get());
//                            bindFromModelList(bindingsList);
//                        }
                    }
                });
            }
        }

        // Map controls to bindings.
        bindingsByNode = new HashMap<>(bindings.size());

        for (Binding binding : bindings) {
            bindingsByNode.put(binding.getNode(), binding);
        }

        // Configure child BindForm objects for binding of validation groups.
        validations = new ValidationsImpl(rootBinder, bindForms);

        bindingPhase.set(BindingPhase.USER_INPUT);

        // Setup all BindSubView annotations.
        for (final Field field : controllerFields) {
            BindSubView bindSubView = field.getAnnotation(BindSubView.class);

            if (bindSubView != null) {
                Validate.validState(Pane.class.isAssignableFrom(field.getType()),
                        "@BindSubView field %s is not a Pane", field.getName());
                Pane pane = ReflectionUtils.readField(controller, Pane.class, field);
                Validate.validState(pane != null, "@BindSubView field %s is null", field.getName());
                View subView = new View(pane);
                subViews.put(pane, subView);
            }
        }

        // Set up read only node processing.
        if (readOnly != null) {
            log.debug("init: Have a read only model, processing read only nodes");
            // TODO now only need one list
            ReadOnlyUtils.bindReadOnlyList(readOnly, readOnlyNodes);
            ReadOnlyUtils.bindReadOnlyList(readOnly, readOnlyTables);
        }

        log.debug("init: RETURN, controller={}", controller.getClass().getSimpleName());
    }

    public void addListener(@NonNull BindListener listener) {
        listeners.add(listener);
    }

    /**
     * Bind model values to the view.
     */
    @Override
    public void bindFromModel() {
        bindFromModel(true);
    }

    /**
     * Bind model values to the view.
     *
     * @param clearMessages
     */
    public void bindFromModel(boolean clearMessages) {
        bindingPhase.set(BindingPhase.BIND_FROM_MODEL);
        setValidationRequired();

        if (clearMessages) {
            for (Binding binding : bindings) {
                binding.clearMessages();
            }
        }

        globalValid.set(true);
        invalidProperties.clear();
        //validations.clear();

        getRootBinder().preBindFromModel();

        for (Binding binding : bindings) {
            binding.bindFromModel();
        }

        validate(false);
        refreshSubViewValid();
        bindingPhase.set(BindingPhase.USER_INPUT);
    }

    /**
     * Bind a specific set of nodes.
     *
     * @param nodes The nodes. Required.
     * @throws NullPointerException If nodes is null.
     */
    public void bindFromModel(Object... nodes) {
        Validate.notNull(nodes);
        bindingPhase.set(BindingPhase.BIND_FROM_MODEL);
        globalValid.set(true);
        setValidationRequired();
        getRootBinder().preBindFromModel();

        for (Object node : nodes) {
            Binding binding = bindingsByNode.get(node);

            if (binding != null) {
                binding.bindFromModel();
            }
        }

        validate(false);
        bindingPhase.set(BindingPhase.USER_INPUT);
    }

    private void bindFromModelList(List<Binding> bindingList) {
        bindingPhase.set(BindingPhase.BIND_FROM_MODEL);
        globalValid.set(true);
        setValidationRequired();
        getRootBinder().preBindFromModel();

        for (Binding binding : bindingList) {
            binding.bindFromModel();
        }

        validate(false);
        bindingPhase.set(BindingPhase.USER_INPUT);
    }

    /**
     * A property indicating if the form is valid.
     *
     * @return The form valid property.
     */
    @Override
    public BooleanProperty formValidProperty() {
        return formValid;
    }

    public boolean isFormValid(){
        return formValid.get();
    }
    /**
     * A property indicating if the form was modified.
     *
     * @return The form modified property.
     */
    @Override
    public BooleanProperty formModifiedProperty() {
        return formModified;
    }

    @Override
    public Map<Node, Binding> getAllNodeBindings() {
        Map<Node, Binding> nodes = new HashMap<>();

        for (Binding binding : bindings) {
            if (binding.getNode() instanceof Node) {
                nodes.put((Node)binding.getNode(), binding);
            }
        }

        for (BindForm bindForm : bindForms) {
            nodes.putAll(bindForm.getAllNodeBindings());
        }

        return nodes;
    }

    /**
     * Get the valid property of a property name. Returns null if not found.
     *
     * @param node The node.
     * @return The valid or invalid property value.
     */
    public BooleanProperty getValidProperty(Object node) {
        Binding binding = bindingsByNode.get(node);
        return binding != null ? binding.validProperty() : null;
    }

    /**
     * Get the enabled property of a property name. Returns null if not found.
     *
     * @param node The node.
     * @return The enabled or disabled property value.
     */
    public BooleanProperty getInContextProperty(Object node) {
        Binding binding = bindingsByNode.get(node);
        return binding != null ? binding.inContextProperty() : null;
    }

    /**
     * Get the property that sets whether lists only show active entries.
     *
     * @param node The node.
     * @return The active property.
     */
    public BooleanProperty getActiveOnlyProperty(Object node) {
        Binding binding = bindingsByNode.get(node);
        return binding != null ? binding.activeOnlyProperty() : null;
    }

    @Override
    public void validate(boolean publishError, Object... excludeNodes) {
        Set<Object> excluded = BindUtils.getAllControllers(excludeNodes);

        log.debug("{} validating publishErrors={}", controller.getClass().getSimpleName(), publishError);
        //log.debug("excluding= {}", Arrays.asList(excludeNodes));

        for (Binding binding : bindings) {

            if (!excluded.contains(binding.getNode())) {
                log.debug(" {} validating binding {} prop {} publishErrors={}", controller.getClass().getSimpleName(),
                        binding, binding.getPropertyName(), publishError);
                binding.validate(publishError);
            } else {
                log.debug("{} not validating binding {} prop publishErrors={}", controller.getClass().getSimpleName(),
                        binding, binding.getPropertyName(), publishError);
            }
        }

        for (BindForm bindForm : bindForms) {
            Binding pb = bindForm.getParentBinding();
            if ((pb == null || pb.isInContext()) && !excluded.contains(bindForm)) {
                log.debug("{} validating bindForm = {} parentB {} publishErrors={}",
                        controller.getClass().getSimpleName(), bindForm, pb, publishError);
                bindForm.validate(publishError, excludeNodes);
            } else {
                log.debug("not {} validating bindForm = {} parentB {} inContext ={} excluded = {} publishErrors={}",
                        controller.getClass().getSimpleName(), bindForm, pb, pb.inContextProperty(),
                        excluded.contains(
                                bindForm), publishError);
            }
        }

        if (publishError) {
            for (TargetBean tb : getTargetBeans().values()) {
                for (ConstraintViolation cv : tb.getUnpublishedValidations()) {
                    log.info("Unpublished constraint violation {} ", cv.getPropertyPath());
                    if (view != null) {
                        //jfxMessages.addMessage(view, null, JfxMessage.error(cv.getMessage()));
                    } else {

                    }
                }
            }
        }

    }

    @Override
    public void bindFromModelExcludingNode(Object node) {
        log.debug("bindFromModelExcludingNode node={}, parentBindForm={}, controller={}", node, parentBindForm,
                controller);
        Validate.notNull(node);

        //Fix for long story issue of moneyField not binding upwards.
        if (parentBindForm != null && parentBindForm != node) {
            log.debug("{} Calling bindFromModelExcludingNode on {}", this, parentBindForm);
            parentBindForm.bindFromModelExcludingNode(controller);
        } else {
            log.debug("Binder for controller {} doesn't have a parent bind form", controller);
        }
        log.debug("{} bindFromModelExcludingNode parentBindForm done, binding individual bindings, excluded node = {}",
                this, node);

        Set<Object> excluded = BindUtils.getAllControllers(node);
        bindingPhase.set(BindingPhase.BIND_FROM_MODEL);

        for (Binding binding : bindings) {
            if (!excluded.contains(binding.getNode())) {
                log.debug("{} bindFromModelExcludingNode calling binding.bindFromModel on node = {}", this, node);
                binding.bindFromModel(node);
            }
        }

        // passing null for validation so it does not exclude it from validating phase
        // when it is excluded it does not clear old validation errors
        validate(false, (Object)null);
        bindingPhase.set(BindingPhase.USER_INPUT);
    }

    @Override
    public BooleanProperty showContainerErrors() {
        return containerErrors;
    }

    @Override
    public void setExcludeNodes(Object... nodes) {
    }

    @Override
    public void addValidationGroup(Class<?> validationGroup) {
        validations.addValidationGroup(validationGroup);
    }

    @Override
    public void removeValidationGroup(Class<?> validationGroup) {
        validations.removeValidationGroup(validationGroup);
    }

    @Override
    public BooleanProperty getValidationActive(Class<?> validationGroup) {
        return validations.getValidationActive(validationGroup);
    }

    @Override
    public ReadOnlyBooleanProperty getFormValidProperty(Class<?> validationGroup) {
        return validations.getFormValidProperty(validationGroup);
    }

    @Override
    public void setValid(String propertyName, Class<?> validationGroup) {
        validations.setValid(propertyName, validationGroup);
    }

    @Override
    public void setInvalid(String propertyName, Class<?> validationGroup) {
        validations.setInvalid(propertyName, validationGroup);
    }

    @Override
    public Set<Class<?>> getValidations() {
        return validations.getValidations();
    }

    public ReadOnlyObjectProperty<BindingPhase> bindingPhaseProperty() {
        return bindingPhase;
    }

    public BindingPhase getBindingPhase() {
        return bindingPhase.get();
    }

    @Override
    public View getView(Binding binding) {
        if (binding != null) {
            View bindingView = bindingViews.get(binding);

            if (bindingView != null) {
                return bindingView;
            }

            if (binding.getNode() instanceof Node) {
                Node node = (Node)binding.getNode();

                for (View subView : subViews.values()) {
                    if (JfxUtils.isInAncestor(node, subView.getPane())) {
                        return subView;
                    }
                }
            }
        }

        if (view != null) {
            return view;
        }

        if (parentBindForm != null) {
            return parentBindForm.getView(binding);
        }

        throw new IllegalStateException("No View has been set on this binder: " + this);
    }

    @Override
    public void setView(View view, Binding binding) {
        if (binding == null) {
            this.view = view;
        } else {
            bindingViews.put(binding, view);
        }
    }

    /**
     * Get the subview for the given pane.
     *
     * @param pane The pane. Required.
     * @return The subview. Null if not defined.
     * @throws NullPointerException If pane is null.
     */
    public View getSubView(Pane pane) {
        Validate.notNull(pane);
        return subViews.get(pane);
    }

    @Override
    public void resetFormModified() {
        thisFormModified.set(false);

        for (BindForm bindForm : bindForms) {
            bindForm.resetFormModified();
        }
    }

    public boolean isThisFormValid() {
        return thisFormValid.get();
    }

    public boolean isThisFormModified() {
        return thisFormModified.get();
    }

    public void setThisFormModified(boolean modified) {
        thisFormModified.set(modified);
    }

    public void addBindForm(BindForm bindForm) {
        Validate.notNull(bindForm, "A BindForm is required");
        bindForm.setParentBindForm(this);
        bindForms.add(bindForm);
        refreshBindFormBindings();
    }

    public <T extends BindForm> void addBindForms(List<T> bindForms) {
        Validate.notNull(bindForms, "bindForms can not be null");
        for (T bindForm : bindForms) {
            bindForm.setParentBindForm(this);
            this.bindForms.add(bindForm);
        }
        refreshBindFormBindings();
    }

    public <T extends BindForm> void removeBindForms(List<T> bindForms) {
        Validate.notNull(bindForms, "bindForms can not be null");
        for (T bindForm : bindForms) {
            Validate.isTrue(bindForms.contains(bindForm), "The BindForm was not found: %s", bindForm);
            bindForm.setParentBindForm(null);
        }
        this.bindForms.removeAll(bindForms);
        refreshBindFormBindings();
    }

    public void removeBindForm(BindForm bindForm) {
        Validate.notNull(bindForm, "A BindForm is required");

        bindForm.setParentBindForm(null);
        bindForms.remove(bindForm);
        refreshBindFormBindings();
    }

    /**
     * Get the target bean for the given bean id.
     *
     * @param id The bean id. Required.
     * @return The target bean.
     * @throws NullPointerException If id is null.
     */
    public Object getTargetBean(String id) {
        Validate.notNull(id);
        TargetBean tb = targetBeans.get(id);
        return tb == null ? null : tb.getBean().get();
    }

    /**
     * Focus to the last edited field, if set.
     *
     * @param runLater Set to true to run it later.
     */
    public void focusLastEdited(boolean runLater) {
        JfxUtils.focusNicely(getRootBinder().getFocusedField(), runLater);
    }

    //== package methods ==============================================================================================
    void setValid(String propertyName) {
        log.trace("setValid propertyName= {}", propertyName);
        invalidProperties.remove(propertyName);
        log.trace("invalidProperties= {}", invalidProperties);
    }

    void setInvalid(String propertyName) {
        log.trace("setInvalid propertyName= {}", propertyName);
        invalidProperties.add(propertyName);
        log.trace("invalidProperties= {}", invalidProperties);
    }

    void setGlobalValid() {
        globalValid.set(true);
    }

    void setGlobalInvalid() {
        globalValid.set(false);
    }

    void setFormModified(Binding binding) {
        if (bindingPhase.get() == BindingPhase.USER_INPUT) {
            thisFormModified.set(true);
        }

        bindFromModelExcludingBinding(binding);
        //getBaseBinder().bindFromModel(false);
        getBaseBinder().refreshSubViewValid();
    }

    @Override
    public void setValidationRequired() {
        log.trace("setValidationRequired");

        for (TargetBean tb : targetBeans.values()) {
            log.trace("setValidationRequired: Setting on target bean id={}", tb.getId());
            tb.setRequiresValidation(true);
        }

        for (BindForm bindForm : bindForms) {
            log.trace("setValidationRequired: Setting on BindForm {}", bindForm.getClass().getSimpleName());
            bindForm.setValidationRequired();
        }
    }

    //== private methods ==============================================================================================
    //== private methods ==============================================================================================
    public Binder getBaseBinder() {
        Binder current = this;
        BindForm parent = getParentBindForm();

        while (parent != null) {
            if (parent instanceof Binder) {
                current = (Binder)parent;
            }

            parent = parent.getParentBindForm();
        }

        Validate.validState(current instanceof Binder, "Could not find the base binder");
        return current;
    }

    private void refreshSubViewValid() {
        for (View subView : subViews.values()) {
            List<Node> nodes = JfxUtils.getAllNodes(subView.getPane());
            Map<Node, Binding> binds = getAllNodeBindings();
            subView.setValid(true);

            for (Node node : nodes) {
                Binding binding = binds.get(node);

                if (binding != null && !binding.getValid().get()) {
                    subView.setValid(false);
                    break;
                }
            }
        }
    }

    private void bindFromModelExcludingBinding(Binding excludeBinding) {
        log.debug("bindFromModelExcludingBinding: ENTRY excludeBinding.propertyName={}",
                excludeBinding != null ? excludeBinding.getPropertyName() : "{null}");

        if (parentBindForm != null) {
            parentBindForm.bindFromModelExcludingNode(controller);
        } else {
            log.debug("Binder for controller {} doesn't have a parent bind form", controller);
        }

        bindingPhase.set(BindingPhase.BIND_FROM_MODEL);
        globalValid.set(true);

        for (Binding binding : bindings) {
            if (!binding.equals(excludeBinding)) {
                binding.bindFromModel();
            }
        }

        validateExcludingBinding(false, excludeBinding);
        bindingPhase.set(BindingPhase.USER_INPUT);
    }

    private void validateExcludingBinding(boolean publishError, Binding excludeBinding) {
        for (Binding binding : bindings) {
            if (!excludeBinding.equals(binding)) {
                binding.validate(publishError);
            }
        }

        for (BindForm bindForm : bindForms) {
            if (!excludeBinding.getNode().equals(bindForm)) {
                bindForm.validate(publishError);
            }
        }
    }

    private void refreshBindFormBindings() {
        formValid.unbind();
        formModified.unbind();

        if (!bindForms.isEmpty()) {
            final int bindFormsSize = bindForms.size();
            final BooleanExpression[] validArray = new BooleanExpression[bindFormsSize];
            final BooleanExpression[] modifiedArray = new BooleanExpression[bindFormsSize];

            for (int j = 0; j < bindFormsSize; j++) {
                validArray[j] = bindForms.get(j).formValidProperty();
                modifiedArray[j] = bindForms.get(j).formModifiedProperty();
            }

            final BooleanArrayBinding childValid = new BooleanArrayBinding(BindingMode.AND, validArray);
            final BooleanArrayBinding childModified = new BooleanArrayBinding(BindingMode.OR, modifiedArray);
            formValid.bind(childValid.and(thisFormValid).and(globalValid));
            formModified.bind(childModified.or(thisFormModified));
        } else {
            formValid.bind(thisFormValid.and(globalValid));
            formModified.bind(thisFormModified);
        }
    }

    private void preBindFromModel() {
        for (BindListener listener : listeners) {
            listener.preBindFromModel();
        }
    }

    public BindForm getBestParentBindForm() {
        return controller instanceof BindForm ? (BindForm)controller : parentBindForm;
    }

    public ObjectProperty getModelBeanProperty(String id) {
        return targetBeans.get(id).getBean();
    }

    //== classes ======================================================================================================
    public static enum BindingPhase {
        INIT,
        BIND_FROM_MODEL,
        USER_INPUT,
        BIND_TO_MODEL;
    }
}

package com.anahata.jfx.bind;

import com.anahata.jfx.bind.converter.Converter;
import com.anahata.jfx.bind.filter.KeystrokeFilter;
import com.anahata.util.metamodel.MetaModelProperty;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Identifies a JavaBean property for auto-bind.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Bind {
    
    public static final String DEFAULT_ID = "default";
    
    /**
     * @return The property being bound to. If more than one is provided, it is a nested property. Required.
     */
    Class<? extends MetaModelProperty>[] property();
    
    /** @return The id of the @BindModel this property is linked to. */
    String id() default DEFAULT_ID;
    
    /**
     * @return An optional converter for binding.
     */
    Class<? extends Converter> converter() default Converter.class;
    
    /**
     * @return An optional keystroke filter.
     */
    Class<? extends KeystrokeFilter> keystrokeFilter() default KeystrokeFilter.class;
    
    /**
     * @return true if this is to be read only. Defaults to false.
     */
    boolean readOnly() default false;
    
    boolean bindFromModelOnChange() default true;
}

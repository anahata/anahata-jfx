package com.anahata.jfx.bind;

import com.anahata.jfx.bind.Binder.BindingPhase;
import com.anahata.jfx.bind.converter.Converter;
import com.anahata.jfx.bind.converter.ConverterFactory;
import com.anahata.jfx.bind.filter.KeystrokeFilter;
import com.anahata.jfx.bind.nodemodel.ManualNodeModel;
import com.anahata.jfx.bind.nodemodel.NodeModel;
import com.anahata.jfx.bind.nodemodel.NodeModelFactory;
import com.anahata.jfx.message.JfxMessage;
import com.anahata.jfx.message.JfxMessages;
import com.anahata.jfx.scene.control.AutoCompleteTextField;
import com.anahata.jfx.scene.control.CalendarTextField;
import com.anahata.jfx.validator.FieldValidator;
import com.anahata.jfx.validator.FieldValidatorFactory;
import com.anahata.util.collections.ListUtils;
import com.anahata.util.lang.builder.DelimitedStringBuilder;
import com.anahata.util.metamodel.MetaModelProperty;
import com.anahata.util.metamodel.MetaModelUtils;
import com.anahata.util.model.ActivatableUtils;
import com.anahata.util.model.ActivePredicate;
import com.anahata.util.reflect.AnahataPropertyUtils;
import com.anahata.util.reflect.ReflectionUtils;
import com.anahata.util.validation.ConditionalValidation;
import com.anahata.util.validation.ValidationUtils;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.ListExpression;
import javafx.beans.binding.SetExpression;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import javax.validation.metadata.BeanDescriptor;
import javax.validation.metadata.ConstraintDescriptor;
import javax.validation.metadata.PropertyDescriptor;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Binds a JavaFX node to an attribute of a bean.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@EqualsAndHashCode(of = "node")
@Getter(AccessLevel.PACKAGE)
@Slf4j
public class Binding {
    //== constants ====================================================================================================

    //<editor-fold defaultstate="collapsed" desc="constants">
    /** Binding logger. */
    private static final Logger blog = LoggerFactory.getLogger(Binding.class.getName() + "#bind");

    /** Validation logger. */
    private static final Logger vlog = LoggerFactory.getLogger(Binding.class.getName() + "#validate");

    /** Timing logger. */
    private static final Logger tlog = LoggerFactory.getLogger(Binding.class.getName() + "#timing");
    //</editor-fold>

    //== attributes ===================================================================================================
    //<editor-fold defaultstate="collapsed" desc="attributes">
    private Validator validator;

    private Object node;

    private Class nodeType;

    private TargetBean targetBean;

    private MetaModelProperty[] properties;

    private Class beanClass;

    private String propertyName;

    private Class propertyType;

    private PropertyDescriptor valPropDesc;

    private Converter converter;

    private KeystrokeFilter filter;

    private boolean writeable;

    private NodeModel nodeModel;

    private JfxMessages jfxMessages;

    private boolean childValidation;

    private boolean block = false;

    private BooleanProperty valid = new SimpleBooleanProperty(true);

    @Getter
    private Binder binder;

    private boolean observableList;

    private boolean collection;

    private boolean set;

    private boolean conditional;

    private FieldValidator fieldValidator;

    private boolean nodeIsBindForm;

    private BooleanProperty activeOnly = new SimpleBooleanProperty(false);

    private BooleanProperty inContext = new SimpleBooleanProperty();

    private String modelId;

    private boolean bindFromModelOnChange;
    //</editor-fold>

    //== init =========================================================================================================
    /**
     * Construct a binding between a JavaFX node (usually a control) and a model field.
     *
     * @param binder          The parent binder.
     * @param validator       The bean validator in this context.
     * @param node            The JavaFX node (control) being bound to.
     * @param nodeGenericType The generic type of the JavaFX node, if present. If not, this is null.
     * @param targetBean      The target model bean being bound to.
     * @param properties      The MetaModelField describing the attribute or method being accessed.
     * @param converter       The converter for the binding, null if there is none.
     * @param filter          Keystroke filter.
     * @param jfxMessages     Global messages for error messaging.
     * @param readOnly        Whether the binding is read only or not.
     * @throws NullPointerException If any arg except converter is null.
     */
    @SuppressWarnings("unchecked")
    Binding(Binder binder, Validator validator, Object node, Class nodeGenericType, TargetBean targetBean,
            MetaModelProperty[] properties, Converter converter, KeystrokeFilter filter, JfxMessages jfxMessages,
            boolean readOnly, String modelId, boolean bindFromModelOnChange) {
        Validate.notNull(binder);
        Validate.notNull(validator);
        Validate.notNull(node,
                "bean attribute was null when binding %s.%s on bean %s; If the field is a JavaFX node, It should match the fx:id "
                + "on the fxml if loading from FXML file. If the field is a JavaFX property or it is not loaded "
                + "from fxml, then it should be programatically instantiated iether at the field level or on the init() method.",
                properties[0].getDeclaringClass().getSimpleName(), properties[0].getName(),
                (targetBean != null ? targetBean.getClass().getSimpleName() : " <null> "));
        Validate.notNull(properties);
        Validate.notNull(jfxMessages);
        Validate.notNull(modelId);

        this.modelId = modelId;
        this.bindFromModelOnChange = bindFromModelOnChange;
        this.binder = binder;
        this.validator = validator;
        this.node = node;
        this.targetBean = targetBean;
        final MetaModelProperty rootProp = properties[0];
        final MetaModelProperty leafProp = properties[properties.length - 1];
        this.beanClass = rootProp.getDeclaringClass();
        this.propertyType = leafProp.getType(); // lowest level has type
        this.propertyName = MetaModelUtils.getNestedPropertyName(properties);
        this.filter = filter;
        java.beans.PropertyDescriptor beanPropDesc = AnahataPropertyUtils.getPropertyDescriptor(
                leafProp.getDeclaringClass(), leafProp.getName());
        Validate.notNull(beanPropDesc, "Could not find property descriptor for leafProperty %s.%s. "
                + "Most likely the metamodel object is in a stale state. "
                + "Try a clean and build of the project containg the class %s and clean and build of the java-fx project",
                leafProp.getDeclaringClass(), leafProp.getName(), beanClass);
        this.writeable = beanPropDesc.getWriteMethod() != null && !readOnly;

        final BeanDescriptor valBeanDesc = validator.getConstraintsForClass(beanClass);
        this.valPropDesc = valBeanDesc.getConstraintsForProperty(leafProp.getName());
        this.jfxMessages = jfxMessages;
        childValidation = valPropDesc != null && valPropDesc.isCascaded();
        conditional = ConditionalValidation.class.isAssignableFrom(beanClass);
        nodeModel = NodeModelFactory.getNodeModel(node);

        if (nodeModel == null) {
            throw new IllegalArgumentException("Unsupported node type: " + node.getClass().getName());
        }

        refreshContext();

        this.targetBean.getBean().addListener(new InvalidationListener() {
            @Override
            public void invalidated(javafx.beans.Observable o) {
                refreshContext();
            }
        });

        // If there is a required field validation, style the node for it.
        if (valPropDesc != null && node instanceof Node) {
            for (ConstraintDescriptor cd : valPropDesc.getConstraintDescriptors()) {
                if (isRequiredValidation(cd.getAnnotation())) {
                    Node n = (Node)node;
                    n.getStyleClass().add("requiredField");
                    break;
                }
            }
        }

        final Property value = nodeModel.getNodeModelValueProperty(node);
        Validate.notNull(value, "nodeMode %s returned null value Property for node %s", nodeModel, node);
        nodeIsBindForm = BindUtils.isBindForm(node);

        if (writeable) {
            if (value instanceof ObservableList) {
                ObservableList list = (ObservableList)value;
                list.addListener(new ListPropertyChangeListener());
            } else if (value instanceof ObservableSet) {
                ObservableSet oset = (ObservableSet)value;
                oset.addListener(new SetPropertyChangeListener());
            } else if (!nodeIsBindForm) {
                value.addListener(new PropertyChangeListener());

                if (node instanceof CalendarTextField) {
                    final CalendarTextField n = (CalendarTextField)node;
                    n.textFieldFocusedProperty().addListener(new FocusListener());
                } else if (node instanceof DatePicker) {
                    final DatePicker dp = (DatePicker)node;
                    dp.focusedProperty().addListener(new FocusListener());
                } else if (node instanceof AutoCompleteTextField) {
                    // The AutoCompleteTextField requires access to the text component.
                    final AutoCompleteTextField n = (AutoCompleteTextField)node;
                    n.textFieldFocusedProperty().addListener(new FocusListener());
                } else if (node instanceof Node) {
                    final Node n = (Node)node;
                    n.focusedProperty().addListener(new FocusListener());
                }
            }

            if (filter != null && node instanceof Node) {
                ((Node)node).addEventFilter(KeyEvent.KEY_TYPED, new StringControlFilter());
            }
        }

        // Work out the node type.
        Class[] args = ReflectionUtils.getGenericArgs(Property.class, value.getClass());
        Validate.notNull(args, "Could not get generic args for the node model of property %s", propertyName);
        Validate.isTrue(args.length == 1,
                "Invalid generic args for the node model of property %s, required only 1, had %d",
                propertyName, args.length);
        nodeType = args[0];

        if (Object.class.equals(nodeType) && nodeGenericType != null) {
            nodeType = nodeGenericType;
        }

        if (converter == null) {
            this.converter = ConverterFactory.getDefaultConverter(propertyType, nodeType);
        } else {
            this.converter = converter;
        }

        observableList = Collection.class.isAssignableFrom(propertyType) && value instanceof ObjectProperty;
        collection = Collection.class.isAssignableFrom(propertyType) && value instanceof ListExpression;
        set = Set.class.isAssignableFrom(propertyType) && value instanceof SetExpression;
        fieldValidator = FieldValidatorFactory.getFieldValidator(propertyType);
    }

    //== package methods ==============================================================================================
    /**
     * Indicates if the field is currently valid (has passed validation).
     *
     * @return The boolean property.
     */
    BooleanProperty validProperty() {
        return valid;
    }

    /**
     * Determine the binding is currently in context. This can be false if the target bean's runtime instance has
     * changed and the bean class for this binding no longer applies.
     *
     * @return true if in context, false if not.
     */
    boolean isInContext() {
        refreshContext();
        return inContext.get();
    }

    BooleanProperty inContextProperty() {
        return inContext;
    }

    /**
     * Bind from the model to the UI.
     *
     * @param excludeNodes The nodes to exclude from binding.
     */
    @SuppressWarnings("unchecked")
    public void bindFromModel(Object... excludeNodes) {
        if (targetBean.getBean().getValue() == null || block) {
            return;
        }

        if (!isInContext()) {
            blog.debug("MODEL -> UI: {}.{} Not binding as target bean class not compatible: {}",
                    beanClass.getSimpleName(), propertyName, targetBean.getBean().get().getClass().getSimpleName());
            return;
        }

        block = true;
        final Object modelValue = getModelValue();

        blog.debug("MODEL -> UI: {}.{}={} bean identityHashCode={} value identityHashCode={}",
                beanClass.getSimpleName(), propertyName, modelValue,
                System.identityHashCode(targetBean.getBean()), System.identityHashCode(modelValue));

        if (nodeIsBindForm) {
            //should de exclude nodes property
            BindUtils.getBindForm(node).setExcludeNodes(excludeNodes);
            log.debug("node is bind form bindFromModel call on= {}", node);
            BindUtils.getBindForm(node).bindFromModel();
        }

        if (observableList) {
            if (nodeModel instanceof ManualNodeModel) {
                Collection coll = (Collection)modelValue;
                ManualNodeModel mnm = (ManualNodeModel)nodeModel;
                mnm.setValue(node, activeOnly.get() ? ActivePredicate.list(coll) : coll);
            } else {
                final ObjectProperty<ObservableList> value = (ObjectProperty<ObservableList>)nodeModel.getNodeModelValueProperty(
                        node);
                ObservableList list = value.get();

                if (list == null) {
                    list = FXCollections.observableArrayList();
                    value.set(list);
                }

                List modelList = (List)modelValue;

                if (modelList == null) {
                    list.clear();
                } else if (activeOnly.get()) {
                    ListUtils.setAll(list, ActivePredicate.list(modelList));
                } else {
                    ListUtils.setAll(list, modelList);
                }
            }
        } else if (collection) {
            if (nodeModel instanceof ManualNodeModel) {
                Collection coll = (Collection)modelValue;
                ManualNodeModel mnm = (ManualNodeModel)nodeModel;
                mnm.setValue(node, activeOnly.get() ? ActivePredicate.list(coll) : coll);
            } else {
                final ListPropertyBase value = (ListPropertyBase)nodeModel.getNodeModelValueProperty(node);
                ObservableList list = value.get();

                if (list == null) {
                    list = FXCollections.observableArrayList();
                    value.set(list);
                }

                List modelList = (List)modelValue;

                if (modelList == null) {
                    list.clear();
                } else if (activeOnly.get()) {
                    ListUtils.setAll(list, ActivePredicate.list(modelList));
                } else {
                    ListUtils.setAll(list, modelList);
                }
            }
        } else if (set) {
            SetPropertyBase value = (SetPropertyBase)nodeModel.getNodeModelValueProperty(node);
            ObservableSet oset = value.get();

            if (oset == null) {
                oset = FXCollections.observableSet();
                value.set(oset);
            }

            Set currSet = (Set)modelValue;
            oset.clear();

            if (currSet != null) {
                if (activeOnly.get()) {
                    oset.addAll(ActivePredicate.list(currSet));
                } else {
                    oset.addAll(currSet);
                }
            }
        } else {
            try {
                Property p = nodeModel.getNodeModelValueProperty(node);
                Object o = p.getValue();
                blog.trace("MODEL -> UI: {}.{}=curr = {} new = {} curr identityHashCode={} new identityHashCode={}",
                        beanClass.getSimpleName(), propertyName, o, modelValue,
                        System.identityHashCode(o), System.identityHashCode(modelValue));
                
                //this is just to workaournd a TextArea quirk
                if (modelValue instanceof String && ((String)modelValue).equals("") 
                        && o instanceof String && ((String)o).equals("")
                        && node instanceof TextArea) {
                    //p.setValue(modelValue);
                    //ignore 
                } else {
                    p.setValue(modelValue);
                }
            } catch (Exception e) {
                throw new RuntimeException("Exception binding " + propertyName + " from model", e);
            }

        }

        block = false;
    }

    void clearMessages() {
        if (block) {
            return;
        }

        log.trace("clearMessages calling valid.set(true)");
        valid.set(true);
        log.trace("clearMessages calling binder.setValid({})", propertyName);
        binder.setValid(propertyName);

        if (node instanceof Node) {
            jfxMessages.clearMessage((Node)node);
        }
    }

    void validate(boolean publishError) {
        long ts1 = System.currentTimeMillis();

        if (block) {
            vlog.debug("{}.{} Exiting due to blocked", beanClass.getSimpleName(), propertyName);
            return;
        }

        if (targetBean.getBean().getValue() == null) {
            vlog.debug("{}.{} Exiting due to null targetBean value", beanClass.getSimpleName(), propertyName);
            return;
        }

        if (!isInContext()) {
            vlog.debug("{}.{} Not validating as target bean class not compatible: {}",
                    beanClass.getSimpleName(), propertyName, targetBean.getBean().get().getClass().getSimpleName());
            return;
        }

        // If the node implements BindForm, don't try to validate it here. Instead, validation will cascade down to it
        // from Binder#validate(boolean). This is only done when @Valid is not present. If @Valid is present
        // (childValidation == true), we will still validate it to get a validation at this level of the model.
        if (nodeIsBindForm && !childValidation) {
            vlog.debug("{}.{} Exiting due to BindForm without child validation,", beanClass.getSimpleName(),
                    propertyName);
            return;
        }

        final Object value = targetBean.getBean().getValue();

        // If the object is Activatable, and inactive, ignore validations as it's "deleted".
        if (ActivatableUtils.isInactive(value)) {
            vlog.debug("{}.{} Exiting due to inactive", beanClass.getSimpleName(), propertyName);
            clearMessages();
            return;
        }

        vlog.trace("{}.{} Validating", beanClass.getSimpleName(), propertyName);

        // Look for class level, field specific validators, which have access to the entire object to validate
        // across other fields. The validators must generate a property path that matches the property name. If
        // found, these will be processed as a field level error.
        // All validations are processed at once. All active validation groups are passed to the validator assigned
        // to the target bean.
        final Class<?>[] standardGroups = ValidationUtils.getValidationGroups(value);
        final Set<Class<?>> otherGroups = new HashSet<>();

        for (Class<?> group : binder.getValidations()) {
            if (binder.getValidationActive(group).get()) {
                otherGroups.add(group);
            }
        }

        final Set<Class<?>> groups = new HashSet<>();
        groups.addAll(Arrays.asList(standardGroups));
        groups.addAll(otherGroups);
        Class<?>[] groupsArray = new Class<?>[groups.size()];
        groupsArray = groups.toArray(groupsArray);

        final Set<ConstraintViolation<Object>> cvs = new HashSet<>();

        //Pablo: this call to targetBean.validate is bad
        //it is passing a groupsArray that may or may not be used if the validations were cached
        //moreover it should be done at the Binder or BindForm level, rather than on a binding per binding basis
        final Set<ConstraintViolation<Object>> classViolations = targetBean.validate(groupsArray);
        long ts2 = System.currentTimeMillis();
        tlog.trace("initial validation = {} ms", ts2 - ts1);

        //Pablo: I don't get this
        if (binder.getParentBinding() != null) {
            final Object parentTargetBean = binder.getParentBinding().getTargetBean().getBean().get();

            if (parentTargetBean != null) {
                Set<ConstraintViolation<Object>> violations = binder.getParentBinding().getTargetBean().validate(
                        groupsArray);

                for (ConstraintViolation<Object> cv : violations) {
                    // Only validate active entries.

                    if (ActivatableUtils.isActive(cv.getLeafBean())) {
                        String valPropName = ValidationUtils.getValidationPropertyName(cv);
                        String firstProp = StringUtils.substringBefore(valPropName, ".");
                        String lastProp = StringUtils.substringAfterLast(valPropName, ".");

                        if (firstProp.equals(binder.getParentBinding().getPropertyName()) && lastProp.equals(
                                propertyName)) {
                            cvs.add(cv);
                        }
                    }
                }
            }

            tlog.trace("parent validation = {} ms", System.currentTimeMillis() - ts2);
        }

        for (ConstraintViolation<Object> cv : classViolations) {
            // Only validate active entries.

            if (ActivatableUtils.isActive(cv.getLeafBean())) {
                String valPropNameCleaned = ValidationUtils.getValidationPropertyName(cv);
                log.debug("valPropNameCleaned={}", valPropNameCleaned);
                if (valPropNameCleaned.startsWith(propertyName)) {
                    String valPropNameRaw = cv.getPropertyPath().toString();
                    log.debug("valPropName={}", valPropNameRaw);
                    if (valPropNameRaw.contains("[") && nodeIsBindForm && collection) {
                        targetBean.validationConstraintIgnored(cv);
                    } else {
                        cvs.add(cv);
                    }
                }
            }
        }

        vlog.trace("cvs= {}", cvs);
        vlog.trace("classViolations= {}", classViolations);
        vlog.trace("bean= {}", targetBean.getBean());
        // Loop through violations, set invalid status for groups encountered.
        binder.setValid(propertyName);

        for (Class<?> group : groups) {
            if (!Default.class.equals(group)) {
                binder.setValid(propertyName, group);
            }
        }

        Set<Node> valNodes = new HashSet<>();

        if (!cvs.isEmpty()) {
            if (vlog.isDebugEnabled()) {
                vlog.debug("Validation errors ({}) for {} {}.{} {}:", publishError ? "published" : "unpublished",
                        objectToString(node), beanClass.getSimpleName(), propertyName,
                        targetBean.getBean().get().toString());
            }

            boolean hasError = false;

            for (ConstraintViolation<Object> cv : cvs) {

                Set<Class<?>> cvGroups = cv.getConstraintDescriptor().getGroups();

                if (vlog.isDebugEnabled()) {
                    DelimitedStringBuilder sb = new DelimitedStringBuilder(", ");

                    for (Class<?> group : cvGroups) {
                        sb.append(group.getSimpleName());
                    }

                    vlog.debug("  Root={} Leaf={} Path={} Groups={} Message={}",
                            cv.getRootBeanClass().getName(), cv.getLeafBean().getClass().getName(),
                            cv.getPropertyPath(), sb.toString(), cv.getMessage());
                }

                //Pablo: If this binding represents a collection and one of the elements in the collection 
                //is not active, and the bindForm backing this collection binding 
                //has showContainerErrors set to false
                //the binder gets marked as invalid, yet the validation error is not published.
                Set<Class<?>> tgroups = new HashSet<>(groups);

                for (Class<?> group : tgroups) {
                    log.debug("Group {}", group);
                    if (cvGroups.contains(group)) {
                        log.debug("Group {} contains ", group);
                        if (Default.class.equals(group)) {
                            binder.setInvalid(propertyName);
                            valid.set(false);
                        } else {
                            binder.setInvalid(propertyName, group);
                        }

                        hasError = true;
                        groups.remove(group);
                    }
                }
            }

            long ts3 = System.currentTimeMillis();

            if (hasError && publishError && !(nodeIsBindForm && !BindUtils.getBindForm(node).showContainerErrors().get())) {
                final Node valNode = node instanceof Node ? (Node)node : null;

                for (ConstraintViolation cv : cvs) {
                    log.debug("adding message {} to node {}", valNode, cv.getMessage());
                    jfxMessages.addMessage(binder.getView(this), valNode, JfxMessage.error(cv.getMessage()));
                    valNodes.add(valNode);
                    targetBean.validationContraintPublished(cv);
                }
            } else {
                log.debug("Not adding message hasError={} publish={} {} {}", hasError, publishError,
                        targetBean.getBean().get(), cvs);
            }

            tlog.trace("publishing of error = {} ms", System.currentTimeMillis() - ts3);
        } else {
            long ts3 = System.currentTimeMillis();
            clearMessages();
            tlog.trace("Clearing of messages = {} ms", System.currentTimeMillis() - ts3);
        }

        if (node instanceof Node && valNodes.isEmpty() && publishError) {
            vlog.debug("Clearing message for {} {}.{}", objectToString(node), beanClass.getSimpleName(),
                    propertyName);
            jfxMessages.clearMessage((Node)node);
        }

        long ts6 = System.currentTimeMillis();
        tlog.trace("total time = {} ms", ts6 - ts1);
    }

    BooleanProperty activeOnlyProperty() {
        return activeOnly;
    }

    //== private methods ==============================================================================================
    /**
     * Get a value from the model being bound to. This will auto-cast the return value. Converters handle the case of
     * model values that need translation.
     *
     * @param <T> The return type.
     * @return The model value.
     */
    @SuppressWarnings("unchecked")
    public <T> T getModelValue() {
        Object value;

        try {
            value = PropertyUtils.getProperty(targetBean.getBean().getValue(), propertyName);
        } catch (NestedNullException e) {
            // If there is a null along the nested properties, just return null.
            value = null;
        } catch (NoSuchMethodException e) {
            log.info("Could not fetch property {} due to NoSuchMethodException", propertyName);
            // If there is a null along the nested properties, just return null.
            value = null;
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(
                    "Exception fetching property " + propertyName + " from model on bean: " + targetBean.getBean().getValue(),
                    e);
        }

        if (converter != null) {
            try {
                value = converter.getAsNodeModelValue(node, value);
            } catch (Throwable e) {
                throw new RuntimeException(
                        "Exception converting model value " + value + " to node value for property " + propertyName + " on Bean " + targetBean.getBean().getValue() + " node=" + node,
                        e);
            }
        }

        return (T)value;
    }

    /**
     * Prepare a value to bind from the UI to the model. This will invoke a converter if necessary.
     *
     * @param value The UI value to bind.
     * @return The model value to bind.
     */
    @SuppressWarnings("unchecked")
    private Object prepareModelValue(Object value) {
        if (converter != null) {
            value = converter.getAsDomainModelValue(node, value);
        }

        return value;
    }

    private void setInvalidWarn(String message) {
        jfxMessages.addMessage(binder.getView(this), (Node)node, JfxMessage.warn(message));
    }

    private boolean isRequiredValidation(Annotation annotation) {
        return annotation instanceof NotNull || annotation instanceof NotEmpty || annotation instanceof NotBlank;
    }

    private String objectToString(Object obj) {
        if (obj == null) {
            return "{null}";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(obj.getClass().getSimpleName());

        if (obj instanceof Node) {
            Node n = (Node)obj;

            if (!StringUtils.isBlank(n.getId())) {
                sb.append("[id=");
                sb.append(n.getId());
                sb.append("]");
            }
        }

        return sb.toString();
    }

    @SuppressWarnings("unchecked")
    private void refreshContext() {
        inContext.set(this.targetBean.getBean().get() != null ? beanClass.isAssignableFrom(
                this.targetBean.getBean().get().getClass())
                : false);
    }

    //== classes ======================================================================================================
    /**
     * Filter keystrokes.
     */
    private class StringControlFilter implements EventHandler<KeyEvent> {

        @Override
        public void handle(KeyEvent keyEvent) {
            if (!isInContext()) {
                return;
            }

            char c = keyEvent.getCharacter().charAt(0);
            boolean defined = Character.isDefined(c) && c != 8 && c != 9;

            if (defined && !keyEvent.isAltDown() && !keyEvent.isControlDown() && !keyEvent.isMetaDown()
                    && !keyEvent.isShiftDown() && !keyEvent.isShortcutDown()) {
                final String message = filter.filterKeystrokes(keyEvent.getCharacter());

                if (message != null) {
                    keyEvent.consume();
                    setInvalidWarn(message);
                }
            }
        }
    }

    private class PropertyChangeListener implements ChangeListener {

        @Override
        public void changed(ObservableValue ov, Object oldValue, Object newValue) {
            long ts1 = System.currentTimeMillis();

            if (!isInContext() || block || binder.isBlock() || binder.getBindingPhase() == BindingPhase.BIND_FROM_MODEL) {
                return;
            }

            // UI level data type validation.
            if (fieldValidator != null && newValue instanceof String) {
                final String message = fieldValidator.validate((String)newValue);

                if (message != null) {
                    valid.set(false);
                    binder.setInvalid(propertyName);
                    jfxMessages.addMessage(binder.getView(Binding.this), (Node)node, JfxMessage.error(message));
                    return;
                }
            }

            long ts2 = System.currentTimeMillis();
            final Object value = prepareModelValue(newValue);
            blog.debug("UI -> MODEL: {}.{} = {}", beanClass.getSimpleName(), propertyName,
                    value != null && value instanceof byte[] ? "byte[].length=" + ((byte[])value).length : value);

            if (!AnahataPropertyUtils.setPropertyNulls(targetBean.getBean().getValue(), propertyName, value)) {
                blog.info("Nested null encountered for {}.{}", beanClass.getSimpleName(), propertyName);
            }

//            System.out.println("------------------------- trying to bind from model");
            //---------------------------------------------------//
            //-------- Danger. Added by pablo to let the model adjust the value passed in the setter as well, not tested //
            //---------------------------------------------------//
            if (bindFromModelOnChange) {
                bindFromModel();
            }

            //---------------------------------------------------//
//            System.out.println("-------------------------");
            long ts3 = System.currentTimeMillis();
            tlog.trace("changed: setting time={} ms", ts3 - ts2);
            blog.debug("Setting validation on rootBinder.controller={}",
                    binder.getRootBinder().getController().getClass().getSimpleName());
            binder.getBaseBinder().setValidationRequired();
            validate(true);
            long ts4 = System.currentTimeMillis();
            tlog.trace("changed: validation={} ms", ts4 - ts3);

            block = true;
            binder.setFormModified(Binding.this);
            block = false;
            tlog.trace("changed: bind from model={} ms", System.currentTimeMillis() - ts4);
            tlog.trace("changed: RETURNING, elapsed={} ms", System.currentTimeMillis() - ts1);
        }
    }

    /**
     * The FocusListener is executed when a user tabs out of the field. It executes class level validators which
     * will run all of the validations. Only class level validations are reported since field level ones are taken care
     * of by validation as the user types.
     */
    private class FocusListener implements ChangeListener<Boolean> {

        @Override
        public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
            if (!isInContext() || block || binder.isBlock() || binder.getBindingPhase() == BindingPhase.BIND_FROM_MODEL) {
                return;
            }

            if (newValue) {
                log.debug("field {} ha gained focus", node);

                if (node instanceof Node) {
                    log.debug("Uopdating last focused field to {}", node);
                    getBinder().getRootBinder().setFocusedField((Node)node);
                } else {
                    log.debug("Not requesting focus to field {}", node);
                }
            } else {
                if (true) {
                    return;
                }
                log.debug("field {} has lost focus", node);
                boolean hasViolation = false;
                final Object modelValue = targetBean.getBean().getValue();

                if (!ActivatableUtils.isInactive(modelValue)) {
                    final Set<ConstraintViolation<Object>> cvs = validator.validate(modelValue,
                            ValidationUtils.getValidationGroups(
                                    modelValue));

                    if (!cvs.isEmpty()) {
                        for (ConstraintViolation cv : cvs) {
                            // Only add root class validations.

                            if (StringUtils.isBlank(cv.getPropertyPath().toString())) {
                                hasViolation = true;
                                jfxMessages.addMessage(binder.getView(Binding.this), null, JfxMessage.error(
                                        cv.getMessage()));
                            }
                        }
                    }
                }

                if (hasViolation) {
                    binder.setGlobalInvalid();
                } else {
                    binder.setGlobalValid();
                    jfxMessages.clearIdMessages();
                }

                // Rebind to the UI so formatting gets refreshed.
                bindFromModel();
            }
        }
    }

    private class ListPropertyChangeListener implements ListChangeListener {

        @Override
        public void onChanged(Change change) {
            if (!isInContext() || block || binder.isBlock() || binder.getBindingPhase() == BindingPhase.BIND_FROM_MODEL) {
                return;
            }

            @SuppressWarnings("unchecked")
            final List list = new ArrayList(change.getList());
            blog.debug("UI -> MODEL: {}.{} = {}", beanClass.getSimpleName(), propertyName, list);

            if (!AnahataPropertyUtils.setPropertyNulls(targetBean.getBean().getValue(), propertyName, list)) {
                blog.info("Nested null encountered for {}.{}", beanClass.getSimpleName(), propertyName);
            }

            binder.getRootBinder().setValidationRequired();
            validate(true);
            binder.setFormModified(Binding.this);
        }
    }

    private class SetPropertyChangeListener implements SetChangeListener {

        @Override
        public void onChanged(Change change) {
            if (!isInContext() || block || binder.isBlock() || binder.getBindingPhase() == BindingPhase.BIND_FROM_MODEL) {
                return;
            }

            @SuppressWarnings("unchecked")
            final Set set = new HashSet(change.getSet());
            blog.debug("UI -> MODEL: {}.{} = {}", beanClass.getSimpleName(), propertyName, set);

            if (!AnahataPropertyUtils.setPropertyNulls(targetBean.getBean().getValue(), propertyName, set)) {
                blog.info("Nested null encountered for {}.{}", beanClass.getSimpleName(), propertyName);
            }

            binder.getRootBinder().setValidationRequired();
            validate(true);
            binder.setFormModified(Binding.this);
        }
    }
}

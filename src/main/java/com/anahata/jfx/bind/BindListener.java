/*
 *  Copyright © - 2013 Anahata Technologies.
 */

package com.anahata.jfx.bind;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface BindListener {
    void preBindFromModel();
}

package com.anahata.jfx.bind;

import java.util.Map;
import javafx.beans.property.BooleanProperty;
import javafx.scene.Node;

/**
 * A bind form has form modified and form valid properties that can be bound to, and validation functionality.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface BindForm extends Validations {
    //== general ======================================================================================================
    
    Binder getRootBinder();
    
    void setRootBinder(Binder binder);
    
    BindForm getParentBindForm();
    
    /**
     * Set the parent BindForm.
     * 
     * @param parentBindForm The parent BindForm.
     */
    void setParentBindForm(BindForm parentBindForm);
    
    Binding getParentBinding();
    
    /**
     * Set the parent binding.
     * 
     * @param parentBinding The parent binding.
     */
    void setParentBinding(Binding parentBinding);
    
    /**
     * Get the view this form or binding belongs to.
     * 
     * @param binding The binding. This is optional, if null then the entire form.
     * @return The view.
     */
    View getView(Binding binding);
    
    /**
     * Set the view that this form belongs to.
     * 
     * @param binding The binding. This is optional, if null then the entire form.
     * @param view The view.
     */
    void setView(View view, Binding binding);
    
    /**
     * Reset the form modified flag.
     */
    void resetFormModified();
    
    //== binding ======================================================================================================
    
    Map<Node, Binding> getAllNodeBindings();
    
    /**
     * Bind all fields from the model to the UI.
     */
    void bindFromModel();
    
    /**
     * Bind from model to UI excluding the given node. This is used when a child performs binding and indicates its
     * parent to rebind excluding itself.
     * 
     * @param node The node to rebind. Required.
     */
    void bindFromModelExcludingNode(Object node);
    
    //== validation ===================================================================================================
    
    /**
     * Validate the full state of the bean.
     * 
     * @param publishError Set to true to display validation errors on fields, false not to.
     * @param excludeNodes Optional list of nodes to be excluded from validation.
     */
    void validate(boolean publishError, Object... excludeNodes);
    
    /**
     * A property indicating if the form is valid.
     * 
     * @return The form valid property.
     */
    BooleanProperty formValidProperty();
    
    /**
     * A property indicating if the form has been modified.
     * 
     * @return The form modified property.
     */
    BooleanProperty formModifiedProperty();
    
    /**
     * A property indicating whether the container should show errors at the top level.
     * 
     * @return The show container errors property.
     */
    BooleanProperty showContainerErrors();
    
    /**
     * Set nodes to exclude in an implicit bind, e.g. the binder setting a list and a list control having to know
     * which items in the list not to rebind.
     * 
     * @param nodes The nodes. Can be null.
     */
    void setExcludeNodes(Object... nodes);
}

package com.anahata.jfx.bind;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import lombok.Delegate;
import org.apache.commons.lang3.Validate;

/**
 * A JavaFX controller that is used in a CDI environment, that supports binding from JavaFX controls to a model.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Dependent
public abstract class BindingController implements BindForm {

    @Inject
    @Delegate(types = { BindForm.class })
    private Binder binder;

    private boolean binderInit = false;

    public final void initialize() {
        init();
        binder.init(this);
        binderInit = true;
        postInit();
    }

    /**
     * Get the binder for this controller.
     *
     * @return The binder.
     * @throws IllegalStateException If this class has not been initialized.
     */
    public final Binder getBinder() {
        Validate.validState(binderInit, "Binder access was attempted without registering it first");
        return binder;
    }

    protected void init() {
    }

    protected void postInit() {
    }
}

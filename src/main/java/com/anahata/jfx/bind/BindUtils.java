package com.anahata.jfx.bind;

import com.anahata.jfx.DelegatingControl;
import com.anahata.jfx.bind.table.BindingTableView;
import java.util.*;
import java.util.Map.Entry;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TableView;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * General bind utilities.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BindUtils {
    /**
     * Determine if an object has a BindForm representation. For a TableView, the BindForm (BindingTableView) will be
     * in the userData field.
     * 
     * @param object The object, can be null.
     * @return true if a BindForm, false if not.
     */
    public static boolean isBindForm(Object object) {
        if (object == null) {
            return false;
        }
        
        if (object instanceof TableView && ((TableView)object).getUserData() != null) {
            object = ((TableView)object).getUserData();
        }
        
        return object instanceof BindForm;
    }
    
    /**
     * Return a BindForm if the object is one, or is a TableView with one set in userData.
     * 
     * @param object The object, can be null.
     * @return The BindForm, or null if object was null or was not a BindForm.
     */
    public static BindForm getBindForm(Object object) {
        if (object == null) {
            return null;
        }
        
        if (object instanceof TableView && ((TableView)object).getUserData() != null) {
            object = ((TableView)object).getUserData();
        }
        
        return object instanceof BindForm ? (BindForm)object : null;
    }
    
    /**
     * Given a set of objects, extract any controllers if present and also add the objects themselves to a set.
     * 
     * @param objects The objects. Can be null.
     * @return A set of objects / controllers. Never null, may be empty.
     */
    public static Set<Object> getAllControllers(Object... objects) {
        if (objects == null) {
            return Collections.emptySet();
        }
        
        Set<Object> controllers = new HashSet<>(objects.length);
        
        for (Object object : objects) {
            controllers.add(object);
            
            if (object instanceof DelegatingControl) {
                DelegatingControl dc = (DelegatingControl)object;
                controllers.add(dc.getController());
            }
            
            if (object instanceof TableView) {
                TableView tv = (TableView)object;
                
                if (tv.getUserData() != null && tv.getUserData() instanceof BindingTableView) {
                    controllers.add(tv.getUserData());
                }
            }
        }
        
        return controllers;
    }
    
    public static <T> void bindBlocking(final Binder binder, final ObservableList<T> source,
            final ObservableList<T> target) {
        Validate.notNull(binder);
        Validate.notNull(source);
        Validate.notNull(target);
        
        source.addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable o) {
                boolean block = binder.isBlock();
                binder.setBlock(true);
                target.setAll(source);
                binder.setBlock(block);
            }
        });
    }
}

package com.anahata.jfx.bind.table;

import com.anahata.jfx.bind.*;
import com.anahata.jfx.binding.BooleanArrayBinding;
import com.anahata.jfx.binding.BooleanArrayBinding.BindingMode;
import com.anahata.jfx.collections.DeltaListChangeListener;
import com.anahata.util.cdi.Cdi;
import java.util.*;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.Validate;

/**
 * This class is used internally by the binder to keep track of TableRow instances created for a TableView.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class BindingTableView implements BindForm {
    //== attributes ===================================================================================================
    @Getter
    private TableView tableView;

    private Class<? extends BindingTableRow> tableRowClass;

    @Getter
    private final List<BindingTableRow> tableRows = new ArrayList<>();

    private List<BooleanExpression> tableValids = new ArrayList<>();

    private List<BooleanExpression> tableModifieds = new ArrayList<>();

    /**
     * Whether the form can be saved.
     */
    private BooleanProperty formValid = new SimpleBooleanProperty(true);

    /**
     * Has the form been modified.
     */
    private BooleanProperty formModified = new SimpleBooleanProperty(false);

    @Getter
    @Setter
    private BindForm parentBindForm;

    @Getter
    @Setter
    private Binding parentBinding;

    private ObjectProperty userData = new SimpleObjectProperty();

    private final ValidationsImpl validations = new ValidationsImpl(null, Collections.<BindForm>emptyList());

    private BooleanProperty containerErrors = new SimpleBooleanProperty(false);

    /**
     * The view which defines the base pane to overlay error messages onto.
     */
    private View view;

    private final Map<Binding, View> bindingViews = new HashMap<>();

    private final BooleanProperty readOnly = new SimpleBooleanProperty();

    @Getter
    private Binder rootBinder;

    //== initialization ===============================================================================================
    @SuppressWarnings("unchecked")
    public BindingTableView(TableView tableView, Class<? extends BindingTableRow> tableRowClass) {
        Validate.notNull(tableView);
        Validate.isTrue(tableView.getUserData() == null, "userData for the passed TableView is not null: %s",
                tableView.getUserData());
        Validate.notNull(tableRowClass);

        this.tableView = tableView;
        tableView.setUserData(this);
        this.tableRowClass = tableRowClass;

        tableView.setRowFactory(new Callback<TableView, TableRow>() {
            @Override
            public TableRow call(TableView tv) {
                BindingTableRow tableRow = Cdi.get(BindingTableView.this.tableRowClass);
                tableRow.setBindingTableView(BindingTableView.this);
                tableRow.setParentBindForm(BindingTableView.this);
                tableRow.readOnlyProperty().bind(readOnly);
                log.debug("Setting rootBinder on BindingTableRow, rootBinder.controller={}",
                        rootBinder.getController().getClass().getSimpleName());
                tableRow.setRootBinder(rootBinder);
                addTableRow(tableRow);
                return tableRow;
            }
        });

        final List<TableColumn> leafColumns = getLeafColumns(tableView);
        final BindingTableRow tempRow = Cdi.get(tableRowClass);
        final Set<String> columnIds = new HashSet<>();

        log.debug("Processing columns of TableView {}, total leaf columns: {}", tableView.getId(), leafColumns.size());

        for (int i = 0; i < leafColumns.size(); i++) {
            final TableColumn column = leafColumns.get(i);
            log.debug("Processing column #{} id={}", i, column.getId());

            if (column.getId() != null) {
                columnIds.add(column.getId());
                Object target = tempRow.getTarget(column.getId());

                if (target != null) {
                    if (target instanceof Node) {
                        column.setCellFactory(new Callback<TableColumn, TableCell>() {
                            @Override
                            public TableCell call(TableColumn tc) {
                                return new BindingTableCell();
                            }
                        });
                    }

                    column.setCellValueFactory(new Callback<CellDataFeatures, ObservableValue>() {
                        @Override
                        public ObservableValue call(CellDataFeatures features) {
                            Object pojo = features.getValue();
                            tempRow.modelProperty().set(pojo);
                            tempRow.setHasValue(true);
                            return new ReadOnlyObjectWrapper<>(tempRow.getRawModelValue(column.getId()));
                        }
                    });
                }
            }
        }

        final Collection<String> missingIds = CollectionUtils.subtract(tempRow.getColumnIds(), columnIds);

        // If there are BindTableColumn id's defined but no matching id in a TableColumn definition, throw an error.
        Validate.validState(missingIds.isEmpty(),
                "There are id's defined in the BindingTableRow [%s] of TableView [%s] without a corresponding "
                + "leaf TableColumn id: %s. Available leaf TableColumn ids are: %s",
                tableRowClass.getSimpleName(), tableView.getId(), missingIds, columnIds);

        // Listen for changed rows to refresh bindings.
        tableView.getItems().addListener(new DeltaListChangeListener() {
            @Override
            protected void onChanged(List added, List removed, List updated) {
                if (updated.isEmpty() && removed.isEmpty()) {
                    return;
                }

                Map updatedMap = new HashMap();

                for (Object obj : updated) {
                    updatedMap.put(obj, obj);
                }

                Set removedSet = new HashSet(removed);

                for (BindingTableRow tableRow : tableRows) {
                    if (tableRow.isHasValue()) {
                        if (updatedMap.containsKey(tableRow.getModel())) {
                            log.debug("Binding from model updated tablerow: {} model: {}", tableRow, tableRow.getModel());
                            tableRow.modelProperty().set(updatedMap.get(tableRow.getModel()));
                            tableRow.bindFromModel();
                            tableRow.updateItem();
                        } else if (removedSet.contains(tableRow.getModel())) {
                            log.debug("Setting to no value removed tablerow: {} model: {}", tableRow,
                                    tableRow.getModel());
                            tableRow.setHasValue(false);
                        }
                    }
                }
            }
        });

        // Listen for userData changes and propogate to children.
        userData.addListener(new InvalidationListener() {
            @Override
            public void invalidated(javafx.beans.Observable o) {
                log.debug("Refreshing userData to table rows");

                for (BindingTableRow tableRow : tableRows) {
                    tableRow.updateItem();
                }
            }
        });
    }

    /**
     * Gets the BindingTableView object associated to a TableView
     *
     * @param table the table
     * @return the associated BindingTableView or null if no BTV is associated.
     */
    public static BindingTableView get(TableView table) {
        return (BindingTableView)table.getUserData();
    }

    //== BindForm =====================================================================================================
    @Override
    public void bindFromModel() {
        for (BindingTableRow tableRow : tableRows) {
            log.debug("bindFromModel: Binding tablerow: {} model: {}", tableRow, tableRow.getModel());
            tableRow.bindFromModel();
        }
    }

    @Override
    public void bindFromModelExcludingNode(Object excludeNode) {
        if (parentBindForm != null) {
            parentBindForm.bindFromModelExcludingNode(excludeNode);
        }
    }

    /**
     * Binds from model the all rows containing the speficied model value.
     *
     * @param model the model object.
     * @return true if a model object was found and bindFromModel was called on the row.
     */
    public boolean bindFromModel(Object model) {
        boolean found = true;
        for (BindingTableRow row : tableRows) {
            if (ObjectUtils.equals(model, row.getModel())) {
                if (row.isHasValue()) {
                    log.debug("Found row for model and set value: {}", row);
                    row.bindFromModel();
                    found = true;
                }
            }
        }
        return found;
    }

    @Override
    public View getView(Binding binding) {
        if (binding != null) {
            View bindingView = bindingViews.get(binding);

            if (bindingView != null) {
                return bindingView;
            }
        }

        if (view != null) {
            return view;
        }

        if (parentBindForm != null) {
            return parentBindForm.getView(binding);
        }

        throw new IllegalStateException("No View has been set on this binder: " + this);
    }

    @Override
    public void setView(View view, Binding binding) {
        if (binding == null) {
            this.view = view;
        } else {
            bindingViews.put(binding, view);
        }
    }

    @Override
    public void resetFormModified() {
        for (BindingTableRow tableRow : new ArrayList<>(tableRows)) {
            tableRow.resetFormModified();
        }
    }

    @Override
    public void validate(boolean publishError, Object... excludeNodes) {
        Set<Object> excluded = BindUtils.getAllControllers(excludeNodes);
        //looping through items and enforcing the tablerow to be created
        for (Object o : tableView.getItems()) {
            BindingTableRow tr = getRow(o);
            if (tr == null) {
                //log.warn("Could not find table row for table item " + o);
            } else if (!excluded.contains(tr)) {
                tr.validate(publishError, excludeNodes);
            }

            //System.out.println("Table Row after sroll is " + tr);
        }
    }

    @Override
    public Map<Node, Binding> getAllNodeBindings() {
        Map<Node, Binding> nodes = new HashMap<>();

        for (BindingTableRow tableRow : new ArrayList<>(tableRows)) {
            if (tableRow.isHasValue()) {
                nodes.putAll(tableRow.getAllNodeBindings());
            }
        }

        return nodes;
    }

    @Override
    public BooleanProperty formValidProperty() {
        return formValid;
    }

    @Override
    public BooleanProperty formModifiedProperty() {
        return formModified;
    }

    @Override
    public BooleanProperty showContainerErrors() {
        return containerErrors;
    }

    @Override
    public void setExcludeNodes(Object... nodes) {
    }

    @Override
    public void setRootBinder(Binder rootBinder) {
        this.rootBinder = rootBinder;
        validations.setRootBinder(rootBinder);

        for (BindingTableRow row : tableRows) {
            row.setRootBinder(rootBinder);
        }
    }

    //== Validations ==================================================================================================
    @Override
    public void addValidationGroup(Class<?> validationGroup) {
        validations.addValidationGroup(validationGroup);
    }

    @Override
    public void removeValidationGroup(Class<?> validationGroup) {
        validations.removeValidationGroup(validationGroup);
    }

    @Override
    public BooleanProperty getValidationActive(Class<?> validationGroup) {
        return validations.getValidationActive(validationGroup);
    }

    @Override
    public ReadOnlyBooleanProperty getFormValidProperty(Class<?> validationGroup) {
        return validations.getFormValidProperty(validationGroup);
    }

    @Override
    public void setValid(String propertyName, Class<?> validationGroup) {
        validations.setValid(propertyName, validationGroup);
    }

    @Override
    public void setInvalid(String propertyName, Class<?> validationGroup) {
        validations.setInvalid(propertyName, validationGroup);
    }

    @Override
    public Set<Class<?>> getValidations() {
        return validations.getValidations();
    }

    @Override
    public void setValidationRequired() {
        for (BindingTableRow row : getTableRows()) {
            row.setValidationRequired();
        }
    }

    //== public methods ===============================================================================================
    public boolean isReadOnly() {
        return readOnly.get();
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly.set(readOnly);
    }

    public BooleanProperty readOnlyProperty() {
        return readOnly;
    }

    public ObjectProperty userDataProperty() {
        return userData;
    }

    public Object getUserData() {
        return userData.get();
    }

    @SuppressWarnings("unchecked")
    public void setUserData(Object userData) {
        this.userData.set(userData);
    }

    /**
     * Get the currently selected BindingTableRow.
     *
     * @return The currently selected BindingTableRow, or null if none are selected.
     */
    public BindingTableRow getSelectedRow() {
        for (BindingTableRow row : tableRows) {
            if (row.isSelected()) {
                return row;
            }
        }

        return null;
    }

    /**
     * For the given model, get the BindingTableRow associated with it.
     *
     * @param model The model.
     * @return The row.
     */
    public BindingTableRow getRow(Object model) {
        if (model == null) {
            return null;
        }

        for (BindingTableRow row : tableRows) {
            if (ObjectUtils.equals(model, row.getModel())) {
                if (row.isHasValue()) {
                    return row;
                }
            }
        }

        return null;
    }

    //== private methods ==============================================================================================
    private void addTableRow(BindingTableRow tableRow) {
        tableRows.add(tableRow);
        tableValids.add(tableRow.formValidProperty());
        tableModifieds.add(tableRow.formModifiedProperty());
        formModified.unbind();
        formModified.bind(new BooleanArrayBinding(BindingMode.OR, tableModifieds));
        formValid.unbind();
        formValid.bind(new BooleanArrayBinding(BindingMode.AND, tableValids));
        validations.rebind(tableRows);
    }

    /**
     * Gets all the leaf columns of a table view.
     *
     * @param tableView the table view
     * @return the leaf columns
     */
    @SuppressWarnings("unchecked")
    private static List<TableColumn> getLeafColumns(TableView tableView) {
        List<TableColumn> ret = new ArrayList<>();

        for (TableColumn tc : (List<TableColumn>)tableView.getColumns()) {
            List<TableColumn> leafs = getLeafColumns(tc);
            ret.addAll(leafs);
        }
        return ret;
    }

    /**
     * Gets all leaf columns for a given grouping column or if the column passed is a leaf column itself it returns a
     * list with the given column.
     *
     * @param tc the table column
     * @return all leaf columns or <code>tc</code> only if <code>tc</code> is a leaf column itsel
     */
    private static List<TableColumn> getLeafColumns(TableColumn tc) {
        List<TableColumn> ret = new ArrayList<>();

        if (tc.getColumns().isEmpty()) {
            ret.add(tc);
        } else {
            for (Object child : tc.getColumns()) {
                ret.addAll(getLeafColumns((TableColumn)child));
            }
        }

        if (log.isDebugEnabled()) {
            if (ret.size() > 1) {
                log.debug("leaf colums of {}: {}", tc.getId(), ret.size());
            }
        }

        return ret;
    }

    /**
     * Calls bind fromModel() on rows where hsaValue is true.
     */
    public void bindFromModelRowsWithValue() {
        for (BindingTableRow row : getTableRows()) {
            if (row.isHasValue()) {
                row.bindFromModel();
            }
        }
    }

    @Override
    public String toString() {
        return "BindingTableView{" + "parentBinding=" + tableRowClass.getSimpleName() + '}';
    }

}

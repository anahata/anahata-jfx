package com.anahata.jfx.bind.table;

import com.anahata.jfx.bind.*;
import com.anahata.jfx.bind.converter.Converter;
import com.anahata.util.metamodel.MetaModelProperty;
import com.anahata.util.metamodel.MetaModelUtils;
import com.anahata.util.reflect.AnahataPropertyUtils;
import com.anahata.util.reflect.ReflectionUtils;
import java.lang.reflect.Field;
import java.util.*;
import javafx.beans.property.*;
import javafx.scene.Node;
import javafx.scene.control.TableRow;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * Support for binding to a TableView row.
 *
 * @param <T> The model type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public abstract class BindingTableRow<T> extends TableRow<T> implements BindForm {
    //== attributes ===================================================================================================

    /**
     * Binder.
     */
    @Inject
    @Getter(AccessLevel.PROTECTED)
    private Binder binder;

    /**
     * Custom form valid property.
     */
    private final BooleanProperty formValid = new SimpleBooleanProperty(true);

    /**
     * Custom form modified property.
     */
    private final BooleanProperty formModified = new SimpleBooleanProperty(false);

    /**
     * Use this to track if binding should be called.
     */
    private final BooleanProperty hasValue = new SimpleBooleanProperty(false);

    /**
     * Binding target (node or jfx property) defined for each column.
     */
    private final Map<String, Object> targets = new HashMap<>();

    /**
     * For each column, the class it was defined against. Used to check if bind valid.
     */
    private final Map<String, Class> propertyTypes = new HashMap<>();

    /**
     * For each column, a property path for fetching the value.
     */
    private final Map<String, String> columnFields = new HashMap<>();

    /**
     * Map columns to bean id's to fetch target beans.
     */
    private final Map<String, Bind> columnIds = new HashMap<>();

    /**
     * Keep track of the valid status of each validation group, as we have other criteria.
     */
    private final Map<Class<?>, BooleanProperty> valids = new HashMap<>();

    /**
     * Map column id's to style classes.
     */
    private final Map<String, String> styleClasses = new HashMap<>();

    /**
     * The parent BindingTableView.
     */
    @Getter(AccessLevel.PROTECTED)
    @Setter(AccessLevel.PACKAGE)
    private BindingTableView bindingTableView;

    /**
     * The model for binding to/from.
     */
    @BindModel(autoBind = false)
    private final ObjectProperty<T> model = new SimpleObjectProperty<>();

    private final BooleanProperty readOnly = new SimpleBooleanProperty();

    //== initialization ===============================================================================================
    
    @PostConstruct
    public void initBindingTableRow() {
        log.debug("initBindingTableRow: ENTERING");
        preInit();
        binder.init(this);
        formValid.bind(binder.formValidProperty().or(hasValue.not()));
        formModified.bind(binder.formModifiedProperty().and(hasValue));

        // Store all columns.
        final List<Field> fields = ReflectionUtils.getAllDeclaredFields(getClass());

        for (Field field : fields) {
            BindTableColumn column = field.getAnnotation(BindTableColumn.class);

            if (column != null) {
                String id = column.id();

                if (StringUtils.isEmpty(id)) {
                    log.debug("@BindTableColumn annotation without id on {}.{}"
                            + " field name will be used as column id",
                            getClass().getName(), field.getName());
                    id = field.getName();
                }

                Object target = ReflectionUtils.readField(this, Object.class, field);
                targets.put(id, target);

                // Obtain binding information to work out model field to fetch.
                Bind bind = field.getAnnotation(Bind.class);

                if (bind != null) {
                    final MetaModelProperty[] mmFields = new MetaModelProperty[bind.property().length];

                    for (int j = 0; j < bind.property().length; j++) {
                        mmFields[j] = ReflectionUtils.getInstance(bind.property()[j]);
                    }

                    final String path = MetaModelUtils.getNestedPropertyName(mmFields);
                    columnFields.put(id, path);
                    log.debug("initBindingTableRow: Mapping column id {} to bind id {}", column.id(), bind.id());
                    columnIds.put(id, bind);
                    propertyTypes.put(id, mmFields[0].getType());
                }

                // Bind the disabled property to the read only property.
                if (target instanceof Node) {
                    Node node = (Node)target;
                    node.disableProperty().bind(readOnly);
                }

                // Map a style class.
                if (!StringUtils.isBlank(column.styleClass())) {
                    styleClasses.put(id, column.styleClass());
                }
            }
        }
        
        postInit();
    }

    //== BindForm =====================================================================================================
    @Override
    public Binder getRootBinder() {
        return binder.getRootBinder();
    }

    @Override
    public void setRootBinder(Binder binder) {
        this.binder.setRootBinder(binder);
    }

    @Override
    public BindForm getParentBindForm() {
        return binder.getParentBindForm();
    }

    @Override
    public void setParentBindForm(BindForm parentBindForm) {
        binder.setParentBindForm(parentBindForm);
    }

    @Override
    public Binding getParentBinding() {
        return binder.getParentBinding();
    }

    @Override
    public void setParentBinding(Binding parentBinding) {
        binder.setParentBinding(parentBinding);
    }

    @Override
    public View getView(Binding binding) {
        return binder.getView(binding);
    }

    @Override
    public void setView(View view, Binding binding) {
        binder.setView(view, binding);
    }

    @Override
    public void resetFormModified() {
        binder.resetFormModified();
    }

    @Override
    public void bindFromModel() {
        if (isHasValue()) {
            binder.bindFromModel();
            postBindFromModel();
        }
    }

    @Override
    public void bindFromModelExcludingNode(Object node) {
        if (isHasValue()) {
            binder.bindFromModelExcludingNode(node);
            postBindFromModel(node);
        }
    }

    @Override
    public void validate(boolean publishError, Object... excludeNodes) {
        if (isHasValue()) {
            binder.validate(publishError, excludeNodes);
        }
    }

    @Override
    public Map<Node, Binding> getAllNodeBindings() {
        if (isHasValue()) {
            return binder.getAllNodeBindings();
        }

        return Collections.emptyMap();
    }

    @Override
    public BooleanProperty formValidProperty() {
        return formValid;
    }

    @Override
    public BooleanProperty formModifiedProperty() {
        return formModified;
    }

    @Override
    public BooleanProperty showContainerErrors() {
        return binder.showContainerErrors();
    }

    @Override
    public void setExcludeNodes(Object... nodes) {
    }

    //== Validations ==================================================================================================
    @Override
    public void addValidationGroup(Class<?> validationGroup) {
        binder.addValidationGroup(validationGroup);

        if (!valids.containsKey(validationGroup)) {
            final SimpleBooleanProperty valid = new SimpleBooleanProperty(true);
            valids.put(validationGroup, valid);
            valid.bind(binder.getFormValidProperty(validationGroup).or(hasValue.not()));
        }
    }

    @Override
    public void removeValidationGroup(Class<?> validationGroup) {
        binder.removeValidationGroup(validationGroup);

        if (valids.containsKey(validationGroup)) {
            valids.get(validationGroup).unbind();
            valids.remove(validationGroup);
        }
    }

    @Override
    public BooleanProperty getValidationActive(Class<?> validationGroup) {
        return binder.getValidationActive(validationGroup);
    }

    @Override
    public ReadOnlyBooleanProperty getFormValidProperty(Class<?> validationGroup) {
        Validate.notNull(validationGroup);
        Validate.validState(valids.containsKey(validationGroup),
                "The validation group %s has not been set in this class", validationGroup.getSimpleName());
        return valids.get(validationGroup);
    }

    @Override
    public void setValid(String propertyName, Class<?> validationGroup) {
        binder.setValid(propertyName, validationGroup);
    }

    @Override
    public void setInvalid(String propertyName, Class<?> validationGroup) {
        binder.setInvalid(propertyName, validationGroup);
    }

    @Override
    public Set<Class<?>> getValidations() {
        return binder.getValidations();
    }

    //== public methods ===============================================================================================
    /**
     * Determine if this row has a value.
     *
     * @return true if it has a value, false if not.
     */
    public boolean isHasValue() {
        return hasValue.get();
    }

    /**
     * Get a JavaFX node given a column id.
     *
     * @param columnId The column id.
     * @return The node, or null if not found.
     */
    public final Object getTarget(String columnId) {
        return targets.get(columnId);
    }

    /**
     * Get the style class set for the column id, to be applied to the cell.
     *
     * @param columnId The column id.
     * @return The style class, or null if not set.
     */
    public final String getStyleClass(String columnId) {
        return styleClasses.get(columnId);
    }

    /**
     * Get a model value of a given model for a column id (converted).
     *
     * @param columnId The column id.
     * @return The model value.
     */
    @SuppressWarnings("unchecked")
    public final Object getModelValue(String columnId) {
        try {
            final Bind bind = columnIds.get(columnId);

            Object value = getRawModelValue(columnId);
            if (value != null) {
                Converter converter;

                try {
                    converter = ReflectionUtils.getInstanceIfNotBase(Converter.class, bind.converter());
                } catch (Exception e) {
                    // Try an inner class.
                    converter = ReflectionUtils.getInstanceIfNotBase(Converter.class, bind.converter(), this);
                }

                if (converter != null) {
                    value = converter.getAsNodeModelValue(null, value);
                }
            }

            return value;
        } catch (NestedNullException e) {
            return null;
        }
    }
    
    /**
     * Get a model value of a given model for a column id without converting it.
     *
     * @param columnId The column id.
     * @return The model value.
     */
    @SuppressWarnings("unchecked")
    public final Object getRawModelValue(String columnId) {
        
        try {
            final Bind bind = columnIds.get(columnId);

            if (bind == null) {
                return null;
            }

            Object target = binder.getTargetBean(bind.id());
            Object value = isInContext(columnId, target) ? AnahataPropertyUtils.getProperty(target, columnFields.get(
                    columnId)) : null;


            return value;
        } catch (NestedNullException e) {
            return null;
        }
    }

    /**
     * The model property.
     *
     * @return The model property.
     */
    public final ObjectProperty<T> modelProperty() {
        return model;
    }

    /**
     * Get the model.
     *
     * @return The model.
     */
    public final T getModel() {
        return modelProperty().getValue();
    }

    public boolean isReadOnly() {
        return readOnly.get();
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly.set(readOnly);
    }

    public BooleanProperty readOnlyProperty() {
        return readOnly;
    }

    @Override
    public void setValidationRequired() {
        binder.setValidationRequired();
    }
    
    //== package methods ==============================================================================================
    /**
     * Get all defined column id's.
     *
     * @return The column id's.
     */
    final Set<String> getColumnIds() {
        return targets.keySet();
    }

    void setHasValue(boolean hasValue) {
        this.hasValue.set(hasValue);
    }

    //== protected methods ============================================================================================
    @Override
    protected final void updateItem(T item, boolean empty) {
        boolean changed = item != super.getItem();
        super.updateItem(item, empty);
        hasValue.set(item != null && !empty);

        if (hasValue.get()) {
            modelProperty().setValue(item);        
            if (changed) {                
                getBinder().bindFromModel(false);
            }
        } else {
            modelProperty().setValue(null);
        }

        updateItem();
    }

    /**
     * The parent BindingController (the controller where the TableView resides)
     *
     * @param <C> the controller type
     * @return the parent controller
     */
    protected <C extends BindingController> C getParentController() {
        Binder parentBinder = (Binder)getBindingTableView().getParentBindForm();
        return (C)parentBinder.getController();
    }

    /**
     * Overridable updateItem, with a default noop implementation. This is called after the full state of the row has
     * been set, i.e. the model has been set, or nulled out, and hasValue has been set.
     */
    protected void updateItem() {
    }

    /**
     * Callback method invoked before the binder has been initialized.
     */
    protected void preInit() {
    }

    /**
     * Callback method invoked after the binder has been initialized.
     */
    protected void postInit() {
    }

    /**
     * Called after binding from model is completed, so custom code can be hooked in.
     *
     * @param excludeNodes Nodes to exclude from binding.
     */
    protected void postBindFromModel(Object... excludeNodes) {
    }

    //== private methods ==============================================================================================
    @SuppressWarnings("unchecked")
    private boolean isInContext(String columId, Object target) {
        final Bind bind = columnIds.get(columId);
        Class<? extends MetaModelProperty>[] path = bind.property();
        Class<? extends MetaModelProperty> metamodelClass = path[0];
        MetaModelProperty instance = ReflectionUtils.getInstance(metamodelClass);
        Class declaringClass = instance.getDeclaringClass();
        return target != null ? declaringClass.isAssignableFrom(target.getClass()) : false;
    }
}

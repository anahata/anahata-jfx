/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.table;

import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableRow;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author pablo
 */
@Slf4j
public class BindingTableCell extends TableCell {

    @Override
    protected void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);        
        final TableRow row = super.getTableRow();
        Node graphic = null;
        log.debug("item= {}", item);

        if (row != null && row.getItem() != null) {
            BindingTableRow bindingTableRow = (BindingTableRow)row;
            graphic = (Node)bindingTableRow.getTarget(getTableColumn().getId());
            log.debug("graphic= {}", graphic);
            String styleClass = bindingTableRow.getStyleClass(getTableColumn().getId());
            if (styleClass != null) {
                getStyleClass().add(styleClass);
            }
        }

        setGraphic(graphic);
    }
    
    public BindingTableRow getBindingTableRow() {
        return  (BindingTableRow) super.getTableRow();
    }
}

package com.anahata.jfx.bind.table;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

/**
 * Identify a TableColumn for binding.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Target(FIELD)
@Retention(RUNTIME)
public @interface BindTableColumn {
    /**
     * @return The id of the TableColumn to map to. Optional. If not present the field name 
     * will be assumed as the id of the TableColumn.
     */
    String id() default "";
    
    /**
     * @return The style class for styling the cell. Optional.
     */
    String styleClass() default "";
}

package com.anahata.jfx.bind.table;

import java.util.List;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableView;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.Validate;

/**
 * Utilities for bindings table views.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BindingTableViewUtils {
    /**
     * Bind a list of BindingTableView object read only properties to a given observable boolean.
     * 
     * @param ov         The observable value. Required.
     * @param tableViews The tables. Required, at least one must be provided.
     * @throws NullPointerException     If any arg is null.
     * @throws IllegalArgumentException If no TableView objects were passed, or if any TableView has a null UserData
     *                                  or a UserData that is not an instance of BindingTableView.
     */
    public static void bindReadOnly(@NonNull ObservableValue<? extends Boolean> ov, TableView... tableViews) {
        Validate.notEmpty(tableViews, "No TableView objects were passed");
        
        for (TableView tableView : tableViews) {
            Validate.isTrue(tableView.getUserData() != null, "The TableView %s had a null UserData", tableView);
            Validate.isTrue(tableView.getUserData() instanceof BindingTableView,
                    "The TableView %s had a UserData that was not a BindingTableView, it was a %s", tableView,
                    tableView.getUserData().getClass().getName());
            BindingTableView btv = (BindingTableView)tableView.getUserData();
            btv.readOnlyProperty().bind(ov);
        }
    }
    
    /**
     * Bind a list of BindingTableView object read only properties to a given observable boolean.
     * 
     * @param ov         The observable value. Required.
     * @param tableViews The tables. Required, at least one must be provided.
     * @throws NullPointerException     If any arg is null.
     * @throws IllegalArgumentException If no TableView objects were passed, or if any TableView has a null UserData
     *                                  or a UserData that is not an instance of BindingTableView.
     */
    public static void bindReadOnly(@NonNull ObservableValue<? extends Boolean> ov, List<TableView> tableViews) {
        Validate.notEmpty(tableViews, "No TableView objects were passed");
        
        for (TableView tableView : tableViews) {
            Validate.isTrue(tableView.getUserData() != null, "The TableView %s had a null UserData", tableView);
            Validate.isTrue(tableView.getUserData() instanceof BindingTableView,
                    "The TableView %s had a UserData that was not a BindingTableView, it was a %s", tableView,
                    tableView.getUserData().getClass().getName());
            BindingTableView btv = (BindingTableView)tableView.getUserData();
            btv.readOnlyProperty().bind(ov);
        }
    }
}

package com.anahata.jfx.bind.table;

import com.anahata.util.metamodel.MetaModelProperty;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

/**
 * Identify a TableView for binding.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Target(FIELD)
@Retention(RUNTIME)
public @interface BindTable {
    /**
    /**
     * @return The property being bound to. If more than one is provided, it is a nested property. Required.
     */
    Class<? extends MetaModelProperty>[] property();
    
    /** @return The id of the @BindModel this property is linked to. */
    String id() default "default";
    
    /**
     * @return The TableRow for each row. Required.
     */
    Class<? extends BindingTableRow> tableRow();
}

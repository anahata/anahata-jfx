package com.anahata.jfx.bind;

import java.util.Set;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface Validations {
    /**
     * Add a validation group to the validation processing.
     *
     * @param validationGroup The validation group.
     */
    void addValidationGroup(Class<?> validationGroup);

    /**
     * Remove a validation group from processing.
     *
     * @param validationGroup The validation group.
     */
    void removeValidationGroup(Class<?> validationGroup);
    
    /**
     * Get a property that can be updated to set whether a validation group is active or not. Inactive validation
     * groups will always return true for getFormValidProperty().
     * 
     * @param validationGroup The validation group. Required.
     * @return The active property.
     * @throws NullPointerException  If validationGroup is null.
     * @throws IllegalStateException If the validation group was not set.
     */
    BooleanProperty getValidationActive(Class<?> validationGroup);
    
    /**
     * Get the valid property for a given validation group.
     *
     * @param validationGroup The validation group. Required.
     * @return The form valid property for the validation group.
     * @throws NullPointerException  If validationGroup is null.
     * @throws IllegalStateException If the validation group was not set.
     *
     */
    ReadOnlyBooleanProperty getFormValidProperty(Class<?> validationGroup);

    void setValid(String propertyName, Class<?> validationGroup);

    void setInvalid(String propertyName, Class<?> validationGroup);

    /**
     * Get validations added here.
     *
     * @return The validations.
     */
    Set<Class<?>> getValidations();
    
    /**
     * Identify that validation must be re-run.
     */
    void setValidationRequired();
}

package com.anahata.jfx.bind;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An observable boolean that identifies whether a form is read only.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface BindReadOnlyModel {
}

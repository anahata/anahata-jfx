package com.anahata.jfx.bind.filter;

import org.apache.commons.lang3.StringUtils;

/**
 * Filter keystrokes for an integer text input field.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class IntegerKeystrokeFilter implements KeystrokeFilter {
    public static final IntegerKeystrokeFilter INSTANCE = new IntegerKeystrokeFilter();
    
    @Override
    public String filterKeystrokes(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }

        if (!StringUtils.containsOnly(value, "-0123456789 ")) {
            return "The value can only contain integer values";
        }

        return null;
    }
}

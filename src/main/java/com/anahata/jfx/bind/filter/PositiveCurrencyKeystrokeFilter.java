package com.anahata.jfx.bind.filter;

import org.apache.commons.lang3.StringUtils;

/**
 * Filter keystrokes for a positive currency text input field.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class PositiveCurrencyKeystrokeFilter implements KeystrokeFilter {
    public static final PositiveCurrencyKeystrokeFilter INSTANCE = new PositiveCurrencyKeystrokeFilter();
    
    @Override
    public String filterKeystrokes(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }

        if (!StringUtils.containsOnly(value, ",.0123456789 ")) {
            return "The value can only contain numeric values, a period or comma";
        }

        return null;
    }
}

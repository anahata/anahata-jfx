package com.anahata.jfx.bind.filter;

/**
 * A text input filter used to consume keystrokes if they do not match a pattern.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface KeystrokeFilter {
    /**
     * Determine if an input value requires filtering.
     *
     * @param value The input value, can be null.
     * @return If null, this indicates that the value does not need filtering. If not null, the keystroke must be
     *         filtered and the return value can be used as a message to present to the user indicating why the key was
     *         invalid.
     */
    public String filterKeystrokes(String value);
}

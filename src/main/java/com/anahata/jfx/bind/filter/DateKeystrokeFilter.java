package com.anahata.jfx.bind.filter;

import org.apache.commons.lang3.StringUtils;

/**
 * Filter keystrokes for a date text input field.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class DateKeystrokeFilter implements KeystrokeFilter {
    public static final DateKeystrokeFilter INSTANCE = new DateKeystrokeFilter();
    
    @Override
    public String filterKeystrokes(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }

        if (!StringUtils.containsOnly(value, "/0123456789")) {
            return "The date must be in the form of dd/mm/yyyy";
        }

        return null;
    }
}

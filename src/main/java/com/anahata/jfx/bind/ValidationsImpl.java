package com.anahata.jfx.bind;

import java.util.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * Contains a set of ValidationStatus objects.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class ValidationsImpl implements Validations {
    private Collection<? extends BindForm> bindForms;
    
    private final Map<Class<?>, ValidationStatus> validations = new HashMap<>();
    
    private Binder rootBinder;
    
    public ValidationsImpl(Binder rootBinder, @NonNull Collection<? extends BindForm> bindForms) {
        this.rootBinder = rootBinder;
        this.bindForms = bindForms;
    }
    
    public void rebind(Collection<? extends BindForm> bindForms) {
        Validate.notNull(bindForms);
        this.bindForms = bindForms;
        
        for (ValidationStatus status : validations.values()) {
            status.rebind(bindForms);
        }
    }
    
    /**
     * Add a validation group to the validation processing.
     * 
     * @param validationGroup The validation group.
     */
    @Override
    public void addValidationGroup(Class<?> validationGroup) {
        if (validations.containsKey(validationGroup)) {
            return;
        }
        
        final ValidationStatus status = new ValidationStatus(rootBinder, validationGroup, bindForms);
        validations.put(validationGroup, status);
    }
    
    /**
     * Remove a validation group from processing.
     * 
     * @param validationGroup The validation group.
     */
    @Override
    public void removeValidationGroup(Class<?> validationGroup) {
        ValidationStatus status = validations.remove(validationGroup);
        status.unbind();
        
        for (BindForm bindForm : bindForms) {
            bindForm.getValidationActive(validationGroup).unbind();
            bindForm.removeValidationGroup(validationGroup);
        }
    }

    @Override
    public BooleanProperty getValidationActive(Class<?> validationGroup) {
        Validate.notNull(validationGroup);
        ValidationStatus status = validations.get(validationGroup);
        Validate.isTrue(status != null, "The validation group %s was not defined", validationGroup.getSimpleName());
        return status.activeProperty();
    }
    
    /**
     * Get the valid property for a given validation group.
     * 
     * @param validationGroup The validation group. Required.
     * @return The form valid property for the validation group.
     * @throws NullPointerException     If validationGroup is null.
     * @throws IllegalArgumentException If the validation group was not set on this binder.
     */
    @Override
    public ReadOnlyBooleanProperty getFormValidProperty(Class<?> validationGroup) {
        Validate.notNull(validationGroup);
        ValidationStatus status = validations.get(validationGroup);
        Validate.isTrue(status != null, "The validation group %s was not defined", validationGroup.getSimpleName());
        if (!status.isValid()) {
            log.debug("form not valid for group {} invalid properties = {} ", validationGroup.getSimpleName(), status.getInvalidProperties().get());
        }
        return status.validProperty();
    }
    
    @Override
    public void setValid(String propertyName, Class<?> validationGroup) {
        ValidationStatus status = validations.get(validationGroup);
        Validate.validState(status != null,
                "Could not set valid status for property %s in validation group %s as the validation group is missing",
                propertyName, validationGroup.getSimpleName());
        status.setValid(propertyName);
    }
    
    @Override
    public void setInvalid(String propertyName, Class<?> validationGroup) {
        ValidationStatus status = validations.get(validationGroup);
        Validate.validState(status != null,
                "Could not set invalid status for property %s in validation group %s as the validation group is missing",
                propertyName, validationGroup.getSimpleName());
        log.debug("invalidProperty {} group {}", propertyName, validationGroup);
        status.setInvalid(propertyName);
    }
    
    /**
     * Get validations added here.
     * 
     * @return The validations.
     */
    @Override
    public Set<Class<?>> getValidations() {
        return validations.keySet();
    }
    
    public void unbind() {
        for (Class<?> validationGroup : validations.keySet()) {
            removeValidationGroup(validationGroup);
        }
    }
    
    @Override
    public void setValidationRequired() {
        for (BindForm bindForm : bindForms) {
            bindForm.setValidationRequired();
        }
    }
    
    public void setRootBinder(Binder binder) {
        rootBinder = binder;
        
        for (ValidationStatus status : validations.values()) {
            status.setRootBinder(rootBinder);
        }
    }
    
    public void clear() {
        for (ValidationStatus status : validations.values()) {
            log.debug("Clearing invalid properties for {}, invalidProps = {}", status, status.getInvalidProperties().get());
            status.clearInvalidProperties();
            log.debug("After clearing invalid properties for {}, invalidProps = {}", status, status.getInvalidProperties().get());
        }
        
        for (BindForm bf: bindForms) {
            for (Class s: new ArrayList<>(bf.getValidations())) {
                log.debug("clearing bindForm ={} for group {}", bf, s);
                //effectively like a clearInvalidProperties but saves having to put a clearInvalidProperties() method in Validation.java
                //and its 20 implementations
                BooleanProperty bp = bf.getValidationActive(s);
                bf.removeValidationGroup(s);
                bf.addValidationGroup(s);
                bf.getValidationActive(s).bind(bp);
                
                log.debug("after clearing bindForm ={} for group {} valid={}", bf, s, bf.getFormValidProperty(s));
                
            }
        }
        
        debugStatus();
    }
    
    public void debugStatus() {
        for (ValidationStatus vs: validations.values()) {
            log.debug("status:: group {} invalidProps {}= props valid={} invalidForms {} validForms {} forms Valid ={} formsValid={} valid={} active={}", 
                    vs.getValidationGroup(), vs.getInvalidProperties().get(), vs.isPropertiesValid(), vs.getInvalidForms(), vs.getValidForms(), vs.isFormsValid(), vs.isValid(), vs.activeProperty().get());
        }
    }
}

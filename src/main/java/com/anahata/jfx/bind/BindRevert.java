package com.anahata.jfx.bind;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Identifies a revert button. Enables and disables automatically. Resets to disabled when pressed.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface BindRevert {
}

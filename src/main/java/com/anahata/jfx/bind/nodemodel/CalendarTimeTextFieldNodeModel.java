/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.ObjectProperty;
import jfxtras.scene.control.CalendarTimeTextField;

/**
 * <p>CalendarTimeTextFieldNodeModel class.</p>
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 * @version $Id: $Id
 */
public class CalendarTimeTextFieldNodeModel implements NodeModel<CalendarTimeTextField, ObjectProperty> {

    /** {@inheritDoc} */
    @Override
    public ObjectProperty getNodeModelValueProperty(CalendarTimeTextField node) {
        return node.calendarProperty();
    }
}

package com.anahata.jfx.bind.nodemodel;

import com.anahata.jfx.scene.control.CalendarTextField;
import javafx.beans.property.ObjectProperty;
import javax.inject.Singleton;

/**
 * NodeModel for JFXtras CalendarTextField component.
 * 
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Singleton
public class CalendarTextFieldNodeModel implements NodeModel<CalendarTextField, ObjectProperty> {
    @Override
    public ObjectProperty getNodeModelValueProperty(CalendarTextField node) {        
        return node.calendarProperty();
    }
}

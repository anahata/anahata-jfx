package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.ObjectProperty;
import javafx.scene.image.ImageView;
import javax.inject.Singleton;

/**
 * An ImageView node model. Relies on the userData property containing the image bytes.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class ImageViewNodeModel implements NodeModel<ImageView, ObjectProperty<byte[]>> {
    @Override
    public ObjectProperty<byte[]> getNodeModelValueProperty(ImageView node) {
        return (ObjectProperty<byte[]>)node.getUserData();
    }
}

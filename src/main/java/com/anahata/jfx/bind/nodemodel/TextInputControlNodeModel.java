package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.StringProperty;
import javafx.scene.control.TextInputControl;
import javax.inject.Singleton;

/**
 * A TextInputControl node model.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class TextInputControlNodeModel implements NodeModel<TextInputControl, StringProperty> {
    @Override
    public StringProperty getNodeModelValueProperty(TextInputControl node) {
        return node.textProperty();
    }
}

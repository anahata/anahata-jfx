package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.StringProperty;
import javafx.scene.control.PasswordField;
import javax.inject.Singleton;

/**
 * A PasswordField node model.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class PasswordFieldNodeModel implements NodeModel<PasswordField, StringProperty> {
    @Override
    public StringProperty getNodeModelValueProperty(PasswordField node) {
        return node.textProperty();
    }
}

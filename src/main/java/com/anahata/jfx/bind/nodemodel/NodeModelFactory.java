package com.anahata.jfx.bind.nodemodel;

import com.anahata.jfx.scene.control.AutoCompleteTextField;
import com.anahata.util.cdi.Cdi;
import java.util.HashMap;
import java.util.Map;
import javafx.beans.property.Property;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import jfxtras.labs.scene.control.BigDecimalField;
import jfxtras.labs.scene.control.BigDecimalLabel;
import jfxtras.scene.control.CalendarTimeTextField;
import jfxtras.scene.control.LocalDateTimeTextField;
import jfxtras.scene.control.gauge.linear.SimpleMetroArcGauge;
import org.apache.commons.lang3.Validate;

/**
 * Create NodeModel instances.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class NodeModelFactory {
    private static final Map<Class<?>, Class<? extends NodeModel>> DEFAULT_MODELS = new HashMap<>();

    private static final Map<Class<?>, Class<? extends NodeModel>> USER_MODELS = new HashMap<>();

    static {
        DEFAULT_MODELS.put(TextField.class, TextInputControlNodeModel.class);
        DEFAULT_MODELS.put(TextArea.class, TextInputControlNodeModel.class);
        DEFAULT_MODELS.put(BigDecimalField.class, BigDecimalFieldNodeModel.class);
        DEFAULT_MODELS.put(SimpleMetroArcGauge.class, SimpleMetroArcGaugeNodeModel.class);
        DEFAULT_MODELS.put(ProgressBar.class, ProgressBarNodeModel.class);
        DEFAULT_MODELS.put(Slider.class, SliderNodeModel.class);
        DEFAULT_MODELS.put(BigDecimalLabel.class, BigDecimalLabelNodeModel.class);
        DEFAULT_MODELS.put(CheckBox.class, CheckBoxNodeModel.class);
        DEFAULT_MODELS.put(ToggleGroup.class, ToggleGroupNodeModel.class);
        DEFAULT_MODELS.put(ImageView.class, ImageViewNodeModel.class);
        DEFAULT_MODELS.put(ComboBox.class, ComboBoxNodeModel.class);
        DEFAULT_MODELS.put(ChoiceBox.class, ChoiceBoxNodeModel.class);
        DEFAULT_MODELS.put(ToggleButton.class, ToggleButtonNodeModel.class);
        DEFAULT_MODELS.put(Hyperlink.class, HyperlinkNodeModel.class);
        DEFAULT_MODELS.put(AutoCompleteTextField.class, AutoCompleteTextFieldNodeModel.class);
        DEFAULT_MODELS.put(PasswordField.class, PasswordFieldNodeModel.class);
        DEFAULT_MODELS.put(Label.class, LabelNodeModel.class);
        DEFAULT_MODELS.put(Text.class, TextNodeModel.class);
//        DEFAULT_MODELS.put(CalendarTextField.class, CalendarTextFieldNodeModel.class);
        DEFAULT_MODELS.put(CalendarTimeTextField.class, CalendarTimeTextFieldNodeModel.class);
        DEFAULT_MODELS.put(DatePicker.class, DatePickerNodeModel.class);
        DEFAULT_MODELS.put(TableView.class, TableViewNodeModel.class);
        DEFAULT_MODELS.put(ListView.class, ListViewNodeModel.class);
        DEFAULT_MODELS.put(RadioButton.class, RadioButtonNodeModel.class);
        DEFAULT_MODELS.put(Button.class, ButtonNodeModel.class);
        DEFAULT_MODELS.put(LocalDateTimeTextField.class, LocalDateTimeNodeModel.class);
        DEFAULT_MODELS.put(Spinner.class, SpinnerNodeModel.class);
    }

    /**
     * Register a custom NodeModel.
     *
     * @param nodeClass      The node class it applies to.
     * @param nodeModelClass The NodeModel implementation.
     * @throws NullPointerException If any arg is null.
     */
    public static void registerNodeModel(Class<?> nodeClass, Class<? extends NodeModel> nodeModelClass) {
        Validate.notNull(nodeClass);
        Validate.notNull(nodeModelClass);
        USER_MODELS.put(nodeClass, nodeModelClass);
    }

    /**
     * Get a node model for the given node.
     *
     * @param node The node.
     * @return The node model. This will be null if there is none.
     */
    @SuppressWarnings("unchecked")
    public static NodeModel getNodeModel(Object node) {
        // A node can implement NodeModel. In this case, return itself.

        if (node instanceof NodeModel) {
            return (NodeModel)node;
        }

        Class<? extends NodeModel> modelClass = USER_MODELS.get(node.getClass());

        if (modelClass == null) {
            modelClass = DEFAULT_MODELS.get(node.getClass());
        }

        if (modelClass == null && node instanceof Property) {
            modelClass = PropertyNodeModel.class;
        }

        Validate.isTrue(modelClass != null, "Could not get a model class for node class %s", node.getClass());
        NodeModel nodeModel = Cdi.get(modelClass);

        if (nodeModel instanceof InitializableNodeModel) {
            InitializableNodeModel inm = (InitializableNodeModel)nodeModel;
            inm.setNode(node);
        }

        return nodeModel;
    }
}

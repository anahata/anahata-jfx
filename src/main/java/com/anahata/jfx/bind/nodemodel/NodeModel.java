package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.Property;

/**
 * The model of a node, which provides access to its data.
 * 
 * @param <T> The node type.
 * @param <U> The value property type of the node.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface NodeModel<T, U extends Property> {
    /**
     * Get the property accessor for the model of a JavaFX node.
     * 
     * @param node The node to interrogate.
     * @return The property accessor.
     */
    public U getNodeModelValueProperty(T node);
}

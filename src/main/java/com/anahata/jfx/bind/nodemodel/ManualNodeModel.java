/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.Property;

/**
 * A node model that performs its own setting of values into the UI.
 * 
 * @param <T> The node type.
 * @param <U> The value property type of the node.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface ManualNodeModel<T, U extends Property<?>> extends NodeModel<T, U> {
    /**
     * Set the node model value.
     * 
     * @param node  The node to set the value on.
     * @param value The value.
     */
    void setValue(T node, Object value);
}

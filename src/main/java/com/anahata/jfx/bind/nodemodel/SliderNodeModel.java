/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.DoubleProperty;
import javafx.scene.control.Slider;
/**
 *
 * @author pablo
 */
public class SliderNodeModel implements NodeModel<Slider, DoubleProperty> {
    @Override
    public DoubleProperty getNodeModelValueProperty(Slider node) {
        return node.valueProperty();
    }
}

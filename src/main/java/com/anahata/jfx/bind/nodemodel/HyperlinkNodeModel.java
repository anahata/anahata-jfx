package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.StringProperty;
import javafx.scene.control.Hyperlink;
import javax.inject.Singleton;

/**
 * A Hyperlink node model.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class HyperlinkNodeModel implements NodeModel<Hyperlink, StringProperty> {
    @Override
    public StringProperty getNodeModelValueProperty(Hyperlink node) {
        return node.textProperty();
    }
}

/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.StringProperty;
import javafx.scene.text.Text;
import javax.inject.Singleton;

/**
 * A Text node model.
 *
 * @author sai.dandem
 */
@Singleton
public class TextNodeModel implements NodeModel<Text, StringProperty> {
    @Override
    public StringProperty getNodeModelValueProperty(Text node) {
        return node.textProperty();
    }
}

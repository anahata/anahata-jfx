/*
 *  Copyright © - 2013 Anahata Technologies.
 */

package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.ObjectProperty;
import javafx.scene.control.DatePicker;
/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class DatePickerNodeModel implements NodeModel<DatePicker, ObjectProperty>{

    @Override
    public ObjectProperty getNodeModelValueProperty(DatePicker node) {
        return node.valueProperty();
    }
    
}

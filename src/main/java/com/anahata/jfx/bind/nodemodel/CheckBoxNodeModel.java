package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.BooleanProperty;
import javafx.scene.control.CheckBox;
import javax.inject.Singleton;

/**
 * A CheckBox node model.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class CheckBoxNodeModel implements NodeModel<CheckBox, BooleanProperty> {
    @Override
    public BooleanProperty getNodeModelValueProperty(CheckBox node) {
        return node.selectedProperty();
    }
}

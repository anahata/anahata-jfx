package com.anahata.jfx.bind.nodemodel;

import com.anahata.jfx.bind.table.BindingTableView;
import com.anahata.util.collections.ListUtils;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javax.inject.Singleton;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class ListViewNodeModel implements ManualNodeModel<ListView, ObjectProperty<ObservableList<?>>> {
    @Override
    @SuppressWarnings("unchecked")
    public ObjectProperty<ObservableList<?>> getNodeModelValueProperty(ListView node) {
        return node.itemsProperty();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setValue(ListView node, Object value) {
        List list = node.getItems();
        ListUtils.setAll(list, (List)value);

        if (node.getUserData() != null && node.getUserData() instanceof BindingTableView) {
            BindingTableView table = (BindingTableView)node.getUserData();
            table.bindFromModel();
        }
    }
}

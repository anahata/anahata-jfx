package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.Property;
import javafx.scene.control.ComboBox;
import javax.inject.Singleton;

/**
 * A ComboBox node model.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class ComboBoxNodeModel implements NodeModel<ComboBox, Property> {
    @Override
    public Property getNodeModelValueProperty(ComboBox node) {
        return node.isEditable() ? node.getEditor().textProperty() : node.valueProperty();
    }
}

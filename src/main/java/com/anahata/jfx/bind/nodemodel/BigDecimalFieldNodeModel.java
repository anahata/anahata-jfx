 package com.anahata.jfx.bind.nodemodel;

import java.math.BigDecimal;
import javafx.beans.property.ObjectProperty;
import javax.inject.Singleton;
import jfxtras.labs.scene.control.BigDecimalField;

/**
 * A JFXtras BigDecimalField node model.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class BigDecimalFieldNodeModel implements NodeModel<BigDecimalField, ObjectProperty<BigDecimal>> {
    @Override
    public ObjectProperty<BigDecimal> getNodeModelValueProperty(BigDecimalField node) {
        return node.numberProperty();
    }
}

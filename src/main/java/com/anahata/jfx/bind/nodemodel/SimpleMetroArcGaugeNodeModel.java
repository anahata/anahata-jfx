/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.DoubleProperty;
import jfxtras.scene.control.gauge.linear.SimpleMetroArcGauge;
/**
 *
 * @author pablo
 */
public class SimpleMetroArcGaugeNodeModel implements NodeModel<SimpleMetroArcGauge, DoubleProperty> {
    @Override
    public DoubleProperty getNodeModelValueProperty(SimpleMetroArcGauge node) {
        return node.valueProperty();
    }
}

package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.BooleanProperty;
import javafx.scene.control.RadioButton;
import javax.inject.Singleton;

/**
 * A CheckBox node model.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class RadioButtonNodeModel implements NodeModel<RadioButton, BooleanProperty> {
    @Override
    public BooleanProperty getNodeModelValueProperty(RadioButton node) {
        return node.selectedProperty();
    }
}

package com.anahata.jfx.bind.nodemodel;

import com.anahata.jfx.bind.table.BindingTableView;
import com.anahata.util.collections.ListUtils;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
@Slf4j
public class TableViewNodeModel implements ManualNodeModel<TableView, ObjectProperty<ObservableList<?>>> {
    @Override
    @SuppressWarnings("unchecked")
    public ObjectProperty<ObservableList<?>> getNodeModelValueProperty(TableView node) {
        log.debug("getNodeModel {}", node);
        return node.itemsProperty();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setValue(TableView node, Object value) {
        log.debug("Setting items on tableView {} {}", node, value);
        List list = node.getItems();
        ListUtils.setAll(list, (List)value);
        
        if (node.getUserData() != null && node.getUserData() instanceof BindingTableView) {
            BindingTableView table = (BindingTableView)node.getUserData();
            table.bindFromModel();
        }
    }
}

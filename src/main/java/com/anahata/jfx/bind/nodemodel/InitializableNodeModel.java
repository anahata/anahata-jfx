package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.Property;

/**
 * A node model that can be initialized.
 * 
 * @param <T> The node type.
 * @param <U> The value property type of the node.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface InitializableNodeModel<T, U extends Property> extends NodeModel<T, U> {
    /**
     * Set the node that the initializable node model is related to.
     * 
     * @param node The node.
     */
    void setNode(T node);
}

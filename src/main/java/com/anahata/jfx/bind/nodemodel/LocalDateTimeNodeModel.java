/*
 *  Copyright © - EDS All rights reserved.
 */
package com.anahata.jfx.bind.nodemodel;

import java.time.LocalDateTime;
import javafx.beans.property.ObjectProperty;
import javax.inject.Singleton;
import jfxtras.scene.control.LocalDateTimeTextField;

/**
 *
 * @author Narayan G. Maharjan &lt;narayan@anahata-it.com.au&gt;
 */
@Singleton
public class LocalDateTimeNodeModel implements NodeModel<LocalDateTimeTextField, ObjectProperty<LocalDateTime>> {

    @Override
    public ObjectProperty<LocalDateTime> getNodeModelValueProperty(LocalDateTimeTextField node) {
        return node.localDateTimeProperty();
    }
}

/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.ObjectProperty;
import javafx.scene.control.Spinner;

/**
 *
 * @author ambasoft
 */
public class SpinnerNodeModel implements NodeModel<Spinner, ObjectProperty> {
    @Override
    public ObjectProperty getNodeModelValueProperty(Spinner node) {
        return node.getValueFactory().valueProperty();
    }
}

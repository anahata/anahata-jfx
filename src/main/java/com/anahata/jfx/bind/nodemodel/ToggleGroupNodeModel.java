package com.anahata.jfx.bind.nodemodel;

import java.util.HashMap;
import java.util.Map;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import org.apache.commons.lang3.Validate;

/**
 * Wraps a ToggleGroup to create some state for binding.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class ToggleGroupNodeModel implements InitializableNodeModel<ToggleGroup, ObjectProperty> {
    private final Map<Object, Toggle> toggles = new HashMap<>();
    
    private final ObjectProperty value = new SimpleObjectProperty();
    
    private boolean block = false;
    
    private ToggleGroup toggleGroup;
    
    @Override
    public ObjectProperty getNodeModelValueProperty(ToggleGroup node) {
        return value;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setNode(ToggleGroup tg) {
        Validate.notNull(tg);
        toggleGroup = tg;
        
        for (Toggle toggle : toggleGroup.getToggles()) {
            Validate.notNull(toggle.getUserData(), "userData not set for toggle %s", toggle);
            toggles.put(toggle.getUserData(), toggle);
        }
        
        toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle oldValue, Toggle newValue) {
                block = true;
                value.set(newValue.getUserData());
                block = false;
            }
        });
        
        value.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object oldValue, Object newValue) {
                if (block) {
                    return;
                }
                
                final Toggle toggle = toggles.get(newValue);
                Validate.validState(toggle != null, "Could not find toggle for value %s", newValue);
                toggleGroup.selectToggle(toggle);
            }
        });
    }
}

package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.Property;
import javax.inject.Singleton;

/**
 * A node model of a JavaFX property.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class PropertyNodeModel implements NodeModel<Property, Property> {
    @Override
    public Property getNodeModelValueProperty(Property node) {
        return node;
    }
}

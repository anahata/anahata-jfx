/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.Property;
import javafx.scene.control.ChoiceBox;
import javax.inject.Singleton;
/**
 *
 * @author sai.dandem
 */
@Singleton
public class ChoiceBoxNodeModel implements NodeModel<ChoiceBox, Property> {
    @Override
    public Property getNodeModelValueProperty(ChoiceBox node) {
        return node.valueProperty();
    }
}

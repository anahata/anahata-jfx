package com.anahata.jfx.bind.nodemodel;

import com.anahata.jfx.scene.control.AutoCompleteTextField;
import javafx.beans.property.ObjectProperty;
import javax.inject.Singleton;

/**
 * Node model for an AutoCompleteTextField control.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class AutoCompleteTextFieldNodeModel implements NodeModel<AutoCompleteTextField, ObjectProperty> {
    @Override
    public ObjectProperty getNodeModelValueProperty(AutoCompleteTextField node) {
        return node.valueProperty();
    }
}

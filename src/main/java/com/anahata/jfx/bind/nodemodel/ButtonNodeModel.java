package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;
import javax.inject.Singleton;

/**
 * A CheckBox node model.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class ButtonNodeModel implements NodeModel<Button, StringProperty> {
    @Override
    public StringProperty getNodeModelValueProperty(Button node) {
        return node.textProperty();
    }
}

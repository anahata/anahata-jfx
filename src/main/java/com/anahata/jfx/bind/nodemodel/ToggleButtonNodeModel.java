package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.BooleanProperty;
import javafx.scene.control.ToggleButton;
import javax.inject.Singleton;

/**
 * A ToggleButton node model.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class ToggleButtonNodeModel implements NodeModel<ToggleButton, BooleanProperty> {
    @Override
    public BooleanProperty getNodeModelValueProperty(ToggleButton node) {
        return node.selectedProperty();
    }
}

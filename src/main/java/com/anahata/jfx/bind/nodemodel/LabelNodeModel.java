package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javax.inject.Singleton;

/**
 * A Label node model.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Singleton
public class LabelNodeModel implements NodeModel<Label, StringProperty> {
    @Override
    public StringProperty getNodeModelValueProperty(Label node) {
        return node.textProperty();
    }
}

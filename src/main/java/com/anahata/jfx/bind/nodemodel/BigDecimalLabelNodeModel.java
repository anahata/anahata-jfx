/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.StringProperty;
import jfxtras.labs.scene.control.BigDecimalLabel;

/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class BigDecimalLabelNodeModel implements NodeModel<BigDecimalLabel, StringProperty> {

    @Override
    public StringProperty getNodeModelValueProperty(BigDecimalLabel node) {
        return node.textProperty();
    }

}

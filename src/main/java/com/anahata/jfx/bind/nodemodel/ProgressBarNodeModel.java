/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.nodemodel;

import javafx.beans.property.DoubleProperty;
import javafx.scene.control.ProgressBar;
/**
 *
 * @author pablo
 */
public class ProgressBarNodeModel implements NodeModel<ProgressBar, DoubleProperty> {
    @Override
    public DoubleProperty getNodeModelValueProperty(ProgressBar node) {
        return node.progressProperty();
    }
}

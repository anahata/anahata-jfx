/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Converter for DatePicker JavaFX 8 node, converts LocalDate <-> Date.
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class DateLocalDateConverter implements Converter<Date, LocalDate> {

    @Override
    public Date getAsDomainModelValue(Object node, LocalDate nodeModelValue) {
        if (nodeModelValue == null) {
            return null;
        }
        Instant instant = nodeModelValue.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
        Date ret = Date.from(instant);
        return ret;
    }

    @Override
    public LocalDate getAsNodeModelValue(Object node, Date domainModelValue) {
        if (domainModelValue == null) {
            return null;
        }
        Instant instant = domainModelValue.toInstant();
        LocalDate ret = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
        return ret;
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

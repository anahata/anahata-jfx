package com.anahata.jfx.bind.converter;

/**
 * Convert between types domain model and node model (JavaFX UI component) types.
 *
 * @param <T> The domain model type.
 * @param <U> The UI node model type.
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface Converter<T, U> {
    /**
     * For the node, get the domain model value.
     *
     * @param node           The node.
     * @param nodeModelValue The node model value.
     * @return The domain model value.
     */
    public T getAsDomainModelValue(Object node, U nodeModelValue);

    /**
     * For the node, get the node model value.
     *
     * @param node             The node.
     * @param domainModelValue The domain model value.
     * @return The node model value.
     */
    public U getAsNodeModelValue(Object node, T domainModelValue);

    /**
     * Format a string value into a defined format.
     *
     * @param value The value to format. Can be null.
     * @return The formatted value. Never null, it will be an empty string if required.
     */
    public String format(String value);
}

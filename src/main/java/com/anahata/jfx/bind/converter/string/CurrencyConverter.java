/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import java.util.Currency;

/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class CurrencyConverter implements Converter<Currency, String> {

    @Override
    public Currency getAsDomainModelValue(Object node, String nodeModelValue) {
        try {
            return Currency.getInstance(nodeModelValue);
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public String getAsNodeModelValue(Object node, Currency domainModelValue) {
        if (domainModelValue == null) {
            return null;
        }
        return domainModelValue.getDisplayName();
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

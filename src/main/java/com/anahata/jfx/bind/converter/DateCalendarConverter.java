package com.anahata.jfx.bind.converter;

import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;

/**
 * Convert between a Date model and Calendar node value.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class DateCalendarConverter implements Converter<Date, Calendar> {
    @Override
    public Date getAsDomainModelValue(Object node, Calendar nodeModelValue) {
        if (nodeModelValue == null) {
            return null;
        }
        
        return nodeModelValue.getTime();
    }

    @Override
    public Calendar getAsNodeModelValue(Object node, Date domainModelValue) {
        if (domainModelValue == null) {
            return null;
        }
        
        return DateUtils.toCalendar(domainModelValue);
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

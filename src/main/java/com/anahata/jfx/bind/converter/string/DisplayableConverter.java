package com.anahata.jfx.bind.converter.string;

import com.anahata.util.formatting.Displayable;
import com.anahata.jfx.bind.converter.Converter;

/**
 * Convert Displayable objects. This is a read-only conversion, if a write is attempted an
 * UnsupportedOpreationException will occur.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class DisplayableConverter implements Converter<Displayable, String> {
    @Override
    public Displayable getAsDomainModelValue(Object node, String nodeModelValue) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getAsNodeModelValue(Object node, Displayable domainModelValue) {
        return domainModelValue != null ? domainModelValue.getDisplayValue() : null;
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

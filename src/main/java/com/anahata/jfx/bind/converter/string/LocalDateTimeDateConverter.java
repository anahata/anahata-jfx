/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Narayan G. Maharjan &lt;narayan@anahata-it.com.au&gt;
 */
@Slf4j
public class LocalDateTimeDateConverter implements Converter<Date, LocalDateTime> {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM,
            FormatStyle.MEDIUM);

    @Override
    public String format(String value) {
        return value;
    }

    private LocalDateTime getLocalDateTime(Date date) {
//        log.debug("Got Date :: {}", date);
        if (date != null) {
            LocalDate ld = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalTime time = date.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
            return LocalDateTime.of(ld, time);
        }
        return null;
    }

    private Date getDate(LocalDateTime ldt) {
//        log.debug("Got LocalDate :: {}", ldt);
        if (ldt != null) {
            return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        }
        return null;
    }

    @Override
    public Date getAsDomainModelValue(Object node, LocalDateTime nodeModelValue) {
//        log.debug("LocalDateTime :: {}", nodeModelValue);
        Date d = getDate(nodeModelValue);
//        log.debug("LocalDateTime -> Date ::", d);
        return d;
    }

    @Override
    public LocalDateTime getAsNodeModelValue(Object node, Date domainModelValue) {
//        log.debug("Date  :: {}", domainModelValue);
        LocalDateTime ldt = getLocalDateTime(domainModelValue);;
//        log.debug("Date->LocalDateTime :: {}", ldt);
        return ldt;
    }

}

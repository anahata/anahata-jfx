package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import org.apache.commons.lang3.StringUtils;

/**
 * Converts a null string to an empty one. Used primarily to work around a JavaFX bug where a null TextArea value
 * causes a NullPointerException if tab is pressed, in JavaFX 2.2.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class NullEmptyStringConverter implements Converter<String, String> {
    public static final NullEmptyStringConverter INSTANCE = new NullEmptyStringConverter();

    @Override
    public String getAsDomainModelValue(Object node, String nodeModelValue) {
        return StringUtils.defaultIfEmpty(nodeModelValue, null);
    }

    @Override
    public String getAsNodeModelValue(Object node, String domainModelValue) {
        return StringUtils.defaultIfEmpty(domainModelValue, "");
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

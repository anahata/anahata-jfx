package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.commons.lang3.StringUtils;

/**
 * Convert a BigDecimal with a String.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class BigDecimalConverter implements Converter<BigDecimal, String> {
    public static final BigDecimalConverter INSTANCE = new BigDecimalConverter();
    
    private static final DecimalFormat FORMAT = (DecimalFormat)NumberFormat.getNumberInstance();
    
    @Override
    public BigDecimal getAsDomainModelValue(Object node, String nodeModelValue) {
        BigDecimal ret = null;
        try {
            ret = new BigDecimal(StringUtils.trim(nodeModelValue));
        } catch (Exception e){
            
        }
        return ret;
        //return !StringUtils.isBlank(nodeModelValue) ?  : null;
    }

    @Override
    public String getAsNodeModelValue(Object node, BigDecimal domainModelValue) {
        if (domainModelValue == null) {
            return null;
        }
        
        FORMAT.setMinimumFractionDigits(domainModelValue.scale());
        FORMAT.setMaximumFractionDigits(domainModelValue.scale());
        return FORMAT.format(domainModelValue);
    }

    @Override
    public String format(String value) {
        return value;
    }
}

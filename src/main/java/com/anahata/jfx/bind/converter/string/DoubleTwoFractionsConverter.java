/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author sai.dandem
 */
public class DoubleTwoFractionsConverter implements Converter<Double, String> {
    public static DoubleConverter INSTANCE = new DoubleConverter();

    private static final DecimalFormat FORMAT = (DecimalFormat)NumberFormat.getNumberInstance();

    static {
        FORMAT.setMinimumFractionDigits(2);
        FORMAT.setMaximumFractionDigits(2);
    }

    @Override
    public Double getAsDomainModelValue(Object node, String string) {
        string = StringUtils.trim(string);
        if (!StringUtils.isBlank(string) && string.matches("^\\d*(\\.\\d{0,9})?$")) {
            return (double) Math.round(Double.valueOf(string) * 100) / 100; // rounding to two decimals
        }
        return null;
    }

    @Override
    public String getAsNodeModelValue(Object node, Double object) {
        return object != null ? FORMAT.format(object) : null;
    }

    @Override
    public String format(String value) {
        return value;
    }
}

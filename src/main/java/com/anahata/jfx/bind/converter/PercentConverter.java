package com.anahata.jfx.bind.converter;

import java.math.BigDecimal;

/**
 * Convert a BigDecimal to a percentage value. The BigDecimal value will be in the range of 0 to 1.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class PercentConverter implements Converter<BigDecimal, BigDecimal> {
    public static final PercentConverter INSTANCE = new PercentConverter();
    
    private static final BigDecimal ONE_HUNDRED = new BigDecimal("100.0");
    
    @Override
    public BigDecimal getAsDomainModelValue(Object node, BigDecimal nodeModelValue) {
        return nodeModelValue == null ? null : nodeModelValue.divide(ONE_HUNDRED);
    }
    
    @Override
    public BigDecimal getAsNodeModelValue(Object node, BigDecimal domainModelValue) {
        return domainModelValue == null ? null : domainModelValue.multiply(ONE_HUNDRED);
    }
    
    @Override
    public String format(String value) {
        return value;
    }
}

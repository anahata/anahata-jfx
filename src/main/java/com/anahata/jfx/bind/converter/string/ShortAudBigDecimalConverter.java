/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.commons.lang3.StringUtils;


/**
 * 
 * @author pablo
 */
public class ShortAudBigDecimalConverter implements Converter<BigDecimal, String> {

    public static final ShortAudBigDecimalConverter INSTANCE = new ShortAudBigDecimalConverter();

    private static final DecimalFormat FORMAT = (DecimalFormat)NumberFormat.getCurrencyInstance();
    static {
        FORMAT.setMaximumFractionDigits(2);
    }

    @Override
    public BigDecimal getAsDomainModelValue(Object node, String nodeModelValue) {
        return !StringUtils.isBlank(nodeModelValue)
                ? new BigDecimal(StringUtils.trim(nodeModelValue))
                : null;
    }

    @Override
    public String getAsNodeModelValue(Object node, BigDecimal domainModelValue) {
        if (domainModelValue == null) {
            return null;
        }

        String val = FORMAT.format(domainModelValue);
        if (val.endsWith(".00")) {
            val = val.substring(0, val.length() - 3);
        }
        return val;
    }

    @Override
    public String format(String value) {
        return value;
    }
    
    public static void main(String[] args) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setMinimumFractionDigits(0);
        String s = nf.format(new BigDecimal(100.50));
        System.out.println(s);
    }

}

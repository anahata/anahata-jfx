/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

/**
 *
 * @author Arslan
 */
public class ShortDateTimeAmPmConverter extends AbstractDateConverter  {

    @Override
    public String getPattern() {
        return "d/M/yy h:mm a";
    }

}

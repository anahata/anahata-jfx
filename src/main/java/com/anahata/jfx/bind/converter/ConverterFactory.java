package com.anahata.jfx.bind.converter;

import com.anahata.jfx.bind.converter.string.*;
import com.anahata.util.reflect.ReflectionUtils;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.Validate;

/**
 * Provide converters.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConverterFactory {
    private static final List<? extends Converter> DEFAULT_CONVERTERS = Arrays.asList(
            NullEmptyStringConverter.INSTANCE,
            LongConverter.INSTANCE,
            DoubleConverter.INSTANCE,
            IntegerConverter.INSTANCE,
            BigDecimalConverter.INSTANCE);

    private static final Map<Class, Map<Class, Converter>> CONVERTERS = new HashMap<>();

    static {
        for (final Converter converter : DEFAULT_CONVERTERS) {
            addConverter(converter);
        }
    }
    
    /**
     * Add a converter to be invoked by default.
     * 
     * @param converter The converter.
     */
    public static void addConverter(Converter converter) {
        final Class[] types = ReflectionUtils.getGenericArgs(Converter.class, converter.getClass());

        if (types != null) {
            Map<Class, Converter> convs = CONVERTERS.get(types[0]);

            if (convs == null) {
                convs = new HashMap<>();
                CONVERTERS.put(types[0], convs);
            }

            convs.put(types[1], converter);
        }
    }
    
    /**
     * Get a default converter given the domain model type and node model type.
     *
     * @param domainModelType The domain model type. Required.
     * @param nodeModelType   The node model type. Required.
     * @return The converter, or null if none found.
     * @throws NullPointerException If any arg is null.
     */
    public static Converter getDefaultConverter(Class domainModelType, Class nodeModelType) {
        Validate.notNull(domainModelType);
        Validate.notNull(nodeModelType);

        domainModelType = ClassUtils.primitiveToWrapper(domainModelType);
        nodeModelType = ClassUtils.primitiveToWrapper(nodeModelType);
        final Map<Class, Converter> convs = CONVERTERS.get(domainModelType);

        if (convs != null) {
            return convs.get(nodeModelType);
        }

        return null;
    }

    /**
     * Validate that a converter can be used against the given types.
     *
     * @param converter       The converter.
     * @param domainModelType The domain model type.
     * @param nodeModelType   The node model type.
     * @throws NullPointerException     If any arg is null.
     * @throws IllegalArgumentException If the converter generic types don't match the given classes.
     */
    public static void validateConverter(Converter converter, Class domainModelType, Class nodeModelType) {
        Validate.notNull(converter);
        Validate.notNull(domainModelType);
        Validate.notNull(nodeModelType);
        final Class[] types = ReflectionUtils.getGenericArgs(Converter.class, converter.getClass());
        Validate.isTrue(types != null, "Converter invalid, could not obtain generic types");
        Validate.isTrue(domainModelType.equals(types[0]),
                "The domain model type doesn't match, converter is %s, arg is %s", types[0].getSimpleName(),
                domainModelType.getSimpleName());
        Validate.isTrue(nodeModelType.equals(types[1]),
                "The node model type doesn't match, converter is %s, arg is %s", types[1].getSimpleName(),
                nodeModelType.getSimpleName());
    }
}

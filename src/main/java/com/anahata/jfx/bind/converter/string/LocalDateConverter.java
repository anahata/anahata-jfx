/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import com.anahata.util.date.DateUtil;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author pablo
 */
@Slf4j
public class LocalDateConverter implements Converter< Date, LocalDate> {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM,
            FormatStyle.MEDIUM);

    @Override
    public String format(String value) {
        return value;
    }

    private LocalDate getLocalDate(Date date) {
        if (date != null) {
            LocalDate ld = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();            
            return ld;
        }
        return null;
    }

    private Date getDate(LocalDate ldt) {
        if (ldt != null) {
            return DateUtil.toDate(ldt);
        }
        return null;
    }

    @Override
    public Date getAsDomainModelValue(Object node, LocalDate nodeModelValue) {
        Date d = getDate(nodeModelValue);
        return d;
    }

    @Override
    public LocalDate getAsNodeModelValue(Object node, Date domainModelValue) {
        LocalDate ldt = getLocalDate(domainModelValue);;
        return ldt;
    }

}

package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import org.apache.commons.io.FileUtils;

/**
 * Converts a long that represents a byte count to readable format using commons io
 * FileUtils.byteCountToDisplaySize(domainModelValue).
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class ByteCountDisplaySizeConverter implements Converter<Long, String> {
    @Override
    public Long getAsDomainModelValue(Object node, String nodeModelValue) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getAsNodeModelValue(Object node, Long domainModelValue) {
        return domainModelValue != null ? FileUtils.byteCountToDisplaySize(domainModelValue) : null;
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

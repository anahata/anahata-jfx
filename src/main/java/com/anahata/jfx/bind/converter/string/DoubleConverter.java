package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import org.apache.commons.lang3.StringUtils;

/**
 * Convert a Long with a String.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class DoubleConverter implements Converter<Double, String> {
    public static DoubleConverter INSTANCE = new DoubleConverter();
    
    @Override
    public Double getAsDomainModelValue(Object node, String string) {
        try {
            return Double.valueOf(StringUtils.trim(string));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String getAsNodeModelValue(Object node, Double object) {
        return object != null ? String.valueOf(object) : null;
    }
    
    @Override
    public String format(String value) {
        return value;
    }
}

package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
/**
 * Calls toString on an object.
 * 
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public class ToStringConverter implements Converter<Object, String> {
    public static ToStringConverter INSTANCE = new ToStringConverter();

    @Override
    public Object getAsDomainModelValue(Object node, String nodeModelValue) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getAsNodeModelValue(Object node, Object domainModelValue) {
        return domainModelValue != null ? domainModelValue.toString() : "";
    }
    
    @Override
    public String format(String value) {
        return value;
    }
}

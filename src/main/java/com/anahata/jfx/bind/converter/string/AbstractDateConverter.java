/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author pablo
 */
public abstract class AbstractDateConverter implements Converter<Date, String> {
    
    private DateFormat dateFormat = new SimpleDateFormat(getPattern());
    
    @Override
    public Date getAsDomainModelValue(Object node, String nodeModelValue) {
        if (nodeModelValue == null) {
            return null;
        }
        try {
            return dateFormat.parse(nodeModelValue);
        } catch (ParseException e) {
            return null;
        }
    }

    @Override
    public String getAsNodeModelValue(Object node, Date domainModelValue) {
        if (domainModelValue == null) {
            return null;
        }
        return dateFormat.format(domainModelValue);
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public abstract String getPattern();
    
}

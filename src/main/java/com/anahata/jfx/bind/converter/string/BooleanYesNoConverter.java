package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.*;

/**
 * Converts a boolean value to from Yes/No string
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class BooleanYesNoConverter implements Converter<Boolean, String> {
    public static final BooleanYesNoConverter INSTANCE = new BooleanYesNoConverter();

    @Override
    public Boolean getAsDomainModelValue(Object node, String b) {
        return "Yes".equalsIgnoreCase(b);
    }

    @Override
    public String getAsNodeModelValue(Object node, Boolean b) {
        return b == null ? null : b ? "Yes" : "No";
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

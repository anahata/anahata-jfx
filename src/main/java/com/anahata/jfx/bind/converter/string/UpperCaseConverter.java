/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
/**
 *
 * @author pablo
 */
public class UpperCaseConverter implements Converter<String, String> {
    public static UpperCaseConverter INSTANCE = new UpperCaseConverter();

    @Override
    public String getAsDomainModelValue(Object node, String nodeModelValue) {
        if (nodeModelValue != null) {
            return nodeModelValue.toUpperCase();
        } else {
            return null;
        }
    }

    @Override
    public String getAsNodeModelValue(Object node, String domainModelValue) {
        if (domainModelValue != null) {
            return domainModelValue.toUpperCase();
        } else {
            return null;
        }
    }
    
    @Override
    public String format(String value) {
        return value;
    }
    
}

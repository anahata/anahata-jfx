package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import org.apache.commons.lang3.StringUtils;

/**
 * Convert a Long with a String.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class LongConverter implements Converter<Long, String> {
    public static LongConverter INSTANCE = new LongConverter();
    
    @Override
    public Long getAsDomainModelValue(Object node, String string) {
        return !StringUtils.isBlank(string) ? Long.valueOf(StringUtils.trim(string)) : null;
    }

    @Override
    public String getAsNodeModelValue(Object node, Long object) {
        return object != null ? String.valueOf(object) : null;
    }
    
    @Override
    public String format(String value) {
        return value;
    }
}

/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author Arslan
 */
public class CapitalizeCaseConverter implements Converter<String, String> {
    
    public static CapitalizeCaseConverter INSTANCE = new CapitalizeCaseConverter();
    
    @Override
    public String getAsDomainModelValue(Object node, String nodeModelValue) {
        if (nodeModelValue != null) {
            return WordUtils.capitalize(nodeModelValue);
        } else {
            return null;
        }
    }

    @Override
    public String getAsNodeModelValue(Object node, String domainModelValue) {
        if (domainModelValue != null) {
            return WordUtils.capitalize(domainModelValue);
        } else {
            return null;
        }
    }

    @Override
    public String format(String value) {
        return value;
    }

}

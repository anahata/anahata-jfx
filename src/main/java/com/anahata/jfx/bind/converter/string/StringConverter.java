package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
/**
 * Convert a String (do nothing).
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class StringConverter implements Converter<String, String> {
    public static StringConverter INSTANCE = new StringConverter();
    
    @Override
    public String getAsDomainModelValue(Object node, String string) {
        return string;
    }

    @Override
    public String getAsNodeModelValue(Object node, String object) {
        return object;
    }
    
    @Override
    public String format(String value) {
        return value;
    }
}

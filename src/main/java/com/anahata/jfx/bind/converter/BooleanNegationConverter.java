package com.anahata.jfx.bind.converter;

/**
 * Negates a boolean value.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class BooleanNegationConverter implements Converter<Boolean, Boolean> {
    public static final BooleanNegationConverter INSTANCE = new BooleanNegationConverter();

    @Override
    public Boolean getAsDomainModelValue(Object node, Boolean b) {
        return !b;
    }

    @Override
    public Boolean getAsNodeModelValue(Object node, Boolean b) {
        return !b;
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

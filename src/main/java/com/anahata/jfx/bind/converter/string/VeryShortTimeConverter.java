/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

/**
 *
 * @author Arslan
 */
public class VeryShortTimeConverter extends AbstractDateConverter  {

    @Override
    public String getPattern() {
        return "HH:mm";
    }

}

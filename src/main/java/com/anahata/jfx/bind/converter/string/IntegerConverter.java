package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import org.apache.commons.lang3.StringUtils;

/**
 * Convert a Long with a String.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class IntegerConverter implements Converter<Integer, String> {
    public static IntegerConverter INSTANCE = new IntegerConverter();
    
    @Override
    public Integer getAsDomainModelValue(Object node, String string) {
        return !StringUtils.isBlank(string) ? Integer.valueOf(StringUtils.trim(string)) : null;
    }

    @Override
    public String getAsNodeModelValue(Object node, Integer object) {
        return object != null ? String.valueOf(object) : null;
    }
    
    @Override
    public String format(String value) {
        return value;
    }
}

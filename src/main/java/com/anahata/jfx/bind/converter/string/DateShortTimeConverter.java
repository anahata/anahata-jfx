/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Converter class to convert the provided date to "dd/MM/yyyy hh:mm a" format.
 *
 * @author sai.dandem
 */
public class DateShortTimeConverter implements Converter<Date, String> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

    @Override
    public Date getAsDomainModelValue(Object node, String nodeModelValue) {
        if (nodeModelValue == null) {
            return null;
        }
        try {
            return DATE_FORMAT.parse(nodeModelValue);
        } catch (ParseException e) {
            return null;
        }
    }

    @Override
    public String getAsNodeModelValue(Object node, Date domainModelValue) {
        if (domainModelValue == null) {
            return null;
        }

        return DATE_FORMAT.format(domainModelValue);
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}

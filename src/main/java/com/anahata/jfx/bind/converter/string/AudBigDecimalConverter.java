/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.commons.lang3.StringUtils;

/**
 * Formats BigDecimal values for BigDecimalField and BigDecimalLabel to use 2 decimal places and $ sign.
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class AudBigDecimalConverter implements Converter<BigDecimal, String> {

    public static final AudBigDecimalConverter INSTANCE = new AudBigDecimalConverter();

    private static final DecimalFormat FORMAT = (DecimalFormat)NumberFormat.getCurrencyInstance();
    static {
        FORMAT.setMinimumFractionDigits(2);
        FORMAT.setMaximumFractionDigits(2);
    }

    @Override
    public BigDecimal getAsDomainModelValue(Object node, String nodeModelValue) {
        return !StringUtils.isBlank(nodeModelValue)
                ? new BigDecimal(StringUtils.trim(nodeModelValue))
                : null;
    }

    @Override
    public String getAsNodeModelValue(Object node, BigDecimal domainModelValue) {
        if (domainModelValue == null) {
            return null;
        }
        
        String ret = FORMAT.format(domainModelValue);
        return ret;
    }

    @Override
    public String format(String value) {
        return value;
    }

    public static String format(BigDecimal value){
        return FORMAT.format(value);
    }
}

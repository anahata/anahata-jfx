/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind.converter.string;

/**
 *
 * @author Arslan
 */
public class ShortTimeConverter extends AbstractDateConverter  {

    @Override
    public String getPattern() {
        return "hh:mm a";
    }

}

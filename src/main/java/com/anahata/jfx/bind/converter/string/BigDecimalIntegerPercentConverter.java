package com.anahata.jfx.bind.converter.string;

import com.anahata.jfx.bind.converter.Converter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.commons.lang3.StringUtils;

/**
 * Convert a BigDecimal to a String as a percentage value. The BigDecimal value will be in the range of 0 to 1. This
 * version does not allow any decimal places in the converted value.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class BigDecimalIntegerPercentConverter implements Converter<BigDecimal, String> {
    public static final BigDecimalIntegerPercentConverter INSTANCE = new BigDecimalIntegerPercentConverter();
    
    private static final DecimalFormat FORMAT = (DecimalFormat)NumberFormat.getNumberInstance();
    
    private static final BigDecimal ONE_HUNDRED = new BigDecimal("100.0");
    
    static {
        FORMAT.setMinimumFractionDigits(0);
        FORMAT.setMaximumFractionDigits(0);
    }
    
    @Override
    public BigDecimal getAsDomainModelValue(Object node, String nodeModelValue) {
        return !StringUtils.isBlank(nodeModelValue) ? new BigDecimal(StringUtils.trim(nodeModelValue)).divide(
                ONE_HUNDRED) : null;
    }

    @Override
    public String getAsNodeModelValue(Object node, BigDecimal domainModelValue) {
        if (domainModelValue == null) {
            return null;
        }
        
        return FORMAT.format(domainModelValue.multiply(ONE_HUNDRED));
    }

    @Override
    public String format(String value) {
        return value;
    }
}

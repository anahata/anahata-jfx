package com.anahata.jfx.bind.converter.string;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class DateConverter extends AbstractDateConverter  {

    @Override
    public String getPattern() {
        return "dd/MM/yyyy";
    }

}

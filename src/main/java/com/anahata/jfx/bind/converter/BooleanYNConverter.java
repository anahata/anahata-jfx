package com.anahata.jfx.bind.converter;

/**
 * Converts a boolean value to from Y/N string
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class BooleanYNConverter implements Converter<Boolean, String> {
    public static final BooleanYNConverter INSTANCE = new BooleanYNConverter();

    @Override
    public Boolean getAsDomainModelValue(Object node, String b) {
        return "Y".equalsIgnoreCase(b);
    }

    @Override
    public String getAsNodeModelValue(Object node, Boolean b) {
        return b ? "Y" : "N";
    }

    @Override
    public String format(String value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

package com.anahata.jfx.layout;

import com.anahata.jfx.JfxUtils;
import com.anahata.jfx.navigation.NavigationConfirmation;
import com.anahata.util.cdi.Cdi;
import java.util.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.AllArgsConstructor;
import lombok.Delegate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class ScreensController extends StackPane {
    /**
     * A list of all managed screens indexed by screen name.
     */
    private Map<String, ScreenInfo> screens = new HashMap<>();

    /**
     * The managed screen being displayed.
     */
    @Getter
    private ScreenInfo current;

    private ScreensController parentScreensController;

    public ScreensController() {
        super();
    }

    public ScreensController(ScreensController source) {
        super();
        for (Map.Entry<String, ScreenInfo> entry : source.screens.entrySet()) {
            ScreenInfo clone = entry.getValue().clone(this);
            screens.put(entry.getKey(), clone);
        }
    }
    
    /**
     * Returns a read only list of all {@link ScreenInfo} objects.
     * @return 
     */
    public List<ScreenInfo> getAllScreenInfos() {
        return (List) Collections.unmodifiableList(new ArrayList(screens.values()));
    }

    public void reload() {
        String currentId = current.screenName;
        unloadAll();
        navigate(currentId);
    }

    public void unloadAll() {
        getChildren().clear();
        for (ScreenInfo si : screens.values()) {
            si.unload();
        }
    }

    /**
     * Preloads some screens (before they are navigated to)
     */
    public void preload(String... screenNames) {
        long ts = System.currentTimeMillis();
        for (String si : screenNames) {
            screens.get(si).load();
        }
        ts = System.currentTimeMillis() - ts;
        log.info("preload took {} ms to load {} screens", ts, screenNames.length);
    }

    /**
     * Preloads all screens (before they are navigated to)
     */
    public void preloadAll() {
        long ts = System.currentTimeMillis();
        for (ScreenInfo si : screens.values()) {
            si.load();
        }
        ts = System.currentTimeMillis() - ts;
        log.info("preloadAll took {} ms to load {} screens", ts, screens.values());
    }
    
    /**
     * Load a screen to manage navigation for.
     *
     * @param screenName The name of the screen.
     * @param fxml       The FXML defining the UI of the screen.
     */
    public void addScreen(String screenName, String fxml) {
        ScreenInfo sci = new ScreenInfo(this, screenName, fxml, null, null, false);

        screens.put(screenName, sci);
    }

//    /**
//     * Add a screen to the controller.
//     *
//     * @param name       The screen name. Required.
//     * @param screen     The screen node.
//     * @param controller The screen controller.
//     */
//    public void addScreen(String name, Node screen, ControlledScreen controller) {
//        screens.put(name, new ScreenInfo(this, name, null, screen, controller, true));
//
//        if (screen instanceof NavigationConfirmation) {
//            navigator.registerConfirmation((NavigationConfirmation)screen);
//        }
//    }
    /**
     * Get the controller of a given screen.
     *
     * @param <T>        The type of the controller.
     * @param screenName The screen name.
     * @return The controller, or null if the screen or controller weren't found.
     */
    @SuppressWarnings("unchecked")
    public <T> T getController(final String screenName) {
        ScreenInfo info = screens.get(screenName);
        return info == null ? null : (T)info.getController();
    }

    /**
     * Get the controller of a given screen.
     *
     * @param <T>        The type of the controller.
     * @param screenName The screen name.
     * @return The controller, or null if the screen or controller weren't found.
     */
    @SuppressWarnings("unchecked")
    public ScreenInfo getScreenInfo(final String screenName) {
        return screens.get(screenName);
    }

    public boolean isScreenRegistered(String screenName) {
        return screens.get(screenName) != null;
    }

    public DialogContext newDialog(final Stage owner, final String screenName, String title, double width) {
        DialogContext info = new DialogContext(owner, this, screenName, title, width, null, true);
        return info;
    }

    public DialogContext newDialog(final Stage owner, final String screenName, String title, double width, boolean modal) {
        DialogContext info = new DialogContext(owner, this, screenName, title, width, null, modal);
        return info;
    }

    public DialogContext newDialog(final Stage owner, final String screenName, String title, double width, double height) {
        DialogContext info = new DialogContext(owner, this, screenName, title, width, height, true);
        return info;
    }

    public DialogContext newDialog(final String screenName, String title, double width) {
        DialogContext info = new DialogContext(getCurrentStage(), this, screenName, title, width, null, true);
        return info;
    }

    public DialogContext newDialog(final String screenName, String title, double width, boolean modal) {
        DialogContext info = new DialogContext(getCurrentStage(), this, screenName, title, width, null, modal);
        return info;
    }

    public DialogContext newDialog(final String screenName, String title, double width, double height) {
        DialogContext info = new DialogContext(getCurrentStage(), this, screenName, title, width, height, true);
        return info;
    }

    public DialogContext newDialog(final String screenName, String title, double width, double height, boolean modal) {
        DialogContext info = new DialogContext(getCurrentStage(), this, screenName, title, width, height, modal);
        return info;
    }

    public Stage getCurrentStage() {
        return JfxUtils.getStage(getCurrent().getNode());
    }

    /**
     * Navigate to a screen.
     *
     * @param name The screen name.
     * @throws IllegalArgumentException if there is no screen with such name
     */
    public ScreenInfo navigate(final String name, boolean invokeOnDeparture, boolean invokeOnArrival) {
        long ts = System.currentTimeMillis();
        String currentName = current != null ? current.getScreenName() : " ";

        log.debug("Navigating to: {}, current ={}", name, currentName);
        ScreenInfo requested = screens.get(name);

        if (requested != null) {
            // In case it was lazy loaded.

            if (current != null) {
                if (!canNavigate()) {
                    return null;
                }
                if (invokeOnDeparture) {
                    current.getController().onDeparture();
                }
                current.getNode().setVisible(false);
            }

            if (!getChildren().contains(requested.getNode())) {
                getChildren().add(requested.getNode());
            }

            requested.getNode().setVisible(true);
            current = requested;
            if (invokeOnArrival) {
                requested.getController().onArrival();
            }
            ts = System.currentTimeMillis() - ts;
            log.trace("Navigating from {} to: {}, took {} ms.", currentName, currentName, ts);
        } else {
            throw new IllegalArgumentException("Could not navigate to screen " + name);
        }

        return requested;
    }

    /**
     * Navigate to a screen.
     *
     * @param name The screen name.
     * @throws IllegalArgumentException if there is no screen with such name
     */
    public ScreenInfo navigate(final String name) {
        return navigate(name, true, true);
    }

    /**
     * Removes a given screen from the screen list.
     *
     * @param name the screen name
     * @return true if the screen was sucessfully removed, false if there was no screen with such name
     */
    public boolean unloadScreen(String name) {
        ScreenInfo si = screens.remove(name);

        if (si == null) {
            return false;
        } else {
            getChildren().remove(si.getNode());
        }

        return true;
    }

    public boolean isDialog() {
        return current.dialogInfo != null;
    }

    /**
     *
     * @return
     */
    public boolean canNavigate() {
        if (current == null) {
            return true;
        }
        if (current.getController() instanceof NavigationConfirmation) {
            NavigationConfirmation nc = ((NavigationConfirmation)current.getController());
            return nc.canNavigateAway();
        }
        return true;
    }

    public void closeDialog() {
        if (isDialog()) {
            current.dialogInfo.stage.close();
        } else {
            log.debug("Not in dialog");
        }
    }

    public static class ScreenInfo {
        @Getter
        private String screenName;

        @Getter
        private String fxmlFile;

        @Getter
        private Node node;

        @Getter
        private ControlledScreen controller;

        private boolean loaded;

        private ScreensController screensController;

        private DialogContext dialogInfo;

        public ScreenInfo(ScreensController screensController, String screenName, String fxmlFile, Node node,
                ControlledScreen controller, boolean loaded) {
            this.screensController = screensController;
            this.screenName = screenName;
            this.fxmlFile = fxmlFile;
            this.node = node;
            this.controller = controller;
            this.loaded = loaded;
        }

        @SuppressWarnings("unchecked")
        public <T extends ControlledScreen> T getController() {
            load();
            log.debug("controller is= {}", controller);
            return (T)controller;
        }

        public Node getNode() {
            load();
            return node;
        }

        public ScreenInfo clone(ScreensController screensController) {
            return new ScreenInfo(screensController, screenName, fxmlFile, null, null, false);
        }

        public void unload() {
            if (!loaded) {
                return;
            }
            node = null;
            controller = null;
            loaded = false;
        }

        private void load() {
            if (loaded) {
                return;
            }
            long ts = System.currentTimeMillis();
            log.debug("Loading {}", fxmlFile);
            FXMLLoader loader = JfxUtils.load(fxmlFile);
            node = loader.getRoot();
            node.managedProperty().bind(node.visibleProperty());
            ControlledScreen cs = loader.getController();
            cs.setScreensController(screensController);
            controller = cs;

            loaded = true;

            ts = System.currentTimeMillis() - ts;
            log.debug("Loading {} took {} ms", fxmlFile, ts);
        }
    }

    @AllArgsConstructor
    public class DialogContext {
        @Getter
        private Stage stage;

        @Delegate
        private final ScreenInfo screenInfo;

        public DialogContext(Stage owner, ScreensController parent, String screenName, String screenTitle, Double width,
                Double height, boolean modal) {
            parentScreensController = parent;

            ScreensController screensController = new ScreensController(parent);
            screenInfo = screensController.screens.get(screenName);
            screenInfo.load();
            screenInfo.dialogInfo = this;
            screensController.current = screenInfo;
            stage = new Stage();
            stage.setResizable(true);
            stage.setTitle(screenTitle);

            if (owner == null) {
                stage.centerOnScreen();
            } else {
                stage.setX(owner.getX() + 20);
                stage.setY(owner.getY() + 20);
            }

            //temporarily hardcoding width
            if (width != null) {
                stage.setWidth(width);
            }

            if (height != null) {
                stage.setHeight(height);
            }

            if (modal) {
                stage.initModality(Modality.APPLICATION_MODAL);
            } else {
                stage.initModality(Modality.NONE);
            }

            //Scene scene = new Scene((Parent)screenInfo.node);
            SceneFactory sceneFactory = Cdi.get(SceneFactory.class);
            AnchorPane ap = new AnchorPane();
            ap.getChildren().add(screenInfo.node);
            AnchorPane.setBottomAnchor(screenInfo.node, 0d);
            AnchorPane.setTopAnchor(screenInfo.node, 0d);
            AnchorPane.setLeftAnchor(screenInfo.node, 0d);
            AnchorPane.setRightAnchor(screenInfo.node, 0d);
            Scene scene = sceneFactory.newScene(ap);
            stage.setScene(scene);
        }

        public void showAndWait() {
            stage.showAndWait();
        }

        public void show() {
            stage.show();
        }
    }

}

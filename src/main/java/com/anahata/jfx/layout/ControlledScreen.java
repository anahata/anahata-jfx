package com.anahata.jfx.layout;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface ControlledScreen {
    /**
     * Set the controller parent.
     * 
     * @param screensController The screens controller.
     */
    public void setScreensController(ScreensController screensController);
    
    /**
     * The screens controller.
     * @return 
     */
    public ScreensController getScreensController();
    
    /**
     * Optional method invoked on arrival.
     */
    public default void onArrival() {};
    
    /**
     * Optional method invoked on arrival.
     */
    public default void onDeparture() {};
    
}

package com.anahata.jfx.layout;

import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
public interface SceneFactory {
    public Scene newScene(Parent parent);
}

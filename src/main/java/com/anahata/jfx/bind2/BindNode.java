/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind2;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface BindNode<T> {
    public void setValue(Object node, T value);
    
    public void register(BindField bindField);
}

/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind2;

import com.anahata.jfx.bind.converter.Converter;
import com.anahata.jfx.bind.filter.KeystrokeFilter;
import com.anahata.jfx.validator.FieldValidator;
import com.anahata.util.reflect.AnahataPropertyUtils;
import javafx.beans.property.ObjectProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.NestedNullException;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@EqualsAndHashCode(of = "fieldId")
@Getter
@Slf4j
public class BindField {
    private final String fieldId;
    
    private final String modelId;
    
    private final BindForm bindForm;
    
    private final ObjectProperty<?> model;
    
    private final String propName;
    
    private final Object node;
    
    private BindNode bindNode;
    
    private final BindView bindView;
    
    private final Converter converter;
    
    private final FieldValidator fieldValidator;
    
    private final KeystrokeFilter keystrokeFilter;

    public BindField(String modelId, BindForm bindForm, ObjectProperty<?> model, String propName, Object node,
            BindView bindView, Converter converter, FieldValidator fieldValidator,
            KeystrokeFilter keystrokeFilter) {
        this.fieldId = modelId + "." + propName;
        this.modelId = modelId;
        this.bindForm = bindForm;
        this.model = model;
        this.propName = propName;
        this.node = node;
        this.bindView = bindView;
        this.converter = converter;
        this.fieldValidator = fieldValidator;
        this.keystrokeFilter = keystrokeFilter;
        
        if (node instanceof BindNode) {
            bindNode = (BindNode)node;
            bindNode.register(this);
        }
    }
    
    @SuppressWarnings("unchecked")
    public void setNodeValue() {
        if (model.getValue() == null) {
            return;
        }
        
        Object value;
        
        try {
            value = AnahataPropertyUtils.getProperty(model.getValue(), propName);
        } catch (NestedNullException e) {
            value = null;
        }
        
        if (converter != null) {
            try {
                value = converter.getAsNodeModelValue(model.getValue(), value);
            } catch (RuntimeException e) {
                log.error("Exception converting model value to node value for : " + propName, e);
                throw e;
            }
        }
        
        log.debug("Setting node value: model=[{}] field=[{}] value=[{}]", modelId,
                model.getValue().getClass().getSimpleName() + "." + propName, value);
        bindNode.setValue(node, value);
    }
    
    @SuppressWarnings("unchecked")
    public void setModelValue(Object value) {
        if (model.getValue() == null || bindForm.isBinding()) {
            return;
        }
        
        log.debug("Setting model value: model=[{}] field=[{}] value=[{}]", modelId,
                model.getValue().getClass().getSimpleName() + "." + propName, value);
        
        if (converter != null) {
            value = converter.getAsDomainModelValue(node, value);
        }
        
        AnahataPropertyUtils.setPropertyNulls(model.getValue(), propName, value);
        bindForm.notifyModelUpdated(this);
    }
}

/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind2;

import com.anahata.jfx.bind.converter.Converter;
import com.anahata.jfx.bind.filter.KeystrokeFilter;
import com.anahata.jfx.validator.FieldValidator;
import com.anahata.util.validation.ValidationUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class BindForm {
    private static final Logger vlog = LoggerFactory.getLogger(BindForm.class.getName() + "#validate");
    
    private final Object formNode;
    
    private final Map<String, ObjectProperty<?>> models = new HashMap<>();
    
    private final Map<String, Map<String, BindField>> modelFields = new HashMap<>();
    
    private final Validator validator;
    
    private final BooleanProperty valid = new SimpleBooleanProperty();
    
    private final BooleanProperty modified = new SimpleBooleanProperty();
    
    @Getter
    @Setter
    private boolean blocked = false;
    
    @Getter(AccessLevel.PACKAGE)
    @Setter(AccessLevel.PACKAGE)
    private boolean binding = false;
    
    public BindForm(@NonNull Object node) {
        formNode = node;
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }
    
    public void addModel(@NonNull String modelId, @NonNull ObjectProperty<?> model, boolean autoBind) {
        Validate.isTrue(!models.containsKey(modelId), "Model with id %s already added", modelId);
        models.put(modelId, model);
        Map<String, BindField> fieldMap = new HashMap<>();
        modelFields.put(modelId, fieldMap);
        
        if (autoBind) {
            model.addListener(new InvalidationListener() {
                @Override
                public void invalidated(Observable o) {
                    bindFromModel();
                }
            });
        }
    }
    
    public void addField(@NonNull String modelId, @NonNull Object node, @NonNull String propName, Converter converter,
            FieldValidator fieldValidator, KeystrokeFilter keystrokeFilter) {
        Validate.isTrue(models.containsKey(modelId), "Model with id %s has not been added", modelId);
        ObjectProperty<?> model = models.get(modelId);
        // TODO add BindView
        BindField field = new BindField(modelId, this, model, propName, node, null, converter, fieldValidator, keystrokeFilter);
        Map<String, BindField> fieldMap = modelFields.get(modelId);
        fieldMap.put(propName, field);
    }
    
    public void bindFromModel() {
        if (blocked) {
            return;
        }
        
        notifyModelUpdated(null);
    }
    
    void notifyModelUpdated(BindField source) {
        if (blocked) {
            return;
        }
        
        binding = true;
        
        for (Map<String, BindField> fieldMap : modelFields.values()) {
            for (BindField field : fieldMap.values()) {
                if (source == null || !field.equals(source)) {
                    field.setNodeValue();
                }
            }
        }
        
        binding = false;
        validate();
    }
    
    void validate() {
        if (blocked) {
            return;
        }
        
        for (Map.Entry<String, ObjectProperty<?>> entry : models.entrySet()) {
            final Object model = entry.getValue().getValue();
            
            if (model != null) {
                final Set<ConstraintViolation<Object>> violations = validator.validate(model);

                if (!violations.isEmpty()) {
                    final String modelId = entry.getKey();
                    Map<String, BindField> fields = modelFields.get(modelId);

                    for (ConstraintViolation<Object> violation : violations) {
                        String propName = ValidationUtils.getValidationPropertyName(violation, true);
                        
                        if (fields.containsKey(propName)) {
                            // TODO markup
                            vlog.debug("Validation error: model=[{}] field=[{}] message=[{}] => value=[{}]",
                                    modelId, model.getClass().getSimpleName() + "." + propName, violation.getMessage(),
                                    violation.getInvalidValue());
                        }
                    }
                }
            }
        }
    }
}

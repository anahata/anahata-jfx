/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind2;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Node;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class BindView {
    private Node node = null;
    
    private final BooleanProperty valid = new SimpleBooleanProperty();
}

package com.anahata.jfx.config;

/**
 * Dynamic configuration provided by the implementing application.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface JavaFXDynamicConfig {
    /**
     * @return The path to get CSS files from. Must start with a leading slash.
     */
    String getCssPath();
}

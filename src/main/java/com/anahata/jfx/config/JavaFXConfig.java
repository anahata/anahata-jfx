package com.anahata.jfx.config;

import com.anahata.util.config.internal.ApplicationPropertiesFactory;
import com.anahata.util.props.StructuredProperties;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.Getter;

/**
 * JavaFX configuration for anahata-util. Requires the following properties to be presented by
 * ApplicationPropertiesFactory:
 * 
 * <ul>
 *   <li>anahatajfx.message.growl.fadein.millis</li>
 *   <li>anahatajfx.message.growl.fadeout.millis</li>
 *   <li>anahatajfx.message.growl.pause.millis</li>
 *   <li>anahatajfx.icon.delete</li>
 * </ul>
 *
 * @author Robert Nagajek
 */
@ApplicationScoped
@Getter
public class JavaFXConfig {
    private static final String PROPS_PREFIX = "anahatajfx";
    
    @Inject
    private ApplicationPropertiesFactory appProps;
    
    @Inject
    private JavaFXDynamicConfig dynamicConfig;
    
    private double messageGrowlFadeInMillis;

    private double messageGrowlFadeOutMillis;

    private double messageGrowlPauseMillis;

    private String deleteIcon;
    
    private String readOnlyCss;
    
    @PostConstruct
    void postConstruct() {
        final StructuredProperties props = new StructuredProperties(appProps.getAppProperties(), PROPS_PREFIX);
        messageGrowlFadeInMillis = props.getDouble("message.growl.fadein.millis");
        messageGrowlFadeOutMillis = props.getDouble("message.growl.fadeout.millis");
        messageGrowlPauseMillis = props.getDouble("message.growl.pause.millis");
        deleteIcon = props.getString("icon.delete");
        readOnlyCss = props.getString("css.readonly");
    }
    
    public String getCssPath() {
        return dynamicConfig.getCssPath();
    }
    
    public String getReadOnlyCssPath() {
        return getCssPath() + "/" + readOnlyCss;
    }
}

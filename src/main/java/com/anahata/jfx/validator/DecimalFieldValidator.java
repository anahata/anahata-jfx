package com.anahata.jfx.validator;

import java.math.BigDecimal;
import org.apache.commons.lang3.StringUtils;

/**
 * Validate a decimal field.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class DecimalFieldValidator implements FieldValidator {
    public static final DecimalFieldValidator INSTANCE = new DecimalFieldValidator();

    @Override
    public String validate(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }

        try {
            BigDecimal bd = new BigDecimal(StringUtils.trim(value));
        } catch (NumberFormatException e) {
            return "The value is not a valid decimal number";
        }

        return null;
    }
}

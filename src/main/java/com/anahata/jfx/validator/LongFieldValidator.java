package com.anahata.jfx.validator;

import org.apache.commons.lang3.StringUtils;

/**
 * Validate a long field.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class LongFieldValidator implements FieldValidator {
    public static final LongFieldValidator INSTANCE = new LongFieldValidator();

    @Override
    public String validate(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }

        try {
            Long num = Long.valueOf(StringUtils.trim(value));
        } catch (NumberFormatException e) {
            return "The value is not a valid number";
        }

        return null;
    }
}

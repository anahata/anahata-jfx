package com.anahata.jfx.validator;

import com.anahata.util.lang.builder.HashMapBuilder;
import java.math.BigDecimal;
import java.util.Map;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

/**
 * Create field validators for given types.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FieldValidatorFactory {
    private static final Map<Class, FieldValidator> VALIDATORS = new HashMapBuilder<Class, FieldValidator>()
            .add(BigDecimal.class, DecimalFieldValidator.INSTANCE)
            .add(Integer.class, IntegerFieldValidator.INSTANCE)
            .add(int.class, IntegerFieldValidator.INSTANCE)
            .add(Long.class, LongFieldValidator.INSTANCE)
            .add(long.class, LongFieldValidator.INSTANCE)
            .build();
    
    /**
     * Get a field validator for the given target type. The source type must be a String.
     * 
     * @param targetType The target type. Required.
     * @return The validator, or null if none apply.
     * @throws NullPointerException If targetType is null.
     */
    public static FieldValidator getFieldValidator(Class targetType) {
        Validate.notNull(targetType);
        return VALIDATORS.get(targetType);
    }
}

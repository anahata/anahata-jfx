package com.anahata.jfx.validator;

/**
 * Validate a field value, usually just data type validation.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface FieldValidator {
    /**
     * Validate a field.
     * 
     * @param value The value to validate. Can be null.
     * @return The validation message if invalid, or null if valid.
     */
    public String validate(String value);
}

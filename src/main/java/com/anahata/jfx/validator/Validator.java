package com.anahata.jfx.validator;

import com.anahata.jfx.message.JfxMessage;

/**
 * Validate JavaFX input fields. There are three levels of validation that can be performed:
 * <ol>
 *   <li>Validate keystrokes as you type. This is used typically to indicate invalid keys, which will normally be
 *       rejected and not reflected in the UI.</li>
 *   <li>Validate the complete text of an input field as you type.</li>
 *   <li>Validate the complete text of an input field only as it loses focus.</li>
 * </ol>
 * 
 * @author Pablo Rodriguez
 * @author Robert Nagajek
 */
public interface Validator {
    /**
     * Validates that a given keystroke is valid.
     * 
     * @param c The character typed.
     * @return The result of the validation. If null, that means validation passed.
     */
    public JfxMessage validateKeyTyped(char c);
    
    /**
     * Validates the entire content of an input field as it is being typed.
     * 
     * @param text The text to validate.
     * @return The result of the validation. If null, that means validation passed.
     */
    public JfxMessage validateProgress(String text);
    
    /**
     * Validates the entire content of an input field after it loses focus.
     * 
     * @param text The text to validate.
     * @return The result of the validation. If null, that means validation passed.
     */
    public JfxMessage validateFinal(String text);
}

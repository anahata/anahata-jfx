package com.anahata.jfx.validator;

import org.apache.commons.lang3.StringUtils;

/**
 * Validate an integer field.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class IntegerFieldValidator implements FieldValidator {
    public static final IntegerFieldValidator INSTANCE = new IntegerFieldValidator();

    @Override
    public String validate(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }

        try {
            Integer num = Integer.valueOf(StringUtils.trim(value));
        } catch (NumberFormatException e) {
            return "The value is not a valid integer";
        }

        return null;
    }
}

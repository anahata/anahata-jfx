package com.anahata.jfx.validator;

import com.anahata.jfx.JfxUtils;
import com.anahata.jfx.ScrollPaneUtils;
import com.anahata.jfx.bind.table.BindingTableCell;
import com.anahata.jfx.message.JfxMessage;
import com.anahata.jfx.message.JfxMessage.Severity;
import com.anahata.jfx.message.JfxMessageContext;
import java.util.Map;
import java.util.WeakHashMap;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javax.enterprise.context.Dependent;
import lombok.extern.slf4j.Slf4j;

/**
 * Markup validated fields with errors dynamically.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Dependent
@Slf4j
public class DynamicValidationMarker {
    private static final String STYLE_NONE = "-fx-border-color: null; -fx-border-radius: null;";

    private static final String STYLE_WARN = "-fx-border-color: orange; -fx-border-radius: 2;";

    private static final String STYLE_ERROR = "-fx-border-color: red; -fx-border-radius: 2;";

    private Map<Node, ValidationBubble> bubbles = new WeakHashMap<>();

    /**
     * Markup fields with validation messages.
     *
     * @param messageEvent The message event.
     */
    public void addMessage(JfxMessageContext messageEvent) {
        final Node node = messageEvent.getNode();

        if (node == null) {
            return;
        }

        final JfxMessage message = messageEvent.getMessage();

        if (!message.isWarnOrHigher()) {
            return;
        }
        
        ensureNodeVisible(node);

        ValidationBubble bubble = bubbles.get(node);

        if (bubble == null) {
            bubble = new ValidationBubble();
            bubbles.put(node, bubble);
        }

        bubble.show(messageEvent.getView(), node, message.getMessage());
        setStyleClass(node, message.getSeverity());
        
    }

    private void ensureNodeVisible(Node node) {
        System.out.println("private void ensureNodeVisible(Node node) " + node + " parent = " + node.getParent());
        Parent currentParent = node.getParent();

        while (currentParent != null) {
            if (currentParent instanceof ScrollPane) {
                ScrollPane sp = ((ScrollPane)currentParent);
                boolean visible = ScrollPaneUtils.isVisible(sp, node);
                log.debug("Node " + node + " visible in ancestor scrollPane " + sp + ": " + visible);
                if (!visible) {
                    ScrollPaneUtils.scrollTo(sp, node, -20d);
                }
            } else if (currentParent instanceof TabPane) {
                TabPane tp = (TabPane)currentParent;
                for (Tab t : tp.getTabs()) {
                    if (!t.isSelected()) {
                        Node tabContent = t.getContent();
                        if (tabContent == node || JfxUtils.isInAncestor(node, tabContent)) {
                            log.debug("Node " + node + " not visible in tab " + t.getText());
                            tp.getSelectionModel().select(t);
                            break;
                        } else {
                            log.debug("Node " + node + " not in tab " + t.getText());
                        }
                    }
                }
            } else if (currentParent instanceof BindingTableCell) {
                
                BindingTableCell btc = (BindingTableCell)currentParent;
                TableView tableView = btc.getTableView();
                tableView.scrollToColumn(btc.getTableColumn());
                tableView.scrollTo(btc.getBindingTableRow().getModel());
                log.debug("Node " + node + ": ancestor " + currentParent + ". scrolled to row for item " + btc.getBindingTableRow().getModel() + " and to column " + btc.getTableColumn().getText());
            } else {
                log.debug("Node " + node + ": ancestor " + currentParent + " is not scroll pane");
            }
            currentParent = currentParent.getParent();
            //currentNode = currentParent;
        }
    }

    public void clearMessages(Node node) {
        ValidationBubble bubble = bubbles.get(node);

        if (bubble != null) {
            bubble.hide();
        }

        removeStyleClasses(node);
    }

    public void clearAllMessages() {

        System.out.println("clear all messages");

        for (Node node : bubbles.keySet()) {
            ValidationBubble bubble = bubbles.get(node);
            bubble.hide();
            removeStyleClasses(node);
        }

        bubbles.clear();
    }

    private void setStyleClass(Node node, Severity severity) {
        if (severity == Severity.INFO) {
            return;
        }

        node.setStyle(severity == Severity.WARN ? STYLE_WARN : STYLE_ERROR);
    }

    private void removeStyleClasses(Node node) {
        node.setStyle(STYLE_NONE);
    }
}

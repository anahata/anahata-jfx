package com.anahata.jfx.validator;

import com.anahata.jfx.bind.View;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.GroupBuilder;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * A validation bubble that displays an error message near a JavaFX field.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class ValidationBubble {

    /**
     * Distance of offset from the right of a control.
     */
    private static final double X_OFFSET = 10;

    private static final double HEIGHT = 20;

    private static final double Y_OFFSET = HEIGHT / 2 + 10; // add 10 cuz unable to clic on combos

    private Group group;

    private VBox vbox;

    private Label message;

    private boolean inParent = false;

    private View view = null;

    private Node node = null;

    public ValidationBubble() {
        message = LabelBuilder.create()
                .styleClass("validationBubbleMessage")
                .prefWidth(Control.USE_COMPUTED_SIZE)
                .minWidth(Control.USE_COMPUTED_SIZE)
                .maxWidth(Double.MAX_VALUE)
                .prefHeight(Control.USE_COMPUTED_SIZE)
                .minHeight(Control.USE_COMPUTED_SIZE)
                .maxHeight(Control.USE_COMPUTED_SIZE)
                .build();

        vbox = VBoxBuilder.create()
                .styleClass("validationBubble")
                .minWidth(Control.USE_COMPUTED_SIZE)
                .maxWidth(Double.MAX_VALUE)
                .prefWidth(Control.USE_COMPUTED_SIZE)
                .children(message)
                .build();

        group = GroupBuilder.create()
                .managed(false)
                .children(vbox)
                .build();
    }

    /**
     * Show relative to a control.
     *
     * @param view The view. Required.
     * @param node The control. Required.
     * @param text The text.
     */
    public void show(View view, final Node node, final String text) {
        Validate.notNull(view);
        Validate.notNull(node);

        log.debug("show: current={} new={}", message.getText(), text);

        if (!StringUtils.equals(text, message.getText())) {
            hide();
        }

        if (!inParent) {
            inParent = true;
            this.view = view;
            this.node = node;
            log.debug("show: Adding group {} node {} to pane {}", System.identityHashCode(group),
                    System.identityHashCode(node), view.getPane());
            message.setText(text);
            group.relocate(0, 0);
            view.getPane().getChildren().add(group);
            group.setMouseTransparent(true);
            group.toFront();
            updateCoords();
        }
    }

    private void updateCoords() {
        // We need to take a snapshot here to force the bubble to render, otherwise groupBounds.getWidth() returns
        // 0 the first time it is shown, and is only correct the second time.
        try {
            group.snapshot(null, null);
        } catch (Exception e) {
            log.warn("Exceptio taking component snapshot");
        }

        Bounds bounds = getNodeBoundsInPane(node, view.getPane());
        double x = bounds.getMaxX() - X_OFFSET;
        double y = bounds.getMinY() - Y_OFFSET;
        Bounds viewBounds = view.getPane().getBoundsInLocal();
        Bounds groupBounds = group.getBoundsInLocal();

        // if it is to the right move to left
        if (x + groupBounds.getWidth() > viewBounds.getWidth()) {
            x = bounds.getMinX() - groupBounds.getWidth();
            if (x < 0) {
                x = viewBounds.getWidth() - groupBounds.getWidth();
            }
        }

        log.debug("show: x={} y={} view width={} height={} group x={} y={} width={} height={}",
                x, y, viewBounds.getWidth(), viewBounds.getHeight(),
                groupBounds.getMinX(), groupBounds.getMinY(), groupBounds.getWidth(), groupBounds.getHeight());
        group.relocate(x, y);
    }

    public void hide() {
        if (inParent) {
            log.debug("hide: Removing group");
            view.getPane().getChildren().remove(group);
            inParent = false;
        }
    }

//    private ChangeListener<Object> updateCoordsListener = new ChangeListener<Object>(){
//        @Override
//        public void changed(
//                ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
//            System.out.println("Bounds in parent changed for node " + node);
//            if (inParent) {
//                //updateCoords();
//                //Platform.runLater(() -> updateCoords());
//            } else {
//                System.out.println("Not displaying so not showing");
//            }
//            
//        }
//    };
    private Bounds getNodeBoundsInPane(Node node, Pane pane) {

        Pane currPane = (node instanceof Pane ? (Pane)node : null);
        Bounds nodeBounds = node.getBoundsInParent();
        //addListener(node);
        double x = nodeBounds.getMinX();
        double y = nodeBounds.getMinY();

        //System.out.println("Processing node " + node);
        while (node.getParent() != null) {

            node = node.getParent();
            //addListener(node);
            //System.out.println("Parent is " + node);

            if (node instanceof Pane) {
                currPane = (Pane)node;
                //System.out.println("currPane " + currPane);
            }

            if (currPane != null && currPane.equals(pane)) {
                break;
            }

            Bounds bounds = node.getBoundsInParent();
            x += bounds.getMinX();
            y += bounds.getMinY();
        }

        if (currPane == null) {
            log.error("Ancestor Pane note found for node {}. View pane = {} ", node, pane);
        }
        //Validate.validState(currPane != null, "Pane not found for node %s", node);

        return new BoundingBox(x, y, nodeBounds.getWidth(), nodeBounds.getHeight());
    }
}
//package com.anahata.jfx.validator;
//
//import com.anahata.jfx.bind.View;
//import javafx.geometry.BoundingBox;
//import javafx.geometry.Bounds;
//import javafx.geometry.Pos;
//import javafx.scene.*;
//import javafx.scene.control.Control;
//import javafx.scene.control.Label;
//import javafx.scene.control.LabelBuilder;
//import javafx.scene.layout.Pane;
//import javafx.scene.layout.StackPane;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.Validate;
//import org.apache.commons.lang3.exception.ExceptionUtils;
//
///**
// * A validation bubble that displays an error message near a JavaFX field.
// *
// * @author Robert Nagajek <robert@anahata-it.com.au>
// */
//@Slf4j
//public class ValidationBubble {
//    /**
//     * Distance of offset from the right of a control.
//     */
//    private static final double X_OFFSET = 20;
//
//    private static final double HEIGHT = 20;
//
//    private static final double Y_OFFSET = HEIGHT / 2;
//
//    private Group group;
//
//    //private VBox vbox;
//
//    private Label message;
//
//    private boolean inParent = false;
//
//    private View view = null;
//
//    private Node node = null;
//
//    public ValidationBubble() {
//        message = LabelBuilder.create()
//                .styleClass("validationBubbleMessage")
//                .prefWidth(Control.USE_COMPUTED_SIZE)
//                .minWidth(Control.USE_PREF_SIZE)
//                .maxWidth(Control.USE_PREF_SIZE)
//                .prefHeight(Control.USE_COMPUTED_SIZE)
//                .minHeight(HEIGHT)
//                .maxHeight(HEIGHT)
//                .build();
//        message.getStyleClass().add("validationBubble");
//        message.setMouseTransparent(true);
////
////        vbox = VBoxBuilder.create()
////                .styleClass("validationBubble")
////                .minWidth(Control.USE_COMPUTED_SIZE)
////                .maxWidth(Control.USE_COMPUTED_SIZE)
////                .prefWidth(Control.USE_COMPUTED_SIZE)
////                .prefHeight(Control.USE_COMPUTED_SIZE)
////                .children(message)
////                .build();
////        
////        vbox.setMouseTransparent(true);
//        
//    }
//
//    /**
//     * Show relative to a control.
//     *
//     * @param view The view. Required.
//     * @param node The control. Required.
//     * @param text The text.
//     */
//    public void show(View view, final Node node, final String text) {
//        Validate.notNull(view);
//        Validate.notNull(node);        
//        
//        //vbox.setStyle("-fx-border-color:green");
//        message.setStyle("-fx-border-color:red");
//        
//        message.setText(text);
//        
////        vbox.maxWidthProperty().bind(message.widthProperty());
////        vbox.maxHeightProperty().bind(message.maxHeightProperty());
//        
//        
//        Node decorator = message;
//        StackPane stackPane = getStackPane(view.getPane());
//        
//        if (!stackPane.getChildren().contains(decorator)) {
//            stackPane.getChildren().add(decorator);            
//        }
//        
//        this.node = node;
//        Bounds bounds = getNodeBoundsInPane(node, stackPane);
//        
//        double x = bounds.getMaxX() - X_OFFSET;
//        double y = bounds.getMinY() - Y_OFFSET;
//        
//        System.out.println(node + " x= " + x);
//        System.out.println(node + " y= " + y);
//        
//        decorator.setTranslateX(x);
//        decorator.setTranslateY(y);
//        decorator.visibleProperty().bind(view.getPane().visibleProperty());
//        
//
//    }
//    
//    private StackPane getStackPane(Node node) {
//        Scene scene = node.getScene();
//        Parent currentRoot = scene.getRoot();
//        StackPane dp;
//        if (currentRoot instanceof StackPane) {
//            log.debug("Decoration pane already injected");
//            dp = (StackPane)currentRoot;
//        } else {
//            log.debug("Injecting decoration pane");
//            dp = new StackPane();
//            dp.setAlignment(Pos.TOP_LEFT);
//            dp.getProperties().putAll(currentRoot.getProperties());
//            scene.setRoot(dp);
//            dp.getChildren().add(currentRoot);
//        }
//        return dp;
//    }
//
//    public void hide() {
//        
//        System.out.println("hiding...." + Thread.currentThread());
//        String s = ExceptionUtils.getStackTrace(new Exception());
//        log.debug(s);
//        message.visibleProperty().unbind();
//        message.setVisible(false);
//        //DecorationUtils.uninstall(node, graphicDecoration);
//        //graphicDecoration.removeDecoration(node);
//    }
//
//    private Bounds getNodeBoundsInPane(Node node, Pane pane) {
//        Pane currPane = (node instanceof Pane ? (Pane)node : null);
//        Bounds nodeBounds = node.getBoundsInParent();
//        double x = nodeBounds.getMinX();
//        double y = nodeBounds.getMinY();
//
//        while (node.getParent() != null) {
//            node = node.getParent();
//
//            if (node instanceof Pane) {
//                currPane = (Pane)node;
//            }
//
//            if (currPane != null && currPane.equals(pane)) {
//                break;
//            }
//
//            Bounds bounds = node.getBoundsInParent();
//            x += bounds.getMinX();
//            y += bounds.getMinY();
//        }
//
//        Validate.validState(currPane != null, "Pane not found for node %s", node);
//
//        return new BoundingBox(x, y, nodeBounds.getWidth(), nodeBounds.getHeight());
//    }
//}

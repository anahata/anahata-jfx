package com.anahata.jfx.validator;

import com.anahata.jfx.message.JfxMessage;
import com.anahata.util.plaf.AnahataFilenameUtils;
import java.util.Arrays;
import java.util.regex.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Simple {@link Validator} implementation that allows to define basic validation constraints.
 *
 * @author pablo
 */
@AllArgsConstructor
@Getter
@Setter
public class SimpleValidator implements Validator {
    /**
     * Cross platform file Name input validator.
     */
    public static final Validator FILE_NAME_INPUT_VALIDATOR = new SimpleValidator(null,
            AnahataFilenameUtils.MAX_FILE_NAME_LENGTH, AnahataFilenameUtils.ILLEGAL_FILE_NAME_CHARACTERS, null,
            AnahataFilenameUtils.VALID_FILE_NAME_PATTERN, "Please enter a valid file name");

    /**
     * Minimum number of characters.
     */
    private Integer minLength;

    /**
     * Maximum number of characters.
     */
    private Integer maxLength;

    /**
     * Set of illegal characters.
     */
    private char[] illegalCharacters;

    /**
     * Custom message for illegal characters.
     */
    private String illegalCharactersMessage;

    /**
     * Pattern for pattern match based validation.
     */
    private Pattern pattern;

    /**
     * Pattern for pattern not matching message.
     */
    private String patternNotMatchingMessage;

    @Override
    public JfxMessage validateKeyTyped(char c) {
        if (ArrayUtils.contains(illegalCharacters, c)) {
            return JfxMessage.error(getIllegalCharactersMessage());
        }

        return null;
    }

    @Override
    public JfxMessage validateProgress(String text) {
        if (minLength != null) {
            if (text.length() < minLength) {
                return JfxMessage.warn("The value must be at least " + minLength + " characters long");
            }
        }

        if (maxLength != null) {
            if (text.length() > maxLength) {
                return JfxMessage.error("The value must be " + maxLength + " or less characters long");
            }
        }

        if (illegalCharacters != null) {
            if (StringUtils.containsAny(text, illegalCharacters)) {
                return JfxMessage.error(getIllegalCharactersMessage());
            }
        }

        if (pattern != null) {
            boolean valid = pattern.matcher(text).matches();

            if (!valid) {
                return JfxMessage.warn(getPatternNotMatchingMessage());
            }
        }

        return null;
    }

    @Override
    public JfxMessage validateFinal(String text) {
        return null;
    }
    
    /**
     * Gets the message for pattern not matching scenarios.
     *
     * @return {@link #patternNotMatchingMessage} or "Value not valid" if a pattern has been set.
     */
    public String getPatternNotMatchingMessage() {
        if (pattern != null && patternNotMatchingMessage == null) {
            return "Value not valid";
        }

        return patternNotMatchingMessage;
    }

    /**
     * Gets the message for illegal characters scenario.
     *
     * @return {@link #illegalCharactersMessage} or "The following characters are not allowed: (comma separated list of
     *         {@link #illegalCharacters}"
     */
    public String getIllegalCharactersMessage() {
        if (illegalCharactersMessage == null && illegalCharacters != null) {
            String illegalCharactersToString = Arrays.toString(
                    illegalCharacters);
            //remove [ ] brackets
            illegalCharactersToString = illegalCharactersToString.substring(1, illegalCharactersToString.length() - 1);

            return "The following characters are not allowed: " + illegalCharactersToString;
        }

        return illegalCharactersMessage;
    }
}

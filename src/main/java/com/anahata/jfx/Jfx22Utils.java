package com.anahata.jfx;

import com.anahata.jfx.bind.Binder;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * A collection of utilities specifically for JavaFX 2.2, which may require workarounds for bugs / limitations.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class Jfx22Utils {
    /**
     * Does a repaint or refresh on a TableView by removing and adding the items.
     * 
     * @param <T> the type of the items in the TableView.
     * @param tableView the TableView.
     */
    public static <T> void refreshTableViewItems(final TableView<T> tableView) {
        ObservableList<T> ol = tableView.getItems();
        tableView.setItems(null);
        tableView.layout();
        tableView.setItems(ol);
    }

    /**
     * Refresh TableView items with a list of new items. This refreshes properly, as in JavaFX 2.2 there is a bug that
     * prevents the refresh working correctly.
     *
     * @param <T>       The contained table view type.
     * @param tableView The table view.
     * @param items     The list of items.
     * @throws NullPointerException If any arg is null.
     * @deprecated Use the other version. It will need updating to allow setting a new list.
     */
    @Deprecated
    public static <T> void refreshTableViewItems(final TableView<T> tableView, final List<T> items) {
        Validate.notNull(tableView);
        Validate.notNull(items);
        tableView.getItems().clear();

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                tableView.getItems().addAll(items);
            }
        });
    }
    
    /**
     * 
     * @param <T>
     * @param combo
     * @param itemsList
     * @param binder 
     */
    public static <T> void bindItems(final ComboBox<T> combo, final ListProperty<T> itemsList, final Binder binder) {
        itemsList.addListener(new ChangeListener<ObservableList<T>>() {
            @Override
            public void changed(
                    ObservableValue<? extends ObservableList<T>> ov, ObservableList<T> t,
                    ObservableList<T> t1) {
                boolean block = binder.isBlock();                
                binder.setBlock(true);
                T value = combo.getValue();
                combo.getItems().setAll(t1);
                
                combo.setValue(null);
                combo.setValue(value);
//                if (t1.contains(value)) {
//                    log.debug("setting value {}", value);
//                    combo.setValue(value);
//                } else {
//                    log.debug("Value lost {} options where {}", value, combo.getItems());
//                }
                binder.setBlock(block);
            }
        });
    }
}

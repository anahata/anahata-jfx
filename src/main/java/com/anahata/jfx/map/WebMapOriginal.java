package com.anahata.jfx.map;

import com.anahata.jfx.JfxUtils;
import com.anahata.jfx.map.event.MarkerListener;
import com.anahata.jfx.map.event.UBDPageOverlayListener;
import com.google.code.geocoder.model.LatLng;
import com.google.code.geocoder.model.LatLngBounds;
import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import netscape.javascript.JSException;
import netscape.javascript.JSObject;
import org.apache.commons.lang3.Validate;
import org.w3c.dom.Document;

/**
 * A Google map with javafx controls to change map type open the map on an a broswer window and java API to set the map
 * location, viewport, zoom and markers.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
final public class WebMapOriginal extends BorderPane {

    private static final Image ACTION_DIRECTIONS = new Image("/com/anahata/jfx/map/directions.png");

    private static final Image ACTION_WEB = new Image("/com/anahata/jfx/map/web.png");

    /**
     * Geo Coordinates for the center of Australia.
     */
    public static final LatLng AU_CENTER = new LatLng("-25.2743980", "133.7751360");

    /**
     * Geo Coordinates for the Australia viewport.
     */
    public static final LatLngBounds AU_VIEWPORT = new LatLngBounds(new LatLng("-43.6583270", "112.92397210"),
                                                                    new LatLng("-9.22680570", "153.63867380"));

    /**
     * Geo Coordinates for an all continents viewport.
     */
    public static final LatLngBounds EARTH_VIEWPORT = new LatLngBounds(new LatLng("-70", "30"),
                                                                       new LatLng("70", "140"));

    //private Timeline locationUpdateTimeline;
    //private BooleanProperty initialized = new SimpleBooleanProperty(false);
    //-----------------------
    //Private attributes
    //-----------------------
    //private boolean initialised = false;
    private WebEngine webEngine;

    /**
     * List of javascript calls to be executed once the map has been initialzed. As the map is initialized
     * asynchronously this list accumulates all map operations requested before the component is initialized.
     */
    private final List<JSFunctionCall> calls = new ArrayList<>();

    /**
     * Flag inidicating if the javascript code has been initialised.
     */
    @Getter
    private boolean initialised = false;

    /**
     * Listens for click events on markers.
     */
    @Getter
    @Setter
    private MarkerListener markerListener;

    /**
     * Listens for click events on locality overlays.
     */
    @Getter
    @Setter
    private UBDPageOverlayListener ubdPageOverlayListener;

    /**
     * The toolbar, inititally disabled, enabled once the javascript code is initialsed.
     */
    private final ToolBar toolBar;

    /**
     * The stack pane that is the center of the border pane. Contains
     * {@link #webView or a the {@link #invalidAddressLabel}}
     *
     */
    private final StackPane centerPane;

    /**
     * The WebView used to render the map.
     */
    private final WebView webView;

    /**
     * Address not valid.
     */
    private final Label statusLabel = new Label("Initializing map");

    /**
     * For maps using one marker only representing the center of the map.
     */
    private final ObjectProperty<Marker> singleMarker = new SimpleObjectProperty<>();

    /**
     * If using directions, the starting point for the directions.
     */
    private final StringProperty directionsSourceLocation = new SimpleStringProperty();

    /**
     * Initialises all JavaFX components.
     */
    public WebMapOriginal() {
        webView = new WebView();
        getStyleClass().add("webMap");

        webView.setVisible(false);
        centerPane = new StackPane();
        centerPane.getChildren().add(statusLabel);
        centerPane.getChildren().add(webView);
        statusLabel.visibleProperty().bind(webView.visibleProperty().not());

        setCenter(centerPane);
        webView.setOnDragDetected((MouseEvent t) -> {
            // do nothing
        });

        this.webEngine = webView.getEngine();

        webEngine.setOnAlert((WebEvent<String> event) -> {
            log.debug("{} javascript alert: {}", WebMapOriginal.this, event);
        });

        webEngine.load(getClass().getResource("googlemap.html").toString());
        webEngine.documentProperty().addListener(
                (ObservableValue<? extends Document> observable, Document oldValue, Document newValue) -> {
                    // ENABLE FIREBUG FOR DEBUGGING
//            enableFirebug(webEngine); 
                    initialiseMap();
                });

        webEngine.getLoadWorker().stateProperty().addListener(
                (ObservableValue<? extends State> ov, State olsState, State newState) -> {
                    if (newState == State.SUCCEEDED) {
                        JSObject jsobj = (JSObject)webEngine.executeScript("window");
                        jsobj.setMember("java", new JavaBridgeImpl());
                    }
                });
        // create map type buttons
        final ToggleGroup mapTypeGroup = new ToggleGroup();
        final ToggleButton road = new ToggleButton("Road");
        road.setToggleGroup(mapTypeGroup);
        final ToggleButton satellite = new ToggleButton("Satellite");
        satellite.setToggleGroup(mapTypeGroup);
        final ToggleButton hybrid = new ToggleButton("Hybrid");
        hybrid.setToggleGroup(mapTypeGroup);
        hybrid.setSelected(true);
        final ToggleButton terrain = new ToggleButton("Terrain");
        terrain.setToggleGroup(mapTypeGroup);
        mapTypeGroup.selectedToggleProperty().addListener(
                (ObservableValue<? extends Toggle> observableValue, Toggle toggle, Toggle toggle1) -> {
                    if (road.isSelected()) {
                        webEngine.executeScript("document.setMapTypeRoad()");
                    } else if (satellite.isSelected()) {
                        webEngine.executeScript("document.setMapTypeSatellite()");
                    } else if (hybrid.isSelected()) {
                        webEngine.executeScript("document.setMapTypeHybrid()");
                    } else if (terrain.isSelected()) {
                        webEngine.executeScript("document.setMapTypeTerrain()");
                    }
                });
        Button directions = new Button("", JfxUtils.makeIcon(ACTION_DIRECTIONS, 16));
        directions.setOnAction((ActionEvent actionEvent) -> {
            openGoogleMapsDirectionsInBrowser();
        });
        directions.visibleProperty().bind(directionsSourceLocation.isNotNull().and(singleMarker.isNotNull()));

        Button web = new Button("", JfxUtils.makeIcon(ACTION_WEB, 16));
        web.setOnAction((ActionEvent actionEvent) -> {
            openInBrowser();
        });

        if (!Desktop.isDesktopSupported() || !Desktop.getDesktop().isSupported(Action.BROWSE)) {
            web.setDisable(true);
            directions.setDisable(true);
        }

        //create toolbar
        toolBar = new ToolBar();
        toolBar.setDisable(true);
        toolBar.getItems().addAll(road, satellite, hybrid, terrain, createSpacer(), directions, web);
        toolBar.disableProperty().bind(webView.visibleProperty().not());
        setTop(toolBar);

    }

    private static void enableFirebug(final WebEngine engine) {
        engine.executeScript(
                "if (!document.getElementById('FirebugLite')){E = document['createElement' + 'NS'] && document.documentElement.namespaceURI;E = E ? document['createElement' + 'NS'](E, 'script') : document['createElement']('script');E['setAttribute']('id', 'FirebugLite');E['setAttribute']('src', 'https://getfirebug.com/' + 'firebug-lite.js' + '#startOpened');E['setAttribute']('FirebugLite', '4');(document['getElementsByTagName']('head')[0] || document['getElementsByTagName']('body')[0]).appendChild(E);E = new Image;E['setAttribute']('src', 'https://getfirebug.com/' + '#startOpened');}");
    }

    public void setDirectionsSourceLocation(String directionsSource) {
        this.directionsSourceLocation.set(directionsSource);

    }

    private void openGoogleMapsDirectionsInBrowser() {

        if (singleMarker.get() != null && directionsSourceLocation.get() != null) {
            try {
                String saddr = directionsSourceLocation.get();
                String daddr = singleMarker.get().getLocation();
                StringBuilder sb = new StringBuilder("saddr=");
                sb.append(URLEncoder.encode(saddr, "UTF-8"));
                sb.append("&daddr=");
                sb.append(URLEncoder.encode(daddr, "UTF-8"));
                sb.append("&hl=en");
                openGoogleMapsInBrowser(sb.toString());
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private void openInBrowser() {
        Marker marker = singleMarker.get();
        String queryString = null;
        if (marker != null && marker.getLocation() != null) {
            try {
                queryString = "q=" + URLEncoder.encode(marker.getLocation(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                log.error("exception encoding location on google map open-in-browser-url", e);
            }
        } else {
            String ll = new JSFunctionCall("getCenterURL").execute().toString();
            String zoom = new JSFunctionCall("getZoom").execute().toString();
            queryString = "ll=" + ll + "&z=" + zoom;
        }
        openGoogleMapsInBrowser(queryString);

    }

    private void openGoogleMapsInBrowser(String queryString) {

        try {
            String url = "https://maps.google.com.au/maps?" + queryString;
            log.debug("Launching google map in browser {}", url);
            Desktop.getDesktop().browse(new URI(url));
        } catch (URISyntaxException | IOException ex) {
            log.error("Exception launching browser", ex);
        }

    }

    /**
     * Displays the map of a given location. Clears any previous markers. Any pending map operations will be discarded.
     *
     * @param location  the location
     * @param fitbounds flag indicating if the viewport should be adjusted to fit the bounds of the location.
     * @param zoom      the zoom level. optional
     * @param markers   a list of marker to overlay over the map.
     */
    public void displayMap(final String location, boolean fitbounds, Integer zoom, List<Marker> markers) {
        showMap();
        calls.clear();
        calls.add(jsClearMarkers());
        calls.add(jsGoToLocation(location, fitbounds));

        if (zoom != null) {
            calls.add(jsSetZoom(zoom));
        }

        if (markers != null) {
            markers.stream().filter((marker) -> (marker != null)).forEach((marker) -> {
                calls.add(jsAddMarker(marker));
            });
        }
        executeCalls();
    }

    /**
     * Displays a map with a marker with as per the marker data. Clears any previous markers. Any pending map operations
     * will be discarded.
     *
     * @param marker the marker which will indicate the center of the map.
     */
    public void displayMap(Marker marker) {
        displayMap(marker, null);
    }

    /**
     * Displays a map with a marker with as per the marker data. Clears any previous markers. Any pending map operations
     * will be discarded.
     *
     * @param marker the marker which will indicate the center of the map.
     * @param zoom
     */
    public void displayMap(Marker marker, Integer zoom) {
        Validate.notNull(marker);
        singleMarker.set(marker);
        showMap();
        calls.clear();
        calls.add(jsClearMarkers());
        if (marker.getCoords() != null) {
            calls.add(jsGoToCoords(marker.getCoords(), null));
        } else {
            calls.add(jsGoToLocation(marker.getLocation(), false));
        }
        calls.add(jsAddMarker(marker));
        if (zoom != null) {
            calls.add(jsSetZoom(zoom));
        }
        executeCalls();

    }

    /**
     * Makes the map visible
     */
    public void showMap() {
        webView.setVisible(true);
    }

    /**
     * Hids the map showing a given status message.
     *
     * @param statusMessage the status message to display in the center of the map area.
     */
    public void hideMap(String statusMessage) {
        statusLabel.setText(statusMessage);
        webView.setVisible(false);
    }

    /**
     * Hides the map showing "Address not found message".
     */
    public void showAddressNotFound() {
        hideMap("Address Not found");
    }

    /**
     * Updates the map view port. Leaves any existing markers.
     *
     * @param center   the center
     * @param viewPort the viewport. optional.
     */
    public void updateViewport(final LatLng center, final LatLngBounds viewPort) {
        showMap();
        calls.add(jsGoToCoords(center, viewPort));
        executeCalls();
    }

    /**
     * Clears any existing markers and renders adds the specified list of markers to the map.
     *
     * @param markers the markers to render on the map.
     */
    public void setMarkers(List<Marker> markers) {
        singleMarker.set(null);
        calls.clear();
        calls.add(jsClearMarkers());
        if (markers != null) {
            markers.stream().filter((marker) -> (marker != null)).forEach((marker) -> {
                calls.add(jsAddMarker(marker));
            });
        }
        executeCalls();
    }

    /**
     * Sets the overlay over the map for the given locality lat-long coordinates.
     *
     * @param localityId     Id of the locality.
     * @param latlongJsonStr JSON format of lat-long(s) of locality.
     */
    public void addOverlay(String localityId, String latlongJsonStr) {
        if (localityId == null || localityId.isEmpty() || latlongJsonStr == null || latlongJsonStr.isEmpty()) {
            throw new IllegalArgumentException("Locality name or latlong json string cannot be empty.");
        }
        calls.add(jsAddOverlay(localityId, latlongJsonStr));
        executeCalls();
    }

    /**
     * Removes the overlay on the map for the given locality.
     *
     * @param localityId Id of the locality.
     */
    public void removeOverlay(String localityId) {
        if (localityId == null || localityId.isEmpty()) {
            throw new IllegalArgumentException("Locality name cannot be empty.");
        }
        calls.add(jsRemoveOverlay(localityId));
        executeCalls();
    }

    /**
     * Clears all the overlays on the map.
     */
    public void removeAllOverlays() {
        calls.add(jsRemoveAllOverlays());
        executeCalls();
    }
//    /**
//     * Not tested.
//     *
//     * @param location
//     * @param fitbounds
//     */
//    public void setLocationAsTyping(final String location, final boolean fitbounds) {
//        // delay location updates to we don't go too fast file typing
//        if (locationUpdateTimeline != null) {
//            locationUpdateTimeline.stop();
//        }
//        locationUpdateTimeline = new Timeline();
//        locationUpdateTimeline.getKeyFrames().add(
//                new KeyFrame(new Duration(3000), new EventHandler<ActionEvent>() {
//            public void handle(ActionEvent actionEvent) {
//                setMapData(location, fitbounds);
//            }
//        }));
//        locationUpdateTimeline.play();
//    }
    //---------------------------------
    //Javascript call methods
    //---------------------------------

    /**
     * Executes all javascript calls in {@link calls} if the component has been initialised.
     */
    private void executeCalls() {
        if (initialised) {

            calls.stream().forEach((call) -> {
                call.execute();
            });

            calls.clear();
        }
    }

    //---------------------------------
    //Javascript function
    //---------------------------------
    /**
     * Creates a goToLocation function call.
     *
     * @param location  the location
     * @param fitbounds flag indicating if the viewport should be adjusted
     * @return the function call
     */
    private JSFunctionCall jsGoToLocation(String location, boolean fitbounds) {
        return new JSFunctionCall("goToLocation", location, fitbounds);
    }

    /**
     * Creates a goToCoords function call.
     *
     * @param center   the center
     * @param viewPort the viewport
     * @return the function call
     */
    private JSFunctionCall jsGoToCoords(LatLng center, LatLngBounds viewPort) {

        if (viewPort != null) {
            return new JSFunctionCall("goToCoords", center.getLat(), center.getLng(),
                                      viewPort.getSouthwest().getLat(), viewPort.getSouthwest().getLng(),
                                      viewPort.getNortheast().getLat(),
                                      viewPort.getNortheast().getLng());
        } else {
            return new JSFunctionCall("goToCoords", center.getLat(), center.getLng());
        }

    }

    /**
     * Creates a addMArker function call.
     *
     * @param marker the marker
     * @return the function call object
     */
    private JSFunctionCall jsAddMarker(Marker marker) {
        String icon = marker.getIcon();

        if (icon != null) {
            icon = "../../../../.." + icon;
        }

        if (marker.getCoords() != null) {
            return new JSFunctionCall("addMarkerByCoords", marker.getId(), marker.getCoords().getLat(),
                                      marker.getCoords().getLng(), marker.getTitle(), icon);
        } else {
            return new JSFunctionCall("addMarkerByLocation", marker.getId(), marker.getLocation(), marker.getTitle(),
                                      icon);
        }
    }

    /**
     * Creates a setZoom function call.
     *
     * @param level the new zoom level
     * @return the js call
     */
    private JSFunctionCall jsSetZoom(int level) {
        return new JSFunctionCall("setZoom", level);
    }

    /**
     * Clears all markers.
     *
     * @return the js call.
     */
    private JSFunctionCall jsClearMarkers() {
        return new JSFunctionCall("clearMarkers");
    }

    /**
     * Sets the overlay over the map for the given locality lat-long coordinates.
     *
     * @param localityId     Id of the locality.
     * @param latlongJsonStr JSON format of lat-long(s) of locality.
     */
    private JSFunctionCall jsAddOverlay(String localityId, String latlongJsonStr) {
        return new JSFunctionCall("addOverlay", localityId, latlongJsonStr);
    }

    /**
     * Removes the overlay on the map for the given locality.
     *
     * @param localityId Id of the locality.
     */
    private JSFunctionCall jsRemoveOverlay(String localityId) {
        return new JSFunctionCall("removeOverlay", localityId);
    }

    /**
     * Clears all the overlays on the map.
     */
    private JSFunctionCall jsRemoveAllOverlays() {
        return new JSFunctionCall("removeAllOverlays");
    }

    /**
     * Sets the java upcall attribute in the javascript space and calls javascript method document.initialise().
     */
    private void initialiseMap() {

        log.debug("{} initialiseMap() setting window members", this);
//        JSObject jsobj = (JSObject)webEngine.executeScript("document");
//        jsobj.setMember("markerListener", new JavaBridgeImpl());
//        jsobj.setMember("slf4j", log);

        log.debug("{} calling document.initMap()", this);
        Boolean ret = (Boolean)new JSFunctionCall("initMap").execute();
        if (ret == null) {
            log.debug("returned null");
        }
        if (ret == true) {
            log.debug("{} document.initialise() OK returned {}, pending calls: {}", this, ret, calls.size());
            initialised = true;
            webView.setVisible(true);
            executeCalls();
        } else {
            log.error("{} document.initialise() KO returned {} ", this, ret);
        }

    }

    /**
     * Creates a horizontal spacer.
     *
     * @return a horizontal spacer.
     */
    private static Node createSpacer() {
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        return spacer;
    }

    /**
     * Abstraction of a Javascript method call
     */
    @ToString
    private class JSFunctionCall {

        /**
         * The method.
         */
        private final String method;

        /**
         * The arguments.
         */
        private final Object[] args;

        /**
         * Sole Constructor.
         *
         * @param method the method
         * @param args   the arguments
         */
        public JSFunctionCall(String method, Object... args) {
            this.method = method;
            this.args = args;
        }

        /**
         * Executes the js code.
         *
         * @return the result of the method call.
         */
        public Object execute() {
            log.debug("{} executing js {}", WebMapOriginal.this, this);
            JSObject jsObject = (JSObject)webEngine.executeScript("document");
            try {
                Object o = jsObject.call(method, args);
                log.debug("{} executed js ok result =, call ={}", WebMapOriginal.this, o, this);
                return o;
            } catch (JSException e) {
                log.warn("Javascript call " + method + " " + Arrays.asList(args) + " threw exception ", e);
                return null;
            }

        }
    }

    /**
     * Callback Interface for javascript functions to invoke java code.
     */
    public static interface JavascriptToJavaBridge {

        public void locationGecodingError();

        public void markerClicked(Object id);

        public void ubdPageClicked(Object id);
    }

    /**
     * Delegating implementation of marker listener that delegates to {@link #markerListener} if one exists.
     */
    public class JavaBridgeImpl implements JavascriptToJavaBridge {

        @Override
        public void markerClicked(Object id) {
            if (markerListener != null) {
                markerListener.markerClicked(id);
            } else {
                log.debug("Marker {} clicked but no listener set");
            }
        }

        @Override
        public void locationGecodingError() {
            log.debug("locationGecodingError()");
            showAddressNotFound();
        }

        @Override
        public void ubdPageClicked(Object id) {
            if (ubdPageOverlayListener != null) {
                ubdPageOverlayListener.ubdPageClicked(id);
            } else {
                log.debug("Locality {} clicked but no listener set", id);
            }
        }
    }
}

package com.anahata.jfx.map;

import com.google.code.geocoder.model.LatLng;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

/**
 * Marker for {@link WebMap}.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Getter
public class Marker {
    /**
     * An id for the marker used for marker click callbacks.
     */
    private final Object id;

    /**
     * An address or location.
     */
    private String location;

    /**
     * The geo coordinates
     */
    private LatLng coords;

    /**
     * A tooltip for the marker.
     */
    private String title;

    /**
     * An icon for the marker.
     */
    private String icon;

    /**
     * All args constructor. One of location or coordinates should be specified.
     *
     * @param id the marker id (optional)
     * @param location the location. One of location or coordinates should be specified.
     * @param coords the coordinates. One of location or coordinates should be specified.
     * @param title a tooltip (optional)
     * @param icon an icon (optional)
     * @throws IllegalArgumentException if either location or coordinates are specified.
     */
    public Marker(Object id, String location, LatLng coords, String title, String icon) {
        Validate.isTrue(location != null || coords != null);
        this.id = id;
        this.location = location;
        this.coords = coords;
        this.title = title;
        this.icon = icon;
    }
}

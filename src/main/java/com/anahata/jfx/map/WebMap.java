package com.anahata.jfx.map;

import com.anahata.jfx.map.event.MarkerListener;
import com.google.code.geocoder.model.LatLng;
import com.google.code.geocoder.model.LatLngBounds;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import lombok.extern.slf4j.Slf4j;

/**
 * A Google map with javafx controls to change map type open the map on an a broswer window and java API to set the map
 * location, viewport, zoom and markers.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
final public class WebMap extends BorderPane {

    private static final Image ACTION_DIRECTIONS = new Image("/com/anahata/jfx/map/directions.png");

    private static final Image ACTION_WEB = new Image("/com/anahata/jfx/map/web.png");

    /**
     * Geo Coordinates for the center of Australia.
     */
    public static final LatLng AU_CENTER = new LatLng("-25.2743980", "133.7751360");

    /**
     * Geo Coordinates for the Australia viewport.
     */
    public static final LatLngBounds AU_VIEWPORT = new LatLngBounds(new LatLng("-43.6583270", "112.92397210"),
                                                                    new LatLng("-9.22680570", "153.63867380"));

    /**
     * Geo Coordinates for an all continents viewport.
     */
    public static final LatLngBounds EARTH_VIEWPORT = new LatLngBounds(new LatLng("-70", "30"),
                                                                       new LatLng("70", "140"));

    /**
     * Initialises all JavaFX components.
     */
    public WebMap() {

    }

    private static void enableFirebug(final WebEngine engine) {
//        engine.executeScript(
//                "if (!document.getElementById('FirebugLite')){E = document['createElement' + 'NS'] && document.documentElement.namespaceURI;E = E ? document['createElement' + 'NS'](E, 'script') : document['createElement']('script');E['setAttribute']('id', 'FirebugLite');E['setAttribute']('src', 'https://getfirebug.com/' + 'firebug-lite.js' + '#startOpened');E['setAttribute']('FirebugLite', '4');(document['getElementsByTagName']('head')[0] || document['getElementsByTagName']('body')[0]).appendChild(E);E = new Image;E['setAttribute']('src', 'https://getfirebug.com/' + '#startOpened');}");
    }

    public void setDirectionsSourceLocation(String directionsSource) {
        //this.directionsSourceLocation.set(directionsSource);
    }

    private void openGoogleMapsDirectionsInBrowser() {
//
//        if (singleMarker.get() != null && directionsSourceLocation.get() != null) {
//            try {
//                String saddr = directionsSourceLocation.get();
//                String daddr = singleMarker.get().getLocation();
//                StringBuilder sb = new StringBuilder("saddr=");
//                sb.append(URLEncoder.encode(saddr, "UTF-8"));
//                sb.append("&daddr=");
//                sb.append(URLEncoder.encode(daddr, "UTF-8"));
//                sb.append("&hl=en");
//                openGoogleMapsInBrowser(sb.toString());
//            } catch (UnsupportedEncodingException ex) {
//                throw new RuntimeException(ex);
//            }
//        }
    }

    private void openInBrowser() {
//        Marker marker = singleMarker.get();
//        String queryString = null;
//        if (marker != null && marker.getLocation() != null) {
//            try {
//                queryString = "q=" + URLEncoder.encode(marker.getLocation(), "UTF-8");
//            } catch (UnsupportedEncodingException e) {
//                log.error("exception encoding location on google map open-in-browser-url", e);
//            }
//        } else {
//            String ll = new JSFunctionCall("getCenterURL").execute().toString();
//            String zoom = new JSFunctionCall("getZoom").execute().toString();
//            queryString = "ll=" + ll + "&z=" + zoom;
//        }
//        openGoogleMapsInBrowser(queryString);

    }

    private void openGoogleMapsInBrowser(String queryString) {

        try {
            String url = "https://maps.google.com.au/maps?" + queryString;
            log.debug("Launching google map in browser {}", url);
            Desktop.getDesktop().browse(new URI(url));
        } catch (URISyntaxException | IOException ex) {
            log.error("Exception launching browser", ex);
        }

    }

    /**
     * Displays the map of a given location. Clears any previous markers. Any pending map operations will be discarded.
     *
     * @param location  the location
     * @param fitbounds flag indicating if the viewport should be adjusted to fit the bounds of the location.
     * @param zoom      the zoom level. optional
     * @param markers   a list of marker to overlay over the map.
     */
    public void displayMap(final String location, boolean fitbounds, Integer zoom, List<Marker> markers) {
        
    }

    /**
     * Displays a map with a marker with as per the marker data. Clears any previous markers. Any pending map operations
     * will be discarded.
     *
     * @param marker the marker which will indicate the center of the map.
     */
    public void displayMap(Marker marker) {
        
    }

    /**
     * Displays a map with a marker with as per the marker data. Clears any previous markers. Any pending map operations
     * will be discarded.
     *
     * @param marker the marker which will indicate the center of the map.
     * @param zoom
     */
    public void displayMap(Marker marker, Integer zoom) {
        

    }

    /**
     * Makes the map visible
     */
    public void showMap() {
        
    }

    /**
     * Hids the map showing a given status message.
     *
     * @param statusMessage the status message to display in the center of the map area.
     */
    public void hideMap(String statusMessage) {
        
    }

    /**
     * Hides the map showing "Address not found message".
     */
    public void showAddressNotFound() {
        
    }

    /**
     * Updates the map view port. Leaves any existing markers.
     *
     * @param center   the center
     * @param viewPort the viewport. optional.
     */
    public void updateViewport(final LatLng center, final LatLngBounds viewPort) {
        
    }

    /**
     * Clears any existing markers and renders adds the specified list of markers to the map.
     *
     * @param markers the markers to render on the map.
     */
    public void setMarkers(List<Marker> markers) {
        
    }
    
    public void setMarkerListener(MarkerListener ml) {
        
    }

    
    
}

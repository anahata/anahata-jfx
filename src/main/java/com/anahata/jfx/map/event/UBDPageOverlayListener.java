package com.anahata.jfx.map.event;

/**
 * Interface to capture click events on ubdPage overlay.
 *
 * @author Sai Pradeep Dandem <sai@anahata-it.com.au>
 */
@FunctionalInterface
public interface UBDPageOverlayListener {
    /**
     * Notifies a ubdPage has been clicked
     *
     * @param id the ubdPage page number.
     */
    public void ubdPageClicked(Object id);
}

package com.anahata.jfx.map.event;

/**
 * Interface to capture click events on markers.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 *
 */
public interface MarkerListener {
    /**
     * notifies a marker has been clicked
     *
     * @param id the marker id.
     */
    public void markerClicked(Object id);
}

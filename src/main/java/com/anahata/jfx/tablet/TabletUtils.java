/*
 *  Copyright © - 2014 Xotica. All rights reserved.
 */
package com.anahata.jfx.tablet;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Skin;
import javafx.scene.control.TextField;
import jfxtras.labs.internal.scene.control.skin.BigDecimalFieldSkin;
import jfxtras.labs.scene.control.BigDecimalField;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;

/**
 *
 * @author pablo
 */
@Slf4j
public class TabletUtils {
    
    public static void configureNumericKeyboard(TextField... fields) {
        for (TextField field : fields) {
            field.getProperties().put("vkType", "numeric");
        }
    }
    public static void configureNumericKeyboard(BigDecimalField... fields) {
        for (BigDecimalField bdf : fields) {
            bdf.skinProperty().addListener(new ChangeListener<Skin<?>>() {
                @Override
                public void changed(
                        ObservableValue<? extends Skin<?>> observable,
                        Skin<?> oldValue,
                        Skin<?> newValue) {
                    try {
                        BigDecimalFieldSkin skin = (BigDecimalFieldSkin)bdf.getSkin();
                        TextField tf = (TextField)FieldUtils.readDeclaredField(skin, "textField", true);
                        tf.getProperties().put("vkType", "numeric");
                    } catch (Exception e) {
                        log.warn("Couldn't set numeric keyboard", e);
                    }
                }
            });
        }

    }
}

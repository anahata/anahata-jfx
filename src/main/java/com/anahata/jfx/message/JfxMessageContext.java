package com.anahata.jfx.message;

import com.anahata.jfx.bind.View;
import javafx.scene.Node;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * A JavaFX message in the context of a control.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@AllArgsConstructor
@Getter
public class JfxMessageContext {
    private View view;
    
    private Node node;

    private JfxMessage message;
}

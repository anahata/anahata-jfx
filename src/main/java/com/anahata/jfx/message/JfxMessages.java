package com.anahata.jfx.message;

import com.anahata.jfx.bind.Binder;
import com.anahata.jfx.bind.View;
import java.util.*;
import javafx.scene.Node;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import org.apache.commons.lang3.Validate;

/**
 * JavaFX global messages.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Dependent
public class JfxMessages {

    @Inject
    private com.anahata.jfx.validator.DynamicValidationMarker marker; 

    private final WeakHashMap<View, Void> views = new WeakHashMap<>();

    /**
     * Append a {@link JfxMessage} to the set of messages associated with the given id, if id is not null. If id is
     * null, the JfxMessage is assumed not to be associated with any specific component instance.
     *
     * @param view    The view.
     * @param node    The JavaFX component id, can be null.
     * @param message The message, can be null. If null, no operation is performed.
     */
    public void addMessage(View view, Node node, JfxMessage message) {
        if (message == null) {
            return;
        }

        Validate.notNull(view, "The view is required");
        final JfxMessageContext context = new JfxMessageContext(view, node, message);

        view.getGrowl().addMessage(context);
        views.put(view, null);
        marker.addMessage(context);
        
    }

//    /**
//     * Get all messages added, regardless of whether they are assigned to an id or not. This returns a copy of the data
//     * so it's safe to modify the array.
//     *
//     * @return All messages in an immutable list, never null.
//     */
//    public List<JfxMessage> getMessages() {
//        List<JfxMessage> messages = new ArrayList<>();
//
//        for (JfxMessageContext context : globalMessages) {
//            messages.add(context.getMessage());
//        }
//
//        for (List<JfxMessageContext> messages1 : idMessages.values()) {
//            for (JfxMessageContext context : messages1) {
//                messages.add(context.getMessage());
//            }
//        }
//
//        return messages;
//    }

//    /**
//     * Get all messages for a specific control if not null, or if null, only return the global messages.
//     *
//     * @param node The id to check, can be null meaning only match global messages.
//     * @return The messages in an immutable list, never null.
//     */
//    public List<JfxMessage> getMessages(Node node) {
//        if (node == null) {
//            List<JfxMessage> messages = new ArrayList<>();
//
//            for (JfxMessageContext context : globalMessages) {
//                messages.add(context.getMessage());
//            }
//
//            return messages;
//        }
//
//        List<JfxMessageContext> messageContexts = idMessages.get(node);
//
//        if (messageContexts == null) {
//            return Collections.emptyList();
//        }
//
//        List<JfxMessage> messages = new ArrayList<>();
//
//        for (JfxMessageContext context : messageContexts) {
//            messages.add(context.getMessage());
//        }
//
//        return messages;
//    }

    /**
     * Clear all messages.
     */
    public void clearMessages() {
        for (View v : views.keySet()) {
            if (v != null) {
                if (v.getGrowl() != null) {
                    v.getGrowl().clearAllMessages();
                }
            }
        }
        marker.clearAllMessages();

    }

    public void clearAndAddMessage(View view, Node node, JfxMessage message) {
        clearMessages();
        addMessage(view, node, message);
    }
    
    public void clearAndAddInfoMessage(Binder binder, String message) {
        clearMessages();        
        addMessage(binder.getView(null), null, JfxMessage.info(message));
    }

    public void clearIdMessages() {
        marker.clearAllMessages();
    }

    /**
     * Clear all messages for a given control.
     *
     * @param node The control, required.
     * @throws NullPointerException If control is null.
     */
    public void clearMessage(Node node) {
        Validate.notNull(node);
        marker.clearMessages(node);
    }
}

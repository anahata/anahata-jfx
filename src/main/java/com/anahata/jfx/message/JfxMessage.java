package com.anahata.jfx.message;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

/**
 * A JavaFX message.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
public class JfxMessage {
    private Severity severity;

    private String message;
    
    /**
     * Create an INFO severity message.
     * 
     * @param message The message, required.
     * @return The message.
     * @throws NullPointerException If message is null.
     */
    public static JfxMessage info(String message) {
        Validate.notNull(message);
        return new JfxMessage(Severity.INFO, message);
    }
    
    /**
     * Create an WARN severity message.
     * 
     * @param message The message, required.
     * @return The message.
     * @throws NullPointerException If message is null.
     */
    public static JfxMessage warn(String message) {
        Validate.notNull(message);
        return new JfxMessage(Severity.WARN, message);
    }
    
    /**
     * Create an ERROR severity message.
     * 
     * @param message The message, required.
     * @return The message.
     * @throws NullPointerException If message is null.
     */
    public static JfxMessage error(String message) {
        Validate.notNull(message);
        return new JfxMessage(Severity.ERROR, message);
    }
    
    /**
     * Create an FATAL severity message.
     * 
     * @param message The message, required.
     * @return The message.
     * @throws NullPointerException If message is null.
     */
    public static JfxMessage fatal(String message) {
        Validate.notNull(message);
        return new JfxMessage(Severity.FATAL, message);
    }
    
    /**
     * Determine if the message is WARN level or higher.
     * 
     * @return true if WARN or higher, false if not.
     */
    public boolean isWarnOrHigher() {
        return severity.getLevel() >= 1;
    }
    
    /**
     * Determine if the message is ERROR level or higher.
     * 
     * @return true if ERROR or higher, false if not.
     */
    public boolean isErrorOrHigher() {
        return severity.getLevel() >= 2;
    }
    
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    public static enum Severity {
        INFO(0),
        WARN(1),
        ERROR(2),
        FATAL(3);
        
        @Getter
        private int level;
    }
}

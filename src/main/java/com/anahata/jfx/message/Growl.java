package com.anahata.jfx.message;

import com.anahata.jfx.config.JavaFXConfig;
import com.anahata.jfx.message.JfxMessage.Severity;
import com.anahata.util.cdi.Cdi;
import java.util.HashSet;
import java.util.Set;
import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.GroupBuilder;
import javafx.scene.control.Control;
import javafx.scene.control.LabelBuilder;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import static com.anahata.jfx.message.JfxMessage.Severity.*;
import javafx.scene.layout.*;
import lombok.Getter;
import lombok.Setter;

/**
 * Display growling messages in a dialog. This will auto-show if there are messages. Clients must create a producer
 * returning a Pane, with the @GrowlParent qualifier.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
@Dependent
public class Growl {
    private static final double WIDTH = 350;
    
    private static final double MIN_HEIGHT = 60;

    private static Image NOTIFY_INFO = new Image("/com/anahata/jfx/dialog/info16.png");

    private static Image NOTIFY_WARN = new Image("/com/anahata/jfx/dialog/warning16.png");

    private static Image NOTIFY_ERROR = new Image("/com/anahata/jfx/dialog/error16.png");

    private static Image NOTIFY_FATAL = new Image("/com/anahata/jfx/dialog/error16.png");
    
    private static Image CLOSE = new Image("/com/anahata/jfx/close12.png");
    
    @Inject
    private JavaFXConfig config;
    
    private Pane parent;
    
    private Group group;
    
    private VBox vbox;
    
    private Status status = Status.HIDDEN;
    
    private Set<JfxMessage> messages = new HashSet<>();
    
    private FadeTransition fadeIn;
    
    private FadeTransition fadeOut;
    
    private PauseTransition pause;
    
    @Getter
    @Setter
    private double margin = 0;

    public Growl(@NonNull Pane parent) {
        
        while (parent.getParent() instanceof Pane) {
            parent = (Pane) parent.getParent();
            if (parent instanceof AnchorPane) {
                this.parent = parent;
                break;
            }
        }
        
        if (this.parent == null) {
            log.warn("Growl parent is not an anchor pane. It is going to look funny.");
            this.parent = parent;
        }
        
        this.config = Cdi.get(JavaFXConfig.class);
        
        final ImageView closeButton = ImageViewBuilder.create()
                .image(CLOSE)
                .cache(true)
                .visible(false)
                .styleClass("growlClose")
                .onMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent t) {
                        removeFromParent();
                    }
                })
                .build();
        
        vbox = VBoxBuilder.create()
                .styleClass("growlWindow")
                .prefWidth(WIDTH)
                .prefHeight(Control.USE_COMPUTED_SIZE)
                .minWidth(Control.USE_PREF_SIZE)
                .minHeight(MIN_HEIGHT)
                .maxWidth(Control.USE_PREF_SIZE)
                .maxHeight(Double.MAX_VALUE)
                .build();
        
        group = GroupBuilder.create()
                .children(
                    vbox,
                    BorderPaneBuilder.create()
                        .styleClass("growlCloseContainer")
                        .prefWidth(WIDTH)
                        .maxWidth(Control.USE_PREF_SIZE)
                        .right(closeButton)
                        .build()
                )
                .onMouseEntered(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent t) {
                        closeButton.setVisible(true);
                    }
                })
                .onMouseExited(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent t) {
                        closeButton.setVisible(false);
                    }
                })
                .build();
        
        pause = new PauseTransition(Duration.millis(config.getMessageGrowlPauseMillis()));
        pause.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                removeFromParent();
            }
        });
        
        fadeIn = new FadeTransition(Duration.millis(config.getMessageGrowlFadeInMillis()), group);
        fadeIn.setFromValue(0);
        fadeIn.setToValue(1);
        fadeIn.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                status = Status.SHOWN;
                pause.playFromStart();
            }
        });
        
        fadeOut = new FadeTransition(Duration.millis(config.getMessageGrowlFadeOutMillis()), group);
        fadeOut.setFromValue(1);
        fadeOut.setToValue(0);
        fadeOut.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                Growl.this.parent.getChildren().remove(group);
                status = Status.HIDDEN;
            }
        });
    }
    
    void addMessage(JfxMessageContext messageContext) {
        log.debug("Got a new message");
        
        if (messageContext.getNode() != null) {
            return;
        }
        
        final JfxMessage message = messageContext.getMessage();
        addMessage(message);
        
    }
    
    public void clearAndAddMessage(JfxMessage message) {
        clearAllMessages();
        addMessage(message);
    }
    
    public void addMessage(JfxMessage message) {
        if (!messages.contains(message)) {
            messages.add(message);
            
            final HBox hbox = HBoxBuilder.create()
                    .spacing(4)
                    .userData(message)
                    .styleClass("growlLine")
                    .children(
                        getSeverityImageView(message.getSeverity()),
                            LabelBuilder.create()
                                .text(message.getMessage())
                                .wrapText(true)
                                .build()
                    )
                    .build();
            
            vbox.getChildren().add(hbox);
        }
        
        addToParent();
    }
    
    public void clearAllMessages() {
        log.debug("Clearing messages");
        
        messages.clear();
        vbox.getChildren().clear();
        removeFromParent();
    }
    
    private void addToParent() {
        if (status == Status.HIDING) {
            fadeOut.stop();
            parent.getChildren().remove(group);
            status = Status.HIDDEN;
        }
        
        if (status == Status.HIDDEN) {
            parent.getChildren().add(group);
            group.toFront();
            group.relocate(parent.getWidth() - WIDTH - margin, margin);
            status = Status.SHOWING;
            fadeIn.playFromStart();
        } else if (status == Status.SHOWN) {
            pause.playFromStart();
        }
    }
    
    private void removeFromParent() {
        log.debug("removeFromParent: status={}", status);
        
        if (status == Status.SHOWING) {
            fadeIn.stop();
            status = Status.SHOWN;
        }
        
        if (status == Status.SHOWN) {
            status = Status.HIDING;
            fadeOut.playFromStart();
        }
    }
    
    /**
     * Get an icon based on a message severity.
     * 
     * @param severity The message severity. Required.
     * @return The ImageView.
     * @throws NullPointerException If severity is null.
     */
    public static ImageView getSeverityImageView(Severity severity) {
        Validate.notNull(severity);
        Image image;
        
        switch (severity) {
            case INFO:
                image = NOTIFY_INFO;
                break;
                
            case WARN:
                image = NOTIFY_WARN;
                break;
                
            case ERROR:
                image = NOTIFY_ERROR;
                break;
                
            default:
                image = NOTIFY_FATAL;
        }
        
        ImageView iv = new ImageView(image);
        iv.setCache(true);
        return iv;
    }
    
    private enum Status {
        SHOWING,
        SHOWN,
        HIDING,
        HIDDEN;
    }
}

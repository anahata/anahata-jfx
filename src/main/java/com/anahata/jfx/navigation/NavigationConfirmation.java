package com.anahata.jfx.navigation;

/**
 * Used to confirm that navigation away from a screen is possible.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface NavigationConfirmation {
    /**
     * Determine if the screen can be navigated away from.
     * 
     * @return true if can be navigated away from, false if not.
     */
    public boolean canNavigateAway();
}

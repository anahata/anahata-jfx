/*
 *  Copyright © - 2014 Anahata Technologies.
 */
package com.anahata.jfx.thread;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import javafx.application.Platform;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Invocation handler that invokes a method in the JavaFX thread when not running on the fx thread.
 *
 * @author Pablo Rodriguez Pina <pablo@anahata-it.com.au>
 */
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class FxThreadProxy<T> implements InvocationHandler {

    /**
     * The delegate.
     */
    @Getter
    private T delegate;

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {

        if (method.getDeclaringClass().equals(Object.class)) {
            //to allow toString at least
            return method.invoke(delegate, args);
        }

        if (Platform.isFxApplicationThread()) {
            return method.invoke(delegate, args);
        }

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    log.trace("Invoking {} in listener {} with args {}", method.getName(), delegate, args);
                    method.invoke(delegate, args);
                } catch (Throwable e) {
                    log.error(
                            "Exception Invoking " + method.getName()
                            + " in delegate " + delegate
                            + " with args " + args, e);
                }
            }
        });

        return null;
    }

    /**
     * Convenience method to create a new proxy instance specifying the interface and the handler.
     *
     * @param <T>
     * @param clazz
     * @param delegate
     * @return
     */
    public static <T> T newInstance(Class<T> clazz, T delegate) {
        T proxy = (T)Proxy.newProxyInstance(clazz.getClassLoader(),
                new Class[]{clazz},
                new FxThreadProxy(delegate)
        );
        return proxy;
    }
}

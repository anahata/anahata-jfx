package com.anahata.jfx;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.FileChooserBuilder;
import javafx.stage.Window;
import javax.imageio.ImageIO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;

/**
 * Image utilities.
 *
 * @author Pablo Rodriguez
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ImageUtils {
    public static final String[] IMAGE_SUFFIXES;

    public static final List<String> IMAGE_EXTENSIONS = new ArrayList<>();

    static {
        IMAGE_SUFFIXES = ImageIO.getReaderFileSuffixes();

        for (String suffix : IMAGE_SUFFIXES) {
            IMAGE_EXTENSIONS.add("*." + suffix.toUpperCase());
            IMAGE_EXTENSIONS.add("*." + suffix.toLowerCase());
        }
    }

    public static Group getOverLayedImage(Image base, Image overlay, int size, boolean fade, Pos position) {
        ImageView iconImageView = JfxUtils.makeIcon(base, size);
        return getOverLayedImage(iconImageView, overlay, size, 0, fade, position);
    }

    public static Group getOverLayedImage(Image base, Image overlay, int size, int spacing, boolean fade, Pos position) {
        ImageView iconImageView = JfxUtils.makeIcon(base, size);
        return getOverLayedImage(iconImageView, overlay, size, spacing, fade, position);
    }

    public static Group getOverLayedImage(Image base, Node overlay, int size, int spacing, boolean fade, Pos position) {
        SnapshotParameters sp = new SnapshotParameters();
        sp.setFill(Color.TRANSPARENT);
        return getOverLayedImage(base, overlay.snapshot(sp, null), size, spacing, fade, position);
    }

    public static Group getOverLayedImage(Node base, Node overlay, int size, int spacing, boolean fade, Pos position) {
        SnapshotParameters sp = new SnapshotParameters();
        sp.setFill(Color.TRANSPARENT);
        return getOverLayedImage(base, overlay.snapshot(sp, null), size, spacing, fade, position);
    }

    /**
     * Overlay an image over a base node.
     *
     * @param base     The base node.
     * @param overlay  The image to overlay.
     * @param size     The size for the image.
     * @param spacing  TODO Pablo fill in.
     * @param fade     If true, makes the saturation and brightness of the base faded behind the image.
     * @param position The position to place the overlay.
     * @return The created node.
     */
    public static Group getOverLayedImage(Node base, Image overlay, int size, int spacing, boolean fade, Pos position) {
        Validate.notNull(base);
        Validate.notNull(overlay);
        Validate.notNull(position);

        if (fade) {
            ColorAdjust colorAdjust = new ColorAdjust();
            colorAdjust.setBrightness(-0.3);
            colorAdjust.setSaturation(-0.3);
            base.setEffect(colorAdjust);
        }

        int overlayedImageSize = (int)((float)size / 1.75);

        ImageView overlayedImageView = new ImageView(overlay);
        overlayedImageView.setCache(true);
        overlayedImageView.setSmooth(true);
        overlayedImageView.setFitWidth(overlayedImageSize);
        overlayedImageView.setFitHeight(overlayedImageSize);

        if (position == Pos.CENTER_RIGHT) {
            overlayedImageView.setY(size - (overlayedImageSize / 2));
            overlayedImageView.setX(size - overlayedImageSize + spacing);
        } else if (position == Pos.CENTER_LEFT) {
            overlayedImageView.setY((size - overlayedImageSize) / 2);
            overlayedImageView.setX(0 - (overlayedImageSize + spacing));
        } else if (position == Pos.BOTTOM_RIGHT) {
            overlayedImageView.setX(size - overlayedImageSize + spacing);
            overlayedImageView.setY(size - overlayedImageSize + spacing);
        } else if (position == Pos.BOTTOM_LEFT) {
            overlayedImageView.setX(0 - spacing);
            overlayedImageView.setY(size - overlayedImageSize + spacing);
        } else {
            throw new UnsupportedOperationException("position " + position + " not yet implemented");
        }

        Group g = new Group();
        g.setBlendMode(BlendMode.SRC_OVER);
        g.getChildren().add(base);
        g.getChildren().add(overlayedImageView);

        return g;
    }

    /**
     * Display a file chooser to select a single image.
     *
     * @param parent The parent window. Can be null.
     * @return The selection, or null if nothing selected.
     */
    public static File showOpenImageDialog(Window parent) {
        final FileChooser fc = FileChooserBuilder.create().extensionFilters(new ExtensionFilter(
                "Image Files", IMAGE_EXTENSIONS)).title("Choose image").build();
        return fc.showOpenDialog(parent);
    }

    /**
     * Display a file chooser to select images.
     *
     * @param parent The parent window. Can be null.
     * @return The selection(s), or null if nothing selected.
     */
    public static List<File> showOpenMultipleImageDialog(Window parent) {
        final FileChooser fc = FileChooserBuilder.create().extensionFilters(new ExtensionFilter(
                "Image Files", IMAGE_EXTENSIONS)).title("Choose images").build();
        return fc.showOpenMultipleDialog(parent);
    }

    /**
     * Display a file chooser to upload an image, store it in the userData property of an ImageView.
     *
     * @param message   Not used.
     * @param imageView The ImageView to update.
     * @return The uploaded image, or null if not set.
     */
    public static byte[] uploadImage(String message, ImageView imageView) {
        File f = showOpenImageDialog(null);

        if (f != null) {
            try {
                byte[] uploadedLogo = FileUtils.readFileToByteArray(f);
                @SuppressWarnings("unchecked")
                ObjectProperty<byte[]> userData = (ObjectProperty<byte[]>)imageView.getUserData();
                userData.setValue(uploadedLogo);
                return uploadedLogo;
            } catch (Exception e) {
                log.error("Could not load logo", e);
            }
        }

        return null;
    }

    /**
     * Setup an ImageView to automatically hide itself it no image has been set, and optionally display a label if the
     * image is missing. Image bytes are stored in the userData property of the ImageView. An Image will automatically
     * be created if the userData property is updated.
     *
     * @param imageView       The ImageView. Required.
     * @param emptyImageLabel The Label to display if the image is null. Optional.
     */
    public static void initBindableImageView(final ImageView imageView, Label emptyImageLabel) {
        final ObjectProperty<byte[]> logoUserData = new SimpleObjectProperty<>();
        imageView.setUserData(logoUserData);
        imageView.visibleProperty().bind(imageView.imageProperty().isNotNull());

        logoUserData.addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable o) {
                @SuppressWarnings("unchecked")
                final ObjectProperty<byte[]> userData = (ObjectProperty<byte[]>)imageView.getUserData();
                final byte[] imageData = userData.get();

                if (imageData != null) {
                    Image image = new Image(new ByteArrayInputStream(imageData));
                    imageView.setSmooth(true);
                    imageView.setPreserveRatio(true);
                    imageView.setImage(image);
                } else {
                    imageView.setImage(null);
                }
            }
        });

        if (emptyImageLabel != null) {
            emptyImageLabel.visibleProperty().bind(imageView.imageProperty().isNull());
        }
    }

    /**
     * Get image bytes from an ImageView stored in the userData property.
     *
     * @param imageView The ImageView. Required.
     * @return The bytes, or null if none have been set.
     */
    public static byte[] getUploadedImageData(final ImageView imageView) {
        ObjectProperty sop = (ObjectProperty)imageView.getUserData();

        if (sop != null) {
            return (byte[])sop.get();
        } else {
            return null;
        }
    }
    
    /**
     * Get image bytes from an ImageView stored in the userData property.
     *
     * @param imageView The ImageView. Required.
     * @return The bytes, or null if none have been set.
     */
    public static void clearUploadedImageData(final ImageView imageView) {
        ObjectProperty sop = (ObjectProperty)imageView.getUserData();

        if (sop != null) {
            sop.set(null);
        } 
    }
}

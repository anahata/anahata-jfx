/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.stage;

import com.anahata.util.lang.builder.DelimitedStringBuilder;
import java.io.Serializable;
import java.util.Arrays;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * Model class to store the stage position when the user exits the application.
 * 
 * @author pablo
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class StagePosition implements Serializable {
    private double x;
    private double y;
    private double width;
    private double height;
    private boolean maximized;
    public StagePosition(Stage s) {
        this.x = s.getX();
        this.y = s.getY();
        this.width = s.getWidth();
        this.height = s.getHeight();
        this.maximized = s.isMaximized();
    }
    
    public boolean isRestorable() {
        return !Screen.getScreensForRectangle(x, y, width, height).isEmpty();
    }
    
    public void restore(Stage s) {
        Validate.isTrue(maximized || isRestorable(), "Stage position " + this + " cannot be restored");
        if (maximized) {
            s.setMaximized(true);
        } else {
            s.setX(x);
            s.setY(y);
            s.setWidth(width);
            s.setHeight(height);
        }
        
    }
    
    public String toString() {
        return new DelimitedStringBuilder("#").append(x).append(y).append(width).append(height).append(maximized).toString();
    }
    
    public static StagePosition fromString(String s) {
        log.info("fromString {}", s);
        StagePosition sp = new StagePosition();
        String[] tokens = s.split("#");
        log.debug("tokens are " + Arrays.asList(tokens));
        sp.x = new Double(tokens[0]);
        sp.y = new Double(tokens[1]);
        sp.width = new Double(tokens[2]);
        sp.height = new Double(tokens[3]);
        sp.maximized = "true".equals(tokens[4]);
        return sp;
    }
    
}

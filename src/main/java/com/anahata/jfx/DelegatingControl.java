package com.anahata.jfx;

/**
 * A control that delegates to a controller.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public interface DelegatingControl {
    /**
     * Get the controller associated with this control.
     * 
     * @return The controller.
     */
    Object getController();
}

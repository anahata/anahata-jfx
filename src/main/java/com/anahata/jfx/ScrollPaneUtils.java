/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 *
 * @author pablo
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class ScrollPaneUtils {
    public static boolean isVisible(ScrollPane sp, Node node) {
        double nodeY = JfxUtils.yDistanceInAncestor(node, (Parent)sp.getContent());
        double contentHeight = sp.getContent().getBoundsInLocal().getHeight();
        double scrollPaneHeight = sp.getViewportBounds().getHeight();
        double currentY = sp.getVvalue();        
        double visibleFrom = contentHeight * currentY;
        double visibleTo = visibleFrom + scrollPaneHeight;
        System.out.println("currentY " + currentY);
        //System.out.println("scrollPaneHeight " + scrollPaneHeightHeight);
        System.out.println("contentHeight " + contentHeight);
        System.out.println("nodeY " + nodeY + " visibleFrom" + visibleFrom + " visibleTo" + visibleTo + " contentHeight" + contentHeight);
        System.out.println("nodeY > visibleFrom" + (nodeY > visibleFrom));
        System.out.println("nodeY < visibleTo" + (nodeY < visibleTo));
        return nodeY > visibleFrom && nodeY < visibleTo;
        
    }
    
    public static void scrollTo(ScrollPane sp, Node node, double offSet) {
        double h = sp.getContent().getBoundsInLocal().getHeight();        
        double v = sp.getViewportBounds().getHeight();
        double y = JfxUtils.yDistanceInAncestor(node, (Parent)sp.getContent());
        y = Math.max(0, y + offSet);
        double newValue = sp.getVmax() * (y / (h - v));
        log.debug("scrolling pane currValue = {} newValue={} ", sp.getVvalue(), newValue);
        sp.setVvalue(newValue);
    }
}

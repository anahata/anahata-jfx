package com.anahata.jfx.control;


import com.anahata.jfx.bind.nodemodel.NodeModel;
import com.sun.javafx.scene.control.skin.TableViewSkin;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import javafx.beans.property.Property;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;

/**
 *
 * @author pablo
 */
public class AutosizeTableView<S> extends TableView implements NodeModel<AutosizeTableView, Property> {
    
    @Override
    protected AutosizeTableViewSkin createDefaultSkin() {
        return new AutosizeTableViewSkin(this);
    }

    public static class AutosizeTableViewSkin extends TableViewSkin {

        //private VirtualFlow2 vf;
        public AutosizeTableViewSkin(TableView tableView) {
            super(tableView);
        }

        @Override
        protected VirtualFlow<TableRow> createVirtualFlow() {
            return new AutoSizeVirtualFlow();
        }

        @Override
        protected double computeMaxHeight(double width, double topInset, double rightInset, double bottomInset,
                double leftInset) {
            return computePrefHeight(width, topInset, rightInset, bottomInset, leftInset);
        }

        @Override
        protected double computeMinHeight(double width, double topInset, double rightInset, double bottomInset,
                double leftInset) {
            return computePrefHeight(width, topInset, rightInset, bottomInset, leftInset);
        }
        
        public AutoSizeVirtualFlow getVirtualFlow() {
            return (AutoSizeVirtualFlow) flow;
        }


        @Override
        protected double computePrefHeight(double width, double topInset, double rightInset, double bottomInset,
                double leftInset) {
            
            double prefHeight = getVirtualFlow().computePrefHeight(width) + getTableHeaderRow().prefHeight(-1);
                    
            System.out.println("skin prefHeight" + prefHeight);
            double ret = topInset + prefHeight + bottomInset;
            System.out.println("skin prefHeight including insets " + prefHeight);
            return ret;
            //return super.computeMinHeight(width, topInset, rightInset, bottomInset, leftInset);
        }

        public class AutoSizeVirtualFlow extends VirtualFlow {

            @Override public double computePrefHeight(double width) {
                double h = isVertical() ? getAllRowsHeight() : 0;
                return h + getHbar().prefHeight(-1);
            }

            public double getAllRowsHeight() {
                double sum = 0.0;
                int rows = Math.max(10, getItemCount()) + 1;
                for (int i = 0; i < rows; i++) {
                    sum += getCellLength(i);
                }
                System.out.println("getAllRowsHeight = " + sum + " cellCount=" + super.getCellCount() + " " + AutosizeTableViewSkin.this.getItemCount());
                return sum;
            }
        };
    }

    @Override
    public Property getNodeModelValueProperty(AutosizeTableView node) {
        return node.itemsProperty();
    }
}

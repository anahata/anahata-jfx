package com.anahata.jfx.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.fxmisc.richtext.StyleClassedTextArea;
import org.languagetool.JLanguageTool;
import org.languagetool.language.AustralianEnglish;
import org.languagetool.rules.RuleMatch;

/**
 *
 * @author pablo
 */
@Slf4j
public class TextEditor extends VBox {
    private double textAreaHeight;

    @Getter
    final StyleClassedTextArea textArea = new StyleClassedTextArea();

    final VBox bottom = new VBox();

    final Label currentRule = new Label();

    final static JLanguageTool languageTool = new JLanguageTool(new AustralianEnglish());

    private List<RuleMatch> rules = new ArrayList();

    public TextEditor() {
        getStylesheets().add("/com/anahata/jfx/control/" + getClass().getSimpleName() + ".css");
        init();
    }

    private void init() {
        check();
        getChildren().addAll(textArea, bottom);
        bottom.maxWidthProperty().bind(textArea.widthProperty());
        textArea.getStyleClass().add("text-editor-text-area");
        getStyleClass().add("text-editor");

        textArea.textProperty().addListener((ov, o, n) -> {
            System.out.println("Text property changed " + textArea.getText());
            check();
            styleTextArea();
            renderBottom();
        });

        textArea.caretPositionProperty().addListener((a, b, c) -> renderBottom());
    }

    public void setText(String text) {
        textArea.replaceText(0, (int)textArea.getText().length(), text);
    }

    public String getText(String text) {
        return textArea.getText();
    }

    public double getTextAreaHeight() {
        return textAreaHeight;
    }

    public void setTextAreaHeight(double textAreaHeight) {
        this.textAreaHeight = textAreaHeight;
        textArea.setPrefHeight(textAreaHeight);
        textArea.setMaxHeight(textAreaHeight);
        textArea.setMinHeight(textAreaHeight);
    }

    /**
     * The text property of the styled text area.
     *
     * @return
     */
    public ObservableValue<String> textProperty() {
        return textArea.textProperty();
    }

    private void check() {
        try {
            //System.out.println("Checking " + textArea.getText());
            rules = languageTool.check(textArea.getText());
            //System.out.println("Checked " + textArea.getText() + " got " + rules);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void styleTextArea() {
        textArea.clearStyle(0, textArea.getText().length());
        rules.forEach(rm -> {
            //log.info("------------------- \nRule Id : {}\nRule Category : {}\nRule Description : {}",
            //        rm.getRule().getId(), rm.getRule().getCategory(), rm.getRule().getDescription());
            
            boolean typo = rm.getRule().getId().startsWith("MORFOLOGIK_RULE");
            String style = typo ? "spell-typo" : "spell-other";
            textArea.setStyle(rm.getFromPos(), rm.getToPos(), Collections.singletonList(style));
        });
    }

    private void renderBottom() {
        bottom.getChildren().clear();
        int count = 1;
        for (RuleMatch rm : rules) {
            if (rm.getFromPos() <= textArea.getCaretPosition() && rm.getToPos() >= textArea.getCaretPosition()) {
                VBox content = new VBox();
                String text = textArea.getText(rm.getFromPos(), rm.getToPos());
                Text label = new Text(count + " - " + text + " : " + rm.getMessage());
                label.getStyleClass().add("typo-message");
                label.wrappingWidthProperty().bind(bottom.widthProperty());
                content.getChildren().add(label);
                if (!rm.getSuggestedReplacements().isEmpty()) {
                    FlowPane suggestionsFlowPane = new FlowPane();
                    suggestionsFlowPane.getChildren().add(new Label("Suggestions:"));
                    content.getChildren().add(suggestionsFlowPane);
                    VBox.setMargin(suggestionsFlowPane, new Insets(0, 0, 0, 10));
                    for (String suggestedReplacement : rm.getSuggestedReplacements()) {
                        Hyperlink hl = new Hyperlink(suggestedReplacement);
                        hl.setOnAction(e -> {
                            textArea.replaceText(rm.getFromPos(), rm.getToPos(), suggestedReplacement);
                            textArea.moveTo(rm.getFromPos() + suggestedReplacement.length());
                            Platform.runLater(() -> textArea.requestFocus());
                        });
                        suggestionsFlowPane.getChildren().add(hl);
                    }
                }

                bottom.getChildren().add(content);
            }
            count++;
        }
    }

}

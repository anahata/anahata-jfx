package com.anahata.jfx.test;

import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utilities for creating JavaFX tests.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestJfxUtils {
    public static Tab tab(TabPane tabPane, String title, Node node) {
        Tab tab = new Tab(title);
        tab.setClosable(false);
        tab.setContent(node);
        tabPane.getTabs().add(tab);
        return tab;
    }
}

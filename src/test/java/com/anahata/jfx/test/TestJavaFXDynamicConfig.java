package com.anahata.jfx.test;

import com.anahata.jfx.config.JavaFXDynamicConfig;
import javax.enterprise.context.ApplicationScoped;

/**
 * Dynamic config.
 * 
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@ApplicationScoped
public class TestJavaFXDynamicConfig implements JavaFXDynamicConfig {
    @Override
    public String getCssPath() {
        return "/";
    }
}

/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.test;

import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSObject;

/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class WebViewTest extends Application {

    @Override
    public void start(Stage primaryStage) {

        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();

        webEngine.load(getClass().getResource("webEngineTest.html").toExternalForm());

        webEngine.getLoadWorker().stateProperty().addListener(
                (ObservableValue<? extends State> ov, State t, State t1) -> {
                    if (t1 == State.SUCCEEDED) {
                        JSObject jsobj = (JSObject)webEngine.executeScript("document");
                        jsobj.call("test");
                    }
                });

        StackPane root = new StackPane();
        root.getChildren().add(webView);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}

/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.test;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.lodgon.openmapfx.core.MapView;

/**
 * Test application to check the functionality of the MapView control.
 *
 * @author sai.dandem
 */
public class MapViewTest extends Application {
    private HBox root;

    private Scene scene;

    private Stage stage;

    private MapView mapView;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        root = new HBox();
        VBox padding = new VBox();
        Button b = new Button("Perth as center");
        b.setOnAction((e) -> mapView.setPerthAsCenter());
        padding.getChildren().add(b);
        padding.setMinWidth(900);
        root.getChildren().add(padding);
        root.setFillHeight(true);
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setMaximized(true);
        configureMap();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

    private void configureMap() throws Exception {
        mapView = new MapView();        
        root.getChildren().add(mapView);
        //Platform.runLater(() -> mapView.setPerthAsCenter());
    }
}

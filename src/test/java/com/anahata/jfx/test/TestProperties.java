package com.anahata.jfx.test;

import com.anahata.util.config.internal.ApplicationPropertiesFactory;
import java.util.Properties;
import lombok.Getter;

/**
 * Configure properties for unit tests.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class TestProperties implements ApplicationPropertiesFactory {
    @Getter
    private static final Properties props = new Properties();
    
    @Override
    public Properties getAppProperties() {
        return props;
    }
}

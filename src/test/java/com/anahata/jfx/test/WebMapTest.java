/*
 *  Copyright © - 2013 Anahata Technologies.
 */

package com.anahata.jfx.test;

import com.anahata.jfx.map.WebMap;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class WebMapTest extends Application {
    
    @Override
    public void start(Stage primaryStage) {
       
        WebMap map = new WebMap();
        map.showMap();
        
        StackPane root = new StackPane();
        root.getChildren().add(map);
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

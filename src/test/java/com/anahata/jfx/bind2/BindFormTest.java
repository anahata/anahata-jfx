/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.bind2;

import com.anahata.util.validator.constraints.AssertFieldTrue;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class BindFormTest {
    @Test
    public void testBindForm() {
        Object formNode = new Object();
        BindForm bindForm = new BindForm(formNode);
        Person person = new Person(0L, "Test", "User", null);
        
        ObjectProperty<Person> model = new SimpleObjectProperty<>(person);
        bindForm.addModel("person", model, true);
        UiTester firstNameNode = new UiTester();
        bindForm.addField("person", firstNameNode, "firstName", null, null, null);
        UiTester notesNode = new UiTester();
        bindForm.addField("person", notesNode, "notes", null, null, null);
        
        bindForm.bindFromModel();
        notesNode.addValue("blah");
        notesNode.addValue(" and some more");
        notesNode.setValue("A very long string causing a validation error");
        notesNode.setValue("Fixed now");
        firstNameNode.setValue(null);
        log.debug("*** Setting null model");
        model.setValue(null);
        log.debug("*** Setting person2");
        Person person2 = new Person(1L, "Second", "Person", "Blah");
        model.setValue(person2);
        log.debug("*** Adding hobby");
        Hobby hobby = new Hobby(0L, "Painting");
        person2.getHobbies().add(hobby);
        UiTester hobbyNode = new UiTester();
        bindForm.addField("person", hobbyNode, "hobbies[0].name", null, null, null);
        bindForm.bindFromModel();
        log.debug("*** Setting hobby name to null");
        hobbyNode.setValue(null);
        
        Sport sport = new Sport(0L, "Basketball");
        person2.getSports().add(sport);
        ObjectProperty<Sport> sportModel = new SimpleObjectProperty<>(sport);
        bindForm.addModel("sport", sportModel, true);
        UiTester sportNode = new UiTester();
        bindForm.addField("sport", sportNode, "name", null, null, null);
        bindForm.bindFromModel();
        
        sportNode.setValue(null);
        bindForm.setBlocked(true);
        sportNode.setValue("Boo");
        bindForm.setBlocked(false);
    }
    
    @NoArgsConstructor
    @EqualsAndHashCode(of = "id")
    @Getter
    @Setter
    public static class Person {
        private Long id;
        
        @NotNull
        private String firstName;
        
        @NotNull
        private String lastName;
        
        @NotNull
        @Size(min = 1, max = 20)
        private String notes;
        
        @NotNull
        @Valid
        private List<Hobby> hobbies = new ArrayList<>();
        
        @NotNull
        @Valid
        private List<Sport> sports = new ArrayList<>();
        
        public Person(Long id, String firstName, String lastName, String notes) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.notes = notes;
        }
        
        @AssertFieldTrue(field = "firstName", message = "First name must begin with 'Boo'")
        public boolean isFirstNameValid() {
            if (firstName == null) {
                return true;
            }
            
            return firstName.startsWith("Boo");
        }
    }
    
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode(of = "id")
    @Getter
    @Setter
    public static class Hobby {
        private Long id;
        
        @NotNull
        private String name;
    }
    
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode(of = "id")
    @Getter
    @Setter
    public static class Sport {
        private Long id;
        
        @NotNull
        private String name;
    }
    
    @Slf4j
    public static class UiTester implements BindNode<String> {
        private BindField bindField;
        
        private String value;
        
        @Override
        public void setValue(Object node, String value) {
            this.value = value;
        }

        @Override
        public void register(BindField bindField) {
            this.bindField = bindField;
        }
        
        public void addValue(String value) {
            log.debug("*** addValue={}", value);
            
            if (this.value == null) {
                this.value = value;
            } else {
                this.value = this.value + value;
            }
            
            bindField.setModelValue(this.value);
        }
        
        public void setValue(String value) {
            log.debug("*** setValue={}", value);
            this.value = value;
            bindField.setModelValue(value);
        }
    }
}

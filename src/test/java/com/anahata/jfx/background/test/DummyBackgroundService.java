/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.background.test;

/**
 *
 * @author pablo
 */
public interface DummyBackgroundService {
    public String hello(); 
}

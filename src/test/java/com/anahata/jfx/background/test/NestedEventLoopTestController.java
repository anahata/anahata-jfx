/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.background.test;

import com.anahata.jfx.concurrent.Background;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

/**
 * FXML Controller class
 *
 * @author pablo
 */
@Slf4j
public class NestedEventLoopTestController implements Initializable {

    @Background 
    @Inject
    DummyBackgroundService service;
            
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        log.debug("Initializing: controller ");
        
        log.debug("Initializing: controller " + service.hello());
        
        log.debug("Initializing: controller " + service.hello());
        
        log.debug("Initializing: controller " + service.hello());
        
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    while(true) {
                        log.debug("Sleeping ");
                        Thread.sleep(500);
                        log.debug("Calling platform.runLater: refresh");
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                refresh();
                            }
                        });
                        log.debug("Finsiehd Calling platform.runLater: refresh");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        
//        Platform.runLater(new Runnable() {
//            @Override
//            public void run() {
//                log.debug("Exiting nested event loop");
//                Toolkit.getToolkit().exitNestedEventLoop(key, "exit");
//            }
//        });
        
//        final String key1 = "asdasd";
//
//        ApplicationTask at = new ApplicationTask() {
//            @Override
//            protected Object call() throws Exception {
//                Thread.sleep(3000);
//                return null;
//            }
//
//            @Override
//            protected void succeeded() {
//                log.debug("suceeded ");
//                final String key2 = "key2";
//                ApplicationTask at = new ApplicationTask() {
//                    @Override
//                    protected Object call() throws Exception {
//                        Thread.sleep(3000);
//                        return null;
//                    }
//
//                    @Override
//                    protected void succeeded() {
//                        log.debug("exiting 2");
//                        Toolkit.getToolkit().exitNestedEventLoop(key2, "exit");
//                    }
//
//                };
//                at.launch();
//                System.out.println("entering 2");
//                String exit = (String)Toolkit.getToolkit().enterNestedEventLoop(key2);
//                log.debug("exited 2: " + exit);
//                log.debug("exiting 1");
//                Toolkit.getToolkit().exitNestedEventLoop(key1, "exit");
//            }
//
//        };
//        at.launch();
//        String exit = (String)Toolkit.getToolkit().enterNestedEventLoop(key1);
//        log.debug("Exited 1 " + exit);
    }
    
    public void refresh() {
        log.debug("Refresh enter");
        service.hello();
        log.debug("Refresh exit");
        
    }

}

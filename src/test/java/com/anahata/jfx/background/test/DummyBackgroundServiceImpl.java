/*
 *  Copyright © - 2013 Anahata Technologies.
 */
package com.anahata.jfx.background.test;

import lombok.extern.slf4j.Slf4j;
/**
 *
 * @author pablo
 */
@Slf4j
public class DummyBackgroundServiceImpl implements DummyBackgroundService {
    public String hello() {
        try {
            log.debug("Sleeping for 5 seconds on ");
            Thread.sleep(5000);
        } catch (Exception e) {
        }
        
        return "hello";
    }
}

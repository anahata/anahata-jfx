package com.anahata.jfx.collections;

import com.anahata.jfx.collections.DeltaListChangeListener;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class DeltaListChangeListenerTest {
    @Test
    public void testDeltaChangeListener() {
        Person first = new Person(0L, "Joe", "Bloggs");
        Person second = new Person(1L, "John", "Doe");
        final ObservableList<Person> olist = FXCollections.observableArrayList(first, second);
        final SimpleListProperty<Person> lprop = new SimpleListProperty<>(olist);
        final TestListListener listener = new TestListListener();
        lprop.addListener(listener);
        
        // Add a third entry with a null id.
        
        Person third = new Person(null, "Mary", "Jane");
        olist.add(third);
        assertEquals(Collections.singletonList(third), listener.getAdded());
        assertTrue(listener.getRemoved().isEmpty());
        assertTrue(listener.getUpdated().isEmpty());
        
        // Replace the second entry.
        
        Person secondReplacement = new Person(1L, "John", "Rambo");
        olist.set(1, secondReplacement);
        assertTrue(listener.getAdded().isEmpty());
        assertTrue(listener.getRemoved().isEmpty());
        
        // Reset all three.
        
        olist.setAll(first, second, third);
        
        olist.clear();
        assertTrue(true);
    }
    
    @Getter
    private static class TestListListener extends DeltaListChangeListener<Person> {
        private List<Person> added;

        private List<Person> removed;

        private List<Person> updated;

        @Override
        public void onChanged(List<Person> added, List<Person> removed, List<Person> updated) {
            this.added = added;
            this.removed = removed;
            this.updated = updated;
            System.out.println("added: " + added + " removed: " + removed + " updated: " + updated);
        }
    }
    
    @Getter
    @Setter
    private static class Person {
        private Long id;
        
        private String firstName;
        
        private String lastName;

        public Person(Long id, String firstName, String lastName) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        @Override
        public int hashCode() {
            if (id == null) {
                return super.hashCode();
            }
            
            int hash = 7;
            hash = 97 * hash + Objects.hashCode(id);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            
            if (obj == null) {
                return false;
            }
            
            if (getClass() != obj.getClass()) {
                return false;
            }
            
            final Person other = (Person)obj;
            
            if (id == null || other.id == null) {
                return false;
            }
            
            return id.equals(other.id);
        }
        
        @Override
        public String toString() {
            return id + " " + firstName + " " + lastName;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.jfx.map;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.lodgon.openmapfx.core.*;
import org.lodgon.openmapfx.desktop.SimpleProviderPicker;
import org.lodgon.openmapfx.layer.LicenceLayer;

public class MapView extends Application {

    LayeredMap map;
    TileProvider[] tileProviders;
    SimpleProviderPicker spp;
    LicenceLayer licenceLayer;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        DefaultBaseMapProvider provider = new DefaultBaseMapProvider();
        //provider.getTileProviders().add(0, new WmsTileProvider("https://mapsengine.google.com/09372590152434720789-00913315481290556980-4/wms/?REQUEST=GetCapabilities&amp;VERSION=1.3.0", "Landgate Imagery"));

        spp = new SimpleProviderPicker(provider);
        
        map = new LayeredMap(provider);
        BorderPane cbp = new BorderPane();
        cbp.setCenter(map);
//        Rectangle clip = new Rectangle(700, 600);
//        cbp.setClip(clip);
//        clip.heightProperty().bind(cbp.heightProperty());
//        clip.widthProperty().bind(cbp.widthProperty());

        BorderPane bp = new BorderPane();
        bp.setTop(spp);
        bp.setCenter(map);

        Scene scene = new Scene(bp, 800, 650);
        stage.setScene(scene);
        stage.show();
        map.setZoom(8);
        map.setCenter(-32.0622557454633, 115.651883586101);
        showUBDPages();

        licenceLayer = new LicenceLayer(provider);
        map.getLayers().add(licenceLayer);
    }

    
    private void showUBDPages() {
//        for (Integer i : UBDConverter.getAllPages()) {
//            UBDPage p = UBDConverter.getPage(i);
//            double[] nw = p.getNorthWest();
//            double[] se = p.getSouthEast();
////            URL im = this.getClass().getResource("/org/lodgon/openmapfx/icons/mylocation.png");
////            Image image = new Image(im.toString());        
//            RectangleLayer positionLayer = new RectangleLayer("" + i);
//            positionLayer.setId("ubdpage-" + 1);
//            map.getLayers().add(positionLayer);
//            positionLayer.updatePosition(nw[0], nw[1], se[0], se[1]);
//        }
        
//        map.centerLatitudeProperty().addListener(new InvalidationListener() {
//            @Override
//            public void invalidated(Observable observable) {
//                
//            }
//        });
    }
}

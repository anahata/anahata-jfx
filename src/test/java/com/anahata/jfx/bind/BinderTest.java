package com.anahata.jfx.bind;

import com.anahata.util.cdi.Cdi;
import com.anahata.util.metamodel.MetaModel;
import com.anahata.util.metamodel.MetaModelProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.junit.Test;

/**
 * Test the binder.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class BinderTest {
    @Test
    public void testBinder() {
        BindTestController controller = Cdi.get(BindTestController.class);
        BindTestParent parent = new BindTestParent("Parent", "Item");
        controller.model.set(parent);
        
        BindTestChild child = new BindTestChild("Child", "Model", "Dolly");
        controller.model.set(child);
        
        BindTestGrandchild grandchild = new BindTestGrandchild("Grandchild", "Girl", "Makeup", "Pink");
        controller.model.set(grandchild);
        
        BindTestGrandchild grandchild2 = new BindTestGrandchild("Grandchild", "Boy", "Lego", "Blue");
        controller.model2.set(grandchild2);
    }
    
    //== controller ====================================================================================================
    
    @Dependent
    public static class BindTestController {
        @Inject
        private Binder binder;
        
        @Bind(property = BindTestParent_mm.firstName.class, readOnly = true)
        private final StringProperty firstName = new SimpleStringProperty();
        
        @Bind(property = BindTestParent_mm.lastName.class, readOnly = true)
        private final StringProperty lastName = new SimpleStringProperty();
        
        @Bind(property = BindTestChild_mm.favouriteToy.class, readOnly = true)
        private final StringProperty favouriteToy = new SimpleStringProperty();
        
        @Bind(property = BindTestInterface_mm.favouriteColour.class, readOnly = true)
        private final StringProperty favouriteColour = new SimpleStringProperty();
        
        @Bind(id = "secondModel", property = BindTestInterface_mm.favouriteColour.class, readOnly = true)
        private final StringProperty favouriteColour2 = new SimpleStringProperty();
        
        @BindModel
        private final ObjectProperty<BindTestParent> model = new SimpleObjectProperty<>();
        
        @BindModel(id = "secondModel")
        private final ObjectProperty<BindTestParent> model2 = new SimpleObjectProperty<>();
        
        @PostConstruct
        public void postConstruct() {
            binder.init(this);
        }
    }
    
    //== model ========================================================================================================
    
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    public static class BindTestParent {
        private String firstName;

        private String lastName;
    }

    @NoArgsConstructor
    @Getter
    @Setter
    public static class BindTestChild extends BindTestParent {
        private String favouriteToy;

        public BindTestChild(String firstName, String lastName, String favouriteToy) {
            super(firstName, lastName);
            this.favouriteToy = favouriteToy;
        }
    }
    
    @NoArgsConstructor
    @Getter
    @Setter
    public static class BindTestGrandchild extends BindTestChild implements BindTestInterface {
        private String favouriteColour;
        
        public BindTestGrandchild(String firstName, String lastName, String favouriteToy, String favouriteColour) {
            super(firstName, lastName, favouriteToy);
            this.favouriteColour = favouriteColour;
        }
    }
    
    public interface BindTestInterface {
        String getFavouriteColour();
        
        void setFavouriteColour(String favouriteColour);
    }
    
    //== metamodel ====================================================================================================
    
    public static class BindTestParent_mm extends MetaModel {
        public static final BindTestParent_mm INSTANCE = new BindTestParent_mm();
        
        public BindTestParent_mm() {
            super(BindTestParent_mm.class);
        }
        
        public static class firstName extends MetaModelProperty {
            public static final BindTestParent_mm.firstName INSTANCE = new BindTestParent_mm.firstName();
            
            public firstName() {
                super(BindTestParent.class, String.class, "firstName");
            }
        }
        
        public static class lastName extends MetaModelProperty {
            public static final BindTestParent_mm.lastName INSTANCE = new BindTestParent_mm.lastName();
            
            public lastName() {
                super(BindTestParent.class, String.class, "lastName");
            }
        }
    }
    
    public static class BindTestChild_mm extends MetaModel {
        public static final BindTestChild_mm INSTANCE = new BindTestChild_mm();
        
        public BindTestChild_mm() {
            super(BindTestChild_mm.class);
        }
        
        public static class favouriteToy extends MetaModelProperty {
            public static final BindTestChild_mm.favouriteToy INSTANCE = new BindTestChild_mm.favouriteToy();
            
            public favouriteToy() {
                super(BindTestChild.class, String.class, "favouriteToy");
            }
        }
    }
    
    public static class BindTestGrandchild_mm extends MetaModel {
        public static final BindTestGrandchild_mm INSTANCE = new BindTestGrandchild_mm();
        
        public BindTestGrandchild_mm() {
            super(BindTestGrandchild_mm.class);
        }
        
        public static class favouriteColour extends MetaModelProperty {
            public static final BindTestGrandchild_mm.favouriteColour INSTANCE = new BindTestGrandchild_mm.favouriteColour();
            
            public favouriteColour() {
                super(BindTestGrandchild.class, String.class, "favouriteColour");
            }
        }
    }
    
    public static class BindTestInterface_mm extends MetaModel {
        public static final BindTestInterface_mm INSTANCE = new BindTestInterface_mm();
        
        public BindTestInterface_mm() {
            super(BindTestInterface_mm.class);
        }
        
        public static class favouriteColour extends MetaModelProperty {
            public static final BindTestInterface_mm.favouriteColour INSTANCE = new BindTestInterface_mm.favouriteColour();
            
            public favouriteColour() {
                super(BindTestInterface.class, String.class, "favouriteColour");
            }
        }
    }
}

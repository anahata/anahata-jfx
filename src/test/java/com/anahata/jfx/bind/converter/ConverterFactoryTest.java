package com.anahata.jfx.bind.converter;

import com.anahata.jfx.bind.converter.string.BigDecimalConverter;
import com.anahata.jfx.bind.converter.string.IntegerConverter;
import com.anahata.jfx.bind.converter.string.LongConverter;
import com.anahata.jfx.bind.converter.string.NullEmptyStringConverter;
import java.math.BigDecimal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for ConverterFactory.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class ConverterFactoryTest {
    @Test
    public void testGetDefaultConverter() {
        assertEquals(NullEmptyStringConverter.INSTANCE, ConverterFactory.getDefaultConverter(String.class, String.class));
        assertEquals(LongConverter.INSTANCE, ConverterFactory.getDefaultConverter(Long.class, String.class));
        assertEquals(LongConverter.INSTANCE, ConverterFactory.getDefaultConverter(long.class, String.class));
        assertEquals(IntegerConverter.INSTANCE, ConverterFactory.getDefaultConverter(Integer.class, String.class));
        assertEquals(IntegerConverter.INSTANCE, ConverterFactory.getDefaultConverter(int.class, String.class));
        assertEquals(BigDecimalConverter.INSTANCE, ConverterFactory.getDefaultConverter(BigDecimal.class,
                String.class));
        assertNull(ConverterFactory.getDefaultConverter(BigDecimal.class, BigDecimal.class));
    }

    @Test
    public void testValidateConverter() {
        ConverterFactory.validateConverter(new TestConverter(), Long.class, String.class);
        
        try {
            ConverterFactory.validateConverter(new TestConverter(), Integer.class, String.class);
            fail("Didn't throw an exception for invalid classes");
        } catch (IllegalArgumentException e) {
            assertEquals("The domain model type doesn't match, converter is Long, arg is Integer", e.getMessage());
        }
        
        try {
            ConverterFactory.validateConverter(new TestConverter(), Long.class, Integer.class);
            fail("Didn't throw an exception for invalid classes");
        } catch (IllegalArgumentException e) {
            assertEquals("The node model type doesn't match, converter is String, arg is Integer", e.getMessage());
        }
    }

    private static class TestConverter implements Converter<Long, String> {
        @Override
        public Long getAsDomainModelValue(Object node, String nodeModelValue) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public String getAsNodeModelValue(Object node, Long domainModelValue) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public String format(String value) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}

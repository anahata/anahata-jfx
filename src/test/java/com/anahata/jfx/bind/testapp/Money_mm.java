
package com.anahata.jfx.bind.testapp;

import java.math.BigDecimal;
import java.util.Currency;
import com.anahata.util.metamodel.MetaModel;
import com.anahata.util.metamodel.MetaModelProperty;

public class Money_mm
    extends MetaModel
{

    public final static Money_mm INSTANCE = new Money_mm();

    public Money_mm() {
        super(TestMoney.class);
    }

    public static class amount
        extends MetaModelProperty
    {

        public final static Money_mm.amount INSTANCE = new Money_mm.amount();

        public amount() {
            super(TestMoney.class, BigDecimal.class, "amount");
        }

    }

    public static class currency
        extends MetaModelProperty
    {

        public final static Money_mm.currency INSTANCE = new Money_mm.currency();

        public currency() {
            super(TestMoney.class, Currency.class, "currency");
        }

    }

    public static class displayValue
        extends MetaModelProperty
    {

        public final static Money_mm.displayValue INSTANCE = new Money_mm.displayValue();

        public displayValue() {
            super(TestMoney.class, String.class, "displayValue");
        }

    }

    public static class zero
        extends MetaModelProperty
    {

        public final static Money_mm.zero INSTANCE = new Money_mm.zero();

        public zero() {
            super(TestMoney.class, Boolean.class, "zero");
        }

    }

}

package com.anahata.jfx.bind.testapp;

import com.anahata.util.metamodel.MetaModel;
import com.anahata.util.metamodel.MetaModelProperty;
import java.util.List;

public class People_mm extends MetaModel {

    public static final People_mm INSTANCE = new People_mm();

    public People_mm() {
        super(People.class);
    }

    public static class people extends MetaModelProperty {

        public static final People_mm.people INSTANCE = new People_mm.people();

        public people() {
            super(People.class, List.class, "people");
        }
    }

    public static class total
            extends MetaModelProperty {

        public final static People_mm.total INSTANCE = new People_mm.total();

        public total() {
            super(People.class, TestMoney.class, "total");
        }

    }

    public static class first
            extends MetaModelProperty {

        public final static People_mm.first INSTANCE = new People_mm.first();

        public first() {
            super(People.class, TestMoney.class, "first");
        }

    }

    public static class second
            extends MetaModelProperty {

        public final static People_mm.second INSTANCE = new People_mm.second();

        public second() {
            super(People.class, TestMoney.class, "second");
        }

    }
}

package com.anahata.jfx.bind.testapp;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Person {
    @NotNull
    @Size(min = 3)
    private String firstName;

    @NotNull
    @Size(min = 3)
    private String lastName;

    @NotNull
    private String phoneNumber;
    
    @NotNull
    @Max(value = 90)
    private Integer age;
    
    @Valid
    private List<Pet> pets;
    
    private TestMoney amount = TestMoney.zero();
    
    private Date dateOfBirth;
    
    public TestMoney getPercent() {
        if(amount == null) {
            return TestMoney.zero();
        }
        return amount.add(BigDecimal.valueOf(15.0));
    }
}

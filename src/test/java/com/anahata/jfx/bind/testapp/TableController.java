package com.anahata.jfx.bind.testapp;

import com.anahata.jfx.bind.Bind;
import com.anahata.jfx.bind.BindModel;
import com.anahata.jfx.bind.BindView;
import com.anahata.jfx.bind.Binder;
import com.anahata.jfx.bind.converter.string.DisplayableConverter;
import com.anahata.jfx.bind.table.BindTable;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import lombok.Getter;

/**
 * Tests of table binding.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Dependent
public class TableController {

    @Inject
    private Binder binder;

    @BindView
    @Getter
    private VBox vbox;

    @BindTable(property = People_mm.people.class, tableRow = PersonTableRow.class)
    private TableView<Person> personTable;

    @BindTable(id = "something", property = Person_mm.pets.class, tableRow = PetTableRow.class)
    private TableView<Pet> petTable;

    @Bind(property = People_mm.total.class, converter = DisplayableConverter.class)
    private final Label total = new Label();
    
    @Bind(property = People_mm.first.class)
    private final TestMoneyField firstmf = new TestMoneyField();
    
    @Bind(property = People_mm.second.class)
    private final TestMoneyField secondmf = new TestMoneyField();

    private TableColumn<Person, String> firstName;

    private TableColumn<Person, String> lastName;

    private TableColumn<Person, TestMoney> amount;

    private TableColumn<Person, TestMoney> percent;

    private TableColumn<Person, String> phone;

    private TableColumn<Pet, String> name;

    @BindModel
    private final ObjectProperty<People> people = new SimpleObjectProperty<>(new People());

    @BindModel(id = "something", autoBind = true)
    private final ObjectProperty<Person> person = new SimpleObjectProperty<>();

    @PostConstruct
    public void postConstruct() {
        personTable = new TableView<>();
        personTable.setPrefHeight(100);
        personTable.setMaxHeight(100);
        firstName = column("firstName", "First Name", personTable);
        lastName = column("lastName", "Last Name", personTable);
        amount = column("amount", "Amount", personTable);
        percent = column("percent", "Percent", personTable);
        phone = column("phone", "Phone", personTable);
        column("secondLastName", "2nd Last Name", personTable);

        petTable = new TableView<>();
        name = column("name", "Name", petTable);

        Button validate = new Button("validate");
        validate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                binder.bindFromModel();
                binder.bindFromModel(total);
                binder.validate(true);
            }
        });

        total.setId("total label");
        vbox = new VBox(
                8,
                personTable,
                new HBox(firstmf, secondmf),
                new HBox(new Label("Total amount: "), total),
                petTable,
                validate
        );

        List<Pet> pets1 = new ArrayList();
        pets1.add(new Pet("pet1"));
        pets1.add(new Pet("pet2"));
        List<Pet> pets2 = new ArrayList();
        pets2.add(new Pet("pet3"));
        people.get().getPeople().add(new Person("Joe", "Bloggs", "08 9111 2222", 45, pets1, TestMoney.zero(), null));
        people.get().getPeople().add(new Person("Jane", "Doe", "08 9333 4444", 55, pets2, TestMoney.zero(), null));
        people.get().getPeople().add(new Person("Jane1", "Doe", "08 9333 4444", 55, pets2, TestMoney.zero(), null));
        people.get().getPeople().add(new Person("Jane2", null, "08 9333 4444", 55, pets2, TestMoney.zero(), null));
        people.get().getPeople().add(new Person(null, null, "08 9333 4444", 55, pets2, TestMoney.zero(), null));

        binder.init(this);
        person.bind(personTable.getSelectionModel().selectedItemProperty());
//        person.addListener(new ChangeListener<Person>() {
//            @Override
//            public void changed(
//                    ObservableValue<? extends Person> observable, Person oldValue, Person newValue) {
//                binder.bindFromModel(petTable);
//            }
//        });

        binder.bindFromModel();
    }

    private static TableColumn column(String id, String heading, TableView tv) {
        TableColumn column = new TableColumn<>(heading);
        column.setId(id);
        column.setMinWidth(150);
        tv.getColumns().add(column);
        return column;
    }
}

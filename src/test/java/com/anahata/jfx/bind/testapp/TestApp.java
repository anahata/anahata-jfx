package com.anahata.jfx.bind.testapp;

import com.anahata.jfx.JfxUtils;
import com.anahata.jfx.test.TestProperties;
import com.anahata.util.cdi.Cdi;
import com.anahata.util.formatting.Displayable;
import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import static com.anahata.jfx.test.TestJfxUtils.*;

/**
 * A test application for JavaFX binding.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class TestApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        TestProperties.getProps().setProperty("anahatajfx.message.growl.fadein.millis", "100");
        TestProperties.getProps().setProperty("anahatajfx.message.growl.fadeout.millis", "100");
        TestProperties.getProps().setProperty("anahatajfx.message.growl.pause.millis", "100");
        TestProperties.getProps().setProperty("anahatajfx.icon.delete", "delete.png");
        TestProperties.getProps().setProperty("anahatajfx.css.readonly", "readonly.css");

        stage.setWidth(1000);
        stage.setHeight(800);

        TabPane tabPane = new TabPane();
        tabPane.setPrefWidth(Double.MAX_VALUE);
        tab(tabPane, "Disabled Processing", createDisabledTab());
        tab(tabPane, "Validation And Bindings", createValidationTab());
        tab(tabPane, "Table Binding", Cdi.get(TableController.class).getVbox());
        Tab autoComboTab = tab(tabPane, "AutoCompleteComboBox", createComboTab());
        tabPane.getSelectionModel().select(autoComboTab);

        Scene scene = new Scene(tabPane);
        String externalForm = TestApp.class.getResource("/css/TestApp.css").toExternalForm();
        scene.getStylesheets().add(externalForm);
        stage.setScene(scene);
        stage.show();
    }

    private VBox createDisabledTab() {
        VBox vbox = new VBox(8);
        vbox.setPadding(new Insets(8, 8, 8, 8));

        // Actions
        HBox actions = new HBox(8);

        ToggleButton enabled = new ToggleButton("Enabled");
        enabled.setSelected(true);
        actions.getChildren().add(enabled);

        ToggleButton readOnlyCss = new ToggleButton("Read Only CSS");
        actions.getChildren().add(readOnlyCss);

        ToggleButton readOnly = new ToggleButton("Read Only");
        actions.getChildren().add(readOnly);

        ToggleButton disableText = new ToggleButton("Disable Text");
        actions.getChildren().add(disableText);

        vbox.getChildren().add(actions);

        // Controls
        HBox controls = new HBox(8);

        final Button button = new Button("Button");
        controls.getChildren().add(button);

        final Hyperlink hyperlink = new Hyperlink("Test Link");
        controls.getChildren().add(hyperlink);

        final Label label = new Label("Label:");
        controls.getChildren().add(label);

        final TextField textField = new TextField("Some text");
        controls.getChildren().add(textField);

        final ComboBox<String> comboBox = new ComboBox<>();
        comboBox.getItems().add("First");
        comboBox.getItems().add("Second");
        comboBox.setValue("First");
        controls.getChildren().add(comboBox);

        final ChoiceBox<String> choiceBox = new ChoiceBox<>();
        choiceBox.getItems().add("First");
        choiceBox.getItems().add("Second");
        choiceBox.setValue("First");
        controls.getChildren().add(choiceBox);

        vbox.getChildren().add(controls);

        // Other
        controls.disableProperty().bind(enabled.selectedProperty().not());
        JfxUtils.bindStylesheet(vbox, readOnlyCss.selectedProperty(), "/css/readonly.css");
        textField.disableProperty().bind(disableText.selectedProperty());

        readOnly.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
//                if (t1) {
//                    button.setDisable(true);
//                    textField.setDisable(true);
//                    comboBox.setDisable(true);
//                } else {
//                    button.setDisable(false);
//                    textField.setDisable(false);
//                    comboBox.setDisable(false);
//                }
            }
        });

        return vbox;
    }

    private Node createValidationTab() {
        PersonController controller = Cdi.get(PersonController.class);
        return controller.getDp();
    }

    private Pane createComboTab() {
        String[] items = new String[]{
            "1",
            null,           
            "combo combo combo combo combo combo combo combo combo combo combo combo "
            + "combo combo combo combo combo combo combo combo",
            "This is test for long long long text inside our AutoCompleteComboBox "
            + "custom control made in JavaFX 2.2. This is test for long long long "
            + "text inside our AutoCompleteComboBox custom control made in JavaFX 2.2",
            "No timber pest activity was detected at the time of Inspection. Please refer to the Report Summary for pest treatment recommendations."
        };

        Model[] modelItems = new Model[]{
            new Model("test title"),
            new Model("title2"),
            new Model("tiabc"),
            new Model("auto"),
            new Model("combo"),
            new Model("auto combo"),
            new Model("combo auto")
        };
        //
        InvalidationListener valueListener = new InvalidationListener() {

            @Override
            public void invalidated(Observable o) {
                log.debug("observable= " + o);
            }
        };
        //
        
        /*
        final AutoCompleteComboBox<String> stringCombo = new AutoCompleteComboBox<>();
        stringCombo.setItems(FXCollections.<String>observableArrayList(items));
        stringCombo.setVisibleRowCount(10);
        stringCombo.valueProperty().addListener(valueListener);
        stringCombo.setPrefListViewWidth(250);
        stringCombo.setPrefWidth(200);
        stringCombo.setPrefHeight(90);
        AutoCompleteComboBox.withTextAreaEditor(stringCombo);

        stringCombo.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {

            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<String>() {
                    {
                        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    }

                    private final Text textNode = new Text();

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            
                            ListView lv = getListView();
                            log.debug("lv pw= {}", lv.prefWidth(-1));
                            log.debug("lv left={}, right={}", lv.getPadding().getLeft(), lv.getPadding().getRight());
                            double cellWidth = lv.prefWidth(-1) - (lv.getPadding().getLeft() + lv.getPadding().getRight());
                            Node node = lv.lookup(".scroll-bar");                            
                            if(node != null) {
                                ScrollBar sb = (ScrollBar)node;
                                // reduce cell width by 2 sb widths so text does not hide behind scrollbar
                                cellWidth = cellWidth - 2 * sb.prefWidth(-1);                                
                            }
                            updateWidth(cellWidth);
                            
                            textNode.setWrappingWidth(cellWidth);

                            textNode.setText(item);
                            setText(item);
                            setGraphic(textNode);

                            double cellHeight = textNode.prefHeight(-1);
                            updateHeight(cellHeight);
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }

                    private void updateWidth(double cellWidth) {
                        setMinWidth(cellWidth);
                        setPrefWidth(cellWidth);
                        setMaxWidth(cellWidth);
                    }
                  
                    private void updateHeight(double cellHeight) {
                        setMinHeight(cellHeight);
                        setPrefHeight(cellHeight);
                        setMaxHeight(cellHeight);
                    }

                };
            }
        })
        */ ;
        
//        TextArea textArea = new TextArea();
//        textArea.setWrapText(true);
//        stringCombo.setEditor(textArea);
        //
//        final AutoCompleteComboBox<Integer> intCombo = new AutoCompleteComboBox<>();
//        intCombo.setItems(FXCollections.observableArrayList(
//                1,
//                10,
//                111,
//                112,
//                1112,
//                1111,
//                1113,
//                2,
//                22,
//                29,
//                3,
//                30,
//                300,
//                3_000,
//                30_000
//        ));
//        intCombo.valueProperty().addListener(valueListener);
//        intCombo.setPromptText("type a number");
//        intCombo.setValue(10);
        
//        intCombo.focusedProperty().addListener(new InvalidationListener() {
//
//            @Override
//            public void invalidated(Observable o) {
//                log.debug("focus invalidated= {}, value= {}", intCombo, intCombo.isFocused());                
//            }
//        });
//        intCombo.focusedProperty().addListener(new ChangeListener<Boolean>() {
//
//            @Override
//            public void changed(
//                    ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
//                log.debug("focus changed oldValue= {}, newValue= {}", oldValue, newValue);
//            }
//        });
        //
        
//        AutoCompleteComboBox<Model> modelCombo = new AutoCompleteComboBox<>();
//        modelCombo.setItems(FXCollections.<Model>observableArrayList(modelItems));
//        modelCombo.setCellFactory(ModelCellFactory.INSTANCE);
//        modelCombo.valueProperty().addListener(valueListener);
        
        //
        
//        AutoCompleteComboBox<Model> customFilterCombo = new AutoCompleteComboBox<>();
//        customFilterCombo.setItems(FXCollections.<Model>observableArrayList(modelItems));
//        customFilterCombo.setCellFactory(ModelCellFactory.INSTANCE);
        
//        customFilterCombo.setDataFilter(new DataFilter<Model>() {
////
//            @Override
//            public boolean matches(Model item, String text) {
//                return StringUtils.endsWithIgnoreCase(item.title, text);
//            }
//        });
        //
        ComboBox<String> normalCombo = new ComboBox<>(FXCollections.observableArrayList(items));
        normalCombo.setEditable(true);
        normalCombo.setValue(items[1]);
        normalCombo.setStyle("-fx-border-color: green;");
        normalCombo.setPrefWidth(100);
        //

        //
        VBox vb = new VBox(10);
        vb.setAlignment(Pos.CENTER);
        vb.getChildren().addAll(
                //                createLabeledCombo("String AutoCombo", stringCombo),
                //createLabeledCombo("Integer AutoCombo", intCombo)
        //                createLabeledCombo("Displayable Model AutoCombo", modelCombo),
        //                createLabeledCombo("Custom DataFilter-endsWith", customFilterCombo),
        //                createLabeledCombo("Normal JFX Combo", normalCombo)
        );

        //HBox box = createLabeledCombo("Test TextArea", stringCombo);
        //vb.getChildren().add(box);
        //
        StackPane sp = new StackPane();
        sp.setAlignment(Pos.CENTER);
        sp.getChildren().addAll(vb);
        return sp;
    }

    private HBox createLabeledCombo(String title, ComboBoxBase<?> combo) {
        HBox hb = new HBox(10);
        combo.setStyle("-fx-border-color: deepskyblue;");
        hb.setAlignment(Pos.CENTER);
        Label label = new Label(title);
        hb.getChildren().addAll(label, combo);
        return hb;
    }

    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    public static class ModelCellFactory implements Callback<ListView<Model>, ListCell<Model>> {

        public static ModelCellFactory INSTANCE = new ModelCellFactory();

        @Override
        public ListCell<Model> call(ListView<Model> p) {
            return new ListCell<Model>() {

                @Override
                protected void updateItem(Model item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null && !empty) {
                        setText(item.getTitle());
                    }
                }
            };
        }
    }

    @Data
    @AllArgsConstructor
    public static class Model implements Displayable {

        private String title;

        @Override
        public String getDisplayValue() {
            return title;
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}

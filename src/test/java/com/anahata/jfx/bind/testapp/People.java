package com.anahata.jfx.bind.testapp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
@Getter
@Setter
public class People {

    private final List<Person> people = new ArrayList<>();

    public TestMoney getTotal() {
        TestMoney totalMoney = new TestMoney(BigDecimal.ZERO);
        for (Person p : people) {
            
            if (p != null) {
                log.debug("p= {}, amount= {}", p, p.getAmount());
                totalMoney = totalMoney.add(p.getAmount());
            }
        }
        totalMoney = totalMoney.add(first).add(getSecond());
        log.debug("returning total= {}", totalMoney.getAmount());
        return totalMoney;
    }
    
    private TestMoney first = TestMoney.zero();
    
    public TestMoney getSecond() {
        if(first == null) {
            return TestMoney.zero();
        }
        return first.add(BigDecimal.valueOf(50.0));
    }
}

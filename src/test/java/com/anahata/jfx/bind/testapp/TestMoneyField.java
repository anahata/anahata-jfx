package com.anahata.jfx.bind.testapp;

import com.anahata.jfx.bind.*;
import com.anahata.jfx.bind.filter.PositiveCurrencyKeystrokeFilter;
import com.anahata.jfx.bind.nodemodel.NodeModel;
import com.anahata.jfx.currency.CurrencyListViewCellFactory;
import com.anahata.util.cdi.Cdi;
import java.math.BigDecimal;
import java.util.*;
import javafx.beans.property.*;
import javafx.beans.value.ObservableBooleanValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * A text entry field that allows editing a monetary value with a currency.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class TestMoneyField extends HBox implements BindForm, NodeModel<TestMoneyField, ObjectProperty> {
    //== attributes ===================================================================================================

    @Bind(property = Money_mm.currency.class)
    @Getter
    private final ComboBox<Currency> currency;

    @Bind(property = Money_mm.amount.class, keystrokeFilter = PositiveCurrencyKeystrokeFilter.class)
    @Getter
    private final TextField amount;

    @Getter
    private final Binder binder;

    //<editor-fold defaultstate="collapsed" desc="@FXProperty">
    public boolean isControlsFocused() {
        return controlsFocused.getValue();
    }

    public ObservableBooleanValue controlsFocusedProperty() {
        return controlsFocused;
    }
    //</editor-fold>

    private final ObservableBooleanValue controlsFocused;

    //<editor-fold defaultstate="collapsed" desc="@FXProperty">
    public TestMoney getValue() {
        return value.getValue();
    }

    public void setValue(TestMoney value) {
        this.value.setValue(value);
    }

    public ObjectProperty<TestMoney> valueProperty() {
        return value;
    }
    //</editor-fold>

    @BindModel
    private final ObjectProperty<TestMoney> value = new SimpleObjectProperty<>(new TestMoney(BigDecimal.ZERO));

    //== init =========================================================================================================
    public TestMoneyField() {
        super();

        amount = new TextField();
        amount.setPrefWidth(70);
        amount.setMinWidth(Control.USE_PREF_SIZE);
        amount.setMaxWidth(Control.USE_PREF_SIZE);
        currency = new ComboBox<>();
//        CurrencyUtils utils = Cdi.get(CurrencyUtils.class);
        prepareCombo(currency);

        controlsFocused = amount.focusedProperty().or(currency.focusedProperty());

        currency.managedProperty().bind(currency.visibleProperty());
        setSpacing(4);
        getChildren().addAll(amount, currency);

        binder = Cdi.get(Binder.class);
        binder.init(this);
        amount.setAlignment(Pos.BASELINE_RIGHT);
        setAlignment(Pos.CENTER_LEFT);

        Map<Node, Binding> bmap = getAllNodeBindings();
        Set<Node> keys = bmap.keySet();
        for(Node n : keys) {
            log.debug("key= {}, value= {}", n, bmap.get(n));
        }
    }

    public void prepareCombo(ComboBox combo) {
        combo.setPrefWidth(80);
        combo.setMinWidth(Control.USE_PREF_SIZE);
        combo.setMaxWidth(Control.USE_PREF_SIZE);
        combo.setButtonCell(CurrencyListViewCellFactory.MEDIUM_INSTANCE.call(null));
        combo.setCellFactory(CurrencyListViewCellFactory.LONG_INSTANCE);
//        if (!activeCurrencies.isUnsatisfied()) {
        combo.setItems(FXCollections.observableArrayList(SORTED_CURRENCIES));
//        }

    }

    /**
     * Compares currencies alphabetically,
     */
    private static class CurrencyComparator implements Comparator<Currency> {

        @Override
        public int compare(Currency o1, Currency o2) {
            return o1.getCurrencyCode().compareTo(o2.getCurrencyCode());
        }
    }

    public static final List<Currency> SORTED_CURRENCIES;

    static {
        List<Currency> all = new ArrayList(Currency.getAvailableCurrencies());
        Collections.sort(all, new CurrencyComparator());
        Currency aud = Currency.getInstance("AUD");
        Currency nzd = Currency.getInstance("NZD");
        Currency eur = Currency.getInstance("EUR");
        Currency chf = Currency.getInstance("CHF");
        List<Currency> top = new ArrayList();
        top.add(aud);
        top.add(nzd);
        top.add(eur);
        top.add(chf);
        all.removeAll(top);
        all.addAll(0, top);
        SORTED_CURRENCIES = Collections.unmodifiableList(all);
    }

    //== public methods ===============================================================================================

    public boolean isCurrencyVisible() {
        return currency.isVisible();
    }

    public void setCurrencyVisible(boolean visible) {
        currency.setVisible(visible);
    }

    public BooleanProperty currencyVisibleProperty() {
        return currency.visibleProperty();
    }

    public StringProperty amountTextProperty() {
        return amount.textProperty();
    }

    public ObjectProperty<Currency> currencyValueProperty() {
        return currency.valueProperty();
    }

    //== NodeModel ====================================================================================================
    @Override
    public ObjectProperty getNodeModelValueProperty(TestMoneyField node) {
        return node.valueProperty();
    }

    //== BindForm =====================================================================================================
    @Override
    public Binder getRootBinder() {
        return binder.getRootBinder();
    }

    @Override
    public void setRootBinder(Binder binder) {
        this.binder.setRootBinder(binder);
    }

    @Override
    public BindForm getParentBindForm() {
        return binder.getParentBindForm();
    }

    @Override
    public void setParentBindForm(BindForm parentBindForm) {
        binder.setParentBindForm(parentBindForm);
    }

    @Override
    public Binding getParentBinding() {
        return binder.getParentBinding();
    }

    @Override
    public void setParentBinding(Binding parentBinding) {
        binder.setParentBinding(parentBinding);
    }

    @Override
    public Map<Node, Binding> getAllNodeBindings() {
        return binder.getAllNodeBindings();
    }

    @Override
    public View getView(Binding binding) {
        return binder.getView(binding);
    }

    @Override
    public void setView(View view, Binding binding) {
        binder.setView(view, binding);
    }

    @Override
    public void resetFormModified() {
        binder.resetFormModified();
    }

    @Override
    public BooleanProperty formValidProperty() {
        return binder.formValidProperty();
    }

    @Override
    public BooleanProperty formModifiedProperty() {
        return binder.formModifiedProperty();
    }

    @Override
    public void validate(boolean publishError, Object... excludeNodes) {
        binder.validate(publishError, excludeNodes);
    }

    @Override
    public void bindFromModel() {
        log.debug("calling bind from model");
        binder.bindFromModel();
    }

    @Override
    public void bindFromModelExcludingNode(Object node) {
        binder.bindFromModelExcludingNode(node);
    }

    @Override
    public BooleanProperty showContainerErrors() {
        return binder.showContainerErrors();
    }

    @Override
    public void setExcludeNodes(Object... nodes) {
    }

    //== Validations ==================================================================================================
    @Override
    public void addValidationGroup(Class<?> validationGroup) {
        binder.addValidationGroup(validationGroup);
    }

    @Override
    public void removeValidationGroup(Class<?> validationGroup) {
        binder.removeValidationGroup(validationGroup);
    }

    @Override
    public BooleanProperty getValidationActive(Class<?> validationGroup) {
        return binder.getValidationActive(validationGroup);
    }

    @Override
    public ReadOnlyBooleanProperty getFormValidProperty(Class<?> validationGroup) {
        return binder.getFormValidProperty(validationGroup);
    }

    @Override
    public void setValid(String propertyName, Class<?> validationGroup) {
        binder.setValid(propertyName, validationGroup);
    }

    @Override
    public void setInvalid(String propertyName, Class<?> validationGroup) {
        binder.setInvalid(propertyName, validationGroup);
    }

    @Override
    public Set<Class<?>> getValidations() {
        return binder.getValidations();
    }

    @Override
    public void setValidationRequired() {
        binder.setValidationRequired();
    }

    
    

}

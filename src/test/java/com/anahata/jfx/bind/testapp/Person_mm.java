package com.anahata.jfx.bind.testapp;

import com.anahata.util.metamodel.MetaModel;
import com.anahata.util.metamodel.MetaModelProperty;
import java.util.Date;
import java.util.List;

public class Person_mm extends MetaModel {

    public static final Person_mm INSTANCE = new Person_mm();

    public Person_mm() {
        super(Person.class);
    }

    public static class firstName extends MetaModelProperty {

        public static final Person_mm.firstName INSTANCE = new Person_mm.firstName();

        public firstName() {
            super(Person.class, String.class, "firstName");
        }
    }

    public static class lastName extends MetaModelProperty {

        public static final Person_mm.lastName INSTANCE = new Person_mm.lastName();

        public lastName() {
            super(Person.class, String.class, "lastName");
        }
    }

    public static class phoneNumber extends MetaModelProperty {

        public static final Person_mm.phoneNumber INSTANCE = new Person_mm.phoneNumber();

        public phoneNumber() {
            super(Person.class, String.class, "phoneNumber");
        }
    }

    public static class age extends MetaModelProperty {

        public static final Person_mm.age INSTANCE = new Person_mm.age();

        public age() {
            super(Person.class, Integer.class, "age");
        }
    }

    public static class pets extends MetaModelProperty {

        public static final Person_mm.pets INSTANCE = new Person_mm.pets();

        public pets() {
            super(Person.class, List.class, "pets");
        }
    }

    public static class amount extends MetaModelProperty {

        public final static Person_mm.amount INSTANCE = new Person_mm.amount();

        public amount() {
            super(Person.class, TestMoney.class, "amount");
        }

    }
    
    public static class dateOfBirth extends MetaModelProperty {

        public final static Person_mm.amount INSTANCE = new Person_mm.amount();

        public dateOfBirth() {
            super(Person.class, Date.class, "dateOfBirth");
        }

    }
    
     public static class percent extends MetaModelProperty {

        public final static Person_mm.percent INSTANCE = new Person_mm.percent();

        public percent() {
            super(Person.class, TestMoney.class, "percent");
        }

    }
}

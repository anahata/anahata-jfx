package com.anahata.jfx.bind.testapp;

import com.anahata.jfx.bind.Bind;
import com.anahata.jfx.bind.table.BindTableColumn;
import com.anahata.jfx.bind.table.BindingTableRow;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public class PersonTableRow extends BindingTableRow<Person> {

    @BindTableColumn(id = "firstName")
    @Bind(property = Person_mm.firstName.class)
    private final Label firstName = new Label();

    @BindTableColumn(id = "lastName")
    @Bind(property = Person_mm.lastName.class)
    private final StringProperty lastName = new SimpleStringProperty();

    @BindTableColumn(id = "amount")
    @Bind(property = Person_mm.amount.class)
    private final TestMoneyField money = new TestMoneyField();

    @BindTableColumn(id = "percent")
    @Bind(property = Person_mm.percent.class)
    private final TestMoneyField percent = new TestMoneyField();

    @BindTableColumn(id = "phone")
    @Bind(property = Person_mm.phoneNumber.class)
    private final TextField phone = new TextField();
   

}

package com.anahata.jfx.bind.testapp;

import com.anahata.util.metamodel.MetaModel;
import com.anahata.util.metamodel.MetaModelProperty;

public class Pet_mm extends MetaModel {
    public static final Pet_mm INSTANCE = new Pet_mm();

    public Pet_mm() {
        super(Pet.class);
    }

    public static class name extends MetaModelProperty {
        public static final Pet_mm.name INSTANCE = new Pet_mm.name();

        public name() {
            super(Pet.class, String.class, "name");
        }
    }
    
}

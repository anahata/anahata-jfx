package com.anahata.jfx.bind.testapp;

import com.anahata.jfx.bind.*;
import com.anahata.jfx.bind.converter.DateCalendarConverter;
//import com.anahata.jfx.scene.control.AutoCompleteComboBox;
import com.anahata.jfx.scene.control.AutoCompleteTextField;
import com.anahata.jfx.scene.control.CalendarTextField;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import lombok.Getter;
import static com.anahata.jfx.test.TestJfxUtils.*;

@Getter
public class PersonController {

    String[] items = new String[]{
        "1",
        null,
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "auto",
        "test",
        "tea",
        "teb",
        "tec",
        "ted",
        "tesa",
        "combo",
        "combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo",
        "combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo combo"
    };

    @Inject
    private Binder binder;
    
    @BindView
    private StackPane dp;

    private VBox vbox;

    private TabPane tabPane;

    //== first tab ==============================================
    private Tab firstTab;

    @BindSubView
    private VBox firstVbox;

    private GridPane firstGridPane;

    private Label firstNameLabel;

    @Bind(property = Person_mm.firstName.class)
    private TextField firstName;
    
    @Bind(property = Person_mm.dateOfBirth.class, converter = DateCalendarConverter.class)
    private CalendarTextField dateOfBirth;

    private Label lastNameLabel;

    @Bind(property = Person_mm.lastName.class)
    private AutoCompleteTextField<String> lastName;

    @Bind(property = Person_mm.age.class)
    private AutoCompleteTextField<Integer> age;

    //== second tab =============================================
    private Tab secondTab;

    @BindSubView
    private VBox secondVbox;

    private GridPane secondGridPane;

    private Label phoneNumberLabel;

    @Bind(property = Person_mm.phoneNumber.class)
    private TextField phoneNumber;

    //== other ==================================================
    private Button validate;
    
    private Button decorate;

    @BindModel
    private final ObjectProperty<Person> model = new SimpleObjectProperty<>(new Person());

    @PostConstruct
    public void postConstruct() {
        tabPane = new TabPane();

        firstVbox = new VBox(8);
        firstVbox.setPadding(new Insets(8, 8, 8, 100));

        firstGridPane = new GridPane();
        firstGridPane.setHgap(8);
        firstGridPane.setVgap(8);
        firstGridPane.setPadding(new Insets(0, 0, 0, 50));
        ColumnConstraints col1 = new ColumnConstraints(100);
        ColumnConstraints col2 = new ColumnConstraints(200, 200, Double.MAX_VALUE);
        firstGridPane.getColumnConstraints().addAll(col1, col2);

        firstNameLabel = new Label("First Name");
        firstGridPane.add(firstNameLabel, 0, 0);

        firstName = new TextField();
        firstGridPane.add(firstName, 1, 0);

        lastNameLabel = new Label("Last Name");
        firstGridPane.add(lastNameLabel, 0, 1);
        
        //lastName = AutoCompleteComboBox.withTextArea();
        lastName.setItems(FXCollections.observableArrayList(items));
        lastName.setPrefHeight(90);
        firstGridPane.add(lastName, 1, 1);

        firstGridPane.add(new Label("Age"), 0, 2);

        //age = new AutoCompleteComboBox<>(FXCollections.observableArrayList(10, 11, 12, 22, 25, 27, 50, 51, 511));
        firstGridPane.add(age, 1, 2);
        
        dateOfBirth = new CalendarTextField();
        firstGridPane.add(dateOfBirth, 1, 3);

        Button reset = new Button("Reset Age to null");
        firstGridPane.add(reset, 0, 4);

        reset.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                model.get().setAge(null);
                binder.bindFromModel();
            }
        });

        Button setTo22 = new Button("Set Age to 22");
        firstGridPane.add(setTo22, 1, 4);

        setTo22.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                model.get().setAge(22);
                binder.bindFromModel();
            }
        });
        
        Button newModel = new Button("New Model");
        firstGridPane.add(newModel, 0, 5);

        newModel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                model.set(new Person());
                //binder.bindFromModel();
            }
        });

        firstVbox.getChildren().add(firstGridPane);

        firstTab = tab(tabPane, "Name", firstVbox);

        secondVbox = new VBox(8);
        secondVbox.setPadding(new Insets(8, 8, 8, 8));

        secondGridPane = new GridPane();
        secondGridPane.setHgap(8);
        secondGridPane.setVgap(8);
        secondGridPane.getColumnConstraints().addAll(col1, col2);

        phoneNumberLabel = new Label("Phone Number");
        secondGridPane.add(phoneNumberLabel, 0, 0);

        phoneNumber = new TextField();
        secondGridPane.add(phoneNumber, 1, 0);

        secondVbox.getChildren().add(secondGridPane);
        secondTab = tab(tabPane, "Phone", secondVbox);

        vbox = new VBox(8);

        validate = new Button("Validate");

        validate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                binder.validate(true);
            }
        });
        decorate = new Button("Decorate");
        decorate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                //MutableDecorator decorator = new MutableDecorator(new Label("decorator"), Pos.TOP_RIGHT, new Point2D(100, 0));                
                //DecorationUtils.install(firstName, decorator);
            }
        });        

        vbox.getChildren().add(tabPane);
        vbox.getChildren().add(validate);
        vbox.getChildren().add(decorate);
        
        dp = new StackPane(vbox);
        
        binder.init(this);
    }
}

package com.anahata.jfx.bind.testapp;

import com.anahata.jfx.bind.Bind;
import com.anahata.jfx.bind.table.BindTableColumn;
import com.anahata.jfx.bind.table.BindingTableRow;
import javafx.scene.control.TextField;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class PetTableRow extends BindingTableRow<Person> {
    @BindTableColumn(id = "name")
    @Bind(property = Pet_mm.name.class)
    private final TextField name = new TextField();
    
    
}

package com.anahata.jfx.bind.testapp;

import com.anahata.util.financial.CurrencyUtils;
import com.anahata.util.formatting.Displayable;
import com.anahata.util.jpa.eclipselink.converter.CurrencyConverter;
import com.anahata.util.lang.Nvl;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Currency;
import java.util.Locale;
import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.Validate;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

/**
 * Represents money in a specific currency. This class is immutable. Operations to manipulate money must be in the same
 * currency, if not an IllegalArgumentException is thrown.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Embeddable
@Access(AccessType.FIELD)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode(of = {"amount", "currency"})
@Slf4j
public class TestMoney implements Serializable, Comparable<TestMoney>, Displayable {

    @Setter
    private static Currency defaultCurrency = Currency.getInstance(Locale.getDefault());

    @Setter
    private static RoundingMode defaultRoundingMode = RoundingMode.HALF_EVEN;

    @DecimalMax("9999999.99")
    @DecimalMin("0")
    @Column(name = "AMOUNT")
    @NotNull
    @Setter
    private BigDecimal amount = null;

    @Column(name = "CURRENCY")
    @Converter(name = "currencyConverter", converterClass = CurrencyConverter.class)
    @Convert("currencyConverter")
    @NotNull
    @Getter
    @Setter
    private Currency currency = null;

    @Transient
    private RoundingMode roundingMode = defaultRoundingMode;

    /**
     * Create a copy of a Money.
     *
     * @param money The Money.
     */
    public TestMoney(TestMoney money) {
        amount = money.getAmount();
        currency = money.getCurrency();
    }

    /**
     * Create with a BigDecimal amount, currency and rounding mode.
     *
     * @param amount       The amount. Required.
     * @param currency     The currency. Optional. If not provided, defaults to the current locale's currency.
     * @param roundingMode The rounding mode. Optional. If not provided, defaults to HALF_UP.
     * @throws NullPointerException If amount is null.
     */
    public TestMoney(BigDecimal amount, Currency currency, RoundingMode roundingMode) {
        Validate.notNull(amount);

        if (currency == null) {
            this.currency = defaultCurrency;
        } else {
            this.currency = currency;
        }

//        log.debug("roundingMode= {}", roundingMode);
//        log.debug("defRoundingMode= {}", defaultRoundingMode);
        if (roundingMode == null) {
            this.roundingMode = defaultRoundingMode;
        } else {
            this.roundingMode = roundingMode;
        }

//        log.debug("amount= {}", amount);
//        log.debug("currenty= {}", currency);
//        log.debug("roundingMode= {}", this.roundingMode);
//        log.debug("defRoundingMode= {}", defaultRoundingMode);
        this.amount = amount.setScale(this.currency.getDefaultFractionDigits(), this.roundingMode);
    }

    /**
     * Create with a BigDecimal amount, currency and default rounding mode.
     *
     * @param amount   The amount. Required.
     * @param currency The currency. Optional. If not provided, defaults to the current locale's currency.
     * @throws NullPointerException If amount is null.
     */
    public TestMoney(BigDecimal amount, Currency currency) {
        this(amount, currency, null);
    }

    /**
     * Create with default currency and rounding mode.
     *
     * @param amount The amount. Required.
     * @throws NullPointerException If amount is null.
     */
    public TestMoney(BigDecimal amount) {
        this(amount, null, null);
    }

    /**
     * Create with a String amount, currency and rounding mode.
     *
     * @param amount       The amount. Required.
     * @param currency     The currency. Optional. If not provided, defaults to the current locale's currency.
     * @param roundingMode The rounding mode. Optional. If not provided, defaults to HALF_UP.
     * @throws NullPointerException If amount is null.
     */
    public TestMoney(String amount, Currency currency, RoundingMode roundingMode) {
        this(new BigDecimal(amount), currency, roundingMode);
    }

    /**
     * Create with a String amount, currency and default rounding mode.
     *
     * @param amount   The amount. Required.
     * @param currency The currency. Optional. If not provided, defaults to the current locale's currency.
     * @throws NullPointerException If amount is null.
     */
    public TestMoney(String amount, Currency currency) {
        this(new BigDecimal(amount), currency, null);
    }

    /**
     * Create with a String amount.
     *
     * @param amount The amount. Required.
     * @throws NullPointerException If any arg is null.
     */
    public TestMoney(String amount) {
        this(new BigDecimal(amount), null, null);
    }

    /**
     * Add money values.
     *
     * @param money The money value. Required.
     * @return The total amount.
     * @throws NullPointerException     If money is null.
     * @throws IllegalArgumentException If the money values are not in the same currency.
     */
    public TestMoney add(TestMoney money) {
        Validate.notNull(money, "A money value is required to add");
        validateState();
        validateSameCurrency(money);
        return new TestMoney(Nvl.nvl(amount).add(Nvl.nvl(money.getAmount())), currency, roundingMode);
    }

    public TestMoney add(@NonNull BigDecimal amountToAdd) {
        validateState();
        return new TestMoney(Nvl.nvl(amount).add(amountToAdd), currency, roundingMode);
    }

    /**
     * Subtract money values.
     *
     * @param money The money value. Required.
     * @return The new amount.
     * @throws NullPointerException     If money is null.
     * @throws IllegalArgumentException If the money values are not in the same currency.
     */
    public TestMoney subtract(TestMoney money) {
        Validate.notNull(money, "A money value is required to subtract");
        validateState();
        validateSameCurrency(money);
        return new TestMoney(Nvl.nvl(amount).subtract(Nvl.nvl(money.getAmount())), currency, roundingMode);
    }

    /**
     * Sum a collection of moneys. The currencies must match.
     *
     * @param moneys          The moneys, required.
     * @param currencyIfEmpty The currency to use if there are no moneys. Required.
     * @return The sum.
     * @throws NullPointerException If any arg is null.
     */
    public static TestMoney sum(Collection<TestMoney> moneys, Currency currencyIfEmpty) {
        Validate.notNull(moneys);
        Validate.notNull(currencyIfEmpty);
        TestMoney sum = new TestMoney(BigDecimal.ZERO, currencyIfEmpty, null);

        for (TestMoney money : moneys) {
            sum = sum.add(money);
        }

        return sum;
    }

    /**
     * Multiply this TestMoney by an integral factor.
     *
     * @param factor The factor to multiply by.
     * @return The result.
     */
    public TestMoney multiply(int factor) {
        validateState();
        return new TestMoney(Nvl.nvl(amount).multiply(new BigDecimal(factor)), currency, roundingMode);
    }

    /**
     * Multiply this TestMoney with a BigDecimal value.
     *
     * @param factor The factor to multiply by. Required.
     * @return The result.
     * @throws NullPointerException If factor is null.
     */
    public TestMoney multiply(BigDecimal factor) {
        Validate.notNull(factor);
        validateState();
        BigDecimal newAmount = Nvl.nvl(amount).multiply(factor);
        return new TestMoney(newAmount, currency, roundingMode);
    }

    /**
     * Divide this TestMoney with a BigDecimal value.
     *
     * @param divisor The divisor value. Must be greater than zero.
     * @return The result value.
     * @throws NullPointerException     If divisor is null.
     * @throws IllegalArgumentException If divisor is <= 0.
     */
    public TestMoney divide(BigDecimal divisor) {
        Validate.notNull(divisor, "The divisor is required");
        Validate.isTrue(divisor.compareTo(BigDecimal.ZERO) > 0, "The divisor must be greater than zero");
        BigDecimal newAmount = Nvl.nvl(amount).divide(divisor, roundingMode);
        return new TestMoney(newAmount, currency, roundingMode);
    }

    /**
     * Divide money values. This is a special operation where the result is an array of TestMoney objects. This is required
 because a single result multiplied by the denominator may not add up to the original, e.g. $100 / 3 = $33.33,
 multiplied again is $99.99.
     *
     * @param divisor The divisor value. Must be greater than zero.
     * @return The new amount.
     * @throws IllegalArgumentException If divisor is <= 0.
     */
    public TestMoney[] divide(int divisor) {
        Validate.isTrue(divisor > 0, "The divisor must be greater than zero");
        validateState();
        TestMoney[] result = new TestMoney[divisor];
        BigDecimal simpleResult = Nvl.nvl(amount).divide(new BigDecimal(divisor), RoundingMode.DOWN);
        BigDecimal remainder = Nvl.nvl(amount).subtract(simpleResult.multiply(new BigDecimal(divisor)));
        BigDecimal increment = BigDecimal.ONE.divide(BigDecimal.TEN.pow(currency.getDefaultFractionDigits()));

        for (int i = 0; i < divisor; i++) {
            BigDecimal value = simpleResult;

            if (remainder.compareTo(BigDecimal.ZERO) > 0) {
                value = simpleResult.add(increment);
                remainder = remainder.subtract(increment);
            }

            result[i] = new TestMoney(value, currency, roundingMode);
        }

        return result;
    }

    /**
     * Negate this value.
     *
     * @return The negated TestMoney value.
     */
    public TestMoney negate() {
        validateState();

        if (amount == null) {
            return null;
        }

        return new TestMoney(amount.negate(), currency, roundingMode);
    }

    /**
     * Determine if the given TestMoney is greater than this one.
     *
     * @param money The money to compare.
     * @return True if the given TestMoney is greater than this one, false if not.
     * @throws NullPointerException     If money is null.
     * @throws IllegalArgumentException If the money values are not in the same currency.
     */
    public boolean greaterThan(TestMoney money) {
        return compareTo(money) > 0;
    }

    /**
     * Determine if the given TestMoney is less than this one.
     *
     * @param money The money to compare.
     * @return True if the given TestMoney is less than this one, false if not.
     * @throws NullPointerException     If money is null.
     * @throws IllegalArgumentException If the money values are not in the same currency.
     */
    public boolean lessThan(TestMoney money) {
        return compareTo(money) < 0;
    }

    /**
     * Checks if the amount is zero.
     *
     * @return true if the amount is zero.
     */
    public boolean isZero() {
        return amount == null || BigDecimal.ZERO.compareTo(amount) == 0;
    }

    /**
     * True if the currency has been set and it is AUD.
     *
     * @return true if the currency is AUD
     */
    @Override
    public String getDisplayValue() {
        validateState();

        if (amount == null) {
            return null;
        }

        NumberFormat format = CurrencyUtils.getCurrencyFormat(currency);
        return format != null ? format.format(amount) : amount.toString();
    }

    @Override
    public int compareTo(TestMoney money) {
        Validate.notNull(money);
        validateState();
        validateSameCurrency(money);
        return ObjectUtils.compare(amount, money.getAmount());
    }

    @Override
    public String toString() {
        return getDisplayValue();
    }

    public BigDecimal getAmount() {
        return amount == null ? null : amount.setScale(currency.getDefaultFractionDigits(), roundingMode);
    }

    BigDecimal getRawAmount() {
        return amount;
    }

    /**
     * Check the state of TestMoney. Used for any operation. This allows TestMoney to be partially populated for binding and
 JPA.
     */
    private void validateState() {
        Validate.validState(currency != null, "Currency cannot be null");
        Validate.validState(roundingMode != null, "RoundingMode cannot be null");
    }

    private void validateSameCurrency(TestMoney money) {
        Validate.notNull(money);
        Validate.isTrue(currency.equals(money.currency), "The currencies do not match, this is %s and other is %s",
                        currency.getCurrencyCode(), money.currency.getCurrencyCode());
    }

    public static TestMoney zero() {
        return new TestMoney(BigDecimal.ZERO, Currency.getInstance("AUD"), RoundingMode.HALF_EVEN);
    }
}

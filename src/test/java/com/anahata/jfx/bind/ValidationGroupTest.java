package com.anahata.jfx.bind;

import com.anahata.jfx.bind.table.BindTable;
import com.anahata.jfx.bind.table.BindTableColumn;
import com.anahata.jfx.bind.table.BindingTableRow;
import com.anahata.jfx.bind.table.BindingTableView;
//import com.anahata.test.runner.JavaFxCdiRunner;
import com.anahata.util.metamodel.MetaModel;
import com.anahata.util.metamodel.MetaModelProperty;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

/**
 * Test validation group processing.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
//@RunWith(JavaFxCdiRunner.class)
public class ValidationGroupTest {
    @Inject
    private Instance<Controller> controllerFactory;
    
    @Test
    @Ignore
    public void testValidation() {
        Controller controller = controllerFactory.get();
        assertTrue(controller.binder.formValidProperty().get());
        assertFalse(controller.binder.getFormValidProperty(TestValidationGroup.class).get());
        BindingTableView btv = (BindingTableView)controller.addresses.getUserData();
        
        for (BindingTableRow btr : btv.getTableRows()) {
            if (btr.isHasValue()) {
                AddressRow ar = (AddressRow)btr;
                ar.address.setText("Valid address");
            }
        }
        
        controller.refresh();
        assertFalse(controller.binder.getFormValidProperty(TestValidationGroup.class).get());
        
        controller.address.set(controller.addressModel);
        controller.refresh();
        controller.binder.bindFromModel(controller.address);
        controller.refresh();
        
        BindingTableView btv2 = (BindingTableView)controller.addressLines.getUserData();
        
        for (BindingTableRow btr : btv2.getTableRows()) {
            if (btr.isHasValue()) {
                AddressLineRow alr = (AddressLineRow)btr;
                alr.line.setText("Valid address");
            }
        }
        
        controller.refresh();
        assertTrue(controller.binder.getFormValidProperty(TestValidationGroup.class).get());
    }
    
    //== ui classes ===================================================================================================
    
    public static class Controller {
        @Inject
        private Binder binder;
        
        private Scene scene;
        
        @Bind(property = Person_mm.name.class)
        private final TextField name = new TextField();
        
        @BindTable(property = Person_mm.addresses.class, tableRow = AddressRow.class)
        private TableView<Address> addresses;
        
        @BindTable(id = "address", property = Address_mm.lines.class, tableRow = AddressLineRow.class)
        private TableView<AddressLine> addressLines;
        
        @BindModel
        private final ObjectProperty<Person> person = new SimpleObjectProperty<>();
        
        @BindModel(id="address", autoBind = false)
        private ObjectProperty<Address> address = new SimpleObjectProperty<>();
        
        private Person personModel;
        
        private Address addressModel;
        
        private AddressLine addressLineModel;
        
        @PostConstruct
        public void postConstruct() {
            // UI.
            
            Stage stage = new Stage();
            Group root = new Group();
            scene = new Scene(root);
            stage.setScene(scene);
            root.getChildren().add(name);
            
            addresses = new TableView<>();
            TableColumn<Address, String> tc1 = new TableColumn<>();
            tc1.setId("address");
            addresses.getColumns().add(tc1);
            root.getChildren().add(addresses);
            
            addressLines = new TableView<>();
            TableColumn<AddressLine, String> tc2 = new TableColumn<>();
            tc2.setId("line");
            addressLines.getColumns().add(tc2);
            root.getChildren().add(addressLines);
            
            addresses.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Address>() {
                @Override
                public void changed(ObservableValue<? extends Address> ov, Address oldValue, Address newValue) {
                    address.set(newValue);
                }
            });
            
            // Model.
            
            personModel = new Person();
            personModel.setName("Unit Test");
            addressModel = new Address();
            addressModel.setAddress("Fail");
            personModel.getAddresses().add(addressModel);
            addressLineModel = new AddressLine();
            addressLineModel.setLine("Fail");
            addressModel.getLines().add(addressLineModel);
            
            // Bind.
            
            binder.init(this);
            binder.addValidationGroup(TestValidationGroup.class);
            person.set(personModel);
            address.set(addressModel);
            
            // Force a render without displaying any UI.
            
            refresh();
        }
        
        public void refresh() {
            scene.snapshot(null);
        }
    }
    
    public static class AddressRow extends BindingTableRow<Address> {
        @Bind(property = Address_mm.address.class)
        @BindTableColumn(id = "address")
        private TextField address = new TextField();
    }
    
    public static class AddressLineRow extends BindingTableRow<AddressLine> {
        @Bind(property = AddressLine_mm.line.class)
        @BindTableColumn(id = "line")
        private TextField line = new TextField();
    }
    
    //== model classes ================================================================================================
    
    @Getter
    @Setter
    public static class Person {
        @NotNull
        private String name;
        
        @Valid
        private List<Address> addresses = new ArrayList<>();
    }
    
    @ToString
    @Getter
    @Setter
    public static class Address {
        @NotNull
        @Size(min = 5, groups = TestValidationGroup.class)
        private String address;
        
        @Valid
        private List<AddressLine> lines = new ArrayList<>();
    }
    
    @ToString
    @Getter
    @Setter
    public static class AddressLine {
        @NotNull
        @Size(min = 5, groups = TestValidationGroup.class)
        private String line;
    }
    
    public interface TestValidationGroup {
    }
    
    //== metamodel classes ============================================================================================
    
    public static class Person_mm extends MetaModel {
        public static final Person_mm INSTANCE = new Person_mm();
        
        public Person_mm() {
            super(Person_mm.class);
        }
        
        public static class name extends MetaModelProperty {
            public static final Person_mm.name INSTANCE = new Person_mm.name();
            
            public name() {
                super(Person.class, String.class, "name");
            }
        }
        
        public static class addresses extends MetaModelProperty {
            public static final Person_mm.addresses INSTANCE = new Person_mm.addresses();
            
            public addresses() {
                super(Person.class, List.class, "addresses");
            }
        }
    }
    
    public static class Address_mm extends MetaModel {
        public static final Address_mm INSTANCE = new Address_mm();
        
        public Address_mm() {
            super(Address_mm.class);
        }
        
        public static class address extends MetaModelProperty {
            public static final Address_mm.address INSTANCE = new Address_mm.address();
            
            public address() {
                super(Address.class, String.class, "address");
            }
        }
        
        public static class lines extends MetaModelProperty {
            public static final Address_mm.lines INSTANCE = new Address_mm.lines();
            
            public lines() {
                super(Address.class, List.class, "lines");
            }
        }
    }
    
    public static class AddressLine_mm extends MetaModel {
        public static final AddressLine_mm INSTANCE = new AddressLine_mm();
        
        public AddressLine_mm() {
            super(AddressLine_mm.class);
        }
        
        public static class line extends MetaModelProperty {
            public static final AddressLine_mm.line INSTANCE = new AddressLine_mm.line();
            
            public line() {
                super(AddressLine.class, String.class, "line");
            }
        }
    }
}

package com.anahata.jfx.bind;

import java.util.Collections;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@RunWith(PowerMockRunner.class)
public class ValidationStatusTest {
    @Mock
    private BindForm bindForm;
    
    @Mock
    private Binder rootBinder;
    
    private final BooleanProperty formValid = new SimpleBooleanProperty(true);
    
    private final BooleanProperty formActive = new SimpleBooleanProperty();
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(bindForm.getFormValidProperty(any(Class.class))).thenReturn(formValid);
        when(bindForm.getValidationActive(any(Class.class))).thenReturn(formActive);
    }
    
    @Test
    public void testIsValid() {
        ValidationStatus status = new ValidationStatus(rootBinder, TestValidationGroup.class, Collections.singleton(bindForm));
        assertTrue(status.isValid());
        status.setInvalid("Test");
        assertFalse(status.isValid());
        status.activeProperty().set(false);
        assertTrue(status.isValid());
    }
    
    private interface TestValidationGroup {
    }
}
